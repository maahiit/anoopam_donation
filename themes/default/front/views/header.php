<!DOCTYPE html>
<html class="no-js" lang="en">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="description" content="Anupam Mission Donation">
      <meta name="author" content=" Anupam Mission IT Team"> 
      <meta name="keywords" content="Anupam Mission, Donation, Online Donation">
      <link rel="icon" href="<?= $front_assets ?>/images/am-logo-200.png" type="image/x-icon">
      <!-- <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"> -->
      <!-- Title --> 

      <title><?= $page_title ?></title>
      <!-- Bootstrap Css --> 

      <link href="<?= $front_assets ?>css/bootstrap.min.css" rel="stylesheet">
      
        <script src="<?= $front_assets ?>js/jquery-3.2.1.min.js.download"></script>
       <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Maven+Pro" />



      <style type="text/css">
        .notifyjs-metro-base.notifyjs-metro-error{
            padding: 10px;
            font-family:initial;
        }
        * {
            font-family: 'Maven Pro', sans-serif;
        }

        h1 {
            font-size: 50px;
            /*font-family: 'Lobster' !important;*/
        }
        .top1 {
            padding-top: 0px;
        }

        .top2 {
            margin-top: -40px;
        }
        .container{
         padding-bottom: 0px !important;; 
        }
        @media (max-width: 450px) {
            h1 {
                font-size: 23px;

            }

            .top1 {
                padding-top: 20px;
            }

            .top2 {
                margin-top: 0px;
            }

            .img-fluid {
                max-width: 65px !important;
            }
            .mobilehiden{
                display:none;
            }
            .header_url{
              display: flow-root !important;
            }
            
            label {
            display: inline-block;
            margin-bottom: 0px !important;; 
        }
        .form-group {
            margin-bottom: 0.5rem;
        }
        .container{
         padding-bottom: 0px !important;; 
        }
            
        }
        .header_url{
                list-style: none;
                display: inline-flex;
        }



        /*  maahi css  */
        #submit, #rbIndividual, #rbOther{
          cursor: pointer;
        }
        label{
                color: #1b5b9f !important;
        }
        body{
          background:url(<?= $front_assets ?>/donation/site_bg.jpg) no-repeat; background-size:cover; background-attachment:fixed;
        }
        .container{
          border-radius: 3px;
        }
        input[type=number]::-webkit-inner-spin-button, 
        input[type=number]::-webkit-outer-spin-button { 
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            margin: 0; 
        }
        #payable_amt_msg{
            font-size: 13px;
            font-style: italic;
        }
        
        input[type=checkbox], input[type=radio]{
              height: 20px;
              width: 20px;
              position: relative;
              top: 5px;
        }
        
        /*09-01-2021*/
        .btn-info {
            color: #fff;
            background-color: #3870b2;
            border-color: #17a2b8;
            padding: 3px 16px;
        }
        
        
      </style>

      
   </head>
          