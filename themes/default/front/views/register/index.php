
<section>
	<div class="bannerimg cover-image bg-background3" data-image-src="../assets/images/banners/banner2.jpg" style="background: url(&quot;../assets/images/banners/banner2.jpg&quot;) center center;">
		<div class="header-text mb-0"> 
			<div class="container">
				<div class="text-center text-white">
					<h1 class="">Register</h1>
					<ol class="breadcrumb text-center">
						<li class="breadcrumb-item"><a href="<?= base_url('home'); ?>">Home</a></li>
						<li class="breadcrumb-item active text-white" aria-current="page">Register</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
</section>
<!--/Breadcrumb-->
<!--Register-section-->
<section class="sptb">
	<div class="container customerpage"> 
		<div class="row">
			<div class="single-page">
				<div class="col-lg-5 col-xl-4 col-md-6 d-block mx-auto">
					<div class="wrapper wrapper2">
						<form id="add_form" name="add_form" class="card-body" method="post" tabindex="500">
							<h3>Register</h3>
							<div class="name">
								<input type="text" name="name">
								<label>Name</label>
							</div>
							<div class="mail">
								<input type="mobile" name="mobile">
								<label>Mobile</label>
							</div>
							<div class="mail">
								<input type="email" name="email_id">
								<label>Email</label>
							</div>
							<div class="passwd">
								<input type="password" name="password">
								<label>Password</label>
							</div>
							<div id="register_msg"></div>
							<div> 
								<button type="button" class="btn btn-primary btn-block" onclick="add_row()">Register</button>
							</div>

							<p class="text-dark mb-0">Already have an account?<a href="<?= base_url('login')?>" class="text-primary ml-1">Sign In</a></p>
						</form>
						<hr class="divider">
						<div class="card-body">
							<div class="text-center">
								<div class="btn-group">
									<a href="https://www.facebook.com/" class="btn btn-icon mr-2 brround"> <span class="fa fa-facebook"></span> </a>
								</div>
								<div class="btn-group">
									<a href="https://www.google.com/gmail/" class="btn  mr-2 btn-icon brround"> <span class="fa fa-google"></span> </a>
								</div>
								<div class="btn-group">
									<a href="https://twitter.com/" class="btn  btn-icon brround"> <span class="fa fa-twitter"></span> </a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--Register-section-->
<!--Newsletter-->
<section class="sptb bg-white border-top">
	<div class="container">
		<div class="row">
			<div class="col-lg-7 col-xl-6 col-md-12">
				<div class="sub-newsletter">
					<h3 class="mb-2"><i class="fa fa-paper-plane-o mr-2"></i> Subscribe To Our Newsletter</h3>
					<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
				</div>
			</div>
			<div class="col-lg-5 col-xl-6 col-md-12">
				<div class="input-group sub-input mt-1">
					<input type="text" class="form-control input-lg " placeholder="Enter your Email">
					<div class="input-group-append ">
						<button type="button" class="btn btn-primary btn-lg br-tr-3  br-br-3"> Subscribe </button>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--/Newsletter-->

<script>


			 
	function add_row()
	{
		var form = $('#add_form')[0]; 
        var formData = new FormData(form);
		$.ajax({
		 		 url: "<?php echo base_url()?>register/register_add", 
		  		 type: "POST",
                 data: formData,
				 async: false,
                 dataType: 'json',
                 contentType: false, 
                 processData: false, 
				 success: function(data) 
				 {
				 	$('#register_msg').html('');	
					if (data["status"] == 1)
					{	
					   $("#add_form")[0].reset();
					   $('#register_msg').html(data['msg']).css('color','green');	
					}
					else if (data['status'] == 2)
					{	
					   $('#register_msg').html(data['msg']).css('color','red');	
		   			}
		   			else
		   			{

					}

				},
				error: function() { 
					alert('error');
				}
			});	
	} 
			
			
		
</script>