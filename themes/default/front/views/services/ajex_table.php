<?php
$count = 1;foreach ($services as $service) : ?>
<tr>
	<td><?= $count++;?></td>
	<td><?= @$service->name; ?></td>
	<td><?= @$service->price; ?></td>
	<td><?= @$service->category; ?></td>
	<td><?= @$service->duration_of_service; ?> minute </td>
	
	<td>
		<a href="javascript:void()" class="badge badge-<?php echo ($service->status == 'active') ? 'primary' : 'danger'?>"><?= ucfirst($service->status); ?></a> 
	</td>
	<td> 

		<a href="javascript:void(0)" onclick="get_record_edit('<?= $service->id ?>')" class="badge badge-primary"><i class="icon icon-pencil"></i></span></a>

		<a href="javascript:void(0)" onclick="delete_record('<?= $service->id ?>')" class="badge badge-danger"><i class="icon icon-trash"></i></span></a>
	</td>

</tr>
<?php endforeach; ?>


