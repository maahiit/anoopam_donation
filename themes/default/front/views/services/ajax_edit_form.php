<div class="card">
<div class="card-header">
<h3 class="card-title">Update Services</h3> </div>
<div class="card-body">
<div class="card-pay"> 

<div class="tab-content">
	<form id="row_update" name="row_update"  method="post" tabindex="500">
	<div class="tab-pane active show" id="tab1">
		
		<div class="row">
			<input type="hidden" name="update_id" value="<?= $row->id ?>"> 
			<div class="col-sm-4">
				<div class="form-group">
					<label class="form-label">Name <span class="text-danger">*</span></label>
					<input type="text" class="form-control" name="name" id="name" required="required" placeholder="Name" value="<?= $row->name ?>"> </div>
				</div>

				<div class="col-sm-4">
					<div class="form-group">
						<label class="form-label">Price <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="price" id="price14" required="required" placeholder="Price" value="<?= $row->price ?>"> </div>
					</div>

					<div class="col-sm-4">
						<div class="form-group">
							<label for="category"><?= lang('Category'); ?><span class="text-danger">*</span></label>
							<select class="form-control" name="category" id="category">
								<?php foreach ($services_category as $key => $value): ?>
									<option <?php if($row->category == $value->id) { echo "selected";  } ?> value="<?= $value->id ?>"><?= $value->name ?></option>
								<?php endforeach ?> 
							</select>            
						</div>
					</div>

						<div class="col-sm-4">
							<div class="form-group">
								<label class="form-label">Duration of service <span class="text-danger">*</span></label>
								<input type="number" class="form-control" name="duration_of_service" id="duration_of_service"  required="required" placeholder="Duration Of Service" value="<?= $row->duration_of_service ?>">  </div>
							</div>

							<div class="col-sm-4">
							<div class="form-group">
								<label class="form-label">Description <span class="text-danger">*</span></label>
								<textarea class="form-control" name="description" id="description" required="required" placeholder="Description" ><?= $row->description ?></textarea> </div>
							</div>


							<div class="col-sm-12">
								<div id="update_service_msg"></div>
							</div>
						</div>


						<button type="button" class="btn btn-primary float-left" onclick="javascript:view_list();">Cancle</button>

						<button type="button" class="btn btn-primary float-right" onclick="submit_update_record()">update</button>



		  </div>
		</form>
	
	
</div>


</div>
</div>
</div>