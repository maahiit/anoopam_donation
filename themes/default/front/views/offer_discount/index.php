
<section>
	<div class="bannerimg cover-image bg-background3" data-image-src="../assets/images/banners/banner2.jpg" style="background: url(&quot;../assets/images/banners/banner2.jpg&quot;) center center;">
		<div class="header-text mb-0"> 
			<div class="container"> 
				<div class="text-center text-white">
					<h1 class="">Offer Discount</h1>
					<ol class="breadcrumb text-center">
						<li class="breadcrumb-item"><a href="<?= base_url('home')?>">Home</a></li>
						<li class="breadcrumb-item"><a href="<?= base_url('mydask')?>">My Dashboard</a></li>
						<li class="breadcrumb-item active text-white" aria-current="page">Offer Discount</li>
					</ol>
				</div> 
			</div> 
		</div>
	</div>
</section>
<!--Breadcrumb-->
<!--User dashboard-->
<section class="sptb">
	<div class="container">
		<div class="row">
			<div class="col-xl-3 col-lg-12 col-md-12">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">My Dashboard</h3> </div>
						<div class="card-body text-center item-user border-bottom">
<div class="profile-pic">
<div class="profile-pic-img"> 
	<span class="bg-success dots" data-toggle="tooltip" data-placement="top" title="" data-original-title="online"></span> 
<?php if (!empty($login_info->logo)) { ?>
	
<img class="brround" alt="user" src="<?= base_url() ?>themes/assets/images/usersign/<?= $login_info->logo; ?>"/>

<?php }else{ ?>
<img src="<?= $front_assets ?>/images/comman_images/25.jpg" class="brround" alt="user"> 
<?php } ?>
	
</div> 
	<a href="userprofile.html" class="text-dark">
		<h4 class="mt-3 mb-0 font-weight-semibold"><?= $login_info->first_name ?> <?= $login_info->last_name ?>	</h4>
	</a>
		 </div>
</div>
							<div class="item1-links  mb-0">
								<a href="<?php echo base_url('mydask');?>" class=" d-flex border-bottom"> <span class="icon1 mr-3"><i class="icon icon-user"></i></span> Edit Profile </a>
								<a href="<?php echo base_url('services');?>" class=" d-flex  border-bottom"> <span class="icon1 mr-3"><i class="icon icon-diamond"></i></span> Services </a>
								<a href="<?php echo base_url('products');?>" class=" d-flex border-bottom"> <span class="icon1 mr-3"><i class="icon icon-heart"></i></span> Products </a>
								<a href="<?php echo base_url('amenities');?>" class="d-flex  border-bottom"> <span class="icon1 mr-3"><i class="icon icon-folder-alt"></i></span> Amenities </a>
								<a href="<?php echo base_url('offer_discount');?>" class=" active d-flex  border-bottom"> <span class="icon1 mr-3"><i class="icon icon-credit-card"></i></span> Offer Discount </a>
								<a href="<?php echo base_url('reviews');?>" class=" d-flex  border-bottom"> <span class="icon1 mr-3"><i class="icon icon-credit-card"></i></span> Reviews </a>
							
							
								<a href="<?= base_url('login/logout'); ?>" class="d-flex"> <span class="icon1 mr-3"><i class="icon icon-power"></i></span> Logout </a>
							</div>
						</div>
					</div>
						
							

							<div class="col-xl-9 col-lg-12 col-md-12">
								<div class="card" id="add_ui" style="display:none;">
									<div class="card-header">
										<h3 class="card-title">Offer Discount</h3> </div>

										<div class="card-body">
											<div class="card-pay">
												<div class="tab-content">

													<form id="add_form" name="add_form"  method="post" tabindex="500">

														<div class="tab-pane active show" id="tab1">

															<div class="row">

																<div class="col-sm-4">
																	<div class="form-group">
																		<label class="form-label">Service <span class="text-danger">*</span></label>

																		<select class="form-control" name="service" id="service">

																			<?php foreach ($services_name as $key => $value):?>

																				<option value="<?= $value->id ?>"><?= $value->name?>

																			</option>

																		<?php endforeach?>

																	</select>

																</div>
															</div>

															<div class="col-sm-4">
																<div class="form-group">

																	<label class="form-label">Title <span class="text-danger">*</span></label>

																	<input type="text" name="title" id="title" class="form-control" required="required" placeholder="Title">
																</div>
															</div>

															<div class="col-sm-4"> 
																<div class="form-group">
																	<label class="form-label">Discount Type<span class="text-danger">*</span></label>
																	<select class="form-control" name="discount_type" id="discount_type">
																		
																		<option value="percentage">Percentage</option>
																		<option value="flat">Flat</option>

																	</select>

																</div>
															</div>
															
																<div class="col-sm-4">
																	<div class="form-group">
																		<label class="form-label">Discount<span class="text-danger">*</span></label>
																		<input class="form-control	"type="text" name="offer_disc" id="offer_disc" required="required" placeholder="Discount">
																	</div>
																</div>


														</div> 

														<button type="button" class="btn btn-primary float-left" onclick="javascrpt:view_list()">Cancle</button>
														<button type="button" class="btn btn-primary float-right" onclick="add_row()">Submit</button> 

													</div>
												</form>

												
											</div>
										</div>
									</div>
								</div>

								<div id="edit_ui"></div>
								<div class="card mb-0" id="table_ui">
									
									<div class="card-header">
										<button class="btn btn-primary float-right" onclick="add_from()">Add Offer Discount</button>
									</div>
								

								<div class="card-header">
									<h3>Offer Discount</h3>
								</div>

								<div class="card-body">
									<div id="offer_discount_msg"></div>
									<div class="table-responsive border-top">
										<table class="table table-bordered table-hover text-nowrap">
											<thead>
												<tr>
													<th>ID</th>
													<th>Service</th>
													<th>Title</th>
													<th>Discount Type</th>
													<th>Discount</th>
													<th>Status</th>
													<th>Actions</th>
												</tr>
											</thead>

											<tbody id="offer_discount_ajax_table">
												<?= $table_ui ?>
												
											</tbody>
										</table>
										
									</div>

									<!-- <ul class="pagination hide">
													<li class="page-item page-prev disabled"> <a class="page-link" href="orders.html#" tabindex="-1">Prev</a> </li>
													<li class="page-item active"><a class="page-link" href="orders.html#">1</a></li>
													<li class="page-item"><a class="page-link" href="orders.html#">2</a></li>
													<li class="page-item"><a class="page-link" href="orders.html#">3</a></li>
													<li class="page-item page-next"> <a class="page-link" href="orders.html#">Next</a> </li>
												</ul> -->
									
								</div>
								</div>



							</div>
							</div>
			</div>
		</section>
		<!--/User dashboard-->
		<!--Newsletter-->
		<section class="sptb bg-white border-top">
			<div class="container">
				<div class="row">
					<div class="col-lg-7 col-xl-6 col-md-12">
						<div class="sub-newsletter">
							<h3 class="mb-2"><i class="fa fa-paper-plane-o mr-2"></i> Subscribe To Our Newsletter</h3>
							<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
						</div>
					</div>
					<div class="col-lg-5 col-xl-6 col-md-12">
						<div class="input-group sub-input mt-1">
							<input type="text" class="form-control input-lg " placeholder="Enter your Email">
							<div class="input-group-append ">
								<button type="button" class="btn btn-primary btn-lg br-tr-3  br-br-3"> Subscribe </button>
							</div>
						</div>
					</div> 
				</div>
			</div>
		</section>

<script>

function view_list(){

	$('#edit_ui').html('');
	$('#table_ui').show();
	$('#add_ui').hide();
	
	
}

function add_from(){

	$('#edit_ui').html('');
	$('#table_ui').hide();
	$('#add_ui').show();
}

function add_row()
{
	var form = $('#add_form')[0]; 
	var formData = new FormData(form);
	$.ajax({
		url: "<?php echo base_url()?>offer_discount/offer_discount_add",  
		type: "POST",
		data: formData,
		async: false,
		dataType: 'json',
		contentType: false, 
		processData: false, 
		success: function(data) 
		{
			$('#offer_discount_msg').html('');	
			if (data["status"] == 1)
			{	
				$("#add_form")[0].reset();
				$('#offer_discount_ajax_table').html(data['table_ui'])
				$('#offer_discount_msg').html(data['msg']).css('color','green');	
				view_list();
				setTimeout(function() {
					$('#offer_discount_msg').html('');	
				},2000);
			}
			else if (data['status'] == 2)
			{	
				$('#offer_discount_msg').html(data['msg']).css('color','red');	
			}
			else
			{

			}

		},
		error: function() { 
			alert('error');
		}
	});	
} 

function get_record_edit(id)
{
	$.ajax({
		url: "<?php echo site_url('offer_discount/get_row')?>",
		method: 'post',
		data: {update_id:id},
		dataType: 'json',
		success: function(response)
		{
			$('#edit_ui').html(response['edit_row']);  
			$('#table_ui').hide();
			$('#add_ui').hide();
		}
	});
}


function submit_update_record()
{
	var form = $('#row_update')[0]; 
	var formData = new FormData(form);
	$.ajax({
		url: "<?php echo site_url('offer_discount/update_row')?>/", 
		type: "POST",
		data: formData,
		cache: false,
		dataType: 'json',
		contentType: false,
		processData: false,
		success: function(response)
		{
		// hide modal
		if(response['status'] == 1)
		{
			//$("#row_update")[0].reset() 
			$('#offer_discount_ajax_table').html(response['table_ui']);  
			$('#table_ui').show();
			$('#add_ui').hide();
			$('#edit_ui').html(''); 

			$('#offer_discount_msg').html(response['msg']);  
				setTimeout(function() {
					$('#offer_discount_msg').addClass('hide');
				},2000);
			intliveicon();
			}

			}
		});
}

function delete_record(id) 
{
	if (confirm('Are You sure you want to delete this record ?')) {

		$.ajax({
			type: "POST",
			url: "<?php echo site_url('offer_discount/delete_row')?>/"+id,
			dataType: 'json',
			success: function(response) {
				if(response['status'] == 1)
				{
					$('#offer_discount_ajax_table').html(response['table_ui']);  
					$('#table_ui').show();
					$('#add_ui').hide();
					$('#edit_ui').html(''); 
				}
			},
			error: function() {
				alert('error');
			}
		})

	} else {

	}

}
</script>