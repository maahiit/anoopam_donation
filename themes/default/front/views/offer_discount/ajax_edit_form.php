<div class="card">
<div class="card-header">
<h3 class="card-title">Update offer Discount</h3> </div> 
<div class="card-body">
<div class="card-pay"> 

<div class="tab-content">
	<form id="row_update" name="row_update"  method="post" tabindex="500">
	<div class="tab-pane active show" id="tab1">
		
		<div class="row">
			<input type="hidden" name="update_id" value="<?= $row->id ?>">
			

												<div class="col-sm-4"> 
													<div class="form-group">
              <label for="services_id">Service<span class="text-danger">*</span>
              </label>
              <select class="form-control" name="service" id="service"> 
              <?php foreach ($services_name as $key => $service): ?>
                  <option <?php if ($row->service == $service->id) { echo "selected"; } ?>
                    
                   value="<?= $service->id ?>"><?= $service->name ?></option>
              <?php endforeach ?>
              </select>             
              </div>
													</div>

													<div class="col-sm-4">
														<div class="form-group">
															<label class="form-label">Title <span class="text-danger">*</span></label>
															<input type="text" class="form-control" name="title" id="title" required="required" placeholder="Title" value="<?= $row-> title; ?>"> </div>
														</div>

														<div class="col-sm-4">
															<div class="form-group"> 
                         <label for="title">Discount Type<span class="text-danger">*</span></label>
                            <select class="form-control" name="discount_type" id="discount_type">
                                  <option value="percentage" <?php if($row->discount_type == 'percentage'){ echo 'selected';}?>>Percentage</option>
                                  <option value="flat" <?php if($row->discount_type == 'flat'){ echo 'selected';}?>>Flat</option>
                              </select>
                         
                    </div>
															</div>

															<div class="col-sm-4">
																<div class="form-group">
																	<label class="form-label">Discount <span class="text-danger">*</span></label>
																	<input type="text" class="form-control" name="offer_disc" id="offer_disc"  required="required" placeholder="Discount" value="<?= $row->offer_disc; ?>"> </div>
																</div>
				

					

						
							<div class="col-sm-12">
								<div id="update_service_msg"></div>
							</div>
						</div>


						<button type="button" class="btn btn-primary float-left" onclick="javascript:view_list();">Cancle</button>

						<button type="button" class="btn btn-primary float-right" onclick="submit_update_record()">update</button>



		  </div>
		</form>
	
	
</div>


</div>
</div>
</div>