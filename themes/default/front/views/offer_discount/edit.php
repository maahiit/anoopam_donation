									<div class="tab-content">
										<form id="add_form" name="add_form"  method="post" tabindex="500">
										<div class="tab-pane active show" id="tab1">
											
											<div class="row"> 

												<div class="col-sm-4"> 
													<div class="form-group">
              <label for="services_id">Service<span class="text-danger">*</span>
              </label>
              <select class="form-control" name="service" id="service"> 
              <?php foreach ($services_name as $key => $service): ?>
                  <option <?php if ($offer_discount->service == $service->id) { echo "selected"; } ?>
                    
                   value="<?= $service->id ?>"><?= $service->name ?></option>
              <?php endforeach ?>
              </select>             
              </div>
													</div>

													<div class="col-sm-4">
														<div class="form-group">
															<label class="form-label">Title <span class="text-danger">*</span></label>
															<input type="text" class="form-control" name="title" id="title" required="required" placeholder="Title" value="<?= $offer_discount->title; ?>"> </div>
														</div>

														<div class="col-sm-4">
															<div class="form-group"> 
																<label for="title">Discount Type<span class="text-danger">*</span></label>
																<select class="form-control" name="discount_type" id="discount_type">
																	<option value="percentage" <?php if($offer_discount->discount_type == 'percentage'){ echo 'selected';}?>>Percentage</option>
																	<option value="flat" <?php if($offer_discount->discount_type == 'flat'){ echo 'selected';}?>>Flat</option>
																</select>
																
															</div>
														</div>

															<div class="col-sm-4">
																<div class="form-group">
																	<label class="form-label">Discount <span class="text-danger">*</span></label>
																	<input type="text" class="form-control" name="offer_disc" id="offer_disc"  required="required" placeholder="Discount" value="<?= $offer_discount->offer_disc; ?>"> </div>
																</div>
																<div class="col-sm-12">
																	<div id="service_msg"></div>
																</div>
															</div>


															<button type="button" class="btn btn-primary float-left">Cancle</button>

															<button type="button" class="btn btn-primary float-right" onclick="add_row()">Submit</button>



											  </div> 
											</form>
										
										 
									</div>