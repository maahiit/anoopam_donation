 <!--Breadcrumb--> 
         <section>
            <div class="bannerimg cover-image bg-background3" data-image-src="../assets/images/banners/banner2.jpg" style="background: url(&quot;../assets/images/banners/banner2.jpg&quot;) center center;">
               <div class="header-text mb-0">
                  <div class="container">
                     <div class="text-center text-white">
                        <h1 class="">Blogs</h1>
                        <ol class="breadcrumb text-center">
                           <li class="breadcrumb-item"><a href="<?= base_url('home')?>">Home</a></li>
                           <li class="breadcrumb-item"><a href="<?= base_url('mydask')?>">My Dashboard</a></li>
                           <li class="breadcrumb-item active text-white" aria-current="page">Blogs</li>
                        </ol>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!--/Breadcrumb--> <!--Add listing--> 

         <section class="sptb">
            <div class="container">
               <div class="row">
                  <div class="col-xl-12 col-lg-12 col-md-12"> 
                     <div class="row">

                        <?php foreach($blogs as $blog):?>

                        <div class="col-xl-4 col-lg-6 col-md-12"> 
                           <div class="card"> 
                              <div class="item7-card-img">
                                 <a href="<?= base_url('blogs_details')?>/<?= $blog->id?>"></a> <img src="<?php base_url()?> themes/assets/images/blogs/<?= $blog->blog_image;?>" alt="img" class="cover-image">
                                 
                                 <div class="item7-card-text"> <span class="badge badge-success"><?= $blog->business_name;?></span> </div>
                              </div>
                              <div class="card-body">
                                 <div class="item7-card-desc d-flex mb-2">
                                    <a href="https://www.spruko.com/demo/claylist/LTR/Claylist/HTML/blog-grid-center.html#"><i class="fa fa-calendar-o text-muted mr-2"></i><?= $blog->inserted_time;?></a> 
                                    <div class="ml-auto"> <a href="https://www.spruko.com/demo/claylist/LTR/Claylist/HTML/blog-grid-center.html#"><i class="fa fa-comment-o text-muted mr-2"></i>4 Comments</a> </div>
                                 </div>
                                 <a href="https://www.spruko.com/demo/claylist/LTR/Claylist/HTML/blog-grid-center.html#" class="text-dark">
                                    <h4 class="font-weight-semibold"><?= $blog->name;?></h4>
                                 </a>
                                 <p><?= $blog->description;?> </p>
                                 <a href="https://www.spruko.com/demo/claylist/LTR/Claylist/HTML/blog-grid-center.html#" class="btn btn-primary btn-sm">Read More</a> 
                              </div>
                           </div>
                        </div>                        

                     <?php endforeach;?>

                     </div>


                     <div class="center-block text-center">
                        <ul class="pagination mb-0">
                           <li class="page-item page-prev disabled"> <a class="page-link" href="https://www.spruko.com/demo/claylist/LTR/Claylist/HTML/blog-grid-center.html#" tabindex="-1">Prev</a> </li>
                           <li class="page-item active"><a class="page-link" href="https://www.spruko.com/demo/claylist/LTR/Claylist/HTML/blog-grid-center.html#">1</a></li>
                           <li class="page-item"><a class="page-link" href="https://www.spruko.com/demo/claylist/LTR/Claylist/HTML/blog-grid-center.html#">2</a></li>
                           <li class="page-item"><a class="page-link" href="https://www.spruko.com/demo/claylist/LTR/Claylist/HTML/blog-grid-center.html#">3</a></li>
                           <li class="page-item page-next"> <a class="page-link" href="https://www.spruko.com/demo/claylist/LTR/Claylist/HTML/blog-grid-center.html#">Next</a> </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!--/Add listing--> <!-- Newsletter--> 
         <section class="sptb bg-white border-top">
            <div class="container">
               <div class="row">
                  <div class="col-lg-7 col-xl-6 col-md-12">
                     <div class="sub-newsletter">
                        <h3 class="mb-2"><i class="fa fa-paper-plane-o mr-2"></i> Subscribe To Our Newsletter</h3>
                        <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                     </div>
                  </div>
                  <div class="col-lg-5 col-xl-6 col-md-12">
                     <div class="input-group sub-input mt-1">
                        <input type="text" class="form-control input-lg " placeholder="Enter your Email"> 
                        <div class="input-group-append "> <button type="button" class="btn btn-primary btn-lg br-tr-3  br-br-3"> Subscribe </button> </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!--/Newsletter-->