			<div>
				
			<div class="cover-image sptb-1 bg-background" data-image-src="../assets/images/banners/banner1.jpg" style="background: url(&quot;../assets/images/banners/banner1.jpg&quot;) center center;">
				<div class="header-text1 mb-0">
					<div class="container">
						<div class="row">
							<div class="col-xl-9 col-lg-12 col-md-12 d-block mx-auto">
								<div class="search-background bg-transparent">
									<div class="form row no-gutters ">
										<div class="form-group  col-xl-4 col-lg-3 col-md-12 mb-0 bg-white">
											<input type="text" class="form-control input-lg br-tr-md-0 br-br-md-0" id="keyword" placeholder="Job Title or Phrase or Keywords"> </div>
										<div class="form-group  col-xl-3 col-lg-3 col-md-12 mb-0 bg-white">
											<input type="text" class="form-control input-lg br-md-0" id="text5" placeholder="Enter Location"> <span><i class="fa fa-map-marker location-gps mr-1"></i> </span> </div>

										<div class="form-group col-xl-3 col-lg-3 col-md-12 select2-lg  mb-0 bg-white">										
										<select id="select_cat_id" class="form-control border-bottom-0 cl-cat" data-placeholder="Select Categories" tabindex="-1" aria-hidden="true">

											<optgroup label="Categories">
												<option> -Select Category- </option>
											<?php 
													foreach($services_category as $cat): 
													?>
											<option value="<?= $cat->categoryid;?>"><?= $cat->services_category_name;?></option>
											<?php endforeach;?>

											</optgroup>
										</select>
										</div>

										<div id="search" class="col-xl-2 col-lg-3 col-md-12 mb-0">
										 <a href="" class="btn btn-lg btn-block btn-primary br-tl-md-0 br-bl-md-0" id="search_here_id">Search Here</a> 
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /header-text -->
			</div>
		</div>
		<!--/Sliders Section-->
<!--Breadcrumb-->
		<div class="bg-white border-bottom">
			<div class="container">
				<div class="page-header">
					<h4 class="page-title">Ad List Right</h4>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javascript::void(0)">Home</a></li>
						<li class="breadcrumb-item"><a href="javascript::void(0)">Pages</a></li>
						<li class="breadcrumb-item active" aria-current="page">Ad List Right</li>
					</ol>
				</div>
			</div>
		</div>
		<!--/Breadcrumb-->
		<!--Add listing-->
		<section class="sptb">
			<div class="container">
				<div class="row">
					<!--Left Side Content-->
					<div class="col-xl-3 col-lg-4 col-md-12">
						<div class="card">
							<div class="card-body">
								<div class="input-group">
									<input type="text" class="form-control br-tl-3  br-bl-3" placeholder="Search">
									<div class="input-group-append ">
										<button type="button" class="btn btn-primary br-tr-3  br-br-3"> Search </button>
									</div>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header">
								<h3 class="card-title">Categories</h3> </div> 
							<div class="card-body">
								<div class="closed" id="container" style="height: 250px; overflow: hidden;"> 

									<?php 

									foreach($services_category as $category):?>

									<div class="filter-product-checkboxs">
										<label class="custom-control  custom-checkbox mb-3 ">
											<input type="checkbox" onclick="service_category()" class="custom-control-input category-filter" name="category[]" id="<?= $category->categoryid;?>" value="<?= $category->categoryid;?>"> <span class="custom-control-label"> <?= $category->services_category_name;?><span class="label label-secondary float-right"><?= $category->total_services;?></span>
											</span>
										</label>
									</div>
								<?php endforeach; ?>
								</div>
							</div>
						
						</div>
						<div class="card mb-lg-0">
							<div class="card-header">
								<h3 class="card-title">Shares</h3> </div>
							<div class="card-body product-filter-desc">
								<div class="product-filter-icons text-center"> <a href="javascript::void(0)" class="facebook-bg"><i class="fa fa-facebook"></i></a> <a href="javascript::void(0)" class="twitter-bg"><i class="fa fa-twitter"></i></a> <a href="javascript::void(0)" class="google-bg"><i class="fa fa-google"></i></a> <a href="javascript::void(0)" class="dribbble-bg"><i class="fa fa-dribbble"></i></a> <a href="javascript::void(0)" class="pinterest-bg"><i class="fa fa-pinterest"></i></a> </div>
							</div>
						</div>
					</div>
					<!--/Left Side Content-->
					

					<!--Add Lists-->
					<div class="col-xl-9 col-lg-8 col-md-12">
						<div class="mb-5">
							<div class="">
								<div class="item2-gl ">
									<div class="bg-white p-5 item2-gl-nav d-flex">
										<h6 class="mb-0 mt-2">Showing 1 to 10 of 30 entries</h6>
										<ul class="nav item2-gl-menu ml-auto">
											<li class=""><a href="javascript::void(0)" class="" data-toggle="tab" title="List style"><i class="fa fa-list"></i></a></li>
											<li><a href="javascript::void(0)" data-toggle="tab" class="show active" title="Grid"><i class="fa fa-th"></i></a></li>
										</ul>
										<div class="d-flex">
											<label class="mr-2 mt-1 mb-sm-1">Sort By:</label>
											<select name="item" class="form-control select-sm w-70 select2 " data-select2-id="1" tabindex="-1" aria-hidden="true">
												<option value="1" data-select2-id="3">Latest</option>
												<option value="2">Oldest</option>
												<option value="3">Price:Low-to-High</option>
												<option value="5">Price:Hight-to-Low</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
						

						<div class="tab-content">
							<div class="tab-pane " id="tab-11"> 
								
									
								<?php foreach($services as $service):?>    
								<!-- Start :: Services Block 1  -->

								<div class="card overflow-hidden">
									<div class="d-md-flex">
										<div class="item-card9-img">
											<div class="arrow-ribbon bg-primary"><?= $service->business_type;?></div> 
											
											<div class="item-card9-imgs">
												<a href="<?= base_url('services_details')?>/<?= $service->id?>"></a> <img src="<?= base_url()?> themes/assests/images/services/<?= $service->image;?>" alt="img" class="cover-image"> </div>
											<div class="item-card9-icons">
												<a href="javascript::void(0)" class="item-card9-icons1 wishlist"> <i class="fa fa fa-heart-o"></i></a>
											</div>
										</div>
										<div class="card border-0 mb-0">
											<div class="card-body ">
												<div class="item-card9"> <a href="javascript::void(0)"><?= $service->category;?></a> <a href="javascript::void(0)" class="text-dark"><h4 class="font-weight-semibold mt-1"></h4><?= $service->name;?></a>
													<button class="btn btn-info" onclick="send_enquiry_service()" >Add Inquiry</button>
													<p class="mb-0 leading-tight"><?= $service->description;?></p>
												</div>
											</div>
											<div class="card-footer pt-4 pb-4">
												<div class="item-card9-footer d-flex">
													<div class="item-card9-cost">
														<h4 class="text-dark font-weight-semibold mb-0 mt-0">&#x20B9;	<?= $service->price;?></h4> </div>
													<div class="ml-auto">
														<div class="rating-stars block">
															<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
															<div class="rating-stars-container">
																<div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
																<div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
																<div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
																<div class="rating-star sm"> <i class="fa fa-star"></i> </div>
																<div class="rating-star sm"> <i class="fa fa-star"></i> </div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- End :: Services Block 1  -->
							<?php endforeach; ?>

							</div>


							<div class="tab-pane active" id="tab-12">
								<div class="row" id="service_list">
									
									<!-- Start :: Services Block 2  -->
									<?php foreach($services as $list):?> 
									<div class="col-lg-6 col-md-12 col-xl-4"> 
										<div class="card overflow-hidden">
											<div class="item-card9-img">
												<div class="arrow-ribbon bg-primary"><?= $list->business_type;?></div>
												<div class="item-card9-imgs">
													<a href="<?= base_url('services_details')?>/<?= $list->id?>"></a> <img src="<?= base_url()?>themes/assets/images/services/<?= $list->image;?>" alt="img" class="cover-image"> </div>
												<div class="item-card9-icons">
													<a href="javascript::void(0)" class="item-card9-icons1 wishlist"> <i class="fa fa fa-heart-o"></i></a>   
												</div>
											</div>
											<div class="card-body">
												<div class="item-card9"> <a href="javascript::void(0)"><?= $list->category;?></a> <a href="javascript::void(0)" class="text-dark mt-2"><h4 class="font-weight-semibold mt-1"><?= $list->name;?> </h4></a>
													<p><?= $list->business_name;?></p>
													<div class="item-card9-desc"> <a href="javascript::void(0)" class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i> <?= $list->city;?></span></a> <a href="javascript::void(0)" class=""><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i> Nov-15-2019</span></a>
													<button  onclick="send_enquiry_list()" >Add Inquiry</button></div>
												</div>
											</div>
											<div class="card-footer">
												<div class="item-card9-footer d-flex">
													<div class="item-card9-cost">
														<h4 class="text-dark font-weight-semibold mb-0 mt-0">&#x20B9;<?= $list->price;?></h4> </div>
													<div class="ml-auto">
														<div class="rating-stars block">
															<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
															<div class="rating-stars-container">
																<div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
																<div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
																<div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
																<div class="rating-star sm"> <i class="fa fa-star"></i> </div>
																<div class="rating-star sm"> <i class="fa fa-star"></i> </div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								<?php endforeach; ?>
									<!-- End :: Services Block 1  -->
									
								</div>
							</div>
							<div class="center-block text-center">
								<ul class="pagination mb-0">
									<li class="page-item page-prev disabled"> <a class="page-link" href="ad-list-right.html#" tabindex="-1">Prev</a> </li>
									<li class="page-item active"><a class="page-link" href="ad-list-right.html#">1</a></li>
									<li class="page-item"><a class="page-link" href="ad-list-right.html#">2</a></li>
									<li class="page-item"><a class="page-link" href="ad-list-right.html#">3</a></li>
									<li class="page-item page-next"> <a class="page-link" href="ad-list-right.html#">Next</a> </li>
								</ul>
							</div>
						</div>
					</div>
					<!--/Add Lists-->
				</div>
			</div>
		</section>
		<!--/Add Listing-->
		<!-- Newsletter-->
		<section class="sptb bg-white border-top">
			<div class="container">
				<div class="row">
					<div class="col-lg-7 col-xl-6 col-md-12">
						<div class="sub-newsletter">
							<h3 class="mb-2"><i class="fa fa-paper-plane-o mr-2"></i> Subscribe To Our Newsletter</h3>
							<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
						</div>
					</div>
					<div class="col-lg-5 col-xl-6 col-md-12">
						<div class="input-group sub-input mt-1">
							<input type="text" class="form-control input-lg " placeholder="Enter your Email">
							<div class="input-group-append ">
								<button type="button" class="btn btn-primary btn-lg br-tr-3  br-br-3"> Subscribe </button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Newsletter--> 

<div id="serviceinquirymodal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
     
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>

      <div class="modal-body">
        
        <form name="add_service_form_service" id="add_service_form_service" method="post">
            
            <div class="row"> 
               <div class="col-md-8">
               <div class="form-group">
                  
                     <label for="name">Name<span class="text-danger">*</span></label>
                     <input type="text" name="name" id="name" required="required" placeholder="Enter Your Name">
                     </div>
                  </div>
 
                     <div class="col-md-8"> 
                        <div class="form-group">
                        <label for="mobile">Mobile Number<span class="text-danger"></span>*</label>
                     <input type="number" name="mobile" id="mobile" required="required" placeholder="Enter Your Contact number">
                     <input type="hidden"  id="item_id" name="item_id" value="<?= $service->id; ?>">
                     <input type="hidden"  id="item_status" name="item_status" value="service">
                     </div>
                     </div> 
                     <div class="col-sm-12"> 
                  <div id="inquiry_msg"></div>
                 </div>  

                     <button type="button" class="btn btn-primary float-right" onclick="add_row_service()">Submit</button>              

                  </div>  

         </form>

      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>

    </div>

 </div>
</div>

<script type="text/javascript"> 

	$(document).ready(function(){

		$('#keyword').keyup(function(){
			var keyword =$(this).val();
			//console.log(keyword);

			$.ajax({
				url: '<?php echo base_url()?>services_listing/get_keyword',
				type:"POST",
				data:{keyword:keyword},								
				success:function(data){
					
					//$('#service_list').html(data);
					
				}
			});
		});

		$('#select_cat_id').click(function(){

			var catid = $(this).val();				

				$.ajax({
				url: '<?php echo base_url()?>services_listing/get_selected_category',
				type:"POST",
				data:{catid:catid},								
				success:function(data){
					
					$('#service_list').html(data);
					
				}
			});


		});
	});  	


	function service_category() 
	{

		var filter = [];
	    $('.category-filter:checked').each(function(){
	        filter.push($(this).val());
	    });

	    $.ajax({
			 url: "<?php echo base_url()?>services_listing/get_category_service", 
			 type: "POST",
	     	 data: {category: filter},
	  		 async: false,
			 success: function(data) {  

			 	$('#service_list').html(data);
	 		 }
		}); 
	}	



	function send_enquiry_service() {
      
      $('#serviceinquirymodal').modal('show');
      
   	}


   	function add_row_service()
		{
			var form = $('#add_service_form_service')[0]; 
			var formData = new FormData(form);

			$.ajax({
			    url: "<?php echo base_url()?>inquiry/inquiry_add", 
			    type: "POST",
			    data: formData,
			  	async: false,
			    dataType: 'json',
			    contentType: false, 
			    processData: false, 
		  		success: function(data) 
		  		{
		   		    $('#inquiry_msg').html(''); 
				  	if (data["status"] == 1)
				  	{ 
				     $("#add_service_form_service")[0].reset();
				     
				     $('#inquiry_msg').html(data['msg']).css('color','green'); 
				     view_list();
				     setTimeout(function() {
				        $('#inquiry_msg').html(''); 
				        },2000);
					}
					else if (data['status'] == 2)
					{ 
					     $('#inquiry_msg').html(data['msg']).css('color','red'); 
					}
				   	else
				   	{

				  	}

		 },
		 error: function() { 
		  alert('error');
		 }
		}); 
		} 
</script>

<div id="listinquirymodal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
     
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>

      <div class="modal-body">
        
        <form name="add_service_form_list" id="add_service_form_list" method="post">
            
            <div class="row"> 
               <div class="col-md-8">
               <div class="form-group">
                  
                     <label for="name">Name<span class="text-danger">*</span></label>
                     <input type="text" name="name" id="name" required="required" placeholder="Enter Your Name">
                     </div>
                  </div>
 
                     <div class="col-md-8"> 
                        <div class="form-group">
                        <label for="mobile">Mobile Number<span class="text-danger"></span>*</label>
                     <input type="number" name="mobile" id="mobile" required="required" placeholder="Enter Your Contact number">
                     <input type="hidden"  id="item_id" name="item_id" value="<?= $list->id; ?>">
                     <input type="hidden"  id="item_status" name="item_status" value="service">
                     </div>
                     </div> 
                     <div class="col-sm-12"> 
                  <div id="inquiry_msg"></div>
                 </div>  

                     <button type="button" class="btn btn-primary float-right" onclick="add_row_list()">Submit</button>              

                  </div>  

         </form>

      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>

    </div>

 </div>
</div>
<script type="text/javascript">
	function send_enquiry_list() {
      
      $('#listinquirymodal').modal('show');
      
   }

   function add_row_list()
{
var form = $('#add_service_form_list')[0]; 
var formData = new FormData(form);
$.ajax({
   url: "<?php echo base_url()?>inquiry/inquiry_add", 
   type: "POST",
     data: formData,
  async: false,
     dataType: 'json',
     contentType: false, 
     processData: false, 
  success: function(data) 
  {
   $('#inquiry_msg').html(''); 
  if (data["status"] == 1)
  { 
     $("#add_service_form_list")[0].reset();
     
     $('#inquiry_msg').html(data['msg']).css('color','green'); 
     
     setTimeout(function() {
                    $('#inquiry_msg').html(''); 
                },2000);
  }
  else if (data['status'] == 2)
  { 
     $('#inquiry_msg').html(data['msg']).css('color','red'); 
   }
   else
   {

  }

 },
 error: function() { 
  alert('error');
 }
}); 
} 

</script>



