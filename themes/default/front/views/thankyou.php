<body style="background:url(<?= $front_assets ?>/donation/Backgraund-Photo-YDS.jpg) no-repeat; background-size:cover; background-attachment:fixed;">
	
<div class="container" style="padding:0;margin:30px auto 0;background:#f8fbfc; opacity:0.9;">
    <div class="container-fluid" style="padding-left:0; padding-right:0;">
        <!--<div class="col-md-12 top1">-->
            <div class="col-md-12">

            <div class="form-row">
                <div class=" col-md-3 col-sm-3 col-xs-3 col-lg-3"><a href="<?= $Settings->site_url ?>"><img src="<?= $front_assets ?>/donation/am-logo.png" style="padding: 5%;width: 100%"></a></div>
                <div class="col-md-6 col-sm-3 col-xs-3 text-center mobilehiden" style="color:#CF0000;" ><h1>Anoopam Mission<h1></div>
                <div class="col-sm-3 col-sm-3 col-xs-3 mobilehiden"><a href="<?= $Settings->site_url ?>"><img src="<?= $front_assets ?>/donation/am-logo.png" style="padding: 5%;width: 100%" ></a></div>
            </div>
        </div>
        
    </div>
</div>
<div class="container" style="background:#f8fbfc; opacity:0.9;min-height: 500px;">
    <div class="container-fluid " >
        
        <?php if (@$transations->payment_status == 'success') { ?>
            
        
        <div class="col-md-12 text-right">
            <a id="back_btn" style="display: none;" href="javascript:void()" onclick="back_btn()">BACK</a>
        </div>

        <div class="col-md-12 text-center" id="thanks_donation_ui">
            	<p style="font-size: 2em;"> Thanks For Donation <?= $transations->amount ?>/- </p>
            	<p style="font-size: 1em;">
                <a href="<?= base_url("/");?>"> Go To Home </a>

            		<a target="_blank" href="<?= base_url("print_receipt/");?><?= @$transations->payment_transition_id ?>"><b>Print Receipt</b></a>
            	 </p>
                 <p><label>
                    <input type="checkbox" name="register" id="register">  If you want to register then check here
                    </label>
                 </p>

        </div>
        <div class="row" id="register_ui" style="display: none;">
               <div class="col-md-3"></div>
               <div class="col-md-6"> 
               <h4 class="text-center">REGISTER</h4>
               <hr> 
              <form name="register_from" id="register_from" method="post" class="form-horizontal">
               <input type="hidden" name="donor_name" value="<?= @$transations->name ?>">
               <input type="hidden" name="donor_email" value="<?= @$transations->donner_email ?>">
               <input type="hidden" name="payment_transition_id" value="<?= @$transations->payment_transition_id ?>">

               <div class="form-row" id="dvFirstName">
                    <div class="form-group col-md-4">
                        <label for="txtDonorName">Mobile No <span style="color:#f00;">*</span></label>
                    </div>
                    <div class="form-group col-md-8">
                        <input type="text" class="form-control" id="donor_mobile" name="donor_mobile" required="" value="<?= @$transations->mobile ?>" >
                    </div>
                </div>
                <div class="form-row" id="dvFirstName">
                    <div class="form-group col-md-4">
                        <label for="txtDonorName">Password <span style="color:#f00;">*</span></label>
                    </div>
                    <div class="form-group col-md-8">
                        <input type="password" class="form-control" id="donor_password" name="donor_password" required="">
                    </div>
                </div>
                <div class="form-row" id="dvFirstName">
                    <div class="form-group col-md-4">
                        <label for="txtDonorName">Confirm Password <span style="color:#f00;">*</span></label>
                    </div>
                    <div class="form-group col-md-8">
                        <input type="password" class="form-control" id="donor_c_password" name="donor_c_password" required="">
                    </div>
                </div>
                <div class="form-row" id="dvFirstName">
                    
                    <div class="form-group col-md-12 text-center">
                       <button onclick="add_from()" type="button" class="btn btn-info" type="submit">Register</button>
                    </div>
                </div>

                </form>
                </div>

        </div>

        <?php } else { ?>
           
            <div class="col-md-12 text-center" id="thanks_donation_ui">
                <p style="font-size: 2em;"> Sorry, We can't proceed your doanation amount from payment gateway. </p>
                <p style="font-size: 1em;"> <a href="<?= base_url("/");?>"><b>Go To Home </b></a></p>                

        </div>
        <?php } ?>   


    </div>
</div>

<script>

    function add_from()
    {
         if($('#donor_password').val() == '' || $('#donor_c_password').val() == '')
         {
            $.Notification.notify('error','top right','<?php echo lang('Error'); ?>', 'Please enter all compulsory field');    
            return false;
         }
         var form = $('#register_from')[0];
         var formData = new FormData(form);

         $.ajax({
                url: "<?php echo site_url('home/register_add')?>/",
                type: "POST",
                data: formData,
                cache: false,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function(response) 
                {
                    if(response['status'] == '1')
                    {
                    $("#register_from")[0].reset();
                    $.Notification.notify('success','top right','<?php echo lang('success'); ?>', response['msg']);
    
                        setTimeout(function() {
                           window.location = "<?php echo site_url('')?>";
                        },1500);
    
                    }else{
                          $.Notification.notify('error','top right','<?php echo lang('Error'); ?>',response['msg']);
                    }
                },
                error: function() {
                    alert('error');
                }
            });
    }
  

    $(".CurrencyField").inputFilter(function (value) {
        return /^-?\d*[.,]?\d{0,2}$/.test(value);
    });
</script>

<script type="text/javascript">
    
    function back_btn()
    {
        $('#register_ui').hide();
        $('#thanks_donation_ui').show();

        $('#regiter_btn').show();
        $('#back_btn').hide();
        $('#register').prop("checked",false);
    }
</script>

<script type="text/javascript">

     $(document).ready(function() 
     {
        $('input[type="checkbox"]').click(function(){
            if($(this).prop("checked") == true){
                
                $('#register_ui').show();
                $('#thanks_donation_ui').hide();

                $('#regiter_btn').hide();
                $('#back_btn').show();

            }
            else if($(this).prop("checked") == false){
                console.log("Checkbox is unchecked.");
            }
        });
    });


   
</script>



<script src="chrome-extension://hhojmcideegachlhfgfdhailpfhgknjm/web_accessible_resources/index.js"></script>
<!--<div>
    <div class="grecaptcha-badge" data-style="bottomright" style="width: 256px; height: 60px; display: block; transition: right 0.3s ease 0s; position: fixed; bottom: 14px; right: -186px; box-shadow: gray 0px 0px 5px; border-radius: 2px; overflow: hidden;">
        <div class="grecaptcha-logo">
            <iframe src="./donation_files/anchor.html" width="256" height="60" role="presentation" name="a-ilkuobi4s51d" frameborder="0" scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox"></iframe></div><div class="grecaptcha-error"></div><textarea id="g-recaptcha-response-100000" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid rgb(193, 193, 193); margin: 10px 25px; padding: 0px; resize: none; display: none;"></textarea></div><iframe style="display: none;" src="./donation_files/saved_resource(1).html">
                
            </iframe>
</div>-->
</body></html>





















