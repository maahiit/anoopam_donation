
		<section> 
			<div class="bannerimg cover-image bg-background3" data-image-src="../assets/images/banners/banner2.jpg" style="background: url(&quot;../assets/images/banners/banner2.jpg&quot;) center center;">
				<div class="header-text mb-0">
					<div class="container"> 
						<div class="text-center text-white">
							<h1 class="">My Dashboard</h1>
							<ol class="breadcrumb text-center">
								<li class="breadcrumb-item"><a href="<?php echo base_url('mydask');?>">Home</a></li>
								<li class="breadcrumb-item active text-white" aria-current="page">My Dashboard</li>
							</ol>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--Breadcrumb-->
		<!--User Dashboard-->
		<section class="sptb">
			<div class="container">
				<div class="row">
					<div class="col-xl-3 col-lg-12 col-md-12"> 
						<div class="card">
							<div class="card-header">
								<h3 class="card-title">My Dashboard</h3> </div>
							<div class="card-body text-center item-user border-bottom">
								<div class="profile-pic">
									<div class="profile-pic-img"> <span class=" bg-success dots" data-toggle="tooltip" data-placement="top" title="" data-original-title="online"></span> 
										<img src="<?= $front_assets ?>images/comman_images/25.jpg" class="brround" alt="user"> </div> <a href="<?php echo base_url('mydask');?>" class="text-dark"><h4 class="mt-3 mb-0 font-weight-semibold"><?= $this->session->userdata('loginid');?> </h4></a> </div>
							</div>
							<div class="item1-links  mb-0">
								<a href="<?php echo base_url('mydask');?>" class="active d-flex border-bottom"> <span class="icon1 mr-3"><i class="icon icon-user"></i></span> Edit Profile </a>
								<a href="<?php echo base_url('services');?>" class=" d-flex  border-bottom"> <span class="icon1 mr-3"><i class="icon icon-diamond"></i></span> Services </a>
								<a href="<?php echo base_url('products');?>" class=" d-flex border-bottom"> <span class="icon1 mr-3"><i class="icon icon-heart"></i></span> Products </a>
								<a href="<?php echo base_url('amenities');?>" class="d-flex  border-bottom"> <span class="icon1 mr-3"><i class="icon icon-folder-alt"></i></span> Amenities </a>
								<a href="<?php echo base_url('offer_discount');?>" class=" d-flex  border-bottom"> <span class="icon1 mr-3"><i class="icon icon-credit-card"></i></span> Offer Discount </a>
								<a href="<?php echo base_url('reviews');?>" class=" d-flex  border-bottom"> <span class="icon1 mr-3"><i class="icon icon-credit-card"></i></span> Reviews </a>
								
							
								<a href="<?= base_url('login/logout'); ?>" class="d-flex"> <span class="icon1 mr-3"><i class="icon icon-power"></i></span> Logout </a>
							</div>
						</div>
						
					</div>
					
					<div class="col-xl-9 col-lg-12 col-md-12">
						<div class="card mb-0">
							<form name="update_form" id="update_form" method="POST" >

							<div class="card-header">
								<h3 class="card-title">Edit Profile</h3> </div>
							<div class="card-body">
								<div class="row">
									<div class="col-sm-6 col-md-6">
										<div class="form-group">
											<label for="fname"><?= lang('fname'); ?><span class="text-danger">*</span></label>
											<input type="text" name="fname" id="fname" class="form-control" value="<?= $user->first_name; ?>" autofocus placeholder="<?= lang('fname');?>">
										</div>
									</div>
									<div class="col-sm-6 col-md-6">
										<div class="form-group">
											<label for="sname"><?= lang('sname'); ?><span class="text-danger">*</span></label>
											<input type="text" name="sname"  id="sname" class="form-control" value="<?= $user->last_name; ?>" placeholder="<?= lang('sname');?>">
										</div>
									</div>

									<div class="col-sm-6 col-md-6">
										<div class="form-group">
											<label for="business_name"><?= lang('Business_type'); ?><span class="text-danger">*</span></label>
											<select id="business_type" name="business_type" class="form-control">
												<option value="salon" <?php if($user->business_type =='salon')   {echo 'selected';}?>>Salon</option>
												<option value="spa" <?php if($user->business_type=='spa')   {echo 'selected';}?>>Spa</option>
											</select>
										</div>
									</div>
									<div class="col-sm-6 col-md-6">
										<div class="form-group">
											<label for="business_name"><?= lang('Business_name'); ?><span class="text-danger">*</span></label>
											<input type="text" required="required" name="business_name" id="business_name" class="form-control" value="<?= $user->business_name; ?>" autofocus placeholder="<?= lang('business_name');?>">
										</div>
									</div>
									<div class="col-sm-6 col-md-6">
										<div class="form-group">
											<label for="number_of_seats"><?= lang('Number_of_seats'); ?><span class="text-danger">*</span></label>
											<input type="text" name="number_of_seats" id="number_of_seats" class="form-control" value="<?= $user->number_of_seats; ?>" autofocus placeholder="<?= lang('number_of_seats');?>">
										</div>
									</div>
									<div class="col-sm-6 col-md-6">
										<div class="form-group">
											<label for="mobile"><?= lang('mobile'); ?><span class="text-danger">*</span></label>
											<input type="number" required="required" onKeyPress="if(this.value.length==10) return false;" name="mobile" id="mobile" class="form-control" placeholder="<?= lang('mobile');?>" value="<?= $user->mobile; ?>">
										</div>
									</div>
									<div class="col-sm-6 col-md-6">
										<div class="form-group">
											<label for="email"><?= lang('email'); ?><span class="text-danger">*</span></label>
											<input type="email" name="email" id="email" class="form-control" placeholder="<?= lang('email');?>" value="<?= $user->email_id; ?>">
										</div>
									</div>
									
									<div class="col-sm-6 col-md-6">
										<div class="form-group"> 
											<label for="name">City<span class="text-danger">*</span></label>

											<select  name="city" id="city" class="form-control" data-style="btn-white" >
												<option>-Select City-</option>

												<?php foreach ($city as $key => $value): ?>
													<option

													<?php 

													if($value->id == $user->city) { echo "selected"; } ?>
													value="<?= $value->id ?>"><?= $value->city ?> </option>    
												<?php endforeach ?>

											</select>

										</div>
									</div>
									<div class="col-sm-6 col-md-6">
										<div class="form-group">
											<label for="area"><?= lang('Area'); ?><span class="text-danger">*</span></label>
											<input type="text" required="required" name="area" id="area" class="form-control" value="<?= $user->area; ?>" autofocus placeholder="<?= lang('Area');?>">
										</div>
									</div>
									<div class="col-sm-6 col-md-6">
										<div class="form-group">
											<label for="area">latitude<span class="text-danger">*</span></label>
											<input type="text"  name="latitude" id="latitude" class="form-control" autofocus placeholder="latitude" value="<?= $user->latitude; ?>">
										</div>
									</div>
									<div class="col-sm-6 col-md-6">
										<div class="form-group">
											<label for="area">longitude<span class="text-danger">*</span></label>
											<input type="text" name="longitude" id="longitude" class="form-control" autofocus placeholder="longitude" value="<?= $user->longitude; ?>">
										</div>
									</div>
									<div class="col-sm-6 col-md-6"> 
										<div class="form-group">
											<label for="name">Packages<span class="text-danger">*</span></label>

											<select  name="plan_id" id="plan_id" class=" form-control" data-style="btn-white"  >
												<option  value="">Select Plan </option>

												<?php foreach ($plans as $key => $plan): ?>

												<option 

													<?php
													     if($user->plan_id == $plan->id) 
														   { echo "selected"; } 
													?>

													value="<?= $plan->id ?>" >

													<?= $plan->plan_name; ?>

												</option>

												<?php endforeach ?>

											</select>

										</div>
									</div>
								</div>	
									<div class="row">

										<div class="col-md-4">
											<div class="form-group">
												<label for="monday_time_start">Days<span class="text-danger">*</span></label>
											</div>
										</div>

										<div class="col-md-4">
											<div class="form-group">
												<label for="monday_time_start">Start Time<span class="text-danger">*</span></label>
											</div>
										</div>

										<div class="col-md-4">
											<div class="form-group">
												<label for="monday_time_start">End Time<span class="text-danger">*</span></label>
											</div>
										</div>

									</div>

									<div class="row">

										<div class="col-md-4">
											<div class="form-group">
												<label for="monday_time_start">Monday<span class="text-danger">*</span></label>
											</div>
										</div>

										<div class="col-md-4">
											<div class="form-group">

												<input type="time"  name="monday_time_start" id="monday_time_start" class="form-control" value="<?= $user->monday_time_start; ?>" autofocus placeholder="<?= lang('monday_time_start');?>">
											</div>
										</div>

										<div class="col-md-4">
											<div class="form-group">

												<input type="time"  name="monday_time_end" id="monday_time_end" class="form-control" value="<?= $user->monday_time_end; ?>" autofocus placeholder="<?= lang('monday_time_end');?>">
											</div>
										</div>

									</div>

									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label for="monday_time_start">Tuesday<span class="text-danger">*</span></label>
											</div>
										</div>


										<div class="col-md-4">
											<div class="form-group">

												<input type="time"  name="tuesday_time_start" id="tuesday_time_start" class="form-control" value="<?= $user->tuesday_time_start; ?>" autofocus placeholder="<?= lang('tuesday_time_start');?>">
											</div>
										</div>

										<div class="col-md-4">
											<div class="form-group">

												<input type="time"  name="tuesday_time_end" id="tuesday_time_end" class="form-control" value="<?= $user->tuesday_time_end; ?>" autofocus placeholder="<?= lang('tuesday_time_end');?>">
											</div>
										</div>

									</div>


									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label for="monday_time_start">Wednesday<span class="text-danger">*</span></label>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">

												<input type="time"  name="wednesday_time_start" id="wednesday_time_start" class="form-control" value="<?= $user->wednesday_time_start; ?>" autofocus placeholder="<?= lang('wednesday_time_start');?>">
											</div>
										</div>

										<div class="col-md-4">
											<div class="form-group">

												<input type="time"  name="wednesday_time_end" id="wednesday_time_end" class="form-control" value="<?= $user->wednesday_time_end; ?>" autofocus placeholder="<?= lang('wednesday_time_end');?>">
											</div>
										</div>

									</div>

									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label for="monday_time_start">Thursday<span class="text-danger">*</span></label>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">

												<input type="time"  name="thursday_time_start" id="thursday_time_start" class="form-control" value="<?= $user->thursday_time_start; ?>" autofocus placeholder="<?= lang('thursday_time_start');?>">
											</div>
										</div>

										<div class="col-md-4">
											<div class="form-group">

												<input type="time" name="thursday_time_end" id="thursday_time_end" class="form-control" value="<?= $user->thursday_time_end; ?>" autofocus placeholder="<?= lang('thursday_time_end');?>">
											</div>
										</div>

									</div>

									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label for="monday_time_start">Friday<span class="text-danger">*</span></label>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">

												<input type="time" name="friday_time_start" id="friday_time_start" class="form-control" value="<?= $user->friday_time_start; ?>" autofocus placeholder="<?= lang('friday_time_start');?>">
											</div>
										</div>

										<div class="col-md-4">
											<div class="form-group">

												<input type="time"  name="friday_time_end" id="friday_time_end" class="form-control" value="<?= $user->friday_time_end; ?>" autofocus placeholder="<?= lang('friday_time_end');?>">
											</div>
										</div>

									</div>

									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label for="monday_time_start">Saturday<span class="text-danger">*</span></label>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">

												<input type="time"  name="saturday_time_start" id="saturday_time_start" class="form-control" value="<?= $user->saturday_time_start; ?>" autofocus placeholder="<?= lang('saturday_time_start');?>">
											</div>
										</div>

										<div class="col-md-4">
											<div class="form-group">

												<input type="time" name="saturday_time_end" id="saturday_time_end" class="form-control" value="<?= $user->saturday_time_end; ?>" autofocus placeholder="<?= lang('saturday_time_end');?>">
											</div>
										</div>

									</div>

									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label for="monday_time_start">Sunday<span class="text-danger">*</span></label>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">

												<input type="time"  name="sunday_time_start" id="sunday_time_start" class="form-control" value="<?= $user->sunday_time_start; ?>" autofocus placeholder="<?= lang('sunday_time_start');?>">
											</div>
										</div>

										<div class="col-md-4">
											<div class="form-group">

												<input type="time"  name="sunday_time_end" id="sunday_time_end" class="form-control" value="<?= $user->sunday_time_end; ?>" autofocus placeholder="<?= lang('sunday_time_end');?>">
											</div>
										</div>

									</div>

									<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label for="about"><?= lang('About'); ?><span class="text-danger">*</span></label>
											<textarea  name="about" id="about" class="form-control" value="<?= $user->about; ?>" placeholder="About"><?= $user->about; ?></textarea>
										</div> 

									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label for="business_address"><?= lang('Business Address'); ?><span class="text-danger">*</span></label>
											<textarea required="required" name="business_address" id="business_address" class="form-control" placeholder="<?= lang('Business Address');?>"><?= $user->business_address; ?></textarea>
										</div>


									</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label for="business_address">Remark<span class="text-danger">*</span></label>
												<textarea name="remarks" id="remarks" class="form-control" placeholder="Remark"><?= $user->remarks; ?></textarea>
											</div>


									</div>
									</div>
									<div class="row">      

										<div class="col-md-2">
											<div class="form-group checkbox checkbox-danger" style="margin-top: 30px;">
												<input type="checkbox" <?php echo ($user->status == 'inactive') ? 'checked="checked"' : ''?> name="status" value="inactive" id="status">
												<label for="status"><?= lang('inactive'); ?><span class="text-danger">*</span></label>
											</div>
										</div>

									</div>
									<div class="col-md-8">
										<div class="form-group">
											<label for="banner">Banner
												<span class="text-danger">*</span>
											</label>
											<input type="file" name="banner" class="filestyle" id="banner" data-size="sm" onchange="ImagePreview(this,'preview_banner_image');" >
											<?php if (!empty($user->banner)) { ?>
												<img id="preview_banner_image" src="<?= base_url() ?>themes/assets/images/usersign/<?= $user->banner; ?>" style="width: 100%;height: 150px;margin-top: 5px" />    
											<?php }else{ ?>
												<img id="preview_banner_image" src="" style="width: 100%;height: 150px;display: none;margin-top: 5px" />
											<?php } ?>                
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label for="logo">Logo<span class="text-danger">*</span></label>
											<input type="file" name="logo" class="filestyle" id="logo" data-size="sm" onchange="ImagePreview(this,'preview_logo_image');" >
											<?php if (!empty($user->logo)) { ?>
												<img id="preview_logo_image" src="<?= base_url() ?>themes/assets/images/usersign/<?= $user->logo; ?>" style="width: 100%;height: 150px;margin-top: 5px" />    
											<?php }else{ ?>
												<img id="preview_logo_image" src="" style="width: 100%;height: 150px;display: none;margin-top: 5px" />
											<?php } ?>                
										</div>
									</div>
								
							</div>
							
							<div class="card-footer"> 
								<button type="button" class="btn btn-primary" onclick="update_user();">Updated Profile</button>
							</div>
							<div id="login_msg"></div>

							</form>
						</div>
					</div>
				
				</div>
			</div>
		</section>
		<!--/User Dashboard-->
		<!-- Newsletter-->
		<section class="sptb bg-white border-top">
			<div class="container">
				<div class="row">
					<div class="col-lg-7 col-xl-6 col-md-12">
						<div class="sub-newsletter">
							<h3 class="mb-2"><i class="fa fa-paper-plane-o mr-2"></i> Subscribe To Our Newsletter</h3>
							<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
						</div>
					</div>
					<div class="col-lg-5 col-xl-6 col-md-12">
						<div class="input-group sub-input mt-1">
							<input type="text" class="form-control input-lg " placeholder="Enter your Email">
							<div class="input-group-append ">
								<button type="button" class="btn btn-primary btn-lg br-tr-3  br-br-3"> Subscribe </button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--/Newsletter-->

	<script>	
    

		function update_user(){	

		var form = $('#update_form')[0]; 
        var formData = new FormData(form);	

			$.ajax({
				url: "<?php echo base_url()?>mydask/update_user",
				type: "POST",
				data: formData,				
				cache: false,
        		contentType: false,
        		processData: false,
        		dataType: 'json',
				success: function(response) {				
					
					$('#login_msg').html('');	
					if (response["status"] == 1)
					{	
					    $('#login_msg').html(response['msg']).css('color','green');	
					}
		   			else
		   			{
					 	$('#login_msg').html(response['msg']).css('color','red');	
					}
				},
				error: function() {
					alert('error');
				}
			});
		}
	</script> 