
<body >
	
<div class="container" style="padding:0;margin:30px auto 0;background:#f8fbfc; opacity:0.9;">
    <div class="container-fluid" style="padding-left:0; padding-right:0;">
        <!--<div class="col-md-12 top1">-->
            <div class="col-md-12">

            <div class="form-row">
                <div class=" col-md-3 col-sm-3 col-xs-3 col-lg-3"><a href="<?= $Settings->site_url ?>"><img src="<?= $front_assets ?>/donation/am-logo.png" style="padding: 5%;width: 100%"></a></div>
                <div class="col-md-6 col-sm-3 col-xs-3 text-center mobilehiden" style="color:#CF0000;" ><h1>Anoopam Mission<h1></div>
                <div class="col-sm-3 col-sm-3 col-xs-3 mobilehiden"><a href="<?= $Settings->site_url ?>"><img src="<?= $front_assets ?>/donation/am-logo.png" style="padding: 5%;width: 100%" ></a></div>
            </div>
        </div>
        
    </div>
</div>
<?php
                             // echo "<pre>";
                             // print_r($_SESSION);

                            ?>

<div class="container" style="background:#f8fbfc; opacity:0.9;">
    <div class="container-fluid">
        <div class="col-md-12  text-right">
            
            
            <?php  if (!$this->User_loggedIn) { ?>

                <a id="login_btn" class="btn btn-info" href="javascript:void(0)" onclick="func_login()">Login</a>    

            <?php }else{ ?>    
            <ul class="header_url">
                <li><a class="dropdown-item" href="<?= base_url('login/logout'); ?>"> <i class="dropdown-icon icon icon-power"></i><i class="fa fa-user"></i> Welcome, <b><?= $this->session->userdata('user_loginname'); ?> </b></a></li>    
                
                <li> <a  onclick="func_donation_history();" class="dropdown-item" href="javascript:void(0);"> <!--<i class="dropdown-icon icon icon-power btn btn-info" ></i>--> <i class="fa fa-caret-down" ></i> My Donation </a></li>

                <li><a class="dropdown-item" href="<?= base_url('login/logout'); ?>" title="Logout"> <i class="fa fa-sign-out"></i>  </a></li>
            </ul>
            <?php } ?>

            <a id="backbtn" style="display: none;" href="javascript:void(0)" onclick="back_btn()" class="btn btn-info">Home</a>
        </div>        
        <div class="row" id="login_ui" style="display: none;">
               <div class="col-md-3"></div>
               <div class="col-md-6"> 
               <h4 class="text-center">Login</h4>
               <hr> 
              <form name="login_from" id="login_from" method="post" class="form-horizontal">
                <div class="form-group col-md-12 text-center">        
                        <div id="login_msg"></div>
                    </div>
               <div class="form-row" id="dvmobilelogin">
                    <div class="form-group col-md-4">
                        <label for="username">Mobile No <span style="color:#f00;">*</span></label>
                    </div>
                    <div class="form-group col-md-8">
                        <input type="text" class="form-control" id="username" name="username" required=""  >
                    </div>
                </div>
                <div class="form-row" id="dvpasswordlogin">

                    <div class="form-group col-md-4">
                        <label for="donor_password">Password <span style="color:#f00;">*</span></label>
                    </div>
                    <div class="form-group col-md-8">
                        <input type="password" class="form-control" id="donor_password" name="donor_password" required="">
                         
                    </div>

                    
                    
                   
                </div>
               
                <div class="form-row" id="dvFirstName">
                    
                    <div class="form-group col-md-12 text-center">
                       <button onclick="auth_login()" type="button" class="btn btn-info" type="submit">Sign In</button>
                    </div>
                </div>

                </form>
                </div>

        </div>
            <?php  if ($this->User_loggedIn) { ?>
        <div class="row" id="donation_history_ui" style="display: none;">
            <div class="col-md-12 text-center"> 
             <h4>My Donation </h4>
               
           </div>
           <div class="col-md-12"> 
            <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Transation Id</th>
                        <th>Amount</th>
                        <th>Date</th>
                        <th>Payment Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($mydonations as $key => $mydonation): ?>
                        <tr>
                            <td><?= $key + 1 ?></td>
                            <td><?= $mydonation['payment_transition_id'] ?></td>
                            <td><?= number_format($mydonation['amount']); ?>/-</td>
                            <td><?= $mydonation['payment_response_time'] ?></td>
                            <td><?= ucfirst($mydonation['payment_status']) ?></td>
                            <td><a target="_blank" href="<?= base_url("print_receipt/");?><?= $mydonation['payment_transition_id'] ?>"><b><i class="fa fa-print"></i>
</b></a></td>

                        </tr>    
                    <?php endforeach ?>
                    
                </tbody>
            </table>
            </div>
            </div>
        </div>
    <?php } ?>


        <div class="col-md-12" id="donation_form_ui">
            <form name="donation_from" id="donation_from" method="post" class="form-horizontal" action="<?php echo site_url('home/add_form')?>" >
                <!--<div class="form-row">
                    <div class="form-group col-md-12">
                    </div>
                </div>-->


                 <div class="form-row" style="display: none;">
                    <div class="form-group col-md-4">
                        <label for="donation_type">Country<span style="color:#f00;">*</span></label>
                    </div>
                    <div class="form-group col-md-8">
                     <select class="form-control" id="donation_country_id" name="donation_country_id" required="">
                           <option value="">--Select--</option>

                            <?php foreach ($countries as $key => $country): ?>
                                <option <?php if ($country->id == 1){ echo "selected"; } ?> value="<?= $country->id; ?>"><?= $country->name; ?></option>  
                                }
                            <?php endforeach ?>
                           
                        </select>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-4">
                    </div>
                    <div class="form-group col-md-8">
                        <label><input type="radio" id="rbIndividual" name="donor_type" value="Individual" onchange="setDonorType(this)" checked=""> Individual</label>
                        <label><input type="radio" id="rbOther" name="donor_type" value="Other" onchange="setDonorType(this)"> Other</label>
                    </div>
                </div>

                <div class="form-row" id="dvFirstName">
                    <div class="form-group col-md-4">
                        <label for="txtDonorName" id="txtDonorName_lable">First Name of Donor<span style="color:#f00;">*</span></label>
                    </div>
                    <div class="form-group col-md-8">
                        <input type="text" class="form-control" id="donor_fname" name="donor_fname" required="">
                    </div>
                </div>
                <div class="form-row" id="dvMiddleName">
                    <div class="form-group col-md-4">
                        <label for="txtSecondName">Father/Husband Name of Donor<span style="color:#f00;">*</span></label>
                    </div>
                    <div class="form-group col-md-8">
                        <input type="text" class="form-control" id="donor_mname" name="donor_mname">
                    </div>
                </div>
                <div class="form-row" id="dvLastName">
                    <div class="form-group col-md-4">
                        <label for="txtSurname">Surname of Donor<span style="color:#f00;">*</span></label>
                    </div>
                    <div class="form-group col-md-8">
                        <input type="text" class="form-control" id="donor_lname" name="donor_lname">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="txtDonorAddress">Full Postal Address<span style="color:#f00;">*</span></label>
                    </div>
                    <div class="form-group col-md-8">
                        <textarea class="form-control" rows="3" id="donor_address" name="donor_address" required=""></textarea>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="txtCity">City<span style="color:#f00;">*</span></label>
                    </div>
                    <div class="form-group col-md-8">
                        <input type="text" class="form-control" id="txtCity" name="txtCity" required="">
                    </div>
                </div>
               
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="txtEmail">Email ID <span style="color:#f00;">*</span></label>
                    </div>
                    <div class="form-group col-md-8">
                        <input type="email" class="form-control" id="donor_email" name="donor_email">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="txtPhone">Telephone / Mobile No<span style="color:#f00;">*</span></label>
                    </div>
                    <div class="form-group col-md-8">
                        <input type="text" class="form-control" id="donor_mobile" name="donor_mobile" required="">
                    </div>
                </div>
                
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="donation_type">Donation Category <span style="color:#f00;">*</span></label>
                    </div>
                    <div class="form-group col-md-8">
                        <select class="form-control" id="donation_category_id" name="donation_category_id" required="">
                            <option value="">--Select--</option>

                            <?php foreach ($categories as $key => $category): ?>
                            	<option value="<?= $category->id; ?>"><?= $category->name; ?></option>	
                            <?php endforeach ?>
                           
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="donation_type">Donation Sub Category<span style="color:#f00;">*</span></label>
                    </div>
                    <div class="form-group col-md-8">
                        <select class="form-control" id="donation_sub_category_id" name="donation_sub_category_id" required="">
                            <option value="">--Select--</option>
                           
                        </select>
                    </div>
                </div>
             
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="txtAmount">Amount of Donation (INR)<span style="color:#f00;">*</span></label>
                    </div>
                    
                    <input type="hidden" id="pan_amount" name="pan_amount" value="<?= $Settings->pan_amount ?>">

                    <div class="form-group col-md-8">

                        <input type="number" class="form-control CurrencyField" id="donation_amount" name="donation_amount" required="" onblur="enter_payment_amt();">
                        <div class="text-danger" id="payable_amt_msg"></div>
                    </div>
                </div>

                <div class="form-row" id="donation_user_pan_ui" style="display: none;">
                    <div class="form-group col-md-4">
                        <label for="txtPAN">PAN No. of Donor <span style="color:#f00;">#</span></label>
                    </div>
                    <div class="form-group col-md-8">
                        <input type="text" class="form-control" id="donation_user_pan" name="donation_user_pan" maxlength="10" >
                    </div>
                </div>


                <div class="form-row" id="donation_trustfund_ui" style="display: none;">
                    <div class="form-group col-md-4">
                        <label for="txtAmount"></label>
                    </div>
                    <div class="form-group col-md-8">
                        <label><input type="checkbox" class="form-group"  id="donation_trustfund" name="donation_trustfund" value="yes"> Would you like to use this amount as <b>Trust Fund</b>?</label>
                    </div>
                </div>

                <?php $prefix_amt = explode(',',$Settings->pre_fix_amount);  ?>

                <div class="form-row" id="default_amount_ui" style="display: none;">
                    <div class="form-group col-md-4"></div>
                    <div class="form-group col-md-8">
                    	<?php foreach ($prefix_amt as $key => $amt): ?>
	                        <button type="button" class="btn btn-primary prefix_amt_class prefix_amt_class_<?= $amt ?>" onclick="get_prefix_amount(<?= $amt ?>)" id="prefix_amt_btn"><?= number_format($amt) ?> /-</button>
                        <?php endforeach ?>
                    </div>
                </div>

                
                <div class="form-row">
                    <div class="form-group col-md-12 text-center">

                        <button onclick="add_from();" type="button" id="submit" class="btn btn-info">Donate Now</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
   

    function func_login()
    {
        $('#login_ui').show();
        $('#donation_form_ui').hide();

        $('#login_btn').hide();
        $('#backbtn').show();
    }
    function func_donation_history()
    {

        $('#donation_history_ui').show();
        $('#donation_form_ui').hide();

        $('#login_btn').hide();
        $('#backbtn').show();
    }

    
    function back_btn()
    {
        $('#login_ui').hide();
        $('#donation_form_ui').show();
        $('#donation_history_ui').hide();

        $('#login_btn').show();
        $('#backbtn').hide();
    }

    function auth_login(){  

        var form = $('#login_from')[0]; 
        var formData = new FormData(form);  

        if($('#username').val() == '' || $('#donor_password').val() == '' )
        {
            $.Notification.notify('error','top right','<?php echo lang('Error'); ?>', 'Please enter all compulsory field');    
            return false;
        }


            $.ajax({
                url: "<?php echo base_url()?>login/login_ajex",
                type: "POST",
                data: formData,             
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(response) {               
                    
                    $('#login_msg').html('');   
                    if (response["status"] == 1)
                    {   
                        $('#login_msg').html(response['msg']).css('color','green'); 
                        
                        setTimeout(function() { 
                          window.location.href = '<?php echo base_url('/')?>';
                        },1500);
                    }
                    else
                    {
                        $('#login_msg').html(response['msg']).css('color','red');   
                    }
                },
                error: function() {
                    alert('Server error');
                }
            });
        }
</script>

<script>
  

	function donation_form()
    {
        
        if($('#donor_fname').val() == '' || $('#donation_amount').val() == '' || $('#donation_category_id').val() == '' || $('#donation_sub_category_id').val() == '')
        {
            $.Notification.notify('error','top right','<?php echo lang('Error'); ?>', 'Please enter all compulsory field1');    
        }else{
            document.getElementById("donation_from").submit();
   
        }
    }

	function get_prefix_amount(amount){
			$('.prefix_amt_class.active').removeClass("active");
			$('.prefix_amt_class_'+amount).addClass("active");
			$('#donation_amount').val(amount);
            enter_payment_amt();
	}


	function add_from()
	{
        var pan_amount = $('#pan_amount').val();
        var donationamt = $('#donation_amount').val();

        /*if($('#donor_fname').val() == '' || $('#donation_amount').val() == '' || $('#donation_category_id').val() == '' || $('#donation_sub_category_id').val() == '')
        {
            $.Notification.notify('error','top right','<?php echo lang('Error'); ?>', 'Please fill all compulsory field');
            return false;
        }
        */

        if($('#donor_fname').val() == '')
        {
            $.Notification.notify('error','top right','<?php echo lang('Error'); ?>', 'Please fill first name.');
            $('#donor_fname').focus();
            return false;
        }
        if($('#donor_email').val() == '')
        {
            $.Notification.notify('error','top right','<?php echo lang('Error'); ?>', 'Please enter email id.');
            $('#donor_email').focus();
            return false;
        }
        if($('#donation_category_id').val() == '')
        {
            $.Notification.notify('error','top right','<?php echo lang('Error'); ?>', 'Please select category.');
            $('#donation_category_id').focus();
            return false;
        }
        if($('#donation_sub_category_id').val() == '')
        {
            $.Notification.notify('error','top right','<?php echo lang('Error'); ?>', 'Please select sub category.');
            $('#donation_sub_category_id').focus();
            return false;
        }
        if($('#donation_amount').val() == '')
        {
            $.Notification.notify('error','top right','<?php echo lang('Error'); ?>', 'Please enter donation amount.');
            $('#donation_amount').focus();
            return false;
        }
        
    



        if (document.getElementById('donation_user_pan').value != "") {
            var objRegExp = /^[a-zA-Z][a-zA-Z][a-zA-Z][cphfatbljgCPHFATBLJG][a-zA-Z][0-9][0-9][0-9][0-9][a-zA-Z]$/;

            if (!objRegExp.test(document.getElementById('donation_user_pan').value)) {
                $.Notification.notify('error','top right','<?php echo lang('Error'); ?>', 'Please enter valid PAN number (AAAAA1234A)');   
                $('#donation_user_pan').focus();
                return false;
            }
        }

        if(parseInt(donationamt) >= parseInt(pan_amount))
        {
             if (document.getElementById('donation_user_pan').value == ""){
                    $.Notification.notify('error','top right','<?php echo lang('Error'); ?>', 'Please enter your PAN number.');    
                    $('#donation_user_pan').focus();
                return false;        
             }
        
        }

		 var form = $('#donation_from')[0];
		 var formData = new FormData(form);
		 $.ajax({
				url: "<?php echo site_url('home/add_form')?>/",
				type: "POST",
				data: formData,
				cache: false,
                dataType: 'json',
        		contentType: false,
        		processData: false,
				success: function(response) 
                {
                    if(response['status'] == '1')
                    {
                        $("#donation_from")[0].reset();
                        //$.Notification.notify('success','top right','<?php echo lang('success'); ?>', response['msg']);
                        setTimeout(function() {
                           window.location = response['url'];
                        },1000);
    
                    }else{
                        alert(response['msg']); 
                    }
				},
				error: function() {
					alert('error');
				}
			});
	   }


    

        $('#donation_country_id').change(function(){
         var donation_country_id = $(this).val();
         $.ajax({
           url: "<?php echo site_url('home/get_category_by_country')?>/",
           method: 'post',
           data: {donation_country_id: donation_country_id},
           dataType: 'json',
           success: function(response){
             // Remove options
             $('#donation_category_id').find('option:not(:first)').remove();
             $('#donation_sub_category_id').find('option:not(:first)').remove();
             
             $.each(response,function(index,data){
                 $('#donation_category_id').append('<option data-min_amt="'+data['minimum_amount']+'" data-max_amt="'+data['maximum_amount']+'" data-trustfund="'+data['trustfund']+'" data-trustfund_mini_amt="'+data['trustfund_minimum_amount']+'" value="'+data['id']+'">'+data['name']+'</option>');
             });
           }
        });
      }); 


        
	$('#donation_category_id').change(function(){
	     var donation_category_id = $(this).val();
	     
	     $.ajax({
	       url: "<?php echo site_url('home/get_subcategory')?>/",
	       method: 'post',
	       data: {donation_category_id: donation_category_id},
	       dataType: 'json',
	       success: function(response){
	         // Remove options
	         $('#donation_sub_category_id').find('option:not(:first)').remove();
	        
	         $.each(response,function(index,data){
		         $('#donation_sub_category_id').append('<option data-min_amt="'+data['minimum_amount']+'" data-max_amt="'+data['maximum_amount']+'" data-trustfund="'+data['trustfund']+'" data-trustfund_mini_amt="'+data['trustfund_minimum_amount']+'" value="'+data['id']+'">'+data['name']+'</option>');
	         });
	       }
	    });
	  }); 


    
    $('#donation_sub_category_id').change(function() 
    {
        var donationamt = $('#donation_amount').val(); 
        $('#donation_trustfund').prop('checked', false);

        if(donationamt !== 0 && donationamt !== '')
        {

            enter_payment_amt();
        }
    });

    function enter_payment_amt()
    {       
            
            var pan_amount = $('#pan_amount').val();
            var sub_cat_min_amt = $('#donation_sub_category_id').find(':selected').data('min_amt');
            var sub_cat_max_amt = $('#donation_sub_category_id').find(':selected').data('max_amt');
            var sub_cat_trustfund = $('#donation_sub_category_id').find(':selected').data('trustfund');
            var sub_cat_trustfund_mini_amt = $('#donation_sub_category_id').find(':selected').data('trustfund_mini_amt');

            var payment_msg = ''; 
            $('#payable_amt_msg').html('');
            $('#donation_trustfund_ui').hide();
            $('#donation_user_pan_ui').hide();

            if($('#donation_category_id').val() !== '' || $('#donation_sub_category_id').val() !== '')
            {
                var donationamt = $('#donation_amount').val();
                if(donationamt !== 0 && donationamt !== '')
                {

                    if(sub_cat_min_amt >= donationamt)
                    {
                        payment_msg = "Minimum donation amount is "+sub_cat_min_amt+"/- for selected sub category.";
                        $('#payable_amt_msg').html(payment_msg);
                    } else if(sub_cat_max_amt <= donationamt) {
                        payment_msg = "You can donate Maximum "+sub_cat_max_amt+"/- amount in selected sub category";
                        $('#payable_amt_msg').html(payment_msg);
                    } else {
                        $('#payable_amt_msg').html('');
                    }


                    if(sub_cat_trustfund == 'yes')
                    {
                        if(sub_cat_trustfund_mini_amt <= donationamt)
                        {
                            $('#donation_trustfund_ui').show();
                        }else{
                            $('#donation_trustfund_ui').hide();
                            
                        }
                    }else{
                       
                        $('#donation_trustfund_ui').hide();
                    }
                    
                    
                    if(parseInt(donationamt) >= parseInt(pan_amount))
                    {
                        $('#donation_user_pan_ui').show();
                        $('#donation_user_pan').prop('required',true);
                    }else{
                        $('#donation_user_pan').prop('required',false);
                        $('#donation_user_pan_ui').hide();
                        $('#donation_user_pan').val('');
                    }
                   

                }    
            }
    }

    $(function() {
        $("#donation_amount").focus(function() {
            //$("#default_amount_ui").show();
        });
        $("#donation_amount").blur(function() 
        {
            setTimeout(function() {               
               $("#default_amount_ui").hide();
            },100000);
        });
    });

   


    function setDonorType(cmb) {
        if ($(cmb).val() == 'Other') {
            $('#txtDonorName_lable').html('Name of Company/Firm<span style="color:#f00;">*</span>');
            $('#dvMiddleName').hide();
            $('#dvLastName').hide();
        } else {
            $('#txtDonorName_lable').html('First Name of Donor<span style="color:#f00;">*</span>');
            $('#dvMiddleName').show();
            $('#dvLastName').show();
        }
    }


    
    /*(function ($) {
        $.fn.inputFilter = function (inputFilter) {
            return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function () {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
            });
        };
    }(jQuery));*/

    $(".CurrencyField").inputFilter(function (value) {
        return /^-?\d*[.,]?\d{0,2}$/.test(value);
    });
</script>

<script src="chrome-extension://hhojmcideegachlhfgfdhailpfhgknjm/web_accessible_resources/index.js"></script></body></html>