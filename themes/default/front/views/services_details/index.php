<!--Sliders Section-->
<div>
<div class="cover-image sptb-1 bg-background" data-image-src="../assets/images/banners/banner1.jpg" style="background: url(&quot;../assets/images/banners/banner1.jpg&quot;) center center;">
<div class="header-text1 mb-0">
<div class="container">
<div class="row">
<div class="col-xl-9 col-lg-12 col-md-12 d-block mx-auto">
<div class="search-background bg-transparent">
	<div class="form row no-gutters ">
		<div class="form-group  col-xl-4 col-lg-3 col-md-12 mb-0 bg-white">
			<input type="text" class="form-control input-lg br-tr-md-0 br-br-md-0" id="text4" placeholder="Job Title or Phrase or Keywords"> </div>
		<div class="form-group  col-xl-3 col-lg-3 col-md-12 mb-0 bg-white">
			<input type="text" class="form-control input-lg br-md-0" id="text5" placeholder="Enter Location"> <span><i class="fa fa-map-marker location-gps mr-1"></i> </span> </div>
		<div class="form-group col-xl-3 col-lg-3 col-md-12 select2-lg  mb-0 bg-white">
			<select class="form-control select2-show-search border-bottom-0 select2-hidden-accessible" data-placeholder="Select Category" data-select2-id="1" tabindex="-1" aria-hidden="true">
				<optgroup label="Categories">
					<option data-select2-id="3">Select</option> 
					<option value="1">Private</option>
					<option value="2">Software</option>
					<option value="3">Banking</option>
					<option value="4">Finaces</option>
					<option value="5">Corporate</option>
					<option value="6">Driver</option>
					<option value="7">Sales</option>
				</optgroup>
			</select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="2" style="width: 219px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-94tf-container"><span class="select2-selection__rendered" id="select2-94tf-container" role="textbox" aria-readonly="true" title="Select">Select</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span>
			</span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
		</div>
		<div class="col-xl-2 col-lg-3 col-md-12 mb-0"> <a href="ad-details.html#" class="btn btn-lg btn-block btn-primary br-tl-md-0 br-bl-md-0">Search Here</a> </div>
	</div>
</div>
</div>
</div>
</div>
</div>
<!-- /header-text -->
</div>
</div>
<!--/Sliders Section-->
<!--Breadcrumb-->
<div class="bg-white border-bottom">
<div class="container">
<div class="page-header">
<h4 class="page-title">Ad Details</h4>
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="ad-details.html#">Home</a></li>
<li class="breadcrumb-item"><a href="ad-details.html#">Pages</a></li>
<li class="breadcrumb-item active" aria-current="page">Ad Details</li>
</ol>
</div>
</div>
</div>
<!--/Breadcrumb-->
<!--Add listing-->
<section class="sptb">
<div class="container"> 
<div class="row">


<div class="col-xl-8 col-lg-8 col-md-12">    
<!--Add Description-->

<div class="card overflow-hidden">  
<div class="ribbon ribbon-top-right text-danger"><span class="bg-danger">featured</span></div>

<div class="card-body h-100">
<div class="item-det mb-4"> <a href="ad-details.html#" class="text-dark"> 
	<h3 class=""><?= $row->name;?></h3></a> 
	<ul class="d-flex">
		<li class="mr-5"><a href="ad-details.html#" class="icons"><i class="icon icon-briefcase text-muted mr-1"></i> <?= $row->business_name;?></a></li>
		<li class="mr-5"><a href="ad-details.html#" class="icons"><i class="icon icon-location-pin text-muted mr-1"></i> <?= $row->city;?></a></li>
		<li class="mr-5"><a href="ad-details.html#" class="icons"><i class="icon icon-calendar text-muted mr-1"></i> </a><button  onclick="send_enquiry()" >Add Inquiry</button></li>
		<li class="mr-5"><a href="ad-details.html#" class="icons"><i class="icon icon-eye text-muted mr-1"></i> 765</a></li>
		<li class="">
			<a href="ad-details.html#" class="icons"> <i class="fa fa-star text-warning"></i> <i class="fa fa-star text-warning"></i> <i class="fa fa-star text-warning"></i> <i class="fa fa-star text-warning"></i> <i class="fa fa-star-half-o text-warning mr-1"></i>4.5</a>
		</li>
	</ul>
</div>
<div class="product-slider">
	<div id="carousel" class="carousel slide" data-ride="carousel"> 
		<div class="arrow-ribbon2 bg-primary">&#x20B9;	<?= $row->price;?></div>
		<div class="carousel-inner">

			<div class="carousel-item"> ad-details_files
<img src="<?= base_url()?>themes/assets/images/services/<?= $row->image;?>" alt="img"> </div>

		</div>
		<a class="carousel-control-prev" href="ad-details.html#carousel" role="button" data-slide="prev"> <i class="fa fa-angle-left" aria-hidden="true"></i> </a>
		<a class="carousel-control-next" href="ad-details.html#carousel" role="button" data-slide="next"> <i class="fa fa-angle-right" aria-hidden="true"></i> </a>
	</div>
	<div class="clearfix">
		<div id="thumbcarousel" class="carousel slide" data-interval="false">
			<div class="carousel-inner">
				<div class="carousel-item active">
					<div data-target="#carousel" data-slide-to="0" class="thumb">ad-details_files
<img src="<?= base_url()?>themes/assets/images/services/<?= $row->image;?>" alt="img"></div>
					
					
				</div>
			</div>
			<a class="carousel-control-prev" href="ad-details.html#thumbcarousel" role="button" data-slide="prev"> <i class="fa fa-angle-left" aria-hidden="true"></i> </a>
			<a class="carousel-control-next" href="ad-details.html#thumbcarousel" role="button" data-slide="next"> <i class="fa fa-angle-right" aria-hidden="true"></i> </a>
		</div>
	</div>
</div>
</div>
</div>
<div class="card">
<div class="card-header">
<h3 class="card-title">Description</h3> </div>
<div class="card-body">
<div class="mb-4">
	<p><?= $row->description;?></p>
	
</div>
<h4 class="mb-4">Specifications</h4>
<div class="row">
	<div class="col-xl-6 col-md-12">
		<ul class="list-unstyled widget-spec mb-0">
			<li class=""> <i class="fa fa-bed text-muted w-5"></i> <?= $row->category; ?> </li>
			<li class=""> <i class="fa fa-bath text-muted w-5"></i> <?= $row->duration_of_service; ?> </li>
			<li class=""> <i class="fa fa-life-ring text-muted w-5"></i> <?= $row->business_type; ?> </li>
			
		</ul>
	</div>
	<div class="col-xl-6 col-md-12">
		<ul class="list-unstyled widget-spec mb-0">
			<li class=""> <i class="fa fa-lock text-muted w-5"></i> Security </li>
			<li class=""> <i class="fa fa-building-o text-muted w-5"></i> Lift </li>
			<li class=""> <i class="fa fa-check text-muted w-5"></i> Swimming fool </li>
			<li class=""> <i class="fa fa-gamepad text-muted w-5"></i> Play Area </li>
			<li class=""> <i class="fa fa-futbol-o text-muted w-5"></i> football Court </li>
			<li class="mb-0"> <i class="fa fa-trophy text-muted w-5"></i> Cricket Court </li>
		</ul>
	</div>
</div>
</div>
<div class="card-footer">
<div class="icons"> <a href="ad-details.html#" class="btn btn-info icons"><i class="icon icon-share mr-1"></i> Share Ad</a> <a href="ad-details.html#" class="btn btn-danger icons" data-toggle="modal" data-target="#report"><i class="icon icon-exclamation mr-1"></i> Report Abuse</a> <a href="ad-details.html#" class="btn btn-primary icons"><i class="icon icon-heart  mr-1"></i> 678</a> <a href="ad-details.html#" class="btn btn-secondary icons"><i class="icon icon-printer  mr-1"></i> Print</a> </div>
</div>
</div>
<!--/Add Description-->
<!--Comments-->
<div class="card">
<div class="card-header">
<h3 class="card-title">Rating And Reviews</h3> </div>
<div class="card-body">
<div class="row">
	<div class="col-md-12">
		<div class="mb-4">
			<p class="mb-2"> <span class="fs-14 ml-2"><i class="fa fa-star text-yellow mr-2"></i>5</span> </p>
			<div class="progress progress-md mb-4 h-4">
				<div class="progress-bar bg-success w-100">9,232</div>
			</div>
		</div>
		<div class="mb-4">
			<p class="mb-2"> <span class="fs-14 ml-2"><i class="fa fa-star text-yellow mr-2"></i>4</span> </p>
			<div class="progress progress-md mb-4 h-4">
				<div class="progress-bar bg-info w-80">8,125</div>
			</div>
		</div>
		<div class="mb-4">
			<p class="mb-2"> <span class="fs-14 ml-2"><i class="fa fa-star text-yellow mr-2"></i> 3</span> </p>
			<div class="progress progress-md mb-4 h-4">
				<div class="progress-bar bg-primary w-60">6,263</div>
			</div>
		</div>
		<div class="mb-4">
			<p class="mb-2"> <span class="fs-14 ml-2"><i class="fa fa-star text-yellow mr-2"></i> 2</span> </p>
			<div class="progress progress-md mb-4 h-4">
				<div class="progress-bar bg-secondary w-30">3,463</div>
			</div>
		</div>
		<div class="mb-5">
			<p class="mb-2"> <span class="fs-14 ml-2"><i class="fa fa-star text-yellow mr-2"></i> 1</span> </p>
			<div class="progress progress-md mb-4 h-4">
				<div class="progress-bar bg-orange w-20">1,456</div>
			</div>
		</div>
	</div>
</div>
</div>
<div class="card-body p-0">
<div class="media mt-0 p-5">
	<div class="d-flex mr-3">
		<a href="ad-details.html#"><img class="media-object brround" alt="64x64" src="./images/comman_images/1.jpg"> </a>
	</div>
	<div class="media-body">
		<h5 class="mt-0 mb-1 font-weight-semibold">Joanne Scott <span class="fs-14 ml-0" data-toggle="tooltip" data-placement="top" title="" data-original-title="verified"><i class="fa fa-check-circle-o text-success"></i></span> <span class="fs-14 ml-2"> 4.5 <i class="fa fa-star text-yellow"></i></span> </h5> <small class="text-muted"><i class="fa fa-calendar"></i> Dec 21st <i class=" ml-3 fa fa-clock-o"></i> 13.00 <i class=" ml-3 fa fa-map-marker"></i> Brezil</small>
		<p class="font-13  mb-2 mt-2"> Ut enim ad minim veniam, quis Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et nostrud exercitation ullamco laboris commodo consequat. </p><a href="ad-details.html#" class="mr-2"><span class="badge badge-primary">Helpful</span></a> <a href="ad-details.html" class="mr-2" data-toggle="modal" data-target="#Comment"><span class="">Comment</span></a> <a href="ad-details.html" class="mr-2" data-toggle="modal" data-target="#report"><span class="">Report</span></a>
		<div class="media mt-5">
			<div class="d-flex mr-3">
				<a href="ad-details.html#"> <img class="media-object brround" alt="64x64" src="./images/comman_images/2.jpg"> </a>
			</div>
			<div class="media-body">
				<h5 class="mt-0 mb-1 font-weight-semibold">Rose Slater <span class="fs-14 ml-0" data-toggle="tooltip" data-placement="top" title="" data-original-title="verified"><i class="fa fa-check-circle-o text-success"></i></span></h5> <small class="text-muted"><i class="fa fa-calendar"></i> Dec 22st <i class=" ml-3 fa fa-clock-o"></i> 6.00 <i class=" ml-3 fa fa-map-marker"></i> Brezil</small>
				<p class="font-13  mb-2 mt-2"> Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris commodo Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur consequat. </p><a href="ad-details.html" data-toggle="modal" data-target="#Comment"><span class="badge badge-default">Comment</span></a> </div>
		</div>
	</div>
</div>
<div class="media p-5 border-top mt-0">
	<div class="d-flex mr-3">
		<a href="ad-details.html#"> <img class="media-object brround" alt="64x64" src="./images/comman_images/3.jpg"> </a>
	</div>
	<div class="media-body">
		<h5 class="mt-0 mb-1 font-weight-semibold">Edward <span class="fs-14 ml-0" data-toggle="tooltip" data-placement="top" title="" data-original-title="verified"><i class="fa fa-check-circle-o text-success"></i></span> <span class="fs-14 ml-2"> 4 <i class="fa fa-star text-yellow"></i></span> </h5> <small class="text-muted"><i class="fa fa-calendar"></i> Dec 21st <i class=" ml-3 fa fa-clock-o"></i> 16.35 <i class=" ml-3 fa fa-map-marker"></i> UK</small>
		<p class="font-13  mb-2 mt-2"> Ut enim ad minim veniam, quis Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et nostrud exercitation ullamco laboris commodo consequat. </p><a href="ad-details.html#" class="mr-2"><span class="badge badge-primary">Helpful</span></a> <a href="ad-details.html" class="mr-2" data-toggle="modal" data-target="#Comment"><span class="">Comment</span></a> <a href="ad-details.html" class="mr-2" data-toggle="modal" data-target="#report"><span class="">Report</span></a> </div>
</div>
</div>
</div>

<!--/Comments-->

<div class="card mb-lg-0"> 
<div class="card-header">
<h3 class="card-title">Leave a reply</h3> </div>
<div class="card-body">
<div>

	<form  method="post" name="add_form" id="add_form">
	<input type="hidden"  id="item_id" name="item_id" value="<?= $row->id; ?>">
	<input type="text"  id="item_status" name="item_status" value="services"> 
	
	<div class="form-group">
		<input type="text" class="form-control" id="name" name="name" placeholder="Your Name"> </div>
	<div class="form-group">
		<input type="email" class="form-control" name="email" id="email" placeholder="Email Address"> </div>
	<div class="form-group">
		<textarea class="form-control" name="comment" id="comment" rows="6" placeholder="Comment"></textarea>
	<button type="button" onclick="add_row()">Send Reply</button> 
	</form>
</div>
</div>
</div>
</div>

<script>

function add_row()
{
var form = $('#add_form')[0]; 
var formData = new FormData(form);
$.ajax({
		 url: "<?php echo base_url()?>reviews/reviews_add", 
		 type: "POST",
	 data: formData,
	 async: false,
	 dataType: 'json',
	 contentType: false, 
	 processData: false, 
	 success: function(data) 
	 {
		$('#reviews_msg').html(''); 
		if (data["status"] == 1)
		{   
		   $("#add_form")[0].reset();
		   $('#reviews_ajex_table').html(data['table_ui'])
		   $('#reviews_msg').html(data['msg']).css('color','green');    
		   view_list();
		   setTimeout(function() {
					$('#reviews_msg').html(''); 
				},2000);
		}
		else (data['status'] == 2)
		{   
		   $('#reviews_msg').html(data['msg']).css('color','red');  
		}
			

	},
	error: function() { 
		alert('error');
	}
}); 
} 
</script>
<!--Right Side Content-->
<div class="col-xl-4 col-lg-4 col-md-12"> 
<div class="card">
<div class="card-header">
<h3 class="card-title">Posted By</h3> </div> 
<div class="card-body  item-user">
<div class="profile-pic mb-0"> ad-details_files
<img src="<?= base_url()?>themes/assets/images/logo/<?= $row->logo;?>" class="brround avatar-xxl" alt="img">

	<div class=""> <a href="userprofile.html" class="text-dark"><h4 class="mt-3 mb-1 font-weight-semibold"><?= $row->business_name;?></h4></a> <span class="text-muted"><?= $row->business_type;?></span>
		<h6 class="mt-2 mb-0"><a href="ad-details.html#" class="btn btn-primary btn-sm">See All Ads</a></h6> </div>
</div>
</div>
<div class="card-body item-user">
<h4 class="mb-4">Contact Info</h4>
<div>
	<h6><span class="font-weight-semibold"><i class="fa fa-envelope mr-2 mb-2"></i></span><a href="ad-details.html#" class="text-body"> <?= $row->email_id;?></a></h6>
	<h6><span class="font-weight-semibold"><i class="fa fa-phone mr-2  mb-2"></i></span><a href="ad-details.html#" class="text-primary"> <?= $row->mobile;?></a></h6>
	<h6><span class="font-weight-semibold"><i class="fa fa-link mr-2 "></i></span><a href="ad-details.html#" class="text-primary">http://spruko.com/</a></h6> </div>
<div class=" item-user-icons mt-4"> <a href="ad-details.html#" class="facebook-bg mt-0"><i class="fa fa-facebook"></i></a> <a href="ad-details.html#" class="twitter-bg"><i class="fa fa-twitter"></i></a> <a href="ad-details.html#" class="google-bg"><i class="fa fa-google"></i></a> <a href="ad-details.html#" class="dribbble-bg"><i class="fa fa-dribbble"></i></a> </div>
</div>
<div class="card-footer">
<div class="text-left"> <a href="ad-details.html#" class="btn  btn-info"><i class="fa fa-envelope"></i> Chat</a> <a href="ad-details.html#" class="btn btn-primary" data-toggle="modal" data-target="#contact"><i class="fa fa-user"></i> Contact Me</a> </div>
</div>
</div>

<div class="card">
<div class="card-header">
<h3 class="card-title">Shares</h3> </div>
<div class="card-body product-filter-desc">
<div class="product-filter-icons text-center"> <a href="ad-details.html#" class="facebook-bg"><i class="fa fa-facebook"></i></a> <a href="ad-details.html#" class="twitter-bg"><i class="fa fa-twitter"></i></a> <a href="ad-details.html#" class="google-bg"><i class="fa fa-google"></i></a> <a href="ad-details.html#" class="dribbble-bg"><i class="fa fa-dribbble"></i></a> <a href="ad-details.html#" class="pinterest-bg"><i class="fa fa-pinterest"></i></a> </div>
</div>
</div>
<div class="card">
<div class="card-header">
<h3 class="card-title">Map location</h3> </div>
<div class="card-body">
<div class="map-header">
	
</div>
</div>
</div>


<div class="card">
<div class="card-header">
<h3 class="card-title">Latest Services</h3> </div>
<div class="card-body pb-3">
<ul class="vertical-scroll" style="overflow-y: hidden; height: 264px;">
	<li style="" class="news-item">
		<table>
			<tbody>
				<tr>
					<td>ad-details_files
<img src="<?= base_url()?> themes/assests/images/services/<?= $row->image;?>" alt="img" class="w-8 border"></td>
					<td>
						<h5 class="mb-1 ">Best New Model Watch</h5><a href="ad-details.html#" class="btn-link">View Details</a><span class="float-right font-weight-bold">&#x20B9;	<?= $row->price;?></span></td>
				</tr>
			</tbody>
		</table>
	</li>
	
	
	<li style="display: none;" class="news-item">
		<table>
			<tbody>
				<tr>
					<td>ad-details_files
<img src="./images/comman_images/7.png" alt="img" class="w-8 border"></td>
					<td>
						<h5 class="mb-1 ">Trending New Model Shoes</h5><a href="ad-details.html#" class="btn-link">View Details</a><span class="float-right font-weight-bold">$17</span></td>
				</tr>
			</tbody>
		</table>
	</li>
</ul>
</div>
</div>

</div>
<!--Right Side Content-->
</div>
</div>
</section>
<!--/Add listing-->
<!-- Newsletter-->
<section class="sptb bg-white border-top">
<div class="container">
<div class="row">
<div class="col-lg-7 col-xl-6 col-md-12">
<div class="sub-newsletter">
<h3 class="mb-2"><i class="fa fa-paper-plane-o mr-2"></i> Subscribe To Our Newsletter</h3>
<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
</div>
</div>
<div class="col-lg-5 col-xl-6 col-md-12">
<div class="input-group sub-input mt-1">
<input type="text" class="form-control input-lg " placeholder="Enter your Email">
<div class="input-group-append ">
<button type="button" class="btn btn-primary btn-lg br-tr-3  br-br-3"> Subscribe </button>
</div>
</div>
</div>
</div>
</div>
</section>
<!--/Newsletter-->

<div id="inquirymodal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
     
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>

      <div class="modal-body">
        
        <form name="add_service_form" id="add_service_form" method="post">
            
            <div class="row"> 
               <div class="col-md-8">
               <div class="form-group">
                  
                     <label for="name">Name<span class="text-danger">*</span></label>
                     <input type="text" name="name" id="name" required="required" placeholder="Enter Your Name">
                     </div>
                  </div>
 
                     <div class="col-md-8"> 
                        <div class="form-group">
                        <label for="mobile">Mobile Number<span class="text-danger"></span>*</label>
                     <input type="number" name="mobile" id="mobile" required="required" placeholder="Enter Your Contact number">
                     <input type="hidden"  id="item_id" name="item_id" value="<?= $row->id; ?>">
                     <input type="hidden"  id="item_status" name="item_status" value="service">
                     </div>
                     </div> 
                     <div class="col-sm-12"> 
                  <div id="inquiry_msg"></div>
                 </div>  

                     <button type="button" class="btn btn-primary float-right" onclick="add_row()">Submit</button>              

                  </div>  

         </form>

      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>

    </div>

 </div>
</div>
<script type="text/javascript">
	function send_enquiry() {
      
      $('#inquirymodal').modal('show');
      
   }

   function add_row()
{
var form = $('#add_service_form')[0]; 
var formData = new FormData(form);
$.ajax({
   url: "<?php echo base_url()?>inquiry/inquiry_add", 
   type: "POST",
     data: formData,
  async: false,
     dataType: 'json',
     contentType: false, 
     processData: false, 
  success: function(data) 
  {
   $('#inquiry_msg').html(''); 
  if (data["status"] == 1)
  { 
     $("#add_service_form")[0].reset();
     
     $('#inquiry_msg').html(data['msg']).css('color','green'); 
     view_list();
     setTimeout(function() {
                    $('#inquiry_msg').html(''); 
                },2000);
  }
  else if (data['status'] == 2)
  { 
     $('#inquiry_msg').html(data['msg']).css('color','red'); 
   }
   else
   {

  }

 },
 error: function() { 
  alert('error');
 }
}); 
} 
</script>