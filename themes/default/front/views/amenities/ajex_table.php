<?php

$count = 1; foreach ($amenities as $amenity) : ?> 
<tr>
	<td><?= $count++;?></td>
	
	<td><?= @$amenity->name; ?></td>
	
	
	<td>
		<a href="javascript:void(0)" class="badge badge-<?php echo ($amenity->status == 'active') ? 'primary' : 'danger'?>"><?= ucfirst($amenity->status); ?></a> 
	</td>
	<td> 

		<a href="javascript:void(0)" onclick="get_record_edit('<?= $amenity->id ?>')" class="badge badge-primary"><i class="icon icon-pencil"></i></span></a>

		<a href="javascript:void(0)" onclick="delete_record('<?= $amenity->id ?>')" class="badge badge-danger"><i class="icon icon-trash"></i></span></a>
	</td>

</tr>
<?php endforeach; ?>


