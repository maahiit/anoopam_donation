
<section>
<div class="bannerimg cover-image bg-background3" data-image-src="../assets/images/banners/banner2.jpg" style="background: url(&quot;../assets/images/banners/banner2.jpg&quot;) center center;">
	<div class="header-text mb-0">
		<div class="container">
			<div class="text-center text-white">
				<h1 class="">Login</h1>
				<ol class="breadcrumb text-center">
					<li class="breadcrumb-item"><a href="<?= base_url('home'); ?>">Home</a></li>
					<li class="breadcrumb-item active text-white" aria-current="page">Login</li>
				</ol>
			</div>
		</div>
	</div>
</div> 
</section>
<!--/Sliders Section-->
<!--Login-Section-->
<section class="sptb">
<div class="container customerpage">
	<div class="row">
		<div class="single-page">
			<div class="col-lg-5 col-xl-4 col-md-6 d-block mx-auto">
				<div class="wrapper wrapper2">
					<form id="login_form" name="login_form" class="card-body" tabindex="500" method="POST">  
						<h3>Login</h3>
						
						<div class="mail">
							<input type="text" name="username" id="username">
							<label>Email / Mobile</label>
						</div>
						
						<div class="passwd"> 
							<input type="password" name="password" id="password">
							<label>Password</label>
						</div>

						<div id="login_msg" class="spacetb10"></div>

						<button class="btn btn-pink btn-block text-uppercase waves-effect waves-light" type="button" onclick="auth_login()"> Login </button>

						<p class="mb-2 spacetb10"><a href="#">Forgot Password</a></p>
						<p class="text-dark mb-0">Don't have account?<a href="<?= base_url('register')?>" class="text-primary ml-1">Sign UP</a></p>

					</form>
					<hr class="divider">
					<div class="card-body">
						<div class="text-center">
							<div class="btn-group">
								<a href="https://www.facebook.com/" class="btn btn-icon mr-2 brround"> <span class="fa fa-facebook"></span> </a>
							</div>
							<div class="btn-group">
								<a href="https://www.google.com/gmail/" class="btn  mr-2 btn-icon brround"> <span class="fa fa-google"></span> </a>
							</div>
							<div class="btn-group">
								<a href="https://twitter.com/" class="btn  btn-icon brround"> <span class="fa fa-twitter"></span> </a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</section>
<!--/Login-Section-->
<!-- Newsletter-->
<section class="sptb bg-white border-top">
<div class="container">
	<div class="row">
		<div class="col-lg-7 col-xl-6 col-md-12">
			<div class="sub-newsletter">
				<h3 class="mb-2"><i class="fa fa-paper-plane-o mr-2"></i> Subscribe To Our Newsletter</h3>
				<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
			</div>
		</div>
		<div class="col-lg-5 col-xl-6 col-md-12">
			<div class="input-group sub-input mt-1">
				<input type="text" class="form-control input-lg " placeholder="Enter your Email">
				<div class="input-group-append ">
					<button type="button" class="btn btn-primary btn-lg br-tr-3  br-br-3"> Subscribe </button>
				</div>
			</div>
		</div>
	</div>
</div>
</section>
<!--/Newsletter-->

<script type="text/javascript">

        function auth_login(){	

		var form = $('#login_form')[0]; 
        var formData = new FormData(form);	

			$.ajax({
				url: "<?php echo base_url()?>login/login_ajex",
				type: "POST",
				data: formData,				
				cache: false,
        		contentType: false,
        		processData: false,
        		dataType: 'json',
				success: function(response) {				
					
					$('#login_msg').html('');	
					if (response["status"] == 1)
					{	
					    $('#login_msg').html(response['msg']).css('color','green');	
					    
 						setTimeout(function() { 
                          window.location.href = '<?= base_url('/mydask');?>';
                        },1500);
					    

					}
		   			else
		   			{
					 	$('#login_msg').html(response['msg']).css('color','red');	
					}
				},
				error: function() {
					alert('error');
				}
			});
		}

    </script>