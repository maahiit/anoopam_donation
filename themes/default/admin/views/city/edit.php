<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if(canaccess("categories","edit_access") != 'true'){ echo "<script>window.location.href ='".site_url()."admin'</script>";}?>


<div class="wrapper">
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-color panel-inverse">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?= $page_title ?></h3>
                    </div>
                    <form name="category_edit" id="category_edit" method="post"  action='<?= base_url("admin/categories/edit/{$categories->id}"); ?>' enctype="multipart/form-data">
            
             <div class="panel-body">

                <div class="row">

                    <input type="hidden" name="id" value="<?= $city->id; ?>">

                      <div class="col-md-6">
                       <div class="form-group">
                        <label for="name">State<span class="text-danger">*</span></label>
                           <select  name="state" id="state" class="form-control" data-style="btn-white"  required="required">
                                            <option value="">State</option>

                                              <?php foreach ($state as $key => $value): ?>
                            <option 
                        <?php if($value->id == $city->state) { echo "selected"; } ?>
                         value="<?= $value->id ?>"><?= $value->state ?> </option>
                            <?php endforeach ?>

                            </select> 
                        </div>
                      </div>
                    
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="name">City<span class="text-danger">*</span></label>
                              <input type="text" required="required" name="city" id="city" class="form-control" placeholder="City" value="<?= $city->city ?>">
                          </div>
                      </div>
                    
                  </div> 
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="image">Image<span class="text-danger">*</span></label>
                        <input type="file" name="image" id="image" class="filestyle" data-size="sm" onchange="ImagePreview(this,'preview_image');">
                        <?php if (!empty($city->image)) { ?>
                      <img id="preview_image" src="<?= base_url() ?>themes/assets/images/city/<?= $city->image; ?>" style="width: 100%;height: 150px;margin-top: 5px" />    
                      <?php }else{ ?>
                      <img id="preview_image" src="" style="width: 100%;height: 150px;display: none;margin-top: 5px" />
                      <?php } ?> 
                        
                      </div>
                      
                    </div>
                    
                  </div>
                  </div>
                    <div class="panel-footer">
                        
                        <button type="submit" id="save" class="btn btn-success waves-effect waves-light pull-right">Save</button>
                        <span class="pull-right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <a href="<?= base_url('admin/city'); ?>"><button type="button" class="btn btn-inverse waves-effect waves-light pull-right" data-dismiss="modal" aria-hidden="true">Close</button></a>
                        
                        <div class="clearfix"></div>
                    </div>
                </form>
                </div>
            </div>
        </div>

        <script>
            function readURL(input,id) {

                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $(id).removeClass('hide').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }




function ImagePreview(input,image_preview) 
    {
      if (input.files && input.files[0]) 
      {
        var reader = new FileReader();
        reader.onload = function(e) {
          $('#'+image_preview).removeClass('hide').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
      }
    }

    $(document).on('click','#remove_field',function() {
             $(this).closest("#row_remove").remove();
     });

     $('#state_id').change(function(){
     var state_id = $(this).val();

     // AJAX request
       $.ajax({
         url: "<?php echo site_url('admin/city/GetAllDistrict')?>/",
         method: 'post',
         data: {state_id: state_id},
         dataType: 'json',
         success: function(response){
           // Remove options
           $('#district_id').find('option').not(':first').remove();
           // Add options
           $.each(response,function(index,data){
             $('#district_id').append('<option value="'+data['id']+'">'+data['district']+'</option>');
           });
         }
      });
    });













        </script>