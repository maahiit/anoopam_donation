<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if(canaccess("smtp_email","edit_access") != 'true'){ echo "<script>window.location.href ='".site_url()."admin'</script>";}?>

<style type="text/css">
    label {
        display: inline-block;
        max-width: 100%;
        margin-bottom: 5px;
        font-weight: 700;
        margin-right: 19px;
    }
</style>
<div class="wrapper">
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-color panel-inverse">

                    <div class="panel-heading">
                        <h3 class="panel-title"><?= $page_title ?></h3>
                    </div>

                    <form name="customer_edit" id="customer_edit" method="post"  action='<?= base_url("admin/smtp_email/edit/{$row->id}"); ?>' enctype="multipart/form-data">

                        <div class="panel-body">
            
                            <div class="row">

                                <input type="hidden" name="id" value="<?= $row->id; ?>">
        
                                <div class="col-md-4">                                    
                                    <div class="form-group">
                                        <label for="name">Name<span class="text-danger">*</span></label>
                                        <input type="text" required="required" name="name" id="name" class="form-control" placeholder="Name" value="<?=  $row->name; ?>">
                                    </div>
                                </div>

                                <div class="col-md-4">                                    
                                    <div class="form-group">
                                        <label for="host">Host<span class="text-danger">*</span></label>
                                        <input type="text" required="required" name="host" id="host" class="form-control" placeholder="Host" value="<?=  $row->host; ?>">
                                    </div>
                                </div>

                                <div class="col-md-4">                                    
                                    <div class="form-group">
                                        <label for="port">Port<span class="text-danger">*</span></label>
                                        <input type="text" required="required" name="port" id="port" class="form-control" placeholder="Port" value="<?=  $row->port; ?>">
                                    </div>
                                </div>

                            </div>

                            <div class="row">                                
        
                                <div class="col-md-4">                                    
                                    <div class="form-group">
                                        <label for="username">Username<span class="text-danger">*</span></label>
                                        <input type="text" required="required" name="username" id="username" class="form-control" placeholder="Username" value="<?=  $row->username; ?>">
                                    </div>
                                </div>

                                <div class="col-md-4">                                    
                                    <div class="form-group">
                                        <label for="password">Password<span class="text-danger">*</span></label>
                                        <input type="password" required="required" name="password" id="password" class="form-control" placeholder="Password" value="<?=  $row->password; ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="ssl_tls">SSL / TLS<span class="text-danger">*</span></label>

                                    <div class="form-group">
                                        <input <?php if ($row->ssl_tls  == 'ssl')  { echo "checked"; } ?> type="radio" name="ssl_tls " value="ssl" id="ssl">
                                        <label for="ssl"><?= lang('SSL'); ?></label>

                                        <input <?php if ($row->ssl_tls  == 'tls')  { echo "checked"; } ?>
                                         type="radio" name="ssl_tls " value="tls" id="tls">
                                        <label for="tls "><?= lang('TLS'); ?></label>
                                    </div>
                                </div>

                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                <label for="auth">SMTP Auth<span class="text-danger">*</span></label>
                                    <div class="form-group" >
                                        <input <?php echo ($row->auth  == 'true') ? 'checked="checked"' : ''?> type="radio" name="auth " value="true" id="auth_true ">
                                        <label for="auth_true "><?= lang('True'); ?></label>

                                        <input <?php echo ($row->auth  == 'false') ? 'checked="checked"' : ''?> type="radio" name="auth " value="false" id="auth_false ">
                                        <label for="auth_false "><?= lang('False'); ?></label>
                                    </div>
                                </div>
                                </div>

                                <div class="col-md-4">                
                                    <div class="form-group checkbox checkbox-danger" style="margin-top: 30px;">
                                        <input <?php echo ($row->status == 'inactive') ? 'checked="checked"' : ''?>  type="checkbox" name="status" value="inactive" id="status">
                                        <label for="status"><?= lang('inactive'); ?></label>
                                    </div>
                                </div>

                               

                            </div>

                             

                        </div>    


        
                    <div class="panel-footer">
                        <button type="submit" id="save" class="btn btn-success waves-effect waves-light pull-right">Save</button>
                        <span class="pull-right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <a href="<?= base_url('admin/smtp_email'); ?>"><button type="button" class="btn btn-inverse waves-effect waves-light pull-right" data-dismiss="modal" aria-hidden="true">Close</button></a>
                        
                        
                        <div class="clearfix"></div>
                    </div>

                </form>
            </div>
        </div>

                </div>
            </div>
        </div>

        <script>
         
$(document).on('click','#remove_field',function() {
             $(this).closest("#row_remove").remove();
     });


function ImagePreview(input,image_preview) 
    {
      if (input.files && input.files[0]) 
      {
        var reader = new FileReader();
        reader.onload = function(e) {
          $('#'+image_preview).attr('src', e.target.result);
          $('#'+image_preview).show();

      }
      reader.readAsDataURL(input.files[0]);
      }
    }





        </script>