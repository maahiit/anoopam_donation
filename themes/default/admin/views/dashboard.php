       <div class="wrapper">
            <div class="container">
<style type="text/css">
    .alert-success {
    background-color: rgba(95, 190, 170, 0.3);
    border-color: rgba(95, 190, 170, 0.4);
    color: #06362c;
}
</style>

                <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="btn-group pull-right m-t-15">
                                    <ul class="dropdown-menu drop-menu-right" role="menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>
                                </div>

                                <h4 class="page-title">Dashboard</h4>
                                <p class="text-muted page-title-alt">Welcome to <?= $Settings->sitename; ?> <b><?= $this->session->userdata('loginname'); ?></b> !</p>
                            </div>
                        </div>
                        <div class="row">

                                <div class="col-lg-3">
                                <div class="card-box">
                                    <div class="bar-widget">
                                        <div class="table-box">
                                            <div class="table-detail">
                                                <div class="iconbox bg-primary">
                                                    <i class="fa fa-inr"></i>
                                                </div>
                                            </div>

                                            <div class="table-detail">
                                               <h4 class="m-t-0 m-b-5"><b><a href="<?= base_url('admin/transition'); ?>" ><?= $total_donation; ?></a></b></h4>
                                               <p class="text-muted m-b-0 m-t-0">Total Donation</p>
                                            </div>
                                            <div class="table-detail text-right">
                                                <span data-plugin="peity-donut" data-colors="#34d3eb,#ebeff2" data-width="50" data-height="45"><?= $total_donation; ?>/50000</span>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-lg-3">
                                <div class="card-box">
                                    <div class="bar-widget">
                                        <div class="table-box">
                                            <div class="table-detail">
                                                <div class="iconbox bg-primary">
                                                    <i class="fa fa-snowflake-o"></i>
                                                </div>
                                            </div>

                                            <div class="table-detail">
                                               <h4 class="m-t-0 m-b-5"><b><a href="<?= base_url('admin/categories'); ?>" ><?= $category; ?></a></b></h4>
                                               <p class="text-muted m-b-0 m-t-0">Categories</p>
                                            </div>
                                            <div class="table-detail text-right">
                                                <span data-plugin="peity-donut" data-colors="#34d3eb,#ebeff2" data-width="50" data-height="45"><?= $category; ?>/100</span>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="card-box">
                                    <div class="bar-widget">
                                        <div class="table-box">
                                            <div class="table-detail">
                                                <div class="iconbox bg-primary">
                                                    <i class="fa fa-diamond"></i>
                                                </div>
                                            </div>

                                            <div class="table-detail">
                                               <h4 class="m-t-0 m-b-5"><b><a href="<?= base_url('admin/subcategories'); ?>" ><?= $subcategory; ?></a></b></h4>
                                               <p class="text-muted m-b-0 m-t-0">Sub Categories</p>
                                            </div>
                                            <div class="table-detail text-right">
                                                <span data-plugin="peity-donut" data-colors="#34d3eb,#ebeff2" data-width="50" data-height="45"><?= $subcategory; ?>/100</span>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="card-box">
                                    <div class="bar-widget">
                                        <div class="table-box">
                                            <div class="table-detail">
                                                <div class="iconbox bg-primary">
                                                    <i class="fa fa-cc-amex"></i>
                                                </div>
                                            </div>

                                            <div class="table-detail">
                                               <h4 class="m-t-0 m-b-5"><b><a href="<?= base_url('admin/payment_gateway'); ?>" ><?= $payment_getway; ?></a></b></h4>
                                               <p class="text-muted m-b-0 m-t-0">Payment Gateway</p>
                                            </div>
                                            <div class="table-detail text-right">
                                                <span data-plugin="peity-donut" data-colors="#34d3eb,#ebeff2" data-width="50" data-height="45"><?= $payment_getway; ?>/100</span>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="card-box">
                                    <div class="bar-widget">
                                        <div class="table-box">
                                            <div class="table-detail">
                                                <div class="iconbox bg-primary">
                                                    <i class="fa fa-handshake-o"></i>
                                                </div>
                                            </div>

                                            <div class="table-detail">
                                               <h4 class="m-t-0 m-b-5"><b><a href="<?= base_url('admin/contributor'); ?>" ><?= $contributor; ?></a></b></h4>
                                               <p class="text-muted m-b-0 m-t-0">Contributor</p>
                                            </div>
                                            <div class="table-detail text-right">
                                                <span data-plugin="peity-donut" data-colors="#34d3eb,#ebeff2" data-width="50" data-height="45"><?= $contributor; ?>/100</span>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                           
                            

                            
                        </div>


<script type="text/javascript">
    function Getdata(id) {
        $('.DisplayDataTR').hide();    
        var CountProject = 0;
        var Sr_Index = 1;

        $('.DisplayData').each(function(){
                var projectId = $(this).attr('projectId');
                var RandIndexNo = $(this).attr('rand_index');
                
                if(projectId == id)
                {       
                        $('#DynamicIndex_'+RandIndexNo).html(Sr_Index);    
                        CountProject++;
                        Sr_Index++;
                }

        });

        if(CountProject == 0)
        {
            $('.DisplayDataTR').show();    
            $('#DisplayDataMsg').html('No Inspection Available');    
        }
        
        $('.DisplayData').hide();
        $('.DisplayDataclass_'+id).show();
        
        $('.basic1').modal('show');
    }
</script>