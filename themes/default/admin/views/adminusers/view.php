<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if(canaccess("adminusers","view_access") != 'true'){ echo "<script>window.location.href ='".site_url()."admin'</script>";}?>
<style type="text/css">
	.search {
	    width: 17%;
	}
</style>
<div class="wrapper">
    <div class="container">

		<div class="row">
	        <div class="col-sm-12">
	        	
	            <div class="pull-right ">
	            
	            	<button type="button" class="btn btn-info" status="none" id="advanced_search_btn"><span class="glyphicon glyphicon-search "></span></button>
	            	
	            	<?php if(canaccess("adminusers","create_access")){ ?> 
	            	<button type="button" id="add" data-toggle="modal" data-target=".bs-example-modal-lg" class="btn btn-purple waves-effect waves-light">Add New</button>
	           		<?php } ?>
	           		
	           		 <a href="<?= base_url("admin/adminusers"); ?>/unset_session_value" id="unset_button"><button type="button" class="btn btn-primary m-r-5" status="none"><span class="glyphicon glyphicon-refresh"></span></button></a> 
	            </div>
	            


	            <h4 class="page-title">Admin User</h4>
	        </div>
	    </div>
	    <br>
	    <div class="row" id="advanced_search_div" 

	    <?php if(empty($_REQUEST)) { ?>

	    	<?php if(!empty($this->session->userdata('state_serach_data'))) { ?>
	    		style="display: block;"
	    	<?php }else{ ?>
				style="display: none;"
	    	<?php  } ?>
	    
		<?php } ?>
	    >
		   <div class="">
		      <div class="panel panel-default">
		         <div class="panel-body">
		            <div class="fixed-table-toolbar">
		               <form name="searchfrom" id="searchfrom" method="post"  action='<?= base_url("admin/adminusers"); ?>' enctype="multipart/form-data">
		                  <div class="columns btn-group pull-right margin">
		                     <div class="searchdatetitle">&nbsp;</div>
		                     <button type="submit" name="filter" id="filter" class="btn btn-success" value="filter">Search</button>
		                  </div>
		                  
		                  <div class="pull-right search margin m-r-15">
		                     <div class="searchdatetitle">Status</div>
		                     <select id="admin_s_status" name="admin_s_status" class="form-control">
		                        <option value="" >Select Status</option>
		                        <option 
		                        <?php 
	              					if(isset($_REQUEST['admin_s_status']))
	              					{
	              					if($_REQUEST['admin_s_status'] == 'active')
	              					{
	              						echo "selected"; 
	              					}	
	              					}
	              					?>
	              					
	              					<?php 
	              					if($this->session->userdata('admin_s_status')) 
	              					{
										if($this->session->userdata('admin_s_status') == 'active')
		              					{
		              						echo "selected"; 
		              					}				                            	
		                            }
                      				?>
		                         value="active">Active</option>
		                        <option 
		                        <?php 
	              					if(isset($_REQUEST['admin_s_status']))
	              					{
	              					if($_REQUEST['admin_s_status'] == 'inactive')
	              					{
	              						echo "selected"; 
	              					}	
	              					}
	              					?>
	              					
	              					<?php 
	              					if($this->session->userdata('admin_s_status')) 
	              					{
										if($this->session->userdata('admin_s_status') == 'inactive')
		              					{
		              						echo "selected"; 
		              					}				                            	
		                            }
                      				?>
                      				value="inactive">Inactive</option>
		                     </select>
		                  </div>
		                  <div class="pull-right search margin m-r-15">
		                     <div class="searchdatetitle">Mobile</div>
		                     <input class="form-control" type="text" placeholder="Any Text.." name="admin_s_mobile" id="admin_s_mobile" value="<?php if(isset($_REQUEST['admin_s_mobile'])) 
		                     			{ 
		                     				echo $_REQUEST['admin_s_mobile']; 
		                     			} 
		                     			else 
		                     			{ 
		                     				if($this->session->userdata('admin_s_mobile')) 
		                     				{ 
		                     					echo $this->session->userdata('admin_s_mobile'); 
		                     				} 
		                     			}
             					?>">
		                  </div>
		                  <div class="pull-right search margin m-r-15">
		                     <div class="searchdatetitle">Email</div>
		                     <input class="form-control" type="text" placeholder="Any Text.." name="admin_s_email" id="admin_s_email" value="<?php if(isset($_REQUEST['admin_s_email'])) 
		                     			{ 
		                     				echo $_REQUEST['admin_s_email']; 
		                     			} 
		                     			else 
		                     			{ 
		                     				if($this->session->userdata('admin_s_email')) 
		                     				{ 
		                     					echo $this->session->userdata('admin_s_email'); 
		                     				} 
		                     			}
             					?>">
		                  </div>
		                  
		                  <div class="pull-right search margin m-r-15">
		                     <div class="searchdatetitle">Designation</div>
		                     <select id="admin_s_designation" name="admin_s_designation" class="form-control">
		                        <option value="">Select Designation</option>
		                        <!-- <option value="1" <?php echo ($de == '1') ? 'selected="selected"' : ''; ?>>Admin</option> -->
		                        <option
		                        <?php 
	              					if(isset($_REQUEST['admin_s_designation']))
	              					{
	              					if($_REQUEST['admin_s_designation'] == '2')
	              					{
	              						echo "selected"; 
	              					}	
	              					}
	              					?>
	              					
	              					<?php 
	              					if($this->session->userdata('admin_s_designation')) 
	              					{
										if($this->session->userdata('admin_s_designation') == '2')
		              					{
		              						echo "selected"; 
		              					}				                            	
		                            }
                      				?>
		                        value="2">Inspector</option>
		                        <option 
		                        <?php 
	              					if(isset($_REQUEST['admin_s_designation']))
	              					{
	              					if($_REQUEST['admin_s_designation'] == '3')
	              					{
	              						echo "selected"; 
	              					}	
	              					}
	              					?>
	              					
	              					<?php 
	              					if($this->session->userdata('admin_s_designation')) 
	              					{
										if($this->session->userdata('admin_s_designation') == '3')
		              					{
		              						echo "selected"; 
		              					}				                            	
		                            }
                      				?>
		                        value="3">Manager</option>
		                        <option 
		                        <?php 
	              					if(isset($_REQUEST['admin_s_designation']))
	              					{
	              					if($_REQUEST['admin_s_designation'] == '4')
	              					{
	              						echo "selected"; 
	              					}	
	              					}
	              					?>
	              					
	              					<?php 
	              					if($this->session->userdata('admin_s_designation')) 
	              					{
										if($this->session->userdata('admin_s_designation') == '4')
		              					{
		              						echo "selected"; 
		              					}				                            	
		                            }
                      				?>
		                        value="4">Accountant</option>
		                     </select>
		                  </div>
		                  <div class="pull-right search margin m-r-15">
		                     <div class="searchdatetitle">Name</div>
		                     <input class="form-control" type="text" placeholder="Any Text.." name="admin_s_name" id="admin_s_name" value="<?php if(isset($_REQUEST['admin_s_name'])) 
		                     			{ 
		                     				echo $_REQUEST['admin_s_name']; 
		                     			} 
		                     			else 
		                     			{ 
		                     				if($this->session->userdata('admin_s_name')) 
		                     				{ 
		                     					echo $this->session->userdata('admin_s_name'); 
		                     				} 
		                     			}
             					?>">
		                  </div>
		                  
		               </form>
		            </div>
		         </div>
		      </div>
		   </div>
		</div>
	    
	    <?php
	    	// print_r($branchs);
	    ?>
      <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12 table-responsive card-box">
                <table class="table table-hover table-condensed">
                	<thead>
                		<th>#</th>
                		<th><?= lang('name'); ?></th>
                		<th>Email</th>
                		<th>Mobile</th>
                		<th><?= lang('status'); ?></th>
                		  <?php if(canaccess("employee","edit_access") == 'true' || canaccess("employee","delete_access") == 'true' ){ ?>	<th><?= lang('actions'); ?></th> <?php } ?>
                	</thead>
                	<tbody>
                		<?php $count = 1;if($this->uri->segment(4) != ""){ $count = $this->uri->segment(4) + 1; } foreach ($users as $sam) : ?>
	        					<tr>
	        						<td><?=$count++;?></td>
									<td><?= $sam->first_name .' ' . $sam->last_name; ?></td>
									
									<td><?= $sam->email_id; ?></td>
									<td><?= $sam->mobile; ?></td>
									<td><span class="label label-<?php echo ($sam->status == 'active') ? 'success' : 'danger'?>"><?= ucfirst($sam->status); ?></span></td>
									<td>
									
									<?php if(canaccess("adminusers","edit_access")){ ?>	<a href='<?= base_url("admin/adminusers/edit/{$sam->id}"); ?>'><button class="btn btn-icon btn-xs waves-effect waves-light btn-purple" data-toggle="tooltip" data-placement="left" title="" data-original-title="<?= lang('edit'); ?>"> <i class="fa fa-edit"></i> </button></a> <?php } ?>



									<?php if(canaccess("adminusers","delete_access")){ ?>		<a class="sa-params" href='javascript:void(0)' id="<?= $sam->id ?>"><button class="btn btn-icon btn-xs waves-effect waves-light btn-danger" data-toggle="tooltip" data-placement="right" title="" data-original-title="<?= lang('delete'); ?>"> <i class="fa fa-remove"></i> </button></a>
									<?php } ?>	
									</td>

								</tr>
						<?php endforeach; ?>
                	</tbody>
                </table>
            </div>
        </div>

        <div class="row">
        	<div class="col-md-12">
        		<div class="pull-right">
        			<?php echo $this->pagination->create_links(); ?>
        		</div>
        	</div>
        </div>

 <!--  Modal content for the above example -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        	<form name="adminuser_add" id="adminuser_add" method="post" enctype="multipart/form-data">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	                <h4 class="modal-title" id="myLargeModalLabel"><?= lang('adduser'); ?></h4>
	            </div>
	            <div class="modal-body">
	              <div class="row">
	              	
	              	<div class="col-md-6">
	              		<div class="form-group">
	              			<label for="fname"><?= lang('fname'); ?><span class="text-danger">*</span></label>
	              			<input type="text" required="required" name="fname" id="fname" class="form-control" placeholder="<?= lang('fname');?>">
	              		</div>
	              	</div>

					<div class="col-md-6">
						<div class="form-group">
	              			<label for="sname"><?= lang('sname'); ?><span class="text-danger">*</span></label>
	              			<input type="text" name="sname" required="required" id="sname" class="form-control" placeholder="<?= lang('sname');?>">
	              		</div>
					</div>

	              </div>

	              <div class="row">
	              	<div class="col-md-6">
	              		<div class="form-group">
	              			<label for="code">Employee Code</label>
	              			<input type="text" name="emp_code" id="emp_code" class="form-control" placeholder="Employee Code">
	              		</div>
	              	</div>
	              	<div class="col-md-6">
	              		<div class="form-group">
		              		<label for="mobile"><?= lang('mobile'); ?></label>
		              		<input type="number" onKeyPress="if(this.value.length==10) return false;" name="mobile" id="mobile" class="form-control" placeholder="<?= lang('mobile');?>">
		              	</div>
	              	</div>

	              	<div class="col-md-6 hide">
	              		<div class="form-group">
	              			<label for="role"><?= lang('role'); ?></label>
	              			<select name="role" id="role" class="form-control">
	              				<?php foreach ($roles as $k => $r ) : ?>
	              					<option value="<?= $k ?>"><?= $r; ?></option>
	              				<?php endforeach; ?>
	              			</select>
	              		</div>
	              	</div>
	              	<div class="col-md-6 hide">
	              		<div class="form-group">
	              			<label for="branch_id"><?= lang('Branch'); ?></label>
	              			<select name="branch_id" id="branch_id" class="form-control">
	              				<?php foreach ($branchs as $b ) : ?>
	              					<option value="<?= $b->id; ?>"><?= $b->name; ?></option>
	              				<?php endforeach; ?>
	              			</select>
	              		</div>
	              	</div>
	              </div>

	              <div class="row">
	              	
	              	<div class="col-md-6">
	              		<div class="form-group">
	              			<label for="email"><?= lang('email'); ?><span class="text-danger">*</span></label>
	              			<input type="email" name="email"  required="required" id="email" class="form-control" placeholder="<?= lang('email');?>">
	              		</div>
	              	</div>
	              	 <div class="col-md-6">
						<div class="form-group">
	              			<label for="dob"><?= lang('dob'); ?></label>
	              			<input  max="<?php echo date('Y-m-d'); ?>" type="date" name="dob" id="dob" class="form-control" placeholder="<?= lang('dob');?>">
	              		</div>
					</div>
	              </div>
	             


	              <div class="row">
	              	
	              	
	              	<div class="col-md-4 hide">
	              		<div class="form-group">
		              		<label for="loginname"><?= lang('loginname'); ?><span class="text-danger">*</span></label>
		              		<input type="text" name="loginname" id="loginname" class="form-control" placeholder="<?= lang('loginname');?>">
		              	</div>
	              	</div>
	              </div>

	              <div class="row">
	              	
	              	<div class="col-md-6">
	              		<div class="form-group">
	              			<label for="pwd"><?= lang('pwd'); ?><span class="text-danger">*</span></label>
	              			<input type="password" name="pwd" required id="pwd" class="form-control" placeholder="<?= lang('pwd');?>">
	              		</div>
	              	</div>
	              	<div class="col-md-6">
	              		<div class="form-group">
	              			<label for="cpwd"><?= lang('cpwd'); ?><span class="text-danger">*</span></label>
	              			<input type="password" name="cpwd" data-parsley-equalto="#pwd" required id="cpwd" class="form-control" placeholder="<?= lang('cpwd');?>">
	              		</div>
	              	</div>
	              	
	              	<div class="col-md-6">
	              		<div class="form-group">
	              			<label for="cpwd">Admin Type<span class="text-danger">*</span></label>
	              			<select class="form-control" name="admin_type" id="admin_type">
	              			    <option value="2">Staff</option>
	              			    <option value="1">Admin</option>
	              			</select>
	              		</div>
	              	</div>
	              	
	              </div>

	              <div class="row">
	              	<div class="col-md-6">
	              		<div class="form-group">
                            <label for="file"><?= lang('usign'); ?></label>
                            <input type="file" name="usersign" class="filestyle" id="file" data-size="sm">
                        </div>
	              	</div>

	              	<div class="col-md-3 hide">
	              		<div class="form-group checkbox checkbox-custom" style="margin-top: 30px;">
	              			<input type="checkbox" name="udash" value="1" id="udash" checked="">
	              			<label for="udash"><?= lang('udash'); ?></label>
	              		</div>
	              	</div>

					<div class="col-md-2">
	              		<div class="form-group checkbox checkbox-danger" style="margin-top: 30px;">
	              			<input type="checkbox" name="status" value="inactive" id="status">
	              			<label for="status"><?= lang('inactive'); ?></label>
	              		</div>
	              	</div>

	              </div>

	              <div class="row">
	              	
	              	<div class="col-md-2">
	              		<img alt="" id="img" class="img-responsive img-thumbnail thumb-lg hide">
	              	</div>

	              	
	              </div>

	            </div>


	            <div class="modal-footer">
	            	<button type="button" class="btn btn-inverse waves-effect waves-light" data-dismiss="modal" aria-hidden="true">Close</button>
	            	<button type="submit" id="save" class="btn btn-success waves-effect waves-light">Save</button>
	            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

	function MyNotify(type,msg) {
		$.Notification.notify('error','top right',type, msg);
	}
	function readURL(input) {

	    if (input.files && input.files[0]) {
	        var reader = new FileReader();

	        reader.onload = function (e) {
	            $('#img').removeClass('hide').attr('src', e.target.result);
	        }

	        reader.readAsDataURL(input.files[0]);
	    }
	}

	$(document).ready(function() {

		// image display logic
		$("[name='usersign']").change(function(){
		    readURL(this);
		});
		
		$('#adminuser_add').on('submit',function(e) {
			e.preventDefault();
			if($('#fname').val() == '') {
				//MyNotify('<?php echo lang('error'); ?>','<?php echo lang('fnamereq'); ?>');
				return false;
			}
			if($('#sname').val() == '') {
				//MyNotify('<?php echo lang('error'); ?>','<?php echo lang('snamereq'); ?>');
				return false;
			}
			if($('#email').val() == '') {
				//MyNotify('<?php echo lang('error'); ?>','<?php echo lang('emailreq'); ?>');
				return false;
			}
			$.ajax({
				url: "<?php echo site_url('modal/adminuser_add')?>/",
				type: "POST",
				data: new FormData(this),

				// Tell jQuery not to process data or worry about content-type
        		// You *must* include these options!
				cache: false,
        		contentType: false,
        		processData: false,
				success: function() {
					// hide modal
					$('.bs-example-modal-lg').modal('hide');
					$.Notification.notify('success','top right','<?php echo lang('success'); ?>', 'Admin add successfully');
					setTimeout(function() {
						location.reload();
					},1000);
				},
				error: function() {
					alert('error');
				}
			});
		});




		// SwwetALert
		//Parameter
        $('.sa-params').click(function () {
        	var id = $(this).attr('id');
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger m-l-10',
                buttonsStyling: false
            }).then(function () {
            	// call ajax function to delete it
            	$.ajax({
            		type: "POST",
            		url: "<?php echo site_url('modal/adminuser_delete')?>/"+id,
            		success: function(data) {
            			swal({
            				title: 'Deleted!',
            				text: 'User has been deleted.',
            				type: 'success'
            			}).then(function() {
            				location.reload();
            			});
            		},
					error: function() {
						alert('error');
					}
            	})
            }, function (dismiss) {
                // dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
            })
        });

	    $("#advanced_search_btn").click(function(){
		    $("#advanced_search_div").slideToggle();
		});    


	});
</script>