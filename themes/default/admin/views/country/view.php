<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style type="text/css">
	.search {
	    width: 17%;
	}
</style>
<div class="wrapper">
    <div class="container"> 

		<div class="row">
	        <div class="col-sm-12">
	            <div class="pull-right ">
	                <button type="button" class="btn btn-info" status="none" id="advanced_search_btn"><span class="glyphicon glyphicon-search "></span></button>
	           <?php if(canaccess("country","create_access")){ ?>  	
	           <button type="button" id="add" data-toggle="modal" data-target=".bs-example-modal-lg" class="btn btn-purple waves-effect waves-light" >Add New</button>
	           <?php } ?>
	           
	            <a href="<?= base_url("admin/country/"); ?>unset_session_value" id="unset_button"><button type="button" class="btn btn-primary  m-r-5" status="none"><span class="glyphicon glyphicon-refresh"></span></button></a>		
	            	
	            </div>
	        	
	            <h4 class="page-title"><?= $page_title ?></h4>
	        </div>
	    </div>
	    <br>
	    <div class="row" id="advanced_search_div" 
	    
	    <?php if(empty($_REQUEST)) { ?>

	    	<?php if(!empty($this->session->userdata('country_serach_data'))) { ?>
	    		style="display: block;"
	    	<?php }else{ ?>
				style="display: none;"
	    	<?php  } ?>
	    
		<?php } ?>
	    
	    >
		   <div class="col-xs-12 col-md-12 col-lg-12">
		      <div class="panel panel-default">
		         <div class="panel-body">
		            <div class="fixed-table-toolbar">
		               <form name="searchfrom" id="searchfrom" method="post"  action='<?= base_url("admin/country"); ?>' enctype="multipart/form-data">
		                  <div class="columns btn-group pull-right margin">
		                     <div class="searchdatetitle">&nbsp;</div>
		                     <button type="submit" name="filter" id="filter" class="btn btn-success" value="filter">Search</button>
		                  </div>
		                  
		                  <div class="pull-left search margin m-r-15">
		                     <div class="searchdatetitle">Name</div>
		                     <input class="form-control" type="text" placeholder="Any Text.." name="country_s_name" id="country_s_name" value="<?php if(isset($_REQUEST['country_s_name'])) 
		                     			{ 
		                     				echo $_REQUEST['country_s_name']; 
		                     			} 
		                     			else 
		                     			{ 
		                     				if($this->session->userdata('country_s_name')) 
		                     				{ 
		                     					echo $this->session->userdata('country_s_name'); 
		                     				} 
		                     			}
             					?>">
		                  </div>
		                  
		               </form>
		            </div>
		         </div>
		      </div>
		   </div>
		</div>
	   
      <!-- Page-Title -->
        <div class="row">
           <div class="col-sm-12 table-responsive card-box">
                <table class="table table-hover table-condensed table table-striped" id="tech-companies-1">
                	<thead>
                		<th width="5%">#</th>
                		<th>Country</th>
                		<th>Code</th>
                		<th width="10%">Status</th>
                		<th width="10%"><?= lang('actions'); ?></th>
                	</thead>
                	<tbody>
                		<?php $count = 1; foreach ($rows as $rowsMst) : ?>
	        					<tr>
	        						<td><?=$count++;?></td>
									<td><?= ucfirst($rowsMst->name); ?></td>
									<td><?= ucfirst($rowsMst->code); ?></td>
									<td><span class="label label-<?php echo ($rowsMst->status == 'active') ? 'success' : 'danger'?>"><?= ucfirst($rowsMst->status); ?></span></td>	
									<td>
									<?php if(canaccess("country","edit_access")){ ?> <a href='<?= base_url("admin/country/edit/{$rowsMst->id}"); ?>'><button class="btn btn-icon btn-xs waves-effect waves-light btn-purple" data-toggle="tooltip" data-placement="left" title="" data-original-title="<?= lang('edit'); ?>"> <i class="fa fa-edit"></i> </button></a>
									<?php } ?>
									<?php if(canaccess("country","delete_access")){ ?>		<a class="sa-params" href='javascript:void(0)' id="<?= $rowsMst->id ?>"><button class="btn btn-icon btn-xs waves-effect waves-light btn-danger" data-toggle="tooltip" data-placement="right" title="" data-original-title="<?= lang('delete'); ?>"> <i class="fa fa-remove"></i> </button></a> 
									<?php } ?>	


									</td>

								</tr>
						<?php endforeach; ?>
                	</tbody>
                </table>
            </div>
        </div>
        <br>

        <div class="row">
          <div class="col-md-12">
            <div class="pull-right">
              <?php echo $this->pagination->create_links(); ?>
            </div>
          </div>
        </div>


 <!--  Modal content for the above example -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
        	<form name="add_form" id="add_form" method="post" enctype="multipart/form-data">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	                <h4 class="modal-title" id="myLargeModalLabel">Add <?= $page_title ?></h4>
	            </div>
	            <div class="modal-body">

	              <div class="row">
	              	
	              	<div class="col-md-12">
	              		<div class="form-group">
	              			<label for="name">Country<span class="text-danger">*</span></label>
	              			<input type="text" required="required" name="name" id="name" class="form-control" placeholder="Country">
	              		</div>
	              	</div>

	              	<div class="col-md-12">
	              		<div class="form-group">
	              			<label for="name">Code<span class="text-danger">*</span></label>
	              			<input type="text" required="required" name="code" id="code" class="form-control" placeholder="Code">
	              		</div>
	              	</div>

	              </div>	              

	            </div>


	            <div class="modal-footer">
	            	<button type="button" class="btn btn-inverse waves-effect waves-light" data-dismiss="modal" aria-hidden="true">Close</button>
	            	<button type="submit" id="save" class="btn btn-success waves-effect waves-light">Save</button>
	            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

	function MyNotify(type,msg) {
		$.Notification.notify('error','top right',type, msg);
	}

	$(document).ready(function() {

		if($("#message").length > 0){
              tinymce.init({
                  selector: "textarea#message",
                  menubar:false,
                  theme: "modern",
                  height:300,
                  plugins: [
                      "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                      "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking",
                      "save table contextmenu directionality template paste textcolor"
                  ],
                  toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview fullpage | forecolor backcolor",
                  style_formats: [
                      {title: 'Bold text', inline: 'b'},
                      {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                      {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                      {title: 'Example 1', inline: 'span', classes: 'example1'},
                      {title: 'Example 2', inline: 'span', classes: 'example2'},
                      {title: 'Table styles'},
                      {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                  ]
              });
          }

		
		
		$('#add_form').on('submit',function(e) {
			e.preventDefault();
			
			if($('#name').val() == '') {
				return false;
			}
			$.ajax({
				url: "<?php echo site_url('admin/country')?>/add_row",
				type: "POST",
				data: new FormData(this),
				cache: false,
        		contentType: false,
        		processData: false,
				success: function() {
					// hide modal
					$('.bs-example-modal-lg').modal('hide');
					$.Notification.notify('success','top right','<?php echo lang('success'); ?>', 'Successfully Add New Record');
					setTimeout(function() {
						location.reload();
					},1000);
				},
				error: function() {
					alert('error');
				}
			});
		});




		// SwwetALert
		//Parameter
        $('.sa-params').click(function () {
        	var id = $(this).attr('id');
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger m-l-10',
                buttonsStyling: false
            }).then(function () {
            	// call ajax function to delete it
            	$.ajax({
            		type: "POST",
            		url: "<?php echo site_url('admin/country')?>/delete_row/"+id,
            		success: function(data) {
            			swal({
            				title: 'Deleted!',
            				text: 'country Successfully deleted.',
            				type: 'success'
            			}).then(function() {
            				location.reload();
            			});
            		},
					error: function() {
						alert('error');
					}
            	})
            }, function (dismiss) {
                // dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
            })
        });

       

	});

	$("#advanced_search_btn").click(function(){
		    $("#advanced_search_div").slideToggle();
	});    

	function ImagePreview(input,image_preview) 
	{
	  if (input.files && input.files[0]) 
	  {
	    var reader = new FileReader();
	    reader.onload = function(e) {
	      $('#'+image_preview).attr('src', e.target.result);
	      $('#'+image_preview).show();

	  }
	  reader.readAsDataURL(input.files[0]);
	  }
	}



</script>