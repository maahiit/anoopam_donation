<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if(canaccess("country","edit_access") != 'true'){ echo "<script>window.location.href ='".site_url()."admin'</script>";}?>


<div class="wrapper">
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-color panel-inverse">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?= $page_title ?></h3>
                    </div>
                    <form name="customer_edit" id="customer_edit" method="post"  action='<?= base_url("admin/country/edit/{$row->id}"); ?>' enctype="multipart/form-data">
                <div class="panel-body">
            
  <div class="row">
        <input type="hidden" name="id" value="<?= $row->id; ?>">
        
        <div class="col-md-4">
                <div class="form-group">
                    <label for="name">Country<span class="text-danger">*</span></label>
                    <input type="text" required="required" name="name" id="name" class="form-control" placeholder="Name" value="<?=  $row->name; ?>">
                </div>
        </div>

        <div class="col-md-4">
                <div class="form-group">
                    <label for="name">Code<span class="text-danger">*</span></label>
                    <input type="text" required="required" name="code" id="code" class="form-control" placeholder="Code" value="<?=  $row->code; ?>">
                </div>
        </div>
                
        <div class="col-md-4">        
                <div class="form-group checkbox checkbox-danger" style="margin-top: 30px;">
                    <input <?php echo ($row->status == 'inactive') ? 'checked="checked"' : ''?>  type="checkbox" name="status" value="inactive" id="status">
                    <label for="status"><?= lang('inactive'); ?></label>
                </div>
        </div>

        </div>
            


        </div>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" id="save" class="btn btn-success waves-effect waves-light pull-right">Save</button>
                        <span class="pull-right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <a href="<?= base_url('admin/country'); ?>"><button type="button" class="btn btn-inverse waves-effect waves-light pull-right" data-dismiss="modal" aria-hidden="true">Close</button></a>
                        
                        
                        <div class="clearfix"></div>
                    </div>
                </form>
                </div>
            </div>
        </div>

        <script>
         
$(document).on('click','#remove_field',function() {
             $(this).closest("#row_remove").remove();
     });


function ImagePreview(input,image_preview) 
    {
      if (input.files && input.files[0]) 
      {
        var reader = new FileReader();
        reader.onload = function(e) {
          $('#'+image_preview).attr('src', e.target.result);
          $('#'+image_preview).show();

      }
      reader.readAsDataURL(input.files[0]);
      }
    }





        </script>