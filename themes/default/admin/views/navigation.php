<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="progress progress-sm sam-progressbar" style="z-index: 9999 !important;">
    <div class="progress-bar progress-bar-primary progress-bar-striped active" role="progressbar" aria-valuenow="82" aria-valuemin="0" aria-valuemax="100" style="width: 82%;">
        <span class="sr-only">82% Complete</span>
    </div>
</div>
<!-- Navigation Bar-->
        

        <header id="topnav">
            <div class="topbar-main">
                <div class="container">

                    <!-- Logo container-->
                    <div class="logo">
                        <img src="<?= site_url().'themes/default/admin/assets/upload/logos/'.$Settings->sitelogo; ?>" alt="<?= $Settings->sitename; ?>" class="img-responsive thumb-sm img_site img-circle admin_mobile_hide">
                        <a href="<?= base_url(); ?>" class="logo logo_name"><span><?= $Settings->sitename; ?></span></a>
                    </div>
                    <!-- End Logo container-->




                    <div class="menu-extras">

                        <ul class="nav navbar-nav navbar-right pull-right">

                            <li class="dropdown navbar-c-items">
                                <a href="" class="dropdown-toggle waves-effect waves-light profile" data-toggle="dropdown" aria-expanded="true"><img src="<?= $assets ?>images/users/default-avatar.svg" alt="user-img" class="img-circle"> </a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?= base_url("admin/contributor/user_profile_edit/{$this->session->userdata('loginid')}"); ?>"><i class="ti-user text-custom m-r-10"></i> Profile</a></li>
                                    <!-- <li><a href="javascript:void(0)"><i class="ti-settings text-custom m-r-10"></i> Settings</a></li> -->
                                    <li class="divider"></li>
                                    <li><a href="<?= base_url('admin/logout'); ?>"><i class="ti-power-off text-danger m-r-10"></i> Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                        <div class="menu-item">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </div>
                    </div>

                </div>
            </div>


<?php 
function canaccess($to,$actions) 
{ 
  $userrole = $_SESSION['identity'];
  $user_id = $_SESSION['loginid'];
  $field_name = $actions;
  $d = ORM::for_table('sam_accessrights')
                ->raw_query("select ".$field_name.",count(*) as count from `sam_accessrights` where `role_id`='".$userrole."' and `page_title` = '".$to."'")->find_many();
    if($d[0]->count != '0'){    
        if($d[0]->$field_name == 'yes'){    
                return true;    
        }else{
                return false;
             }
    }
  return false; 
}  
?>

   <div class="navbar-custom">
                <div class="container">

                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">
                            <!-- <li> -->
                              <!-- Logo container-->
                              <!-- <div class="logo">
                                  <a href="index.html" class="logo"><span><?= $Settings->sitename; ?></span></a>
                              </div> -->
                              <!-- End Logo container-->
                            <!-- </li> -->
                            
                            
                    <li class="has-submenu">
                        <a href="<?= base_url('admin'); ?>"><i class="md md-dashboard"></i><?= lang('dashboard'); ?></a>
                    </li>
            
                    <?php if(canaccess("payment_gateway","view_access")) { ?> 
                        <li>
                            <a href="<?= base_url('admin/transition'); ?>">
                                <i class="fa fa-cc-amex" aria-hidden="true"></i>
                                 Donations
                            </a>
                        </li> 
                    <?php } ?>

                    <?php if(canaccess("contributor","view_access")) { ?> 
                        <li>
                            <a href="<?= base_url('admin/contributor'); ?>">
                                <i class="fa fa-handshake-o" aria-hidden="true"></i>
                                 Contributor
                            </a>
                        </li> 
                    <?php } ?>

                    

                    <?php if(canaccess("adminusers","view_access")) { ?> 
                        <li>
                            <a href="<?= base_url('admin/adminusers'); ?>">
                                <i class="fa fa-user-plus" aria-hidden="true"></i>
                                 Admin Users
                            </a>
                        </li> 
                    <?php } ?>

                    <?php if(canaccess("categories","view_access")) { ?> 
                        <li>
                            <a href="<?= base_url('admin/categories'); ?>">
                                <i class="fa fa-snowflake-o" aria-hidden="true"></i>
                                 Category
                            </a>
                        </li> 
                    <?php } ?>
                    <?php if(canaccess("subcategories","view_access")) { ?> 
                        <li>
                            <a href="<?= base_url('admin/subcategories'); ?>">
                                <i class="fa fa-diamond" aria-hidden="true"></i>
                                 Sub Category
                            </a>
                        </li> 
                    <?php } ?>

                    <?php if(canaccess("payment_gateway","view_access")) { ?> 
                        <li>
                            <a href="<?= base_url('admin/payment_gateway'); ?>">
                                <i class="fa fa-cc-amex" aria-hidden="true"></i>
                                 Payment Gateway
                            </a>
                        </li> 
                    <?php } ?>

                     
                    

                    <?php if(canaccess("settings","view_access")){ ?>   
                        <li class="has-submenu">
                        <a href="javascript:void(0)"><i class="fa fa-cogs"></i><?= lang('system'); ?></a>
                        <ul class="submenu">


                             
                            <?php if(canaccess("smtp_email","view_access")){ ?>    
                            <li><a href="<?= base_url('admin/smtp_email'); ?>">SMTP Email</a></li>
                            <?php } ?>                               

                            <?php if(canaccess("country","view_access")){ ?>    
                            <li><a href="<?= base_url('admin/country'); ?>">Country</a></li>
                            <?php } ?> 
                            <?php if(canaccess("settings","view_access")){ ?>    
                            <li><a href="<?= base_url('admin/city'); ?>">City</a></li>
                            <?php } ?> 

                            
                            <?php if(canaccess("settings","view_access")){ ?>    
                            <li><a href="<?= base_url('settings'); ?>"><?= lang('syssettings'); ?></a></li>
                            <?php } ?>
                            
                            
                            <li class="hide"><a href="<?= base_url('imagemap'); ?>"><?= lang('imagemapdata'); ?></a></li>
                            <li class="hide"><a href="<?= base_url('emaillog'); ?>"><?= lang('emaillog'); ?></a></li>
                        </ul>
                    </li>
               <?php } ?>

                           



  
                        </ul>
                        <!-- End navigation menu -->
                    </div>
                </div> <!-- end container -->
            </div> <!-- end navbar-custom -->
        </header>
        <!-- End Navigation Bar-->

 

        <!-- Notification Area -->
        <?php

            if($success = $this->session->flashdata('success')) {
                echo "<script>
                $(document).ready(function() {
                    $.Notification.notify('success','top right','".lang('success')."', '".$success."');
                });
                </script>";
            }

            if($error = $this->session->flashdata('error')) {
                echo "<script>
                $(document).ready(function() {
                    $.Notification.notify('error','top right','".lang('error')."', '".$error."');
                });
                </script>";
            }
        ?>