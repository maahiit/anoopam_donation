<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if(canaccess("categories","edit_access") != 'true'){ echo "<script>window.location.href ='".site_url()."admin'</script>";}?>
 

<div class="wrapper">

    <div class="container">

        <div class="row">

            <div class="col-lg-12">

                <div class="panel panel-color panel-inverse">

                    <div class="panel-heading">
                        <h3 class="panel-title"><?= $page_title ?></h3>
                    </div>

                    <form name="customer_edit" id="customer_edit" method="post"  action='<?= base_url("admin/payment_gateway/edit/{$row->id}"); ?>' enctype="multipart/form-data">

                        <div class="panel-body">
            
                            <div class="row">

                                <input type="hidden" name="id" value="<?= $row->id; ?>">
                                
                                <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="name">Name<span class="text-danger">*</span></label>
                                            <input type="text" required="required" name="name" id="name" class="form-control" placeholder="Name" value="<?=  $row->name; ?>">
                                        </div>
                                </div>

                                <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="name">Api url<span class="text-danger">*</span></label>
                                            <input type="text" required="required" name="api_url" id="api_url" class="form-control" placeholder="Api url" value="<?=  $row->api_url; ?>">
                                        </div>
                                </div>

                             
                                
                            	<div class="col-md-4">
            	              		<div class="form-group">
            	              			<label for="user_name">User Name<span class="text-danger">*</span></label>
            	              			<input type="text" name="user_name" id="user_name" class="form-control" placeholder="User Name" value="<?=  $row->user_name; ?>">
            	              		</div>
            	              	</div>
            	              	<div class="col-md-4">
            	              		<div class="form-group">
            	              			<label for="password">Password<span class="text-danger">*</span></label>
            	              			<input type="text" name="password" id="password" class="form-control" placeholder="Password" value="<?=  $row->password; ?>">
            	              		</div>
            	              	</div>
            	              	<div class="col-md-4">
            	              		<div class="form-group">
            	              			<label for="password">Product id<span class="text-danger">*</span></label>
            	              			<input type="text" name="product_id" id="product_id" class="form-control" placeholder="Product id" value="<?=  $row->product_id; ?>">
            	              		</div>
            	              	</div>
            	              	<div class="col-md-4">
            	              		<div class="form-group">
            	              			<label for="client_code	">Client Code<span class="text-danger">*</span></label>
            	              			<input type="text" name="client_code" id="client_code" class="form-control" placeholder="Client Code" value="<?=  $row->client_code; ?>">
            	              		</div>
            	              	</div>
            	              	
            	              	<div class="col-md-4">
            	              		<div class="form-group">
            	              			<label for="account_no	">Account No<span class="text-danger">*</span></label>
            	              			<input type="text" name="account_no" id="account_no" class="form-control" placeholder="Account No" value="<?=  $row->account_no; ?>">
            	              		</div>
            	              	</div>
            	              	<div class="col-md-4">
            	              		<div class="form-group">
            	              			<label for="req_hash_key">Hash Key<span class="text-danger">*</span></label>
            	              			<input type="text" name="req_hash_key" id="req_hash_key" class="form-control" placeholder="Hash Key" value="<?=  $row->req_hash_key; ?>">
            	              		</div>
            	              	</div>

                            </div>

                        

                            <div class="row">

                                <!--<div class="col-md-8">
                                    <div class="form-group">
                                        <label for="url">Remark<span class="text-danger">*</span></label>
                                        <textarea class="form-control" name="remark" id="remark" placeholder="Remark" required="required"><?= $row->remark;?></textarea>
                                    </div>

                                </div>-->
                
                                <div class="col-md-4">        
                                    <div class="form-group checkbox checkbox-danger" style="margin-top: 30px;">
                                    <input <?php echo ($row->status == 'inactive') ? 'checked="checked"' : ''?>  type="checkbox" name="status" value="inactive" id="status">
                                    <label for="status"><?= lang('inactive'); ?></label>
                                    </div>
                                </div>

                            </div>

                    </div>       
                    
                    <div class="panel-footer">

                        <button type="submit" id="save" class="btn btn-success waves-effect waves-light pull-right">Save</button>
                        <span class="pull-right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <a href="<?= base_url('admin/payment_gateway'); ?>"><button type="button" class="btn btn-inverse waves-effect waves-light pull-right" data-dismiss="modal" aria-hidden="true">Close</button></a>                        
                        
                        <div class="clearfix"></div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>
</div>

<script>
         
    $(document).on('click','#remove_field',function() {
                 $(this).closest("#row_remove").remove();
         });

</script>