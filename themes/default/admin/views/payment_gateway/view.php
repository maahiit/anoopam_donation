<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style type="text/css">
	.search {
	    width: 17%;
	}
</style>
<div class="wrapper">
    <div class="container"> 

		<div class="row">
	        <div class="col-sm-12">
	            <div class="pull-right ">
	                <button type="button" class="btn btn-info" status="none" id="advanced_search_btn"><span class="glyphicon glyphicon-search "></span></button>
	           <?php if(canaccess("categories","create_access")){ ?>  	
	           <button type="button" id="add" data-toggle="modal" data-target=".bs-example-modal-lg" class="btn btn-purple waves-effect waves-light" >Add New</button>
	           <?php } ?>
	           
	            <a href="<?= base_url("admin/payment_gateway/"); ?>unset_session_value" id="unset_button"><button type="button" class="btn btn-primary  m-r-5" status="none"><span class="glyphicon glyphicon-refresh"></span></button></a>		
	            	
	            </div>
	        	
	            <h4 class="page-title"><?= $page_title ?></h4>
	        </div>
	    </div>
	    <br>
	    <div class="row" id="advanced_search_div" 
	    
	    <?php if(empty($_REQUEST)) { ?>

	    	<?php if(!empty($this->session->userdata('payment_gateway_serach_data'))) { ?>
	    		style="display: block;"
	    	<?php }else{ ?>
				style="display: none;"
	    	<?php  } ?>
	    
		<?php } ?>
	    
	    >
		   <div class="col-xs-12 col-md-12 col-lg-12">
		      <div class="panel panel-default">
		         <div class="panel-body">
		            <div class="fixed-table-toolbar">
		               <form name="searchfrom" id="searchfrom" method="post"  action='<?= base_url("admin/payment_gateway"); ?>' enctype="multipart/form-data">
		                  <div class="columns btn-group pull-right margin">
		                     <div class="searchdatetitle">&nbsp;</div>
		                     <button type="submit" name="filter" id="filter" class="btn btn-success" value="filter">Search</button>
		                  </div>
		                  
		                  <div class="pull-left search margin m-r-15">
		                     <div class="searchdatetitle">Name</div>
		                     <input class="form-control" type="text" placeholder="Any Text.." name="payment_gateway_s_name" id="payment_gateway_s_name" value="<?php if(isset($_REQUEST['payment_gateway_s_name'])) 
		                     			{ 
		                     				echo $_REQUEST['payment_gateway_s_name']; 
		                     			} 
		                     			else 
		                     			{ 
		                     				if($this->session->userdata('payment_gateway_s_name')) 
		                     				{ 
		                     					echo $this->session->userdata('payment_gateway_s_name'); 
		                     				} 
		                     			}
             					?>">
		                  </div>
		                  
		               </form>
		            </div>
		         </div>
		      </div>
		   </div>
		</div>
	   
      <!-- Page-Title -->
        <div class="row">
           <div class="col-sm-12 table-responsive card-box">
                <table class="table table-hover table-condensed table table-striped" id="tech-companies-1">
                	<thead>
                		<th width="5%">#</th>
                		<th>Payment Gateway</th>
                		<th>Username</th>                		
                		<th>Payment Geteway URL</th>                		
                		<th width="10%">Status</th>
                		<th width="10%"><?= lang('actions'); ?></th>
                	</thead>
                	<tbody>
                		<?php $count = 1; foreach ($rows as $rowsMst) : ?>
	        					<tr>
	        						<td><?=$count++;?></td>
									<td><?= ucfirst($rowsMst->name); ?></td>
									<td><?= $rowsMst->user_name; ?></td>
									<td><?= ucfirst($rowsMst->api_url); ?></td>
									<td><span class="label label-<?php echo ($rowsMst->status == 'active') ? 'success' : 'danger'?>"><?= ucfirst($rowsMst->status); ?></span></td>	
									<td>
									<?php if(canaccess("categories","edit_access")){ ?> <a href='<?= base_url("admin/payment_gateway/edit/{$rowsMst->id}"); ?>'><button class="btn btn-icon btn-xs waves-effect waves-light btn-purple" data-toggle="tooltip" data-placement="left" title="" data-original-title="<?= lang('edit'); ?>"> <i class="fa fa-edit"></i> </button></a>
									<?php } ?>
									<?php if(canaccess("categories","delete_access")){ ?>		<a class="sa-params" href='javascript:void(0)' id="<?= $rowsMst->id ?>"><button class="btn btn-icon btn-xs waves-effect waves-light btn-danger" data-toggle="tooltip" data-placement="right" title="" data-original-title="<?= lang('delete'); ?>"> <i class="fa fa-remove"></i> </button></a> 
									<?php } ?>	


									</td>

								</tr>
						<?php endforeach; ?>
                	</tbody>
                </table>
            </div>
        </div>
        <br>

        <div class="row">
          <div class="col-md-12">
            <div class="pull-right">
              <?php echo $this->pagination->create_links(); ?>
            </div>
          </div>
        </div>


 <!--  Modal content for the above example -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
        	<form name="add_form" id="add_form" method="post" enctype="multipart/form-data">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	                <h4 class="modal-title" id="myLargeModalLabel">Add <?= $page_title ?></h4>
	            </div>
	        <div class="modal-body">

	            <div class="row">
	              	
	              	<div class="col-md-6">
	              		<div class="form-group">
	              			<label for="name">Name<span class="text-danger">*</span></label>
	              			<input type="text" required="required" name="name" id="name" class="form-control" placeholder="Name">
	              		</div>
	              	</div>

	              	<div class="col-md-6">
	              		<div class="form-group">
	              			<label for="api_url">Return Url<span class="text-danger">*</span></label>
	              			<input type="text" required="required" name="api_url" id="api_url" class="form-control" placeholder="Api Url">
	              		</div>
	              	</div>
	              	<div class="col-md-6">
	              		<div class="form-group">
	              			<label for="user_name">User Name<span class="text-danger">*</span></label>
	              			<input type="text" name="user_name" id="user_name" class="form-control" placeholder="User Name">
	              		</div>
	              	</div>
	              	<div class="col-md-6">
	              		<div class="form-group">
	              			<label for="password">Password<span class="text-danger">*</span></label>
	              			<input type="text" name="password" id="password" class="form-control" placeholder="Password">
	              		</div>
	              	</div>
	              	<div class="col-md-6">
	              		<div class="form-group">
	              			<label for="password">Product id<span class="text-danger">*</span></label>
	              			<input type="text" name="product_id" id="product_id" class="form-control" placeholder="Product id">
	              		</div>
	              	</div>
	              	<div class="col-md-6">
	              		<div class="form-group">
	              			<label for="client_code	">Client Code<span class="text-danger">*</span></label>
	              			<input type="text" name="client_code" id="client_code" class="form-control" placeholder="Client Code">
	              		</div>
	              	</div>
	              	
	              	<div class="col-md-6">
	              		<div class="form-group">
	              			<label for="account_no	">Account No<span class="text-danger">*</span></label>
	              			<input type="text" name="account_no" id="account_no" class="form-control" placeholder="Account No">
	              		</div>
	              	</div>
	              	<div class="col-md-6">
	              		<div class="form-group">
	              			<label for="req_hash_key">Hash Key<span class="text-danger">*</span></label>
	              			<input type="text" name="req_hash_key" id="req_hash_key" class="form-control" placeholder="Hash Key">
	              		</div>
	              	</div>
	              	
	              	
	              	
	              	
	            </div>

	            <div class="row">

	              	<!--<div class="col-md-6">
	              		<div class="form-group">
	              			<label for="api_key">Api Key<span class="text-danger">*</span></label>
	              			<input type="text" required="required" name="api_key" id="api_key" class="form-control" placeholder="Api Key">
	              		</div>
	              	</div>-->

	              <!--	<div class="col-md-6">
	              		<div class="form-group">
	              			<label for="api_secret_key">Api Secret Key<span class="text-danger">*</span></label>
	              			<input type="text" required="required" name="api_secret_key" id="api_secret_key" class="form-control" placeholder="Api Secret Key">
	              		</div>
	              	</div>-->

	            </div>

	           <!-- <div class="row">

	              	<div class="col-md-6">
	              		<div class="form-group">
	              			<label for="email">Email<span class="text-danger">*</span></label>
	              			<input type="email" required="required" name="email" id="email" class="form-control" placeholder="Email">
	              		</div>
	              	</div>

	              	<div class="col-md-6">
	              		<div class="form-group">
	              			<label for="url">Url<span class="text-danger">*</span></label>
	              			<input type="text" required="required" name="url" id="url" class="form-control" placeholder="Url">
	              		</div>
	              	</div>

	            </div>-->


	           <!-- <div class="row">

	            	<div class="col-md-12">
	              		<div class="form-group">
	              			<label for="url">Remark<span class="text-danger">*</span></label>
	              			<textarea class="form-control" name="remark" id="remark" required="required" placeholder="Remark"></textarea>
	              		</div>
	              	</div>

	            </div>	-->              

	        </div>


	            <div class="modal-footer">
	            	<button type="button" class="btn btn-inverse waves-effect waves-light" data-dismiss="modal" aria-hidden="true">Close</button>
	            	<button type="submit" id="save" class="btn btn-success waves-effect waves-light">Save</button>
	            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

	function MyNotify(type,msg) {
		$.Notification.notify('error','top right',type, msg);
	}

	$(document).ready(function() {

		if($("#message").length > 0){
              tinymce.init({
                  selector: "textarea#message",
                  menubar:false,
                  theme: "modern",
                  height:300,
                  plugins: [
                      "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                      "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking",
                      "save table contextmenu directionality template paste textcolor"
                  ],
                  toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview fullpage | forecolor backcolor",
                  style_formats: [
                      {title: 'Bold text', inline: 'b'},
                      {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                      {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                      {title: 'Example 1', inline: 'span', classes: 'example1'},
                      {title: 'Example 2', inline: 'span', classes: 'example2'},
                      {title: 'Table styles'},
                      {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                  ]
              });
          }

		
		
		$('#add_form').on('submit',function(e) {
			e.preventDefault();
			
			if($('#name').val() == '' || 
			   $('#api_url').val() == '' || 
			   $('#api_key').val() == '' || 
			   $('#api_secret_key').val() == '' || 
			   $('#email').val() == '' || 
			   $('#url').val() == '' || 
			   $('#remark').val() == ''){
				return false;
			}
			$.ajax({
				url: "<?php echo site_url('admin/payment_gateway')?>/add_row",
				type: "POST",
				data: new FormData(this),
				cache: false,
        		contentType: false,
        		processData: false,
				success: function() {
					// hide modal
					$('.bs-example-modal-lg').modal('hide');
					$.Notification.notify('success','top right','<?php echo lang('success'); ?>', 'Successfully Add New Record');
					setTimeout(function() {
					//	location.reload();
					},1000);
				},
				error: function() {
					alert('error');
				}
			});
		});




		// SwwetALert
		//Parameter
        $('.sa-params').click(function () {
        	var id = $(this).attr('id');
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger m-l-10',
                buttonsStyling: false
            }).then(function () {
            	// call ajax function to delete it
            	$.ajax({
            		type: "POST",
            		url: "<?php echo site_url('admin/payment_gateway')?>/delete_row/"+id,
            		success: function(data) {
            			swal({
            				title: 'Deleted!',
            				text: 'payment_gateway Successfully deleted.',
            				type: 'success'
            			}).then(function() {
            				location.reload();
            			});
            		},
					error: function() {
						alert('error');
					}
            	})
            }, function (dismiss) {
                // dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
            })
        });

       

	});

	$("#advanced_search_btn").click(function(){
		    $("#advanced_search_div").slideToggle();
	});    

	function ImagePreview(input,image_preview) 
	{
	  if (input.files && input.files[0]) 
	  {
	    var reader = new FileReader();
	    reader.onload = function(e) {
	      $('#'+image_preview).attr('src', e.target.result);
	      $('#'+image_preview).show();

	  }
	  reader.readAsDataURL(input.files[0]);
	  }
	}



</script>