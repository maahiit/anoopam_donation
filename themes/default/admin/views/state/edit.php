<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if(canaccess("state","edit_access") != 'true'){ echo "<script>window.location.href ='".site_url()."admin'</script>";}?>


<div class="wrapper">
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-color panel-inverse">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?= $page_title ?></h3>
                    </div>
                    <form name="customer_edit" id="customer_edit" method="post"  action='<?= base_url("admin/state/edit/{$state->id}"); ?>' enctype="multipart/form-data">
                <div class="panel-body">
            
              <div class="row">
                    <input type="hidden" name="id" value="<?= $state->id; ?>">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">State<span class="text-danger">*</span></label>
                            <input type="text" required="required" name="state" id="state" class="form-control" placeholder="State" value="<?=  $state->state; ?>">
                        </div>
                    </div>
                    
                   
                  </div>


                    </div>
                    <div class="panel-footer">
                        <button type="submit" id="save" class="btn btn-success waves-effect waves-light pull-right">Save</button>
                        <span class="pull-right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <a href="<?= base_url('admin/state'); ?>"><button type="button" class="btn btn-inverse waves-effect waves-light pull-right" data-dismiss="modal" aria-hidden="true">Close</button></a>
                        
                        
                        <div class="clearfix"></div>
                    </div>
                </form>
                </div>
            </div>
        </div>

        <script>
            function readURL(input,id) {

                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $(id).removeClass('hide').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }




function ImagePreview(input,image_preview) 
    {
      if (input.files && input.files[0]) 
      {
        var reader = new FileReader();
        reader.onload = function(e) {
          $('#'+image_preview).removeClass('hide').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
      }
    }

$(document).on('click','#remove_field',function() {
             $(this).closest("#row_remove").remove();
     });












        </script>