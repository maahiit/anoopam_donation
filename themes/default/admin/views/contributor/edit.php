<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if(canaccess("contributor","edit_access") != 'true'){ echo "<script>window.location.href ='".site_url()."admin'</script>";}?>
<style>
    #danges label::after
    {
        background-color: #fb3865 !important;
    }
     #susscess label::after
    {
        background-color: #449d44 !important;
    }
    
</style>

<div class="wrapper">
<div class="container">

<div class="row">
<div class="col-lg-12">
<div class="panel panel-color panel-inverse">
    <div class="panel-heading">
        <h3 class="panel-title">Contributor Edit</h3>
    </div>
    <form name="user_add" id="user_add" method="post"  action='<?= base_url("contributor/edit/{$user->id}"); ?>' enctype="multipart/form-data">
        
    <div class="panel-body">
        
        <div class="row">
            <input type="hidden" name="id" value="<?= $user->id; ?>">
    
            <div class="col-md-4">
                <div class="form-group">
                    <label for="fname"><?= lang('fname'); ?><span class="text-danger">*</span></label>
                    <input type="text" name="fname" id="fname" class="form-control" value="<?= $user->first_name; ?>" autofocus placeholder="<?= lang('fname');?>">
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="sname"><?= lang('sname'); ?><span class="text-danger">*</span></label>
                    <input type="text" name="sname"  id="sname" class="form-control" value="<?= $user->last_name; ?>" placeholder="<?= lang('sname');?>">
                </div>
            </div>

         
            <div class="col-md-4">
                <div class="form-group">
                    <label for="mobile"><?= lang('mobile'); ?></label>
                    <input type="number" onKeyPress="if(this.value.length==10) return false;" name="mobile" id="mobile" class="form-control" placeholder="<?= lang('mobile');?>" value="<?= $user->mobile; ?>">
                </div>
            </div>

            </div>
        
          <div class="row">

            <div class="col-md-4">
                <div class="form-group">
                    <label for="email"><?= lang('email'); ?><span class="text-danger">*</span></label>
                    <input type="email" name="email" id="email" class="form-control" placeholder="<?= lang('email');?>" value="<?= $user->email_id; ?>">
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="password"><?= lang('password'); ?></label>
                    <input type="text"  name="password" id="password" class="form-control" placeholder="Password" value="<?= $user->password; ?>">
                </div>
            </div>
             
            <div class="col-md-4">
                <div class="form-group">
                    <label for="google_id"><?= lang('Contributor_type'); ?></label>
                    <select name="google_id" id="google_id" class="form-control">
                        <option value="">select ontribution Type</option>
                        <option <?php if ($user->contributor_type == 'individual') { echo "selected"; } ?> value="individual">Individual</option>
                        <option <?php if ($user->contributor_type == 'other') { echo "selected"; } ?> value="other">Other</option>
                    </select>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="google_id"><?= lang('Google_id'); ?></label>
                    <input type="text"  name="google_id" id="google_id" class="form-control" placeholder="<?= lang('Google_id'); ?>" value="<?= $user->google_id; ?>">
                </div>
            </div>
            
            <div class="col-md-4">
            <div class="form-group"> 
                <label for="name">City<span class="text-danger">*</span></label>
                
                 <select data-live-search="true" name="city" id="city" class="selectpicker form-control" data-style="btn-white" >
                    <?php foreach ($city as $key => $value): ?>
                      <option 
                      <?php if($value->id == $user->city) { echo "selected"; } ?>
                       value="<?= $value->id ?>"><?= $value->city ?> </option>    
                    <?php endforeach ?>
                </select>

            </div>
        </div>
            
            
         
          </div>
    
 
        
        <div class="row">
           
            <div class="col-md-4">
                <div class="form-group">
                   <label for="business_address"><?= lang('Business Address'); ?></label>
                    <textarea required="required" name="business_address" id="business_address" class="form-control" placeholder="<?= lang('Business Address');?>"><?= $user->business_address; ?></textarea>
                </div>
            </div>

          
          </div>



          <div class="row">           

            <div class="col-md-2">
                <div class="form-group checkbox checkbox-danger" style="margin-top: 30px;">
                    <input type="checkbox" <?php echo ($user->status == 'inactive') ? 'checked="checked"' : ''?> name="status" value="inactive" id="status">
                    <label for="status"><?= lang('inactive'); ?></label>
                </div>
            </div>

          </div>

    </div>
    <div class="panel-footer">
        
        <!-- <button type="submit" id="save" class="btn btn-success waves-effect waves-light pull-right">Save</button> -->
        <span class="pull-right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
        <a href="<?= base_url('user'); ?>"><button type="button" class="btn btn-inverse waves-effect waves-light pull-right" data-dismiss="modal" aria-hidden="true">Close</button></a>
        <div class="clearfix"></div>
    </div>
</form>
</div>
</div>
</div>

<script>
function readURL(input) {

if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
        $('#img').removeClass('hide').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
}
}

$(document).ready(function() {3
// image display logic
$("[name='usersign']").change(function(){
    readURL(this);
});
});


function ImagePreview(input,image_preview) 
  {
    if (input.files && input.files[0]) 
    {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#'+image_preview).attr('src', e.target.result);
        $('#'+image_preview).show();

    }
    reader.readAsDataURL(input.files[0]);
    }
  }
</script>