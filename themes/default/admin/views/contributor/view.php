<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if(canaccess("contributor","view_access") != 'true'){ echo "<script>window.location.href ='".site_url()."admin'</script>";}?>

<style type="text/css"> 
	.search {
	    width: 17%;
	}
</style>

<div class="wrapper">

    <div class="container">
		
		<div class="row">

	        <div class="col-sm-12">
	        	
	            <div class="pull-right  m-b-15">
	            	<?php if(canaccess("contributor","create_access")){ ?> 	
	            	<button type="button" class="btn btn-info" status="none" id="advanced_search_btn"><span class="glyphicon glyphicon-search "></span></button>
	            	
	           	       <?php } ?> 		
	       

	            	<a href="<?= base_url("admin/contributor"); ?>/unset_session_value" id="unset_button"><button type="button" class="btn btn-primary" status="none"><span class="glyphicon glyphicon-refresh"></span></button></a>
	            	
	            </div>

	            <h4 class="page-title">Contributor</h4>
	        </div>
	    </div>

	    <div class="row" id="advanced_search_div" 
		      <?php if(empty($_REQUEST)) { ?>

		    	<?php if(!empty($this->session->userdata('user_serach_data'))) { ?>
		    		style="display: block;"
		    	<?php }else{ ?>
					style="display: none;"
		    	<?php  } ?>
		    
			<?php } ?>

      	>
		    <div class="col-xs-12 col-md-12 col-lg-12">

		    <div class="panel panel-default">

		        <div class="panel-body">

		            <div class="fixed-table-toolbar">

		                <form name="searchfrom" id="searchfrom" method="post"  action='<?= base_url("admin/contributor"); ?>' enctype="multipart/form-data">

		                    <div class="columns btn-group pull-right margin">

		                    	<div class="searchdatetitle">&nbsp;</div>
		                     
		                     	<input type="hidden" name="SearchValue" id="SearchValue">

		                     	<a onclick="formSubmit('search')" href="javascript:void();"><button type="button" class="btn btn-success" status="none">Search</button></a>
		                    </div>

		                    <!-- <?php if(isset($_REQUEST['user_s_status']) AND $_REQUEST['user_s_status'] != '') {
	                                $st = $_REQUEST['user_s_status'];
	                            } else {
	                                $st = '';
	                            }
	                        ?> -->
		                  
		                    <div class="pull-left search margin m-r-15">
		                    	<div class="searchdatetitle">Business Name</div>
		                     		<input class="form-control" type="text" placeholder="Any Text.." name="business_s_name" id="business_s_name" value="<?php if(isset($_REQUEST['business_s_name'])) 
		                     			{ 
		                     				echo $_REQUEST['business_s_name']; 
		                     			} 
		                     			else 
		                     			{ 
		                     				if($this->session->userdata('business_s_name')) 
		                     				{ 
		                     					echo $this->session->userdata('business_s_name'); 
		                     				} 
		                     			}
             							?>">
		                    </div>	                 

		                    <div class="pull-left search margin m-r-15">
		                    	<div class="searchdatetitle">Mobile</div>
		                     	<input class="form-control" type="text" placeholder="Any Text.." name="user_s_mobile" id="user_s_mobile" value="<?php if(isset($_REQUEST['user_s_mobile'])) 
		                     			{ 
		                     				echo $_REQUEST['user_s_mobile']; 
		                     			} 
		                     			else 
		                     			{ 
		                     				if($this->session->userdata('user_s_mobile')) 
		                     				{ 
		                     					echo $this->session->userdata('user_s_mobile'); 
		                     				} 
		                     			}
             							?>">
		                  	</div>
		                  
		                    <div class="pull-left search margin m-r-15">
		                     	<div class="searchdatetitle">City</div>
		                     	<select id="city_s_search" name="city_s_search" class="form-control">
		                     		<option value="">Select City</option>
		              				<?php foreach ($cities as $key => $city): ?>
		              				<option 
		              					<?php 
		              					if(isset($_REQUEST['city_s_search']))
		              					{
			              					if($_REQUEST['city_s_search'] == $city['id'])
			              					{
			              						echo "selected";
			              					}	
		              					}else{
		              						if($this->session->userdata('city_s_search')) 
			              					{
												if($this->session->userdata('city_s_search') ==  $city['id'])
				              					{
				              						echo "selected"; 
				              					}				                            	
				                            }
		              					}
		              					?>
		              					value="<?= $city['id']; ?>"><?= $city['city']; ?></option>
		              					<?php endforeach ?>
		                      
		                     	</select>
		                  	</div>

		                  
		                   	<div class="pull-left search margin m-r-15">
		                     	<div class="searchdatetitle">Bisuness Type</div>
		                     	<select  id="business_s_type" name="business_s_type" class="form-control">
		                        	<option value="">Select Status</option>
		                        	<option
		                        		<?php 
		              					if(isset($_REQUEST['business_s_type']))
		              					{
		              					if($_REQUEST['business_s_type'] == 'salon')
		              					{
		              						echo "selected"; 
		              					}	
		              					}
		              					?>
		              					
		              					<?php 
		              					if($this->session->userdata('business_s_type')) 
		              					{
											if($this->session->userdata('business_s_type') == 'salon')
			              					{
			              						echo "selected"; 
			              					}				                            	
			                            }
		                  				?>
				                        value="salon" >salon</option>
				                        <option 
				                        <?php 
		              					if(isset($_REQUEST['business_s_type']))
		              					{
		              					if($_REQUEST['business_s_type'] == 'spa')
		              					{
		              						echo "selected"; 
		              					}	
		              					}
		              					?>
		              					
		              					<?php 
		              					if($this->session->userdata('business_s_type')) 
		              					{
											if($this->session->userdata('business_s_type') == 'spa')
			              					{
			              						echo "selected"; 
			              					}				                            	
			                            }
		                  				?>
				                        value="spa" >Spa</option>
		                        </select>
		                    </div>         
		                 
		               </form>
		            </div>
		        </div>
		    </div>
		</div>
		</div>
	    
	    
      	<!-- Page-Title -->
        <div class="row">

            <div class="col-sm-12 card-box">
                 <div class="table-responsive" data-pattern="priority-columns">

                    <table id="tech-companies-1" class="table  table-striped">
                	<thead>
                		<th>#</th>                        
                		<th>Name</th>
                		<th>Mobile</th>
                		<th>Email</th>                		
                		<th>last otp</th>                		
                		<th>Type</th>
                		<th>Contributor Type</th>                		               		
                		<th><?= lang('status'); ?></th>
                		
                		  <?php if(canaccess("employee","edit_access") == 'true' || canaccess("employee","delete_access") == 'true' ){ ?>	
                		<th><?= lang('actions'); ?></th> <?php } ?>                		
                	</thead>

                	<tbody>
                		<?php 

                		$count = 1;if($this->uri->segment(3) != ""){ $count = $this->uri->segment(3) + 1; } foreach ($user as $sam) : ?>
                		<?php 
			    		$CI     = & get_instance();
					        
			    		?>
	        					<tr>
	        						<td><?=$count++;?></td>
									<td><?= $sam->name; ?></td>
									<td><?= $sam->mobile; ?></td>
									<td><?= $sam->email_id; ?></td>
									<td><?= $sam->otp; ?></td>									
									<td><?= $sam->type; ?></td>
									<td><?= $sam->contributor_type; ?></td>						
									<td><span class="label label-<?php echo ($sam->status == 'active') ? 'success' : 'danger'?>"><?= ucfirst($sam->status); ?></span></td>									
									<td>
									
									<?php if(canaccess("contributor","edit_access")){ ?>	<a href='<?= base_url("admin/contributor/edit/{$sam->id}"); ?>'><button class="btn btn-icon btn-xs waves-effect waves-light btn-purple" data-toggle="tooltip" data-placement="left" title="" data-original-title="<?= lang('edit'); ?>"> <i class="fa fa-edit"></i> </button></a> <?php } ?>



									<?php if(canaccess("contributor","delete_access")){ ?>		<a class="sa-params" href='javascript:void(0)' id="<?= $sam->id ?>"><button class="btn btn-icon btn-xs waves-effect waves-light btn-danger" data-toggle="tooltip" data-placement="right" title="" data-original-title="<?= lang('delete'); ?>"> <i class="fa fa-remove"></i> </button></a>
									<?php } ?>	
									</td>

								</tr>
						<?php endforeach; ?>
                	</tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="row">
        	<div class="col-md-12">
        		<div class="pull-right">
        			<?php echo $this->pagination->create_links(); ?>
        		</div>
        	</div>
        </div>

 		<!--  Modal content for the above example -->
		<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">

    		<div class="modal-dialog modal-lg">

        		<div class="modal-content">

        			<form name="user_add" id="user_add" method="post" enctype="multipart/form-data">

	            		<div class="modal-header">
	                		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	                		<h4 class="modal-title" id="myLargeModalLabel">Add Contributor</h4>
	            		</div>

	            		<div class="modal-body">
	             
	            			<div class="row">           
    
            					<div class="col-md-4">
                					<div class="form-group">
                    					<label for="name"><?= lang('name'); ?><span class="text-danger">*</span></label>
                    					<input type="text"  name="name" id="name" class="form-control"  autofocus placeholder="<?= lang('name');?>">
                					</div>
            					</div>

            					<div class="col-md-4 hide">
                					<div class="form-group">
                    					<label for="sname"><?= lang('sname'); ?><span class="text-danger">*</span></label>
                    					<input type="text" name="sname"  id="sname" class="form-control" placeholder="<?= lang('sname');?>">
                					</div>
            					</div>

             					<div class="col-md-4 hide">    
                					<div class="form-group">
                    					<label for="code">Bisuness Type</label>
                    					<select id="business_type" name="business_type" class="form-control">
                        					<option value="salon">Salon</option>
                        					<option value="spa">Spa</option>
                    					</select>
                					</div>
            					</div>          

          					</div>

          					<div class="row">         

            					<div class="col-md-4">
            						<div class="form-group">
                    					<label for="business_name"><?= lang('Business_name'); ?><span class="text-danger">*</span></label>
                    					<input type="text" required="required" name="business_name" id="business_name" class="form-control"  autofocus placeholder="<?= lang('business_name');?>">
                					</div>
            					</div>

            					<div class="col-md-4 hide">
            						<div class="form-group">
                    					<label for="number_of_seats"><?= lang('Number_of_seats'); ?><span class="text-danger">*</span></label>
                    					<input type="text"  name="number_of_seats" id="number_of_seats" class="form-control" autofocus placeholder="<?= lang('number_of_seats');?>">
                					</div>
            					</div>

          						<div class="col-md-4 ">
                					<div class="form-group">
                    					<label for="mobile"><?= lang('mobile'); ?></label>
                    					<input type="number" required="required" onKeyPress="if(this.value.length==10) return false;" name="mobile" id="mobile" class="form-control" placeholder="<?= lang('mobile');?>" >
                					</div>
            					</div>

            				</div>           
            
           					<div class="row">

            					<div class="col-md-4 hide">
                					<div class="form-group">
                    					<label for="email"><?= lang('email'); ?><span class="text-danger">*</span></label>
                    					<input type="email" name="email" id="email" class="form-control" placeholder="<?= lang('email');?>" >
                					</div>
            					</div>            
	              	
          						<div class="col-md-4 hide">
          							<div class="form-group">
          								<label for="pwd"><?= lang('pwd'); ?><span class="text-danger">*</span></label>
          								<input type="password" name="pwd"  id="pwd" class="form-control" placeholder="<?= lang('pwd');?>">
          							</div>
          						</div>

          						<div class="col-md-4 hide">
          							<div class="form-group">
          								<label for="cpwd"><?= lang('cpwd'); ?><span class="text-danger">*</span></label>
          								<input type="password" name="cpwd" data-parsley-equalto="#pwd"  id="cpwd" class="form-control" placeholder="<?= lang('cpwd');?>">
          							</div>
          						</div>
        
        						<div class="col-md-4">
            						<div class="form-group">
                						<label for="name">City<span class="text-danger">*</span></label>
                
                 						<select  name="city" id="city" class=" form-control" data-style="btn-white"  >
                    						<?php foreach ($cities as $key => $value): ?>
                      						<option  value="<?= $value['id'] ?>"><?= $value['city'] ?></option>    
                    						<?php endforeach ?>
                						</select>
            						</div>
        						</div>

            					<div class="col-md-4 ">
            						<div class="form-group">
                    					<label for="area">Area<span class="text-danger">*</span></label>
                    					<input required="required" type="text"name="area" id="area" class="form-control" autofocus placeholder="Area">
                					</div>
            					</div>
            
            
            					<div class="col-md-4 hide">
            						<div class="form-group">
                    					<label for="area">latitude<span class="text-danger">*</span></label>
                    					<input type="text"  name="latitude" id="latitude" class="form-control" autofocus placeholder="latitude">
                					</div>
            					</div>
            
            					<div class="col-md-4 hide">
            						<div class="form-group">
                    					<label for="area">longitude<span class="text-danger">*</span></label>
                    					<input type="text" name="longitude" id="longitude" class="form-control" autofocus placeholder="longitude">
                					</div>
            					</div>
            
            
                				<div class="col-md-4 hide"> 
            						<div class="form-group">
                						<label for="name">Packages<span class="text-danger">*</span></label>
                
                 						<select  name="plan_id" id="plan_id" class=" form-control" data-style="btn-white"  >
                     						<option  value="">Select Plan </option>    
                    						<?php foreach ($plans as $key => $plan): ?>
                      						<option  value="<?= $plan['id'] ?>"><?= $plan['plan_name'] ?> 
                      						</option>    
                    						<?php endforeach ?>
                						</select>
            						</div>
        						</div>       

          					</div>       
        
         					<div class="row ">

        						<div class="col-md-4 hide">
            						<div class="form-group">
                    					<label for="about">About<span class="text-danger">*</span></label>
                    					<textarea  name="about" id="about" class="form-control" placeholder="About"  ></textarea>
                					</div>
            					</div>

             					<div class="col-md-4">
                    				<div class="form-group">
                   						<label for="business_address"><?= lang('Business Address'); ?></label>
                    					<textarea required="required" name="business_address" id="business_address" class="form-control" placeholder="<?= lang('Business Address');?>"></textarea>
                					</div>
                				</div>
                
                  				<div class="col-md-4 ">
            						<div class="form-group">
               							<label for="business_address">Remark</label>
                						<textarea name="remarks" id="remarks" class="form-control" placeholder="Remark"></textarea>
            						</div>
            					</div>

        					</div>             

	            		</div>

	            		<div class="modal-footer"> 
	            			<button type="button" class="btn btn-inverse waves-effect waves-light" data-dismiss="modal" aria-hidden="true">Close</button>
	            			<button type="submit" id="save" class="btn btn-success waves-effect waves-light">Save</button>
	            		</div>

            		</form>
        		</div><!-- /.modal-content -->
    		</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	</div>
</div>


<script>

	function MyNotify(type,msg)
	{
		$.Notification.notify('error','top right',type, msg);
	}

	function readURL(input)
	{

	    if (input.files && input.files[0]) {
	        var reader = new FileReader();

	        reader.onload = function (e) {
	            $('#img').removeClass('hide').attr('src', e.target.result);
	        }

	        reader.readAsDataURL(input.files[0]);
	    }
	}

	$(document).ready(function() {

		// image display logic
		$("[name='usersign']").change(function(){
		    readURL(this);
		});	
    

		$('#user_add').on('submit',function(e) {
			e.preventDefault();
			if($('#business_name').val() == '') {
				//MyNotify('<?php echo lang('error'); ?>','<?php echo lang('fnamereq'); ?>');
				return false;
			}
			if($('#mobile').val() == '') {
				//MyNotify('<?php echo lang('error'); ?>','<?php echo lang('snamereq'); ?>');
				return false;
			}
			if($('#area').val() == '') {
				//MyNotify('<?php echo lang('error'); ?>','<?php echo lang('emailreq'); ?>');
				return false;
			}
			if($('#business_address').val() == '') {
				//MyNotify('<?php echo lang('error'); ?>','<?php echo lang('emailreq'); ?>');
				return false;
			}
			$.ajax({
				url: "<?php echo site_url('modal/user_add')?>/",
				type: "POST",
				data: new FormData(this),

				// Tell jQuery not to process data or worry about content-type
        		// You *must* include these options!
				cache: false,
        		contentType: false,
        		processData: false,
        		dataType: 'json',
				success: function(response) {
					// hide modal
					
					 if(response['status'] == 2)
                      {
                           MyNotify('<?php echo lang('error'); ?>','This Email Id Already Exist');
                           return false;
                          
                      }else{
            
                        $('.bs-example-modal-lg').modal('hide');
                        $.Notification.notify('success','top right','<?php echo lang('success'); ?>', 'Successfully Created .');
                        setTimeout(function() {
                          location.reload();
                        },1000);
                         
                      }
					
					
				},
				error: function() {
					alert('error');
				}
			});
	});

		// SwwetALert
		//Parameter
    $('.sa-params').click(function () 
    {
        	var id = $(this).attr('id');
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger m-l-10',
                buttonsStyling: false
            }).then(function () {
            	// call ajax function to delete it
            	$.ajax({
            		type: "POST",
            		url: "<?php echo site_url('modal/user_delete')?>/"+id,
            		success: function(data) {
            			swal({
            				title: 'Deleted!',
            				text: 'Data has been deleted.',
            				type: 'success'
            			}).then(function() {
            				location.reload();
            			});
            		},
					error: function() {
						alert('error');
					}
            	})
            }, function (dismiss) {
                // dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
            })
        });

	    $("#advanced_search_btn").click(function(){
		    $("#advanced_search_div").slideToggle();
		});
		   
	});
	
	function formSubmit(type)
    {
		if(type == 'Search')
		{
			$('#SearchValue').val(type);
		}else{
			$('#SearchValue').val(type);
		}
         document.getElementById("searchfrom").submit();
    } 

    function ImagePreview(input,image_preview) 
  	{
    	if (input.files && input.files[0]) 
    	{
	      	var reader = new FileReader();
	      	reader.onload = function(e) {
	        $('#'+image_preview).attr('src', e.target.result);
	        $('#'+image_preview).show();
    		}
    		reader.readAsDataURL(input.files[0]);
    	}
  	}

</script>  