<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if(canaccess("smtp_settings","view_access") != 'true'){ echo "<script>window.location.href ='".site_url()."admin'</script>";}?>
<style type="text/css">
    .form-group {
    margin-bottom: 0px;
}
</style>
<div class="wrapper">
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-color panel-inverse">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?= $page_title ?></h3> 
                    </div>
                    <form autocomplete="off" name="syssettings" id="syssettings" method="post"  action='<?= base_url("admin/email_setting/"); ?>' enctype="multipart/form-data">
                    <div class="panel-body">
                        
                        <div class="row">
                            <div class="col-md-6">
                                <label for="smtp_status"><?= lang('SMTP Status'); ?><span class="text-danger">*</span></label>
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <div class="form-group radio radio-success" style="margin-top: 0px;">
                                            <input <?php echo ($setting->smtp_status == 'on') ? 'checked="checked"' : ''?>  type="radio" name="smtp_status" value="on" id="on">
                                            <label for="on"><?= lang('ON'); ?></label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                         <div class="form-group radio radio-danger" style="margin-top: 0px;">
                                             <input <?php echo ($setting->smtp_status == 'off') ? 'checked="checked"' : ''?>  type="radio" name="smtp_status" value="off" id="off">
                                            <label for="off"><?= lang('OFF'); ?></label>
                                        </div>
                                    </div>
                                    <label class="text-danger"><?= form_error('smtp_status'); ?></label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="smtp_encryption"><?= lang('SMTP Encryption'); ?><span class="text-danger">*</span></label>
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <div class="form-group radio radio-success" style="margin-top: 0px;">
                                            <input <?php echo ($setting->smtp_encryption == 'TLS') ? 'checked="checked"' : ''?>  type="radio" name="smtp_encryption" value="TLS" id="TLS">
                                            <label for="TLS"><?= lang('TLS'); ?></label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                         <div class="form-group radio radio-success" style="margin-top: 0px;">
                                             <input <?php echo ($setting->smtp_encryption == 'SSL') ? 'checked="checked"' : ''?>  type="radio" name="smtp_encryption" value="SSL" id="SSL">
                                            <label for="SSL"><?= lang('SSL'); ?></label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                         <div class="form-group radio radio-danger" style="margin-top: 0px;">
                                             <input <?php echo ($setting->smtp_encryption == 'None') ? 'checked="checked"' : ''?>  type="radio" name="smtp_encryption" value="None" id="None">
                                            <label for="None"><?= lang('None'); ?></label>
                                        </div>
                                    </div>
                                    <label class="text-danger"><?= form_error('smtp_encryption'); ?></label>
                                </div>
                            </div>

                        </div>
                        <br>

                        <div class="row">
                             
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="smtp_username">SMTP Username <span class="text-danger">*</span></label>
                                    <input type="text" name="smtp_username" class="form-control" value="<?= $setting->smtp_username; ?>" required="required">
                                
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="smtp_password">SMTP Password<span class="text-danger">*</span></label>
                                    <input type="password" required="required" name="smtp_password" id="smtp_password"  class="form-control" placeholder="SMTP Password" value="<?= $setting->smtp_password; ?>">
                                    <label class="text-danger"><?= form_error('smtp_password'); ?></label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                             <div class="col-md-6">
                                <div class="form-group">
                                    <label for="smtp_name">SMTP Name</label>
                                    <input type="text" placeholder="SMTP Name" required="required" name="smtp_name" id="smtp_name"  class="form-control" value="<?= $setting->smtp_name; ?>">
                                    <label class="text-danger"><?= form_error('smtp_name'); ?></label>
                                </div>
                            </div>
                            
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="smtp_host"><?= lang('SMTP Host'); ?><span class="text-danger">*</span></label>
                                    <input type="text" required="required" name="smtp_host" id="smtp_host"  class="form-control" placeholder="SMTP Host" value="<?= $setting->smtp_host; ?>">
                                    <label class="text-danger"><?= form_error('smtp_host'); ?></label>
                                </div>
                            </div>
                            
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="smtp_port">SMTP Port<span class="text-danger">*</span></label>
                                    <input type="number" required="required" name="smtp_port" id="smtp_port" placeholder="SMTP Port" class="form-control" value="<?= $setting->smtp_port; ?>">
                                    <label class="text-danger"><?= form_error('smtp_port'); ?></label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="panel-footer">
                        <a href="<?= base_url('admin'); ?>"><button type="button" class="btn btn-inverse waves-effect waves-light pull-right" data-dismiss="modal" aria-hidden="true">Close</button></a>
                        <span class="pull-right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <button type="submit" id="save" class="btn btn-success waves-effect waves-light pull-right">Save</button>
                        <div class="clearfix"></div>
                    </div>
                </form>
                </div>
            </div>
        </div>

        