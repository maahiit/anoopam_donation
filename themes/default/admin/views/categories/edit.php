<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php 
    if(canaccess("categories","edit_access") != 'true')
    { 
        echo "<script>window.location.href ='".site_url()."admin'</script>";
    }
?>

<div class="wrapper">

    <div class="container">

        <div class="row">

            <div class="col-lg-12">

                <div class="panel panel-color panel-inverse">

                    <div class="panel-heading">
                        <h3 class="panel-title"><?= $page_title ?></h3>
                    </div>

                    <form name="customer_edit" id="customer_edit" method="post"  action='<?= base_url("admin/categories/edit/{$row->id}"); ?>' enctype="multipart/form-data">

                        <div class="panel-body">
            
                            <div class="row">

                                <input type="hidden" name="id" value="<?= $row->id; ?>">
        
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="name">Name<span class="text-danger">*</span></label>
                                        <input type="text" required="required" name="name" id="name" class="form-control" placeholder="Name" value="<?=  $row->name; ?>">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="minimum_amount">Minimum Amount<span class="text-danger">*</span></label>
                                        <input type="number" required="required" name="minimum_amount" id="minimum_amount" class="form-control" placeholder="Minimum Amount" value="<?=  $row->minimum_amount; ?>">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="maximum_amount">Maximum Amount<span class="text-danger">*</span></label>
                                        <input type="number" required="required" name="maximum_amount" id="maximum_amount" class="form-control" placeholder="Maximum Amount" value="<?=  $row->maximum_amount; ?>">
                                    </div>
                                </div>

                            </div>

                                <?php $payment_gateway_arr = explode(',',$row->payment_gateway); ?>

                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="payment_gateway">Payment Gateway<span class="text-danger">*</span></label>
                                        <select  data-live-search="true" multiple="" class="selectpicker form-control" name="payment_gateway[]" id="payment_gateway" data-style="btn-white">

                                            <option value="">Select Payment Gateway</option>

                                            <?php foreach ($payment_gateways as $payment_gateway):?>
    <option <?php if(in_array($payment_gateway->id,$payment_gateway_arr)) { echo "selected"; } ?> value="<?= $payment_gateway->id ?>"><?= $payment_gateway->name?></option>
                                            <?php endforeach;?>
                            
                                        </select>
                        
                                    </div>
                                </div>

                                <?php $country_id_arr = explode(',',$row->country_id); ?>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="country_id">Country<span class="text-danger">*</span></label>
                                        
                                        <select  data-live-search="true" multiple="" class="selectpicker form-control" name="country_id[]" id="country_id" data-style="btn-white">
                                            <option value="">Select Country</option>
                                            <?php foreach($countries as $country) : ?>
                                                <option 
                                                <?php if(in_array($country->id,$country_id_arr)) { echo "selected"; } ?>  value="<?= $country->id?>"><?= $country->name?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                

                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="ssl_tls">Trust fund<span class="text-danger">*</span></label>   <div class="form-group">
                                    
                                        <input <?php if ($row->trustfund  == 'yes')  { echo "checked"; } ?> type="radio" name="trustfund" value="yes" id="trustfund_yes">
                                        <label for="trustfund_yes"><?= lang('Yes'); ?></label>            

                                        <input <?php if ($row->trustfund  == 'no')  { echo "checked"; } ?> type="radio" name="trustfund" value="no" id="trustfund_no">
                                        <label for="trustfund_no"><?= lang('No'); ?></label>
                                            
                                        </div>

                                    </div>
                                </div>
                                
                                <div class="col-md-4 <?php if ($row->trustfund  == 'no')  { echo "hide"; } ?>" id="trustfund_amount_ui">
                                    <div class="form-group">
                                        <label for="maximum_amount">Trustfund minimum amount<span class="text-danger">*</span></label>
                                        <input type="number"  name="trustfund_minimum_amount" id="trustfund_minimum_amount" class="form-control" placeholder="Maximum Amount" value="<?= $row->trustfund_minimum_amount ?>"> 
                                    </div>
                                </div>  
                            </div>

                            <div class="row">                                

                                <div class="col-md-4">                
                                    <div class="form-group checkbox checkbox-danger" style="margin-top: 30px;">
                                        <input <?php echo ($row->status == 'inactive') ? 'checked="checked"' : ''?>  type="checkbox" name="status" value="inactive" id="status">
                                        <label for="status"><?= lang('inactive'); ?></label>
                                    </div>
                                </div>

                            </div>      

                            

                        </div>

                        <div class="panel-footer">
                            <button type="submit" id="save" class="btn btn-success waves-effect waves-light pull-right">Save</button>
                            <span class="pull-right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            <a href="<?= base_url('admin/categories'); ?>"><button type="button" class="btn btn-inverse waves-effect waves-light pull-right" data-dismiss="modal" aria-hidden="true">Close</button></a>                        
                        
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
         
    $(document).on('click','#remove_field',function()
    {
        $(this).closest("#row_remove").remove();
    });    

    $(document).ready(function(){
        $('input[name="trustfund"]:radio').change(function ()
        {
            if($('input[name=trustfund]:checked').val() == 'yes')
            {
                $('#trustfund_amount_ui').removeClass('hide');
            }else{
                $('#trustfund_amount_ui').addClass('hide');
            }

        });
    });

</script>