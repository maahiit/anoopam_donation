<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<style type="text/css">
	.search {
	    width: 17%;
	}

	label {
	    display: inline-block;
	    max-width: 100%;
	    margin-bottom: 5px;
	    font-weight: 700;
	    margin-right: 19px;
	}
</style>

<div class="wrapper">

    <div class="container"> 

		<div class="row">

	        <div class="col-sm-12">
	            <div class="pull-right ">
	                <button type="button" class="btn btn-info" status="none" id="advanced_search_btn"><span class="glyphicon glyphicon-search "></span></button>
	           <?php if(canaccess("categories","create_access")){ ?>  	
	           <button type="button" id="add" data-toggle="modal" data-target=".bs-example-modal-lg" class="btn btn-purple waves-effect waves-light" >Add New</button>
	           <?php } ?>
	           
	            <a href="<?= base_url("admin/categories/"); ?>unset_session_value" id="unset_button"><button type="button" class="btn btn-primary  m-r-5" status="none"><span class="glyphicon glyphicon-refresh"></span></button></a>		
	            	
	            </div>
	        	
	            <h4 class="page-title"><?= $page_title ?></h4>
	        </div>

	    </div>
	    <br>

	    <div class="row" id="advanced_search_div"
	    
		    <?php if(empty($_REQUEST)) //trim(str)
		    	  { ?>

		    		<?php if(!empty($this->session->userdata('categories_serach_data'))) 
		    			  { ?>
		    					style="display: block;"

		    		<?php 
						  }else
						  { ?>
								style="display: none;"

		    			  <?php  } ?>
		    
			<?php } ?>
		>
		    
		    
		   	<div class="col-xs-12 col-md-12 col-lg-12">

		    <div class="panel panel-default">

		        <div class="panel-body">

		            <div class="fixed-table-toolbar">
 
		               <form name="searchfrom" id="searchfrom" method="post"  action='<?= base_url("admin/categories"); ?>' enctype="multipart/form-data">

		                  	<div class="columns btn-group pull-right margin">
			                    <div class="searchdatetitle">&nbsp;</div>
			                    <button type="submit" name="filter" id="filter" class="btn btn-success" value="filter">Search</button>
		                  	</div>
		                  
		                  	<div class="pull-left search margin m-r-15">
		                    	<div class="searchdatetitle">Name</div>
		                    	<input class="form-control" type="text" placeholder="Any Text.." name="categories_s_name" id="categories_s_name" value="<?php if(isset($_REQUEST['categories_s_name'])) 
		                     			{ 
		                     				echo $_REQUEST['categories_s_name']; 
		                     			} 
		                     			else 
		                     			{ 
		                     				if($this->session->userdata('categories_s_name')) 
		                     				{ 
		                     					echo $this->session->userdata('categories_s_name'); 
		                     				} 
		                     			}
             					?>">
		                  	</div>
		                  
		               </form>		               
		            </div>
		        </div>
		    </div>
		   	</div>
		</div>
	   
      	<!-- Page-Title -->
        <div class="row">

           <div class="col-sm-12 table-responsive card-box">
                <table class="table table-hover table-condensed table table-striped" id="tech-companies-1">
                	<thead>
                		<th width="5%">#</th>
                		<th>Name</th>
						<th>Payment Getway</th>
                		<th>Country</th>
                		<th>Min Amount</th>
                		<th>Max Amount</th>
                		<th width="10%">Status</th>
                		<th width="10%"><?= lang('actions'); ?></th>
                	</thead>
                	<tbody>
                		 <?php  $CI =& get_instance(); $CI->load->library('sam'); ?>
                		<?php $count = 1; foreach ($rows as $rowsMst) : ?>
						<tr>
							<td><?=$count++;?></td>
							<td><?= ucfirst($rowsMst->name); ?></td>
							 <td>
							 	<?php
				                if(!empty($rowsMst->payment_gateway))
				                {
				                	$paymentgateway_name = '';
				                	foreach(explode(',',$rowsMst->payment_gateway) as $paymentgateway)
				                	{
				                	   $paymentgateway_name .= $CI->sam->get_real_value('payment_gateway','id',$paymentgateway,'name').' | ';  
				                	}
				                	echo substr($paymentgateway_name,0,-2);
				                }
				                ?>
							 </td>
							 <td>
							 	 <?php
				                if(!empty($rowsMst->country_id))
				                {
				                	$country_name = '';
				                	foreach(explode(',',$rowsMst->country_id) as $countryid)
				                	{
				                	   $country_name .= $CI->sam->get_real_value('zyd_country','id',$countryid,'name').' | ';  
				                	}
				                	echo substr($country_name,0,-2);
				                }
				                ?>
							 </td>
							<td><?= number_format($rowsMst->minimum_amount); ?></td>
							<td><?= number_format($rowsMst->maximum_amount); ?></td>
							<td><span class="label label-<?php echo ($rowsMst->status == 'active') ? 'success' : 'danger'?>"><?= ucfirst($rowsMst->status); ?></span></td>	
							<td>
							<?php if(canaccess("categories","edit_access")){ ?> <a href='<?= base_url("admin/categories/edit/{$rowsMst->id}"); ?>'><button class="btn btn-icon btn-xs waves-effect waves-light btn-purple" data-toggle="tooltip" data-placement="left" title="" data-original-title="<?= lang('edit'); ?>"> <i class="fa fa-edit"></i> </button></a>
							<?php } ?>
							<?php if(canaccess("categories","delete_access")){ ?>		<a class="sa-params" href='javascript:void(0)' id="<?= $rowsMst->id ?>"><button class="btn btn-icon btn-xs waves-effect waves-light btn-danger" data-toggle="tooltip" data-placement="right" title="" data-original-title="<?= lang('delete'); ?>"> <i class="fa fa-remove"></i> </button></a> 
							<?php } ?>	


							</td>

						</tr>
				<?php endforeach; ?>
                	</tbody>
                </table>
            </div>
        </div>
        <br>

        <div class="row">
          <div class="col-md-12">
            <div class="pull-right">
              <?php echo $this->pagination->create_links(); ?>
            </div>
          </div>
        </div>


 		<!--  Modal content for the above example -->
		<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">

    		<div class="modal-dialog modal-md">

        		<div class="modal-content">

        			<form name="add_form" id="add_form" method="post" enctype="multipart/form-data">

	            		<div class="modal-header">
	                		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	                		<h4 class="modal-title" id="myLargeModalLabel">Add <?= $page_title ?></h4>
	            		</div>

	            		<div class="modal-body">

	              			<div class="row">
	              	
	              				<div class="col-md-6">
	              					<div class="form-group">
	              						<label for="name">Name<span class="text-danger">*</span></label>
	              						<input type="text" required="required" name="name" id="name" class="form-control" placeholder="Name">
	              					</div>
	              				</div>              				
	              			</div>	              			

	              			<div class="row">

	              				<div class="col-md-6">
	              					<div class="form-group">
	              						<label for="payment_gateway">Payment Gateway<span class="text-danger">*</span></label>					

	              						<select  data-live-search="true" multiple="" class="selectpicker form-control" name="payment_gateway[]" id="payment_gateway" data-style="btn-white">
	              							<option value="">Select Payment Gateway</option>
	              							<?php foreach ($payment_gateways as $payment_gateway) : ?>           							
	              							<option value="<?= $payment_gateway->id?>"><?= $payment_gateway->name?></option>
	              							<?php endforeach; ?>
	              						</select>            						

	              					</div>
	              				</div>

	              				<div class="col-md-6">
	              					<div class="form-group">
	              						<label for="country_id">Country<span class="text-danger">*</span></label>	              						

	              						<select  data-live-search="true" multiple="" class="selectpicker form-control" name="country_id[]" id="country_id" data-style="btn-white" >
	              							<option value="">Select Country</option>
	              							<?php foreach ($countries as $country) : ?>            							
	              							<option value="<?= $country->id;?>"><?= $country->name;?></option>
	              							<?php endforeach; ?>
	              						</select>   						

	              						
	              					</div>
	              				</div>
	              				
	              			</div>

	              			<div class="row">

	              				<div class="col-md-6"> 
	              					<div class="form-group">
	              						<label for="minimum_amount">Minimum Amount<span class="text-danger">*</span></label>
	              						<input type="number" required="required" name="minimum_amount" id="minimum_amount" class="form-control" placeholder="Minimum Amount">
	              					</div>
	              				</div>

	              				<div class="col-md-6">
	              					<div class="form-group">
	              						<label for="maximum_amount">Maximum Amount<span class="text-danger">*</span></label>
	              						<input type="number" required="required" name="maximum_amount" id="maximum_amount" class="form-control" placeholder="Maximum Amount">
	              					</div>
	              				</div>              				

	              			</div>

	              			<div class="row">
	              				<div class="col-md-6">
			              			<div class="form-group">
			              				<label for="ssl_tls">Trust fund<span class="text-danger">*</span></label>	<div class="form-group">
					              	
					              		<input type="radio" name="trustfund" value="yes" id="trustfund_yes">
			                            <label for="trustfund_yes"><?= lang('Yes'); ?></label>            

			                            <input type="radio" name="trustfund" value="no" id="trustfund_no">
			                            <label for="trustfund_no"><?= lang('No'); ?></label>
				                            
				                        </div>

				                    </div>
		                        </div>
		                        
		                        <div class="col-md-6 hide" id="trustfund_amount_ui">
	              					<div class="form-group">
	              						<label for="maximum_amount">Trustfund minimum amount<span class="text-danger">*</span></label>
	              						<input type="number" required="required" name="trustfund_minimum_amount" id="trustfund_minimum_amount" class="form-control" placeholder="Maximum Amount">
	              					</div>
	              				</div>  
	              			</div>

	              			<div class="row">
	              				
	              				<div class="col-md-6">
	              					<div class="form-group">
	              						<label for="status">Status<span class="text-danger">*</span></label>
		              					<div class="form-group checkbox checkbox-danger" style="margin-top: 30px;">
	                                        <input <?php if( isset($status) && $status =='inactive') { echo "checked";} ?>  type="checkbox" name="status" value="inactive" id="status">
	                                        <label for="status"><?= lang('inactive'); ?></label>
	                                    </div>
	                                </div>
	              				</div>

	              			</div>

	              				              			

	            		</div>


	            		<div class="modal-footer">
			            	<button type="button" class="btn btn-inverse waves-effect waves-light" data-dismiss="modal" aria-hidden="true">Close</button>
			            	<button type="submit" id="save" class="btn btn-success waves-effect waves-light">Save</button>
	            		</div>

            		</form>
        		</div><!-- /.modal-content -->
    		</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	</div>
</div>

<script>

	function MyNotify(type,msg)
	{
		$.Notification.notify('error','top right',type, msg);
	}

	$(document).ready(function()
	{

		if($("#message").length > 0)
		{
              	tinymce.init({
                  selector: "textarea#message",
                  menubar:false,
                  theme: "modern",
                  height:300,
                  plugins: [
                      "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                      "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking",
                      "save table contextmenu directionality template paste textcolor"
                  ],
                  toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview fullpage | forecolor backcolor",
                  style_formats: [
                      {title: 'Bold text', inline: 'b'},
                      {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                      {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                      {title: 'Example 1', inline: 'span', classes: 'example1'},
                      {title: 'Example 2', inline: 'span', classes: 'example2'},
                      {title: 'Table styles'},
                      {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                  ]
              	});
        }		
		
		$('#add_form').on('submit',function(e)
		{
			e.preventDefault();
			
			if($('#name').val() == '' ||
				$('#payment_gateway').val() == '' ||
				$('#country_id').val() == '' ||
				$('#minimum_amount').val() == '' ||
				$('#maximum_amount').val() == '' 
				)
			{
				return false;
			}

			$.ajax({
				url: "<?php echo site_url('admin/categories')?>/add_row",
				type: "POST",
				data: new FormData(this),
				cache: false,
        		contentType: false,
        		processData: false,
				success: function(){
					// hide modal
					$('.bs-example-modal-lg').modal('hide');
					$.Notification.notify('success','top right','<?php echo lang('success'); ?>', 'Successfully Add New Record');
					setTimeout(function() {
						location.reload();
					},1000);
				},
				error: function() {
					alert('error');
				}
			});
		});

		// SwwetALert
		//Parameter
        $('.sa-params').click(function ()
        {
        	var id = $(this).attr('id');
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger m-l-10',
                buttonsStyling: false
            }).then(function ()
             	{
            	// call ajax function to delete it
            	$.ajax({
            		type: "POST",
            		url: "<?php echo site_url('admin/categories')?>/delete_row/"+id,
            		success: function(data) {
            			swal({
            				title: 'Deleted!',
            				text: 'Categories Successfully deleted.',
            				type: 'success'
            			}).then(function() {
            				location.reload();
            			});
            		},
					error: function() {
						alert('error');
					}
            	})
            	}, function (dismiss) {
                // dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
            	})
        });      

	});


	$("#advanced_search_btn").click(function()
	{
		$("#advanced_search_div").slideToggle();
	});    

	function ImagePreview(input,image_preview) 
	{
	  	if (input.files && input.files[0]) 
	  	{
		    var reader = new FileReader();
		    reader.onload = function(e) {
		    $('#'+image_preview).attr('src', e.target.result);
		    $('#'+image_preview).show();
	    	}
	    	reader.readAsDataURL(input.files[0]);
		}
	}

	 $(document).ready(function(){
        $('input[name="trustfund"]:radio').change(function ()
        {
	        if($('input[name=trustfund]:checked').val() == 'yes')
	        {
	        	$('#trustfund_amount_ui').removeClass('hide');
	        }else{
				$('#trustfund_amount_ui').addClass('hide');
	        }

        });
    });

</script>