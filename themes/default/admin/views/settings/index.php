<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if(canaccess("settings","view_access") != 'true'){ echo "<script>window.location.href ='".site_url()."admin'</script>";}?>

<style type="text/css">
  .form-group {
    margin-bottom: 0px;
}
</style>
<div class="wrapper">
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-color panel-inverse">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?= $page_title ?></h3>
                    </div>
                    <form name="syssettings" id="syssettings" method="post"  action='<?= base_url("settings/"); ?>' enctype="multipart/form-data">
                    <div class="panel-body">
                        
                        <div class="row">
                    
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="sitename"><?= lang('Sitename'); ?><span class="text-danger">*</span></label>
                                    <input type="text" required="required" name="sitename" id="sitename" class="form-control" value="<?= $setting->sitename; ?>" autofocus placeholder="<?= lang('sitename');?>">
                                    <label class="text-danger"><?= form_error('sitename'); ?></label>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="site_title"><?= lang('Site_Title'); ?><span class="text-danger">*</span></label>
                                    <input type="text" required="required" name="site_title" id="site_title" class="form-control" value="<?= $setting->site_title; ?>" autofocus placeholder="<?= lang('site_title');?>">
                                    <label class="text-danger"><?= form_error('site_title'); ?></label>
                                </div>
                            </div>

                             <div class="col-md-4">
                                <div class="form-group">
                                    <label for="site_url"><?= lang('Site_url'); ?><span class="text-danger">*</span></label>
                                    <input type="text" required="required" name="site_url" id="site_url" class="form-control" value="<?= $setting->site_url; ?>" autofocus placeholder="<?= lang('site_url');?>">
                                    <label class="text-danger"><?= form_error('site_url'); ?></label>
                                </div>
                          </div>

                        </div>

                        
                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="rows_per_page"><?= lang('rows_per_page'); ?><span class="text-danger">*</span></label>
                                    <input type="text" required="required" name="rows_per_page" id="rows_per_page"  class="form-control" placeholder="10" value="<?= $setting->rows_per_page; ?>">
                                    <label class="text-danger"><?= form_error('rows_per_page'); ?></label>
                                </div>
                            </div>

                          <div class="col-md-4">
                                <div class="form-group">
                                    <label for="Copyright"><?= lang('Copyright'); ?><span class="text-danger">*</span></label>
                                    <input type="text" required="required" name="copyright" id="copyright" class="form-control" value="<?= $setting->copyright; ?>" autofocus placeholder="<?= lang('copyright');?>">
                                    <label class="text-danger"><?= form_error('copyright'); ?></label>
                                </div>
                          </div>

                          <div class="col-md-4">
                                <div class="form-group">
                                    <label for="pre_fix_amount"><?= lang('Pre_fix_amount'); ?><span class="text-danger">*</span></label>
                                    <input type="text" required="required" name="pre_fix_amount" id="pre_fix_amount" class="form-control" value="<?= $setting->pre_fix_amount; ?>" autofocus placeholder="<?= lang('pre_fix_amount');?>">
                                    <label class="text-danger"><?= form_error('pre_fix_amount'); ?></label>
                                </div>
                          </div>

                        </div>

                        <div class="row">

                          <div class="col-md-4">
                                <div class="form-group">
                                    <label for="pan_amount"><?= lang('Pan_amount'); ?><span class="text-danger">*</span></label>
                                    <input type="text" required="required" name="pan_amount" id="pan_amount" class="form-control" value="<?= $setting->pan_amount; ?>" autofocus placeholder="<?= lang('pan_amount');?>">
                                    <label class="text-danger"><?= form_error('pan_amount'); ?></label>
                                </div>
                          </div>

                         

                        </div>

                           


                          <div class="row"><div class="col-md-12"><h3>SMS Getway</h3><hr></div></div>
                          <div class="row">

                             <div class="col-md-4">
                                <div class="form-group">
                                    <label for="sms_getway_url"><?= lang('SMS_getway_url'); ?><span class="text-danger">*</span></label>
                                    <input type="text" required="required" name="sms_getway_url" id="sms_getway_url" class="form-control" value="<?= $setting->sms_getway_url; ?>" autofocus placeholder="<?= lang('sms_getway_url');?>">
                                    <label class="text-danger"><?= form_error('sms_getway_url'); ?></label>
                                </div>
                          </div>

                          <div class="col-md-4">
                                <div class="form-group">
                                    <label for="sms_getway_api_url"><?= lang('SMS_getway_api_url'); ?><span class="text-danger">*</span></label>
                                    <input type="text" required="required" name="sms_getway_api_url" id="sms_getway_api_url" class="form-control" value="<?= $setting->sms_getway_api_url; ?>" autofocus placeholder="<?= lang('sms_getway_api_url');?>">
                                    <label class="text-danger"><?= form_error('sms_getway_api_url'); ?></label>
                                </div>
                          </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="sms_getway_api_key"><?= lang('SMS_getway_api_key'); ?><span class="text-danger">*</span></label>
                                    <input type="text" required="required" name="sms_getway_api_key" id="sms_getway_api_key" class="form-control" value="<?= $setting->sms_getway_api_key; ?>" autofocus placeholder="<?= lang('sms_getway_api_key');?>">
                                    <label class="text-danger"><?= form_error('sms_getway_api_key'); ?></label>
                                </div>
                          </div>

                          <div class="col-md-4">
                                <div class="form-group">
                                    <label for="sms_getway_api_secret_key"><?= lang('SMS_getway_api_secret_key'); ?><span class="text-danger">*</span></label>
                                    <input type="text" required="required" name="sms_getway_api_secret_key" id="sms_getway_api_secret_key" class="form-control" value="<?= $setting->sms_getway_api_secret_key; ?>" autofocus placeholder="<?= lang('sms_getway_api_secret_key');?>">
                                    <label class="text-danger"><?= form_error('sms_getway_api_secret_key'); ?></label>
                                </div>
                          </div>
                          </div>      

                          <div class="row"><div class="col-md-12"><h3>Google API</h3><hr></div></div>
                          <div class="row">

                          

                          <div class="col-md-4">
                                <div class="form-group">
                                    <label for="google_api_app_id"><?= lang('Google_api_app_id'); ?><span class="text-danger">*</span></label>
                                    <input type="text" required="required" name="google_api_app_id" id="google_api_app_id" class="form-control" value="<?= $setting->google_api_app_id; ?>" autofocus placeholder="<?= lang('google_api_app_id');?>">
                                    <label class="text-danger"><?= form_error('google_api_app_id'); ?></label>
                                </div>
                          </div>

                       

                          <div class="col-md-4">
                                <div class="form-group">
                                    <label for="google_api_key"><?= lang('Google_api_key'); ?><span class="text-danger">*</span></label>
                                    <input type="text" required="required" name="google_api_key" id="google_api_key" class="form-control" value="<?= $setting->google_api_key; ?>" autofocus placeholder="<?= lang('google_api_key');?>">
                                    <label class="text-danger"><?= form_error('google_api_key'); ?></label>
                                </div>
                          </div>

                          <div class="col-md-4">
                                <div class="form-group">
                                    <label for="google_api_secret_key"><?= lang('Google_api_secret_key'); ?><span class="text-danger">*</span></label>
                                    <input type="text" required="required" name="google_api_secret_key" id="google_api_secret_key" class="form-control" value="<?= $setting->google_api_secret_key; ?>" autofocus placeholder="<?= lang('google_api_secret_key');?>">
                                    <label class="text-danger"><?= form_error('google_api_secret_key'); ?></label>
                                </div>
                          </div>

                        </div>


                        <div class="row"><div class="col-md-12"><h3>Image</h3><hr></div></div>

                        <div class="row">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="sitelogo"><?= lang('Sitelogo'); ?></label>
                                    <input type="file" name="sitelogo" class="filestyle" id="sitelogo" data-size="sm">
                                </div>
                                 
                            </div>
                            <div class="col-md-1">
                              <img src="<?= base_url() ?>themes/default/admin/assets/upload/logos/<?= $setting->sitelogo; ?>" alt="" id="logo" class="img-responsive img-thumbnail thumb-lg">
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="sitelogo">Login Page Background Image</label>
                                    <input type="file" name="login_bg_image" class="filestyle" id="login_bg_image" data-size="sm">
                                </div>
                                 
                            </div>                                                        
                            <div class="col-md-1">
                              <img src="<?= base_url() ?>themes/default/admin/assets/upload/logos/<?= $setting->login_bg_image; ?>" alt="" id="pdf_back" class="img-responsive img-thumbnail thumb-lg">
                            </div>

                         </div>

                          <div class="row">

                            <div class="col-md-4">
                                <div class="form-group hide">
                                    <label for="timezone">Select Your Timezone</label>
                                    <select id="timezone" name="timezone">
                                    <?php 
                                        foreach($timezonelist as $timezone => $name)
                                        {
                                            $c = '';
                                            if($setting->timezone == $timezone) { $c = 'selected'; }
                                            print '<option '.$c.' value="' . $timezone . '">' . $name . '</option>';
                                        }
                                    ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4 ">
                                <div class="form-group hide">
                                    <label for="language"><?= lang('language'); ?><span class="text-danger">*</span></label>
                                    <select name="language" id="language" class="form-control" required="required">
                                        <?php foreach ($languages as $k => $v) : ?>
                                            <option <?php echo ($setting->language == $k) ? 'selected="selected"' : '' ?> value="<?= $k ?>"><?= $v; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <label class="text-danger"><?= form_error('language'); ?></label>
                                </div>
                            </div>    

                          </div>                

                         




                    </div> <!-- Panel  -->

                    <div class="panel-footer">
                        <a href="<?= base_url('admin'); ?>"><button type="button" class="btn btn-inverse waves-effect waves-light pull-right" data-dismiss="modal" aria-hidden="true">Close</button></a>
                        <span class="pull-right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <button type="submit" id="save" class="btn btn-success waves-effect waves-light pull-right">Save</button>
                        <div class="clearfix"></div>
                    </div>
                </form>
                </div>
            </div>
        </div>

        <script>
            function readURL(input,id) {

                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $(id).removeClass('hide').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $(document).ready(function() {

                $("#timezone").select2();
                // image display logic
                $("[name='sitelogo']").change(function(){
                    readURL(this,'#logo');
                });

                $("[name='login_bg_image']").change(function(){
                    readURL(this,'#pdf_back');
                });

                if($("#order_description").length > 0){
                tinymce.init({
                  selector: "textarea#order_description",
                  menubar:false,
                  theme: "modern",
                  height:100,
                  plugins: [
                      "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                      "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking",
                      "save table contextmenu directionality template paste textcolor"
                  ],
                  toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview fullpage | forecolor backcolor",
                  style_formats: [
                      {title: 'Bold text', inline: 'b'},
                      {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                      {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                      {title: 'Example 1', inline: 'span', classes: 'example1'},
                      {title: 'Example 2', inline: 'span', classes: 'example2'},
                      {title: 'Table styles'},
                      {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                  ]
              });
          }



            });
        </script>