<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="wrapper">
    <div class="container">
		

		<div class="row">
	        <div class="col-sm-12">
	            <div class="btn-group pull-right m-t-15">
	            	<button type="button" id="search" status="<?=$status?>" class="btn btn-purple waves-effect waves-light"><i class="fa fa-search"></i><?php if($status != 'none') echo ' Hide'; ?></button>
	            </div>
	            <h4 class="page-title"><?= lang('emaillog'); ?></h4>
	        </div>
	    </div>
	    <br>

	    <div class="row" id="seach_panel" style="display: <?=$status?>;">
            <div class="col-sm-12">
                <div class="panel panel-border panel-primary">
                    <div class="panel-heading">
                        <!-- <h3 class="panel-title"><?= lang('search'); ?></h3> -->
                    </div>
                    <div class="panel-body">
                    	<form action="<?= site_url().'emaillog/index'; ?>" method="POST">

                        <div class="col-md-4">&nbsp;</div>
                        
                        <div class="col-md-2">
                        	<label for="fromdate"><?= lang('fromdate'); ?></label>
                        	<input type="text" name="fromdate" id="fromdate" class="form-control datepicker-autoclose" placeholder="mm/dd/yyyy" value="<?= (isset($_REQUEST['fromdate']) ? $_REQUEST['fromdate'] : '');?>">
                        </div>
                        <div class="col-md-2">
                        	<label for="todate"><?= lang('todate'); ?></label>
                        	<input type="text" name="todate" id="todate" class="form-control datepicker-autoclose" placeholder="mm/dd/yyyy" value="<?= (isset($_REQUEST['todate']) ? $_REQUEST['todate'] : '');?>">
                        </div>
                        <div class="col-md-3">
                        	<label for="anytext"><?= lang('contains'); ?></label>
                        	<input type="text" name="anytext" id="anytext" placeholder="<?= lang('contains'); ?>" class="form-control" value="<?= (isset($_REQUEST['anytext']) ? $_REQUEST['anytext'] : ''); ?>">
                        </div>

                        
                        <div class="col-md-1">
                        	<button type="submit" id="search" class="btn btn-purple waves-effect waves-light" style="margin-top: 26px;"><?= lang('search'); ?></button>
                        </div>
                        <!-- <div class="col-md-1"></div> -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
	    <?php
	    	// print_r($branchs);
	    ?>
      <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12" style="background-color: #fff;">
                <table class="table table-hover table-condensed">
                	<thead>
                		<th>Email ID</th>
                		<th><?= lang('subject'); ?></th>
                		<th><?= lang('recipient'); ?></th>
                		<th><?= lang('datecreated'); ?></th>
                		<th><?= lang('datesent'); ?></th>
                	</thead>
                	<tbody>
                		<?php foreach ($emaillog as $sam) : ?>
	        					<tr>
	        						<td><?= $sam->id; ?></td>
									<td><?= $sam->subject; ?></td>
                                    <td><?= $sam->recepient; ?></td>
									<td><?= date('d/m/Y h:i:s A', strtotime($sam->date_created)); ?></td>
									<td><?= date('d/m/Y h:i:s A', strtotime($sam->date_sent)); ?></td>

								</tr>
						<?php endforeach; ?>
                	</tbody>
                </table>
            </div>
        </div>

        <div class="row">
        	<div class="col-md-12">
        		<div class="pull-right">
        			<?php echo $this->pagination->create_links(); ?>
        		</div>
        	</div>
        </div>



<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content p-0 b-0">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading">
                    <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="panel-title">Image</h3>
                </div>
                <div class="panel-body" style="text-align: center;">
                    <img src="" id="modalimg" alt="" style="max-width:100%;">
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

	function MyNotify(type,msg) {
		$.Notification.notify('error','top right',type, msg);
	}
	function readURL(input,id) {

	    if (input.files && input.files[0]) {
	        var reader = new FileReader();

	        reader.onload = function (e) {
	            $(id).attr('src', e.target.result);
	        }

	        reader.readAsDataURL(input.files[0]);
	    }
	}

	$(document).ready(function() {


		$('#search').on('click', function() {
			// console.log(this);
			if($(this).attr('status') == 'none') {
				$(this).attr('status','block');
				$('#seach_panel').show('slow');
				$(this).html('<i class="fa fa-search"></i>&nbsp;Hide');
			} else {
				$(this).attr('status','none');
				$('#seach_panel').hide('slow');
				$(this).html('<i class="fa fa-search"></i>');
			}
		});


		$('.show_img').on('click', function() {
			// 
			console.log(this);
			path = $(this).attr('src');
			$('#modalimg').attr('src', path);
		});

        $('.sm-select').select2();

	});
</script>