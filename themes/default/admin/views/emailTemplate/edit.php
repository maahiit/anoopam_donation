<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="wrapper">
    <div class="container">

      <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-color panel-inverse">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?= $page_title ?></h3>
                    </div>
                    <form name="emailtemplate_add" id="emailtemplate_add" method="post"  action='<?= base_url("emailtemplate/edit/{$email->id}"); ?>' enctype="multipart/form-data">
                    <div class="panel-body">
                        
                        <div class="row">
                            <input type="hidden" name="id" value="<?= $email->id; ?>">

                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="name"><?= lang('templatename'); ?><span class="text-danger">*</span></label>
                                    <input type="text" required="required" name="name" id="name" class="form-control" placeholder="<?= lang('templatename');?>" value="<?= $email->template_name; ?>">
                                </div>
                            </div>

                            <div class="col-md-1">
                                <div class="form-group checkbox checkbox-danger" style="margin-top: 30px;">
                                    <input type="checkbox" <?php echo ($email->status == 'inactive') ? 'checked="checked"' : ''?> name="status" value="inactive" id="status">
                                    <label for="status"><?= lang('inactive'); ?></label>
                                </div>
                            </div>
                    

                          </div>

                          

                          <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="subject"><?= lang('subject'); ?></label>
                                    <input type="text" name="subject" class="form-control" id="subject" placeholder="<?= lang('subject'); ?>" value="<?= $email->subject; ?>">
                                </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message"><?= lang('message'); ?></label>
                                    <textarea name="message" id="message" class="form-control" rows="10" style="resize: none;"><?= $email->email_body; ?></textarea>
                                </div>
                            </div>
                          </div>


                    </div>
                    <div class="panel-footer">
                        <a href="<?= base_url('emailtemplate'); ?>"><button type="button" class="btn btn-inverse waves-effect waves-light pull-right" data-dismiss="modal" aria-hidden="true">Close</button></a>
                        <span class="pull-right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <button type="submit" id="save" class="btn btn-success waves-effect waves-light pull-right">Save</button>
                        <div class="clearfix"></div>
                    </div>
                </form>
                </div>
            </div>
        </div>

        <script>
            function readURL(input) {

                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#img').removeClass('hide').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $(document).ready(function() {
                // image display logic
                // $("[name='usersign']").change(function(){
                //     readURL(this);
                // });

                if($("#message").length > 0){
                      tinymce.init({
                          selector: "textarea#message",
                          menubar:false,
                          theme: "modern",
                          height:300,
                          plugins: [
                              "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                              "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking",
                              "save table contextmenu directionality template paste textcolor"
                          ],
                          toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview fullpage | forecolor backcolor",
                          style_formats: [
                              {title: 'Bold text', inline: 'b'},
                              {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                              {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                              {title: 'Example 1', inline: 'span', classes: 'example1'},
                              {title: 'Example 2', inline: 'span', classes: 'example2'},
                              {title: 'Table styles'},
                              {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                          ]
                      });
                  }


            });
        </script>