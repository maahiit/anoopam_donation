<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="wrapper">
    <div class="container">
		

		<div class="row">
	        <div class="col-sm-12">
	            <div class="btn-group pull-right m-t-15">
	            	<button type="button" id="add" data-toggle="modal" data-target=".bs-example-modal-lg" class="btn btn-purple waves-effect waves-light">Add New</button>
	            </div>
	            <h4 class="page-title"><?= lang('emailtemplate'); ?></h4>
	        </div>
	    </div>
	    <br>
	    <?php
	    	// print_r($branchs);
	    ?>
      <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12 table-responsive card-box">
                <div class="table-responsive" data-pattern="priority-columns">
                <table class="table table-hover table-condensed table table-striped">
                	<thead>
                		<th>#</th>
                		<th><?= lang('templatename'); ?></th>
                		<th><?= lang('subject'); ?></th>
                		<th><?= lang('status'); ?></th>
                		<th><?= lang('actions'); ?></th>
                	</thead>
                	<tbody>
                		<?php foreach ($email as $key=>$sam) : ?>
	        					<tr>
									<td><?= $key + 1; ?></td>
									<td><?= $sam->template_name; ?></td>
									<td><?= $sam->subject;?></td>
									<td><span class="label label-<?php echo ($sam->status == 'active') ? 'success' : 'danger'?>"><?= ucfirst($sam->status); ?></span></td>
									<td>
										<a href='<?= base_url("emailtemplate/edit/{$sam->id}"); ?>'><button class="btn btn-icon btn-xs waves-effect waves-light btn-purple" data-toggle="tooltip" data-placement="left" title="" data-original-title="<?= lang('edit'); ?>"> <i class="fa fa-edit"></i> </button></a>
										<a class="sa-params hide" href='javascript:void(0)' id="<?= $sam->id ?>"><button class="btn btn-icon btn-xs waves-effect waves-light btn-danger" data-toggle="tooltip" data-placement="right" title="" data-original-title="<?= lang('delete'); ?>"> <i class="fa fa-remove"></i> </button></a>
									</td>

								</tr>
						<?php endforeach; ?>
                	</tbody>
                </table>
                </div>
            </div>
        </div>

        <br>


        <div class="row">
        	<div class="col-sm-12" style="background-color: #cbccd6;">
                <table class="table table-hover table-condensed">
                	<thead>
                		<th>Short Code</th>
                		<th>Description</th>
                	</thead>
                	<tbody>
                		<tr>
                			<td><strong>[REGISTRATION]</strong></td>
                			<td><strong>To Display Registration No With it.</strong></td>
                		</tr>
                		<tr>
                			<td><strong>[USER_NAME]</strong></td>
                			<td><strong>To Display User Name.</strong></td>
                		</tr>
                		<tr>
                			<td><strong>[USER_EMAIL]</strong></td>
                			<td><strong>To Display User Email.</strong></td>
                		</tr>
                		<tr>
                			<td><strong>[USER_PASSWORD]</strong></td>
                			<td><strong>To Display User Password.</strong></td>
                		</tr>
                		<tr>
                			<td><strong>[SITE_NAME]</strong></td>
                			<td><strong>To Display Site Name.</strong></td>
                		</tr>
                	</tbody>
                </table>
            </div>
        </div>

 <!--  Modal content for the above example -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        	<form name="emailtemplate_add" id="emailtemplate_add" method="post" enctype="multipart/form-data">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	                <h4 class="modal-title" id="myLargeModalLabel"><?= lang('addemailtemplate'); ?></h4>
	            </div>
	            <div class="modal-body">
	              <div class="row">
	              	
	              	<div class="col-md-5">
	              		<div class="form-group">
	              			<label for="name"><?= lang('templatename'); ?><span class="text-danger">*</span></label>
	              			<input type="text" required="required" name="name" id="name" class="form-control" placeholder="<?= lang('templatename');?>">
	              		</div>
	              	</div>

	              </div>

	              <div class="row">
	              	<div class="col-md-12">
	              		<div class="form-group">
                            <label for="subject"><?= lang('subject'); ?></label>
                            <input type="text" name="subject" class="form-control" id="subject" placeholder="<?= lang('subject'); ?>">
                        </div>
	              	</div>
	              </div>

	              <div class="row">
	              	<div class="col-md-12">
	              		<div class="form-group">
                            <label for="message"><?= lang('message'); ?></label>
                            <textarea name="message" class="form-control" id="message" rows="10" style="resize: none;"></textarea>
                        </div>
	              	</div>
	              </div>

	            </div>


	            <div class="modal-footer">
	            	<button type="button" class="btn btn-inverse waves-effect waves-light" data-dismiss="modal" aria-hidden="true">Close</button>
	            	<button type="submit" id="save" class="btn btn-success waves-effect waves-light">Save</button>
	            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

	function MyNotify(type,msg) {
		$.Notification.notify('error','top right',type, msg);
	}
	function readURL(input,id) {

	    if (input.files && input.files[0]) {
	        var reader = new FileReader();

	        reader.onload = function (e) {
	            $(id).removeClass('hide').attr('src', e.target.result);
	        }

	        reader.readAsDataURL(input.files[0]);
	    }
	}

	$(document).ready(function() {

		// image display logic
		// $("[name='exteriorimage']").change(function(){
		//     readURL(this,'#exteriorimg');
		// });

		if($("#message").length > 0){
              tinymce.init({
                  selector: "textarea#message",
                  menubar:false,
                  theme: "modern",
                  height:300,
                  plugins: [
                      "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                      "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking",
                      "save table contextmenu directionality template paste textcolor"
                  ],
                  toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview fullpage | forecolor backcolor",
                  style_formats: [
                      {title: 'Bold text', inline: 'b'},
                      {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                      {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                      {title: 'Example 1', inline: 'span', classes: 'example1'},
                      {title: 'Example 2', inline: 'span', classes: 'example2'},
                      {title: 'Table styles'},
                      {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                  ]
              });
          }

		
		
		$('#emailtemplate_add').on('submit',function(e) {
			e.preventDefault();
			tinymce.triggerSave();
			if($('#name').val() == '') {
				MyNotify('<?php echo lang('error'); ?>','<?php echo lang('namereq'); ?>');
				return false;
			}
			$.ajax({
				url: "<?php echo site_url('modal/emailtemplate_add')?>/",
				type: "POST",
				data: new FormData(this),

				// Tell jQuery not to process data or worry about content-type
        		// You *must* include these options!
				cache: false,
        		contentType: false,
        		processData: false,
				success: function() {
					// hide modal
					$('.bs-example-modal-lg').modal('hide');
					$.Notification.notify('success','top right','<?php echo lang('success'); ?>', '<?php echo lang('emailsuccess'); ?>');
					setTimeout(function() {
						location.reload();
					},1000);
				},
				error: function() {
					alert('error');
				}
			});
		});




		// SwwetALert
		//Parameter
        $('.sa-params').click(function () {
        	var id = $(this).attr('id');
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger m-l-10',
                buttonsStyling: false
            }).then(function () {
            	// call ajax function to delete it
            	$.ajax({
            		type: "POST",
            		url: "<?php echo site_url('modal/emailtemplate_delete')?>/"+id,
            		success: function(data) {
            			swal({
            				title: 'Deleted!',
            				text: 'Email Template has been deleted.',
            				type: 'success'
            			}).then(function() {
            				location.reload();
            			});
            		},
					error: function() {
						alert('error');
					}
            	})
            }, function (dismiss) {
                // dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
            })
        });




	});
</script>