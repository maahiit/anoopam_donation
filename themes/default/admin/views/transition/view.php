<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<style type="text/css">
	.search {
	    width: 17%;
	}
</style>

<div class="wrapper">

    <div class="container"> 

		<div class="row">

	        <div class="col-sm-12">
	            <div class="pull-right ">
                <button type="button" class="btn btn-info" status="none" id="advanced_search_btn"><span class="glyphicon glyphicon-search "></span></button>
	           <a  onclick="formSubmit('excel')" href="javascript:void();" id="unset_button">
                    		<button type="button" class="btn btn-primary" status="none">
                    		<span class="glyphicon glyphicon-download-alt"></span> Export</button>
                    </a>
                    
	            <a href="<?= base_url("admin/transition/"); ?>unset_session_value" id="unset_button"><button type="button" class="btn btn-primary  m-r-5" status="none"><span class="glyphicon glyphicon-refresh"></span></button></a>		
	            	
	            </div>
	        	
	            <h4 class="page-title"><?= $page_title ?></h4>
	        </div>

	    </div>
	    <br>

	    <div class="row" id="advanced_search_div"
	    
		    <?php if(empty($_REQUEST))
		    	  { ?>

		    		<?php if(!empty($this->session->userdata('transition_serach_data'))) 
		    			  { ?>
		    					style="display: block;"

		    		<?php 
						  }else
						  { ?>
								style="display: none;"

		    			  <?php  } ?>
		    
			<?php } ?>
		>
		    
		    
		   	<div class="col-xs-12 col-md-12 col-lg-12">

		    <div class="panel panel-default">

		        <div class="panel-body">

		            <div class="fixed-table-toolbar">

		               <form name="searchfrom" id="searchfrom" method="post"  action='<?= base_url("admin/transition"); ?>' enctype="multipart/form-data">
                        <input type="hidden" name="SearchValue" id="SearchValue">
		                  	<div class="columns btn-group pull-right margin">
			                    <div class="searchdatetitle">&nbsp;</div>
			                    <button type="submit" name="filter" id="filter" class="btn btn-success" value="filter">Search</button>
		                  	</div>
		                  
		                  	<div class="pull-left search margin m-r-15">
		                    	<div class="searchdatetitle">Name</div>
		                    	<input class="form-control" type="text" placeholder="Any Text.." name="transition_s_name" id="transition_s_name" value="<?php if(isset($_REQUEST['transition_s_name'])) 
		                     			{ 
		                     				echo $_REQUEST['transition_s_name']; 
		                     			} 
		                     			else 
		                     			{ 
		                     				if($this->session->userdata('transition_s_name')) 
		                     				{ 
		                     					echo $this->session->userdata('transition_s_name'); 
		                     				} 
		                     			}
             					?>">
		                  	</div>
		                  	
		                  	 <div class="pull-left search margin m-r-15">
		                     <div class="searchdatetitle">Zone</div>
		                     <select id="transition_s_category" name="transition_s_category" class="form-control">
		                     	<option value="">Select Category</option>
		              			<?php foreach ($categories as $key => $category): ?>
		              				<option 
		              					<?php 
		              					    if($this->session->userdata('transition_s_category')) 
			              					{
												if($this->session->userdata('transition_s_category') == $category->id)
				              					{
				              						echo "selected"; 
				              					}				                            	
				                            }	
		              					?>
		              				 value="<?= $category->id; ?>"><?= $category->name; ?></option>
		              			<?php endforeach ?>
		                      
		                     </select>
		                  </div>
		                  
		                  <div class="pull-left search margin m-r-15">
		                     <div class="searchdatetitle">Zone</div>
		                     <select id="transition_s_subcategory" name="transition_s_subcategory" class="form-control">
		                     	<option value="">Select Sub Category</option>
		              			<?php foreach ($sub_categories as $key => $sub_category): ?>
		              				<option 
		              					<?php 
		              					    if($this->session->userdata('transition_s_subcategory')) 
			              					{
												if($this->session->userdata('transition_s_subcategory') == $sub_category->id)
				              					{
				              						echo "selected"; 
				              					}				                            	
				                            }	
		              					?>
		              				 value="<?= $sub_category->id; ?>"><?= $sub_category->name; ?></option>
		              			<?php endforeach ?>
		                      
		                     </select>
		                  </div>
		                  
		                  <div class="pull-left search margin m-r-15">
                         <div class="searchdatetitle">Date From</div>
                         <input  class="form-control transition_s_from" type="date" name="transition_s_from" id="transition_s_from" value="<?php if(isset($_REQUEST['transition_s_from'])) 
                              { 
                                echo $_REQUEST['transition_s_from']; 
                              } 
                              else 
                              { 
                                if($this->session->userdata('transition_s_from')) 
                                { 
                                  echo $this->session->userdata('transition_s_from'); 
                                } 
                              }
                      ?>">
                      </div>
                      
                      <div class="pull-left search margin m-r-15">
		                     <div class="searchdatetitle">Payment Status</div>
		                     <select id="transition_se_s_status" name="transition_se_s_status" class="form-control">
		                        <option value="" >Select Status</option>
		                         <option 
	              					
	              					<?php 
	              					if($this->session->userdata('transition_se_s_status')) 
	              					{
										if($this->session->userdata('transition_se_s_status') == 'pending')
		              					{
		              						echo "selected"; 
		              					}				                            	
		                            }
                      				?>
                      				value="pending">Pending</option>
		                       
		                     
		                        <option 
		                        
	              					<?php 
	              					if($this->session->userdata('transition_se_s_status')) 
	              					{
										if($this->session->userdata('transition_se_s_status') == 'success')
		              					{
		              						echo "selected"; 
		              					}				                            	
		                            }
                      				?>
		                         value="success">Success</option>
		                       
		                      <option 
	              					
	              					<?php 
	              					if($this->session->userdata('transition_se_s_status')) 
	              					{
										if($this->session->userdata('transition_se_s_status') == 'fail')
		              					{
		              						echo "selected"; 
		              					}				                            	
		                            }
                      				?>
                      				value="fail">Fail</option>
		                     
		                     <option 
	              					
	              					<?php 
	              					if($this->session->userdata('transition_se_s_status')) 
	              					{
										if($this->session->userdata('transition_se_s_status') == 'payment_status')
		              					{
		              						echo "selected"; 
		              					}				                            	
		                            }
                      				?>
                      				value="cancel">Cancel</option>
		                     </select>
		                  </div>
		                  
		               </form>		               
		            </div>
		        </div>
		    </div>
		   	</div>
		</div>
	   
      	<!-- Page-Title -->
        <div class="row">

           <div class="col-sm-12 table-responsive card-box">
                <table class="table table-hover table-condensed table table-striped" id="tech-companies-1">
                	<thead>
                		<th width="5%">#</th>
                		<th>Name</th>
                		<th>Mobile </th>
                		<th>Email</th>
                		<th>Pan No</th>
                		<th>Sub Category </th>
                		<th>Payment Amount </th>
                		<th>Payment Date</th>
                		<th width="10%">Payment Status</th>
                		<th>Action</th>
                	</thead>
                	<tbody>
                		<?php  $CI =& get_instance(); $CI->load->library('sam'); ?>
                		<?php $count = 1; foreach ($rows as $rowsMst) : ?>
	        					<tr>
	        						<td><?=$count++;?></td>
									<td><?= ucfirst($rowsMst->name); ?></td>
									<td><?= $rowsMst->mobile ?></td>
									<td><?= $rowsMst->donner_email ?></td>
									<td><?php echo ($rowsMst->pan != '') ?  @$rowsMst->pan : '-'?></td>
									<td><?= $CI->sam->get_real_value('subcategories_mst','id',$rowsMst->sub_category_id,'name') ?></td>
									<td><?php echo ($rowsMst->amount != '') ?  number_format(@$rowsMst->amount) : '-'?></td>

									<td><?= $rowsMst->payment_response_time ?></td>

									<td><span class="label label-<?php echo ($rowsMst->payment_status == 'success') ? 'success' : 'danger'?>"><?= ucfirst($rowsMst->payment_status); ?></span></td>	
									<td>
									<?php if($rowsMst->payment_status == 'success'){ ?> 
									<a target="_blank" href="<?= base_url("print_receipt/");?><?= $rowsMst->payment_transition_id ?>"><b><i class="fa fa-print"></i></b></a>
									
										
										    
									<?php } ?>
									<?php if(canaccess("categories","delete_access")){ ?>		
								        <a class="sa-params" href='javascript:void(0)' id="<?= $rowsMst->id ?>"><button class="btn btn-icon btn-xs waves-effect waves-light btn-danger" data-toggle="tooltip" data-placement="right" title="" data-original-title="<?= lang('delete'); ?>"> <i class="fa fa-remove"></i> </button></a>  			
									<?php } ?>	


									</td>

								</tr>
						<?php endforeach; ?>
                	</tbody>
                </table>
            </div>
        </div>
        <br>

        <div class="row">
          <div class="col-md-12">
            <div class="pull-right">
              <?php echo $this->pagination->create_links(); ?>
            </div>
          </div>
        </div>


 		<!--  Modal content for the above example -->
		<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">

    		<div class="modal-dialog modal-md">

        		<div class="modal-content">

        			<form name="add_form" id="add_form" method="post" enctype="multipart/form-data">

	            		<div class="modal-header">
	                		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	                		<h4 class="modal-title" id="myLargeModalLabel">Add <?= $page_title ?></h4>
	            		</div>

	            		<div class="modal-body">

	              			<div class="row">
	              	
	              				<div class="col-md-6">
	              					<div class="form-group">
	              						<label for="name">Name<span class="text-danger">*</span></label>
	              						<input type="text" required="required" name="name" id="name" class="form-control" placeholder="Name">
	              					</div>
	              				</div>              				

	              			</div>              			

	              			<div class="row">
	              				
	              				<div class="col-md-6">
	              					<div class="form-group">
	              						<label for="status">Status<span class="text-danger">*</span></label>
		              					<div class="form-group checkbox checkbox-danger" style="margin-top: 30px;">
	                                        <input <?php if( isset($status) && $status =='inactive') { echo "checked";} ?>  type="checkbox" name="status" value="inactive" id="status">
	                                        <label for="status"><?= lang('inactive'); ?></label>
	                                    </div>
	                                </div>
	              				</div>

	              			</div>

	              				              			

	            		</div>


	            		<div class="modal-footer">
			            	<button type="button" class="btn btn-inverse waves-effect waves-light" data-dismiss="modal" aria-hidden="true">Close</button>
			            	<button type="submit" id="save" class="btn btn-success waves-effect waves-light">Save</button>
	            		</div>

            		</form>
        		</div><!-- /.modal-content -->
    		</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	</div>
</div>

<script>

	function MyNotify(type,msg)
	{
		$.Notification.notify('error','top right',type, msg);
	}

	$(document).ready(function()
	{

		if($("#message").length > 0)
		{
              	tinymce.init({
                  selector: "textarea#message",
                  menubar:false,
                  theme: "modern",
                  height:300,
                  plugins: [
                      "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                      "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking",
                      "save table contextmenu directionality template paste textcolor"
                  ],
                  toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview fullpage | forecolor backcolor",
                  style_formats: [
                      {title: 'Bold text', inline: 'b'},
                      {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                      {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                      {title: 'Example 1', inline: 'span', classes: 'example1'},
                      {title: 'Example 2', inline: 'span', classes: 'example2'},
                      {title: 'Table styles'},
                      {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                  ]
              	});
        }		
		
		$('#add_form').on('submit',function(e)
		{
			e.preventDefault();
			
			if($('#name').val() == '')
			{
				return false;
			}

			$.ajax({
				url: "<?php echo site_url('admin/transition')?>/add_row",
				type: "POST",
				data: new FormData(this),
				cache: false,
        		contentType: false,
        		processData: false,
				success: function(){
					// hide modal
					$('.bs-example-modal-lg').modal('hide');
					$.Notification.notify('success','top right','<?php echo lang('success'); ?>', 'Successfully Add New Record');
					setTimeout(function() {
						location.reload();
					},1000);
				},
				error: function() {
					alert('error');
				}
			});
		});

		// SwwetALert
		//Parameter
        $('.sa-params').click(function ()
        {
        	var id = $(this).attr('id');
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger m-l-10',
                buttonsStyling: false
            }).then(function ()
             	{
            	// call ajax function to delete it
            	$.ajax({
            		type: "POST",
            		url: "<?php echo site_url('admin/transition')?>/delete_row/"+id,
            		success: function(data) {
            			swal({
            				title: 'Deleted!',
            				text: 'Data Successfully deleted.',
            				type: 'success'
            			}).then(function() {
            				location.reload();
            			});
            		},
					error: function() {
						alert('error');
					}
            	})
            	}, function (dismiss) {
                // dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
            	})
        });      

	});


	$("#advanced_search_btn").click(function()
	{
		$("#advanced_search_div").slideToggle();
	});    

	function ImagePreview(input,image_preview) 
	{
	  	if (input.files && input.files[0]) 
	  	{
		    var reader = new FileReader();
		    reader.onload = function(e) {
		    $('#'+image_preview).attr('src', e.target.result);
		    $('#'+image_preview).show();
	    	}
	    	reader.readAsDataURL(input.files[0]);
		}
	}
	
	function formSubmit(type)
    {
        if(type == 'Search')
        {
          $('#SearchValue').val(type);
        } else {
          $('#SearchValue').val(type);
        }
        document.getElementById("searchfrom").submit();
    }

</script>