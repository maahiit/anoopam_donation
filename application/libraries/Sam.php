<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
 *  ==============================================================================
 *  Author    : SalmanKhan
 *  Email     : mailuser0011@gmail.com
 *  For       : Rent Serv
 *  Web       : 
 *  Copyright : SalmanKhan
 *  ==============================================================================
 */

class Sam {

    public function __construct() {

    }
    public function __get($var) {
        return get_instance()->$var;
    }

    public function logged_in() {
        return (bool) $this->session->userdata('identity');
    }

    public function user_logged_in() {
        return (bool) $this->session->userdata('user_identity');
    }

    public function get_roles() {
        return ['admin' => 'Admin', 'user' => 'User', 'contributor' => 'Contributor'];
    }

     public function get_module() {
       
        return [
            
                 'settings'         => 'settings',
                 'payment_gateway'  => 'Payment gateway',
                 'categories'       => 'Categories',
                 'subcategories'    => 'Sub Categories',
                 'smtp_email'       => 'SMTP Email',
                 'country'          => 'Country',
                 'contributor'      => 'Contributor',
                 'adminusers'       => 'Adminusers'
        
               ];
    }


    public function get_reports_list() {
        return ['1' => 'Delivery Report', '2' => 'Inspection Extract'];
    }

    public function get_appointment_status() {
        return ['0' => 'Pending', '1' => 'Completed', '2' => 'Cancelled'];
    }

    public function get_inspection_status() {
        return ['0' => 'Pending', '1' => 'In Process', '2' => 'Completed'];
    }
    
    public function followup_mode() {
        return ['phone' => 'Phone', 'email' => 'Email', 'visit' => 'Visit', 'trail' => 'Trail','lab_test' => 'Lab Test','offer_submit' => 'Offer Submit', 'trial_report_submission' => 'Trial Report Submission','other' => 'Other'];
    }

    public function get_control_types() {
        return [

                '1' => "TextBox Control",
                '2' => "DatePicker",
                '3' => "Multiline TextBox Control",
                '4' => "DropDown Options",
                '5' => "Numeric Control",
                '6' => "Photo",
                '7' => "Signature control",
                '8' => "Check box",
                '9' => "Inspection Section (Title)",
                '10' => "Inspection Sub-Section (Subtitle)"
            ];
    }

    public function get_body_style() {
        return [
                'Pantech'       => 'Pantech',
                'Van'           => 'Van',
                'Hatchback'     => 'Hatchback',
                'Curtain Side'  => 'Curtain Side',
                'Bus'           => 'Bus',
                'Flatbed Truck' => 'Flatbed Truck',
                'Dual cab Ute'  => 'Dual cab Ute',
                'Sedan'         => 'Sedan',
                'People Carrier'=> 'People Carrier',
                'SUV'           => 'SUV',
                'SUV WAGON'     => 'SUV WAGON',
                '4 Wheel Drive' => '4 Wheel Drive',
                'HATCH'         => 'HATCH',
                'Tray Ute'      => 'Tray Ute',
                'Truck'         => 'Truck',
                'Convertible'   => 'Convertible',
                'Ute'           => 'Ute',
                'Tray'          => 'Tray'
            ];
    }

    public function get_languages() {
        return [
                'english'       => 'English'
            ];
    }

    public function pagination_config($num_links = 2) {
        $config['num_links'] = $num_links;
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='javascript:void(0);'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tag_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tag_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        return $config;
    }


    // get below user id start
    function getbellowuserids($user_id,$bellowusers=[])
    {
        $bellowusers=$bellowusers;

        if(!in_array($user_id,$bellowusers))
        {
          $bellowusers[]=$user_id;  
        }

        $query = "SELECT id FROM `sam_users`WHERE find_in_set('".$user_id."',reporting_manager)";
        $d = ORM::for_table('sam_users')->raw_query($query)->find_one();

        if($d['id'] != ""){
              $bellowusers[] = $d['id'];
              $bellowusers = $this->getbellowuserids($d['id'],$bellowusers);  
        }

      return $bellowusers;
    }
    // get below user id end



    /*-----------------------------------------------------------
    * -----------------Image Upload Function--------------------*
    * ----------------------------------------------------------*/

    public function upload_image($name,$path,$tmp_name = '') {

        // File uploading
        $file_name = $tmp_name.$_FILES[$name]['name'];
        $file_tmpname = $_FILES[$name]['tmp_name'];
        
        move_uploaded_file($file_tmpname,$path.$file_name);
        return true;
    }
    
    public function upload_image_lowercase($name,$path,$tmp_name = '') {

        // File uploading
        $file_name = $tmp_name.strtolower(str_replace(' ', '_', $_FILES[$name]['name']));
        $file_tmpname = $_FILES[$name]['tmp_name'];
        
        move_uploaded_file($file_tmpname,$path.$file_name);
        return true;
    }
    
    
        public function upload_multiple_image($name,$path,$i,$tmp_name = '') {

        // Multiple File uploading
        $file_name = $tmp_name.$_FILES[$name]['name'][$i];
        $file_tmpname = $_FILES[$name]['tmp_name'][$i];
        
        move_uploaded_file($file_tmpname,$path.$file_name);
        return true;
    }


    public static function timezoneList()
    {
        $timezoneIdentifiers = DateTimeZone::listIdentifiers();
        $utcTime = new DateTime('now', new DateTimeZone('UTC'));

        $tempTimezones = array();
        foreach ($timezoneIdentifiers as $timezoneIdentifier) {
            $currentTimezone = new DateTimeZone($timezoneIdentifier);

            $tempTimezones[] = array(
                'offset' => (int)$currentTimezone->getOffset($utcTime),
                'identifier' => $timezoneIdentifier
            );
        }

        // Sort the array by offset,identifier ascending
        usort($tempTimezones, function($a, $b) {
            return ($a['offset'] == $b['offset'])
                ? strcmp($a['identifier'], $b['identifier'])
                : $a['offset'] - $b['offset'];
        });

        $timezoneList = array();
        foreach ($tempTimezones as $tz) {
            $sign = ($tz['offset'] > 0) ? '+' : '-';
            $offset = gmdate('H:i', abs($tz['offset']));
            $timezoneList[$tz['identifier']] = '(UTC ' . $sign . $offset . ') ' .
                $tz['identifier'];
        }

        return $timezoneList;
    }

     public function _delete_by_id($id,$_table) {
        $d = ORM::for_table($_table)->where('id',$id)->find_one();
        if($d) {
            // change is_deleted to 1
            $d->is_deleted = '1';
            $d->save();
            return TRUE;
        } else {
           return FALSE;
        }
    }

     public function get_real_value($tablename,$columname,$columvalue,$columreturn)
    {
        $query = "SELECT `$columreturn` FROM `$tablename` WHERE `$columname` = '".$columvalue."'";
        $record = ORM::for_table($tablename)->raw_query($query)->find_one();
        if($record)
        {
             $d = $record->$columreturn;
            return $d;
        }else{
            return '';
        }
    }



}