<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

class Offer_discount extends MY_Front_Controller{   


	function __construct(){
        parent::__construct();
        if (!$this->User_loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }
        $this->load->library('form_validation');  
        $this->load->library('Sam','sam'); 
        $this->load->model('Mdl_offer_discount','mdl');    
  	}
    
    private $_table = 'offer_discount'; 

	function index()   
	{
		 $meta['page_title'] = 'Offer Discount - Salon';
         $this->data['services_name'] = $this->mdl->get_services();             
         $this->data['users'] = $this->mdl->get_users();
		 $this->data['offer_discount'] = $this->mdl->get_all_offer_discounts();     
         $this->data['table_ui'] =  $this->load->view('/front/views/offer_discount/ajex_table',$this->data,true);
		$this->frontpage_construct('offer_discount/index', $meta, $this->data);
	}

	function offer_discount_add() 
	{
            $d = ORM::for_table('offer_discount')->create();
            $d->user_id                 = $this->session->userdata('user_identity');
            $d->service            		= $_POST['service'];
            $d->title          			= $_POST['title'];
            $d->discount_type        	= $_POST['discount_type'];
            $d->offer_disc              = $_POST['offer_disc'];
            $d->inserted_time           = date('Y-m-d H:i:s');
            $d->updated_time            = date('Y-m-d H:i:s');            
            $d->created_by_user_id      = $this->session->userdata('user_identity');  
            $d->updated_by_user_id      = $this->session->userdata('user_identity');
        
            if($d->save())
            {
                 $this->data['offer_discount'] = $this->mdl->get_all_offer_discounts();                 
                 $this->data['table_ui'] =  $this->load->view('/front/views/offer_discount/ajex_table',$this->data,true);

                $output['status'] = 1;
                $output['msg'] = 'Offer Discount Successfully Added';
                $output['table_ui'] = $this->data['table_ui'];
            }
            else
            {
                $output['status'] = 0;
                $output['msg'] = 'Offer Discount not added !!'; 
                $output['table_ui'] = '';
            }  
            echo json_encode($output);
    }

    public function get_row()  
    {
        $update_id = $_POST['update_id']; 
        if(!empty($update_id))
        {
            $output['status']    = '1';
            $this->data['row'] = $this->mdl->get($update_id);
            $this->data['services_name'] = $this->mdl->get_services();             
            $output['edit_row'] =  $this->load->view('/front/views/offer_discount/ajax_edit_form',$this->data,true);
            echo json_encode($output);       
        } else {
            $output['status']    = '0';
            echo json_encode($output);
        }
    }




    public function update_row()   
    {
        $d = ORM::for_table($this->_table)->where('id',$_POST['update_id'])-> find_one();
        if($d){
            
            $d->service                 = $_POST['service'];
            $d->title                   = $_POST['title'];
            $d->discount_type           = $_POST['discount_type'];
            $d->offer_disc              = $_POST['offer_disc'];
            $d->updated_time            = date('Y-m-d H:i:s');            
            $d->updated_by_user_id      = $this->session->userdata('user_identity');
            if($d->save())
            {
                 $this->data['offer_discount'] = $this->mdl->get_all_offer_discounts();                 
                 $this->data['table_ui'] =  $this->load->view('/front/views/offer_discount/ajex_table',$this->data,true);

                $output['status'] = 1;
                $output['msg'] = 'Offer Discount Successfully Update';
                $output['table_ui'] = $this->data['table_ui'];
            }
            else
            {
                $output['status'] = 0;
                $output['msg'] = 'Offer Discount not added !!'; 
                $output['table_ui'] = '';
            }  
            echo json_encode($output);
        } else  {
            return FALSE; 
        }
    }

     public function delete_row($id) {

        $deletestatus = $this->sam->_delete_by_id($id,$this->_table);
        if($deletestatus)
        {
             $this->data['offer_discount'] = $this->mdl->get_all_offer_discounts();
             $this->data['table_ui'] =  $this->load->view('/front/views/offer_discount/ajex_table',$this->data,true);

            $output['status'] = 1;
            $output['msg'] = 'Offer Discount Successfully Delete';
            $output['table_ui'] = $this->data['table_ui'];
        }
        else
        {
            $output['status'] = 0;
            $output['msg'] = 'Offer Discount not added !!'; 
            $output['table_ui'] = '';
        }  
        echo json_encode($output);
    }




    
} 