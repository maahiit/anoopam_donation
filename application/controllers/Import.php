<?php
// defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH."/third_party/import_excel_reader.php";

class Import extends MY_Controller {

	function __construct()
    {
        parent::__construct();

        // if (!$this->loggedIn) {
        //     $this->session->set_userdata('requested_page', $this->uri->uri_string());
        //     admin_redirect('login');
        // }

        // $this->load->library('form_validation');
        // $this->load->model('mdl_bodyimage','mdl');
        // $this->load->library('pagination');
        $this->load->library('database');
    }
    
	public function index() {

        $data = new Spreadsheet_Excel_Reader("doctors.xls");
        for($i=0;$i<count($data->sheets);$i++) // Loop to get all sheets in a file.
        {   
            if(count($data->sheets[$i]['cells'])>0) // checking sheet not empty
            {
                $totimpted = count($data->sheets[$i]['cells']);
                for($j=1;$j<=count($data->sheets[$i]['cells']);$j++) // loop used to get each row of the sheet
                { 
                    // unset($insert_userdata);
                    $d = ORM::for_table('sam_auto_details')->create();

                     //$user_id = json_decode(get_single_fild_data('user','emp_code',$data->sheets[$i][cells][$j][1],'id'));
                    // $user_id = 112;

                    $d->reg_no = $data->sheets[$i]['cells'][$j][14];
                    $d->customer_id = 0;
                    $d->organization_name = "";
                    $d->cur_branch_loc = 1;
                    $d->veh_type = $data->sheets[$i]['cells'][$j][1];
                    $d->sipp_code = $data->sheets[$i]['cells'][$j][2];
                    $d->makes_name = $data->sheets[$i]['cells'][$j][3];
                    $d->model_name = $data->sheets[$i]['cells'][$j][4];
                    $d->model_year = $data->sheets[$i]['cells'][$j][5];
                    $d->variant_name = $data->sheets[$i]['cells'][$j][6];
                    $d->veh_color = $data->sheets[$i]['cells'][$j][7];
                    $d->gear_box_type = $data->sheets[$i]['cells'][$j][9];
                    $d->drive_type = $data->sheets[$i]['cells'][$j][10];
                    $d->fuel_type = $data->sheets[$i]['cells'][$j][11];
                    $d->vin_no = $data->sheets[$i]['cells'][$j][12];
                    $d->unit_no = $data->sheets[$i]['cells'][$j][13];
                    $d->tank_size = $data->sheets[$i]['cells'][$j][16];
                    $d->doors = $data->sheets[$i]['cells'][$j][17];
                    $d->gears = $data->sheets[$i]['cells'][$j][18];
                    $d->engine_size = $data->sheets[$i]['cells'][$j][22];
                    $d->engine_no = $data->sheets[$i]['cells'][$j][23];
                    $d->air_con = $data->sheets[$i]['cells'][$j][27];
                    $d->key_code = $data->sheets[$i]['cells'][$j][28];
                    $d->status = 'active';
                    $d->created_by = 1;
                    $d->modified = date('Y-m-d H:i:s');
                    $d->modified_by = 1;

                    $d->save();
                }
            }
        }
        echo "Total ".$totimpted." Records imported !!";
        exit;
		// $meta['page_title'] = lang('bodyimage');
  //       $this->data['bodyimage'] = $this->mdl->get_all('id',$config['per_page'],$this->uri->segment(3));
  //       $this->data['body_style'] = $this->sam->get_body_style();
		// $this->page_construct('bodyImage/view', $meta, $this->data);
	}

}