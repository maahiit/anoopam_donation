<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

class Services_listing extends MY_Front_Controller{  


	function __construct(){ 
        parent::__construct();
        $this->load->library('form_validation'); 
        $this->load->library('Sam','sam'); 
        $this->load->model('Mdl_services_listing','mdl'); 
        $this->load->model('Mdl_home_settings','mdl_home'); 
  	}
    
    private $_table = 'services';

	function index()  
	{
		$meta['page_title'] = 'Services - List';
		 $this->data['homes'] = $this->mdl_home->get(1);
        $this->data['services_category'] = $this->mdl->get_service_cat_services_count();   
		$this->data['services'] = $this->mdl->get_all_services();
		
		 
		$this->frontpage_construct('services_listing/index', $meta, $this->data); 
	}

	function get_category_service()
	{
		$category = $this->input->post('category');
		$services = $this->mdl->get_all_services($category);

		$html = "";
		foreach ($services as $key => $list) {
			$html .= '<div class="col-lg-6 col-md-12 col-xl-4">
						<div class="card overflow-hidden">

							<div class="item-card9-img"><div class="arrow-ribbon bg-primary">
							'.$list->business_type.'
							</div>

							<div class="item-card9-imgs">
								<a href="'.base_url("services_details").'/'.$list->id.'">
								</a> 
								<img src="'.base_url().'themes/assets/images/services/'.$list->image.'" alt="img" class="cover-image">
							</div>

							<div class="item-card9-icons">
								<a href="javascript::void(0)" class="item-card9-icons1 wishlist">
								 <i class="fa fa fa-heart-o"></i>
								</a>   
							</div>
						</div>

						<div class="card-body">
							<div class="item-card9"> 
								<a href="javascript::void(0)">
								'.$list->category.'
								</a>

								<a href="javascript::void(0)" class="text-dark mt-2">
									<h4 class="font-weight-semibold mt-1">
									'.$list->name.' 
									</h4>
								</a>

								<p>
								'. $list->business_name.'
								</p>
							
							<div class="item-card9-desc">

								<a href="javascript::void(0)" class="mr-4">
								<span class="">
									<i class="fa fa-map-marker text-muted mr-1">
									</i> 
									'.$list->city.'
								</span>
								</a> 

								<a href="javascript::void(0)" class="">
								<span class="">
									<i class="fa fa-calendar-o text-muted mr-1">
									</i> 
									Nov-15-2019
								</span>
								</a>

								<button  onclick="send_enquiry_list()" >Add Inquiry</button>
							</div>
						</div>
					</div>
							<div class="card-footer">
								<div class="item-card9-footer d-flex">

									<div class="item-card9-cost">
										<h4 class="text-dark font-weight-semibold mb-0 mt-0">&#x20B9;
										'.$list->price.'
										</h4> 
									</div>
									
									<div class="ml-auto">
										<div class="rating-stars block">
											<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
										<div class="rating-stars-container">
											<div class="rating-star sm is--active"> 
												<i class="fa fa-star">
												</i> 
											</div>
										<div class="rating-star sm is--active"> 
											<i class="fa fa-star">
											</i> 
										</div>
										<div class="rating-star sm is--active"> 
											<i class="fa fa-star">
											</i> 
										</div>
										<div class="rating-star sm"> 
											<i class="fa fa-star">
											</i> </div>
										<div class="rating-star sm"> 
											<i class="fa fa-star">
											</i> 
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>';
		}
		
		print_r($html); 
		exit;

	}



	function get_selected_category() 
	{		
		
		$catid = $this->input->post('catid');


		$services = $this->mdl->get_all_selected_services($catid); 

		$html = "";	
		foreach ($services as $key => $selected_list) {
			$html .= '<div class="col-lg-6 col-md-12 col-xl-4">
						<div class="card overflow-hidden">

							<div class="item-card9-img"><div class="arrow-ribbon bg-primary">
							'.$selected_list->business_type.'
							</div>

							<div class="item-card9-imgs">
								<a href="'.base_url("services_details").'/'.$selected_list->id.'">
								</a> 
								<img src="'.base_url().'themes/assets/images/services/'.$selected_list->image.'" alt="img" class="cover-image">
							</div>

							<div class="item-card9-icons">
								<a href="javascript::void(0)" class="item-card9-icons1 wishlist">
								 <i class="fa fa fa-heart-o"></i>
								</a>   
							</div>
						</div>

						<div class="card-body">
							<div class="item-card9"> 
								<a href="javascript::void(0)">
								'.$selected_list->category.'
								</a>

								<a href="javascript::void(0)" class="text-dark mt-2">
									<h4 class="font-weight-semibold mt-1">
									'.$selected_list->name.' 
									</h4>
								</a>

								<p>
								'. $selected_list->business_name.'
								</p>
							
							<div class="item-card9-desc">

								<a href="javascript::void(0)" class="mr-4">
								<span class="">
									<i class="fa fa-map-marker text-muted mr-1">
									</i> 
									'.$selected_list->city.'
								</span>
								</a> 

								<a href="javascript::void(0)" class="">
								<span class="">
									<i class="fa fa-calendar-o text-muted mr-1">
									</i> 
									Nov-15-2019
								</span>
								</a>

								<button  onclick="send_enquiry_list()" >Add Inquiry</button>
							</div>
						</div>
					</div>
							<div class="card-footer">
								<div class="item-card9-footer d-flex">

									<div class="item-card9-cost">
										<h4 class="text-dark font-weight-semibold mb-0 mt-0">&#x20B9;
										'.$selected_list->price.'
										</h4> 
									</div>
									
									<div class="ml-auto">
										<div class="rating-stars block">
											<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
										<div class="rating-stars-container">
											<div class="rating-star sm is--active"> 
												<i class="fa fa-star">
												</i> 
											</div>
										<div class="rating-star sm is--active"> 
											<i class="fa fa-star">
											</i> 
										</div>
										<div class="rating-star sm is--active"> 
											<i class="fa fa-star">
											</i> 
										</div>
										<div class="rating-star sm"> 
											<i class="fa fa-star">
											</i> </div>
										<div class="rating-star sm"> 
											<i class="fa fa-star">
											</i> 
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>';
		}
		
		print_r($html); 
		exit;

	}


	function get_keyword()
	{
		$keyword = $this->input->post('keyword');

		echo $keyword;

		$keyword_result = $this->mdl->get_keyword($keyword);

		/*$html = "";
		foreach ($services as $key => $list) {
			$html .= '<div class="col-lg-6 col-md-12 col-xl-4">
						<div class="card overflow-hidden">

							<div class="item-card9-img"><div class="arrow-ribbon bg-primary">
							'.$list->business_type.'
							</div>

							<div class="item-card9-imgs">
								<a href="'.base_url("services_details").'/'.$list->id.'">
								</a> 
								<img src="'.base_url().'themes/assets/images/services/'.$list->image.'" alt="img" class="cover-image">
							</div>

							<div class="item-card9-icons">
								<a href="javascript::void(0)" class="item-card9-icons1 wishlist">
								 <i class="fa fa fa-heart-o"></i>
								</a>   
							</div>
						</div>

						<div class="card-body">
							<div class="item-card9"> 
								<a href="javascript::void(0)">
								'.$list->category.'
								</a>

								<a href="javascript::void(0)" class="text-dark mt-2">
									<h4 class="font-weight-semibold mt-1">
									'.$list->name.' 
									</h4>
								</a>

								<p>
								'. $list->business_name.'
								</p>
							
							<div class="item-card9-desc">

								<a href="javascript::void(0)" class="mr-4">
								<span class="">
									<i class="fa fa-map-marker text-muted mr-1">
									</i> 
									'.$list->city.'
								</span>
								</a> 

								<a href="javascript::void(0)" class="">
								<span class="">
									<i class="fa fa-calendar-o text-muted mr-1">
									</i> 
									Nov-15-2019
								</span>
								</a>

								<button  onclick="send_enquiry_list()" >Add Inquiry</button>
							</div>
						</div>
					</div>
							<div class="card-footer">
								<div class="item-card9-footer d-flex">

									<div class="item-card9-cost">
										<h4 class="text-dark font-weight-semibold mb-0 mt-0">&#x20B9;
										'.$list->price.'
										</h4> 
									</div>
									
									<div class="ml-auto">
										<div class="rating-stars block">
											<input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3">
										<div class="rating-stars-container">
											<div class="rating-star sm is--active"> 
												<i class="fa fa-star">
												</i> 
											</div>
										<div class="rating-star sm is--active"> 
											<i class="fa fa-star">
											</i> 
										</div>
										<div class="rating-star sm is--active"> 
											<i class="fa fa-star">
											</i> 
										</div>
										<div class="rating-star sm"> 
											<i class="fa fa-star">
											</i> </div>
										<div class="rating-star sm"> 
											<i class="fa fa-star">
											</i> 
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>';
		}
		
		print_r($html); 
		exit;*/

	}

} 