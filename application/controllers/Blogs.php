<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

class Blogs extends MY_Front_Controller{  
 

	function __construct(){ 
        parent::__construct();
        $this->load->library('form_validation'); 
        $this->load->library('Sam','sam'); 
        $this->load->model('Mdl_blogs','mdl'); 
        $this->load->model('Mdl_home_settings','mdl_home');   
  	}
    
    private $_table = 'blog_mst';

	function index()  
	{
		$meta['page_title'] = 'Blogs - List';

		$this->data['homes'] = $this->mdl_home->get(1);          
		$this->data['blogs'] = $this->mdl->get_all_blogs();

		$this->frontpage_construct('blogs/index', $meta, $this->data); 
	}

} 