<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mydask extends MY_Front_Controller{

	function __construct() {
		parent::__construct();
		if (!$this->User_loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }
		
		$this->load->model('Mdl_mydask','mdl');  
	}

	
	private $_table = 'sam_users';  

	function index()  
	{
		$meta['page_title'] = 'Register - Salon';
		$id = $this->session->userdata('user_loginid');

		$this->data['city'] = $this->mdl->get_city();
		$this->data['plans'] = $this->mdl->get_plan(); 
		$this->data['user'] = $this->mdl->get($id);

		$this->frontpage_construct('mydask/index', $meta, $this->data);  
	}

	public function update_user() { 

		$id = $this->session->userdata('user_loginid');
		$d = ORM::for_table($this->_table)->where('id',$id)->find_one();
		
		if($d) {
		
	        $d->business_type                   = isset($_POST['business_type']) ? $_POST['business_type'] : 'salon';
	        $d->first_name              = $_POST['fname'];
	        $d->last_name               = $_POST['sname'];
	        $d->business_name           = $_POST['business_name'];
	        $d->number_of_seats         = $_POST['number_of_seats'];
	        $d->area                    = $_POST['area'];	        
	        $d->about                   = $_POST['about'];
	        $d->mobile                	= $_POST['mobile'];
        	$d->business_address        = $_POST['business_address'];
        	$d->email_id 	            = $_POST['email'];
        	$d->city                    = $_POST['city'];
        	$d->remarks                 = $_POST['remarks'];
        	$d->use_dashboard           = isset($_POST['udash']) ? $_POST['udash'] : 0;
	        $d->status                  = isset($_POST['status']) ? $_POST['status'] : 'active';
	        
            $d->updated_time                = date('Y-m-d H:i:s');            
	        $d->monday_time_start       = $_POST['monday_time_start'];
            $d->tuesday_time_start      = $_POST['tuesday_time_start'];
            $d->wednesday_time_start    = $_POST['wednesday_time_start'];
            $d->thursday_time_start     = $_POST['thursday_time_start'];
            $d->friday_time_start       = $_POST['friday_time_start'];
            $d->saturday_time_start     = $_POST['saturday_time_start'];
            $d->sunday_time_start       = $_POST['sunday_time_start'];
            
            $d->monday_time_end         = $_POST['monday_time_end'];
            $d->tuesday_time_end        = $_POST['tuesday_time_end'];
            $d->wednesday_time_end      = $_POST['wednesday_time_end'];
            $d->thursday_time_end       = $_POST['thursday_time_end'];
            $d->friday_time_end         = $_POST['friday_time_end'];
            $d->saturday_time_end	    = $_POST['saturday_time_end'];
            $d->sunday_time_end	        = $_POST['sunday_time_end'];
            
            $d->latitude               = $_POST['latitude'];
            $d->longitude              = $_POST['longitude'];
            $d->plan_id                = $_POST['plan_id'];

            $temp = rand(999999,000000);
            if(!empty($_FILES['banner']['name']))
            {
            	if(is_uploaded_file($_FILES['banner']['tmp_name'])) {
	               $this->sam->upload_image('banner','themes/assets/images/usersign/',$temp);
	               $d->banner = $temp.''.$_FILES['banner']['name'];
	        	}	
            }
	        $temp = rand(999999,000000);
	        if(!empty($_FILES['logo']['name'])){
	        	if(is_uploaded_file($_FILES['logo']['tmp_name'])) {
	            	$this->sam->upload_image('logo','themes/assets/images/usersign/',$temp);
	         	   $d->logo = $temp.''.$_FILES['logo']['name'];
	        	}
	    	}
	        
	    	if($d->save())
        {
            $output['status'] = 1;
            $output['msg'] = 'Successfully updated profile'; 
        }else{
            $output['status'] = 0;
            $output['msg'] = 'Successfully not updated !!';
        }    
        echo json_encode($output);
	
	}
	

	}
}