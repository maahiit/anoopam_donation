<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

class Register extends MY_Front_Controller{


	function __construct(){
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Mdl_register','mdl'); 
  	}



	function index()  
	{
		$meta['page_title'] = 'Register - Salon';
		$this->frontpage_construct('register/index', $meta, $this->data);  
	}

	function register_add() 
	{
        $mobileExist = ORM::for_table('sam_users')
                                ->where('mobile',$_POST['mobile'])
                                ->find_one();
                                
        if($mobileExist) { $output['status'] = 2;$output['msg'] = 'mobile no already Exist';echo json_encode($output); die(); }                                

        $emailExist = ORM::for_table('sam_users')
                                ->where('email_id',$_POST['email_id'])
                                ->find_one();

        if($emailExist) { $output['status'] = 2;$output['msg'] = 'Email id already Exist';echo json_encode($output); die(); }

        $checkexist = ORM::for_table('sam_users')
                                ->where('email_id',$_POST['email_id'])
                                ->where('mobile',$_POST['mobile'])
                                ->find_one();

        if($checkexist)
        {
            $output['status'] = 2;
            $output['msg'] = 'Salon Alreeay Exist';

        }else
        { 
            $d = ORM::for_table('sam_users')->create();
            $d->name            = $_POST['name'];
            $d->mobile          = $_POST['mobile'];
            $d->email_id        = $_POST['email_id'];
            $d->password_txt    = ($_POST['password']!= "") ? $_POST['password'] : $d->password;
            
            $d->password        = ($_POST['password']!= "") ? md5($_POST['password']) : $d->password;  
            $d->type            = 'salon';
            $d->user_identity   = 4;
            
            if($d->save())
            {
                $output['status'] = 1;
                $output['msg'] = 'Successfully Register';
            }
            else
            {
                $output['status'] = 0;
                $output['msg'] = 'Something Wrong !!';
            }   
        }
		 
        echo json_encode($output); 
        
     
	}


}