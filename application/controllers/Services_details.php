<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

class Services_details extends MY_Front_Controller{   


	function __construct(){
        parent::__construct();
        $this->load->library('form_validation');  
        $this->load->library('Sam','sam'); 
        $this->load->model('Mdl_services_details','mdl'); 
        $this->load->model('Mdl_home_settings','mdl_home'); 
  	}
    
    private $_table = 'services';

	function index($id = '')   
	{	
		if(empty($id))
		{
				redirect('services_listing');
		}

		$meta['page_title'] = 'Services - Details';  
		$this->data['row'] = $this->mdl->get($id); 	 
		if(empty($this->data['row']))
		{
				redirect('services_listing');	
		}
		$this->frontpage_construct('services_details/index', $meta, $this->data); 
	}

} 