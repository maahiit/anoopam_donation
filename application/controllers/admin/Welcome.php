<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {

	function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            admin_redirect('login');
        }
        $this->load->library('form_validation');
        $this->load->model('mdl_home','mdl');
    }
    
	public function index()
	{
        $this->data['salons'] = $this->mdl->get_salon_count();

        $this->data['category'] = $this->mdl->get_category();
        $this->data['subcategory'] = $this->mdl->sget_ubcategory();
        $this->data['payment_getway'] = $this->mdl->get_payment_getway();
        $this->data['contributor'] = $this->mdl->get_contributor();
        $this->data['total_donation'] = $this->mdl->get_total_donation();

            
		$meta['page_title'] = 'Dashboard';
        $this->load->model('mdl_customer');
        $this->data['customersdata'] = $this->mdl_customer->get_all();
		$this->page_construct('dashboard', $meta, $this->data);
	}
}
