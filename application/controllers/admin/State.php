<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class State extends MY_Controller {

	function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            admin_redirect('login');
        }

        $this->load->library('form_validation');
        $this->load->model('Mdl_state','mdl');
        $this->load->library('pagination');
    }


   public function unset_session_value() {

    $this->session->unset_userdata('state_s_state');
    $this->session->unset_userdata('state_serach_page');
    $this->session->unset_userdata('state_serach_data');
    redirect('admin/state');

  }   

    public function index() {

        $w  = '';
        
        
       if($this->session->state_serach_page != 'state')
        {
            $this->session->unset_userdata('state_serach_data');
            $this->session->unset_userdata('state_serach_page');
        }  

        $paginationdata = $this->data['Settings']->rows_per_page;
        if($_POST) 
        {
            if(isset($_POST['state_s_state']) AND $_POST['state_s_state'] != '') 
            {
                $w .= " AND state like '%".$_POST['state_s_state']."%'";
                $this->session->set_userdata('state_s_state',$_POST['state_s_state']);
            }
            $_SESSION['state_serach_data'] = $w;
            $this->session->set_userdata('state_serach_data',$w);
            $this->session->set_userdata('state_serach_page','state');
        }

        if(isset($this->session->state_serach_data) AND $this->session->state_serach_data != '')
        {
             $w = $this->session->userdata('state_serach_data');
        }
        if($_POST)
        { 
               $Record = $this->mdl->get_search_count($paginationdata,$w);
        }else{
            if(isset($this->session->state_serach_data) AND $this->session->state_serach_data != '')
            {
                 $w = $this->session->userdata('state_serach_data');
                 $Record = $this->mdl->get_search_count($paginationdata,$w);
            }
            else
            {   
                  $Record =  $this->mdl->get_count();  
            }
        }

        $config = $this->sam->pagination_config();
        $config['base_url'] = site_url().'admin/state/index';
        $config['total_rows'] = $Record;
        $config['per_page'] = $paginationdata;
        $this->pagination->initialize($config);

        $this->data['state'] = $this->mdl->get_all_with_pagi('id',$config['per_page'],$this->uri->segment(4),$w);
        
        $meta['page_title'] = 'State Master';
        $this->page_construct('state/view', $meta, $this->data);
    }

     public function edit($id) {
        
        $meta['page_title'] = 'State Master';
        if($_POST && $_POST['id'] != '') {

            if($this->mdl->update_State($_POST)) 
            {
                $this->session->set_flashdata('success','Successfully Update Record ');
                redirect('admin/state');

            } else {
                $this->session->set_flashdata('error',lang('cupdatef'));
                redirect('admin/state');
            }
        } else {
            $this->data['state'] = $this->mdl->get($id);
            $this->page_construct('state/edit',$meta, $this->data);
        }
    }
    

    
}