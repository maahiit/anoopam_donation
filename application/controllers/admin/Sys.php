<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sys extends MY_Controller {

    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            admin_redirect('login');
        }

        $this->load->library('form_validation');
        $this->load->model('mdl_settings','mdl');
    }
    
    public function index() {
       
        $meta['page_title'] = lang('syssettings');

        // Form Validation
        $this->form_validation->set_rules('sitename', lang('sitename'), 'trim|required');
        $this->form_validation->set_rules('language', lang('language'), 'trim|required');
        $this->form_validation->set_rules('rows_per_page', lang('rows_per_page'), 'trim|required');

        if($this->form_validation->run() == true) {
            // update details
            $data = [
                        "sitename"                  => $this->input->post('sitename'),
                        "site_title"                  => $this->input->post('site_title'),
                        "site_url"                  => $this->input->post('site_url'),
                        "copyright"                  => $this->input->post('copyright'),
                        "pre_fix_amount"                  => $this->input->post('pre_fix_amount'),
                        "pan_amount"                  => $this->input->post('pan_amount'),
                        "sms_getway_url"                  => $this->input->post('sms_getway_url'),
                        "sms_getway_api_url"                  => $this->input->post('sms_getway_api_url'),
                        "sms_getway_api_key"                  => $this->input->post('sms_getway_api_key'),
                        "sms_getway_api_secret_key"                  => $this->input->post('sms_getway_api_secret_key'),
                        "google_api_app_id"                  => $this->input->post('google_api_app_id'),
                        "google_api_key"                  => $this->input->post('google_api_key'),
                        "google_api_secret_key"                  => $this->input->post('google_api_secret_key'),
                        "language"                  => $this->input->post('language'),
                        "rows_per_page"             => $this->input->post('rows_per_page'),           
                        "timezone"                  => $this->input->post('timezone')
                    ];

            if($this->mdl->update_settings($data)) {
                $this->session->set_flashdata('success',lang('settingupdate'));
                redirect('settings');
            } else {
                $this->session->set_flashdata('error',lang('settingupdatef'));
                redirect('settings');
            }
        } else {
            $this->data['setting'] = $this->mdl->get(1);
            $this->data['languages'] = $this->sam->get_languages();
            $this->data['timezonelist'] = sam::timezoneList();
            $this->page_construct('settings/index', $meta, $this->data);
        }
    }

}
