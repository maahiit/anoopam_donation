<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Transition extends MY_Controller {

	function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            admin_redirect('login');
        }

        $this->load->library('form_validation');
        $this->load->model('Mdl_transition','mdl');
        $this->load->library('pagination');
    }

    public function unset_session_value()
    {
        $this->session->unset_userdata('transition_s_name');
        $this->session->unset_userdata('transition_s_category');
        $this->session->unset_userdata('transition_s_subcategory');
        $this->session->unset_userdata('transition_s_from');
        $this->session->unset_userdata('transition_se_s_status');
        
        $this->session->unset_userdata('transition_serach_page');
        $this->session->unset_userdata('transition_serach_data');
        redirect('admin/transition');
    }   

    public function index()
    {
        $w  = '';       
        
       if($this->session->transition_serach_page != 'transition')
        {
            $this->session->unset_userdata('transition_serach_data');
            $this->session->unset_userdata('transition_serach_page');
        }  

        $paginationdata = $this->data['Settings']->rows_per_page;
        if($_POST) 
        {
            if(isset($_POST['transition_s_name']) AND $_POST['transition_s_name'] != '') 
            {
                $w .= " AND name like '%".$_POST['transition_s_name']."%'";
                $this->session->set_userdata('transition_s_name',$_POST['transition_s_name']);
            }
            if(isset($_POST['transition_s_category']) AND $_POST['transition_s_category'] != '') 
            {
                $w .= " AND category_id = '".$_POST['transition_s_category']."'";
                $this->session->set_userdata('transition_s_category',$_POST['transition_s_category']);
            }
            if(isset($_POST['transition_s_subcategory']) AND $_POST['transition_s_subcategory'] != '') 
            {
                $w .= " AND sub_category_id = '".$_POST['transition_s_subcategory']."'";
                $this->session->set_userdata('transition_s_subcategory',$_POST['transition_s_subcategory']);
            }
            if(isset($_POST['transition_se_s_status']) AND $_POST['transition_se_s_status'] != '') 
            {
                $w .= " AND payment_status = '".$_POST['transition_se_s_status']."'";
                $this->session->set_userdata('transition_se_s_status',$_POST['transition_se_s_status']);
            }
            
            if(isset($_POST['transition_s_from']) AND $_POST['transition_s_from'] != '') 
            {
                $w .= " AND DATE(payment_response_time) = '".$_POST['transition_s_from']."'";
                $this->session->set_userdata('transition_s_from',$_POST['transition_s_from']);
            }
            $_SESSION['transition_serach_data'] = $w;
            $this->session->set_userdata('transition_serach_data',$w);
            $this->session->set_userdata('transition_serach_page','transition');
        }

        if(isset($this->session->transition_serach_data) AND $this->session->transition_serach_data != '')
        {
             $w = $this->session->userdata('transition_serach_data');
        }
        if($_POST)
        { 
               $Record = $this->mdl->get_search_count($paginationdata,$w);
        }else
        {
            if(isset($this->session->transition_serach_data) AND $this->session->transition_serach_data != '')
            {
                 $w = $this->session->userdata('transition_serach_data');
                 $Record = $this->mdl->get_search_count($paginationdata,$w);
            }
            else
            {   
                  $Record =  $this->mdl->get_count();  
            }
        }

        $config = $this->sam->pagination_config();
        $config['base_url'] = site_url().'admin/transition/index';
        $config['total_rows'] = $Record;
        $config['per_page'] = $paginationdata;
        $this->pagination->initialize($config);
        
        $this->data['rows'] = $this->mdl->get_all_with_pagi('id',$config['per_page'],$this->uri->segment(4),$w); 
        
        $this->data['categories'] = $this->mdl->get_category();
        $this->data['sub_categories'] = $this->mdl->get_sub_categories();
        
        if(isset($_POST['SearchValue']))
        {
        	if($_POST['SearchValue'] == 'excel')
        	{
        	  $this->export_excel($w);
        	}    
        }
        
        $meta['page_title'] = 'Donations';
        $this->page_construct('transition/view', $meta, $this->data);
    }

    public function add_row() 
    {       

        $d = ORM::for_table('transition')->create();

        $d->name                     =$_POST['name'];        
        $d->status                   = isset($_POST['status']) ? $_POST['status'] : 'active';
        $d->is_deleted               = '0';
        $d->inserted_time            = date('Y-m-d H:i:s');
        $d->updated_time             = date('Y-m-d H:i:s');
        $d->created_by_user_id       = $this->session->userdata('loginid');
        $d->updated_by_user_id       = $this->session->userdata('loginid');       

        $d->save();
        echo $d->id;
    }
     

    public function edit($id)
    {

        $meta['page_title'] = 'Edit transition Master';
        if($_POST && $_POST['id'] != '') {

            if($this->mdl->update_row($_POST)) 
            {
                $this->session->set_flashdata('success','Successfully Update Record ');
                redirect('admin/transition');

            } else {
                $this->session->set_flashdata('error',lang('cupdatef'));
                redirect('admin/transition');
            }
        }else {
            $this->data['row'] = $this->mdl->get($id);
             
            $this->page_construct('transition/edit',$meta, $this->data);
        }
    }
    
     function export_excel($w)
    {
        
        $query = "SELECT * FROM transition WHERE is_deleted = '0' $w ORDER BY id DESC";
		$d = ORM::for_table('transition')->raw_query($query)->find_array();
			
       $date =  date('d-m-Y');
       $excelName = 'donation_'.$date;
    
       $this->load->library("excel");
       $object = new PHPExcel();
       $object->setActiveSheetIndex(0);
       $table_columns = array("Id", "name","Email","Mobile","Category","Sub Category","Type","Amount","Pan Number","Transition No","Payment status","Trust fund","Payment Datetime");
    
      $column = 0;
    
      foreach($table_columns as $field)
      {
        $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
        $column++;
      }
    
      $excel_row = 2;
       
      foreach($d as $key=>$row)
      {
        
        $categoryname = $this->sam->get_real_value('categories_mst','id',$row['category_id'],'name');
        $subcategoryname = $this->sam->get_real_value('subcategories_mst','id',$row['sub_category_id'],'name');
        
       $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $key);
       $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row['name']);
       $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row['donner_email']);
       $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row['mobile']);
       $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $categoryname);
       $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $subcategoryname);
       $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row['contributor_type']);
       $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row['amount']);
       $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row['pan']);
       $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row['payment_transition_id']);
       $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row['payment_status']);
       $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $row['donation_trustfund']);
       $object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, $row['payment_response_time']);
       

       
       
       
       $excel_row++;
      }
    
      $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="'.$excelName.'.xls"');
      $object_writer->save('php://output');
     }


    public function delete_row($id)
    {
        $this->sam->_delete_by_id($id,'transition'); 
    }
    
}