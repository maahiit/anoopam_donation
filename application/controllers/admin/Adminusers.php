<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminusers extends MY_Controller {

    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            admin_redirect('login');
        }

        $this->load->library('form_validation');
        $this->load->model('Mdl_adminuser','mdl');
        $this->load->library('pagination');
    }

    public function unset_session_value() {

    $this->session->unset_userdata('admin_s_status');
    $this->session->unset_userdata('admin_s_mobile');
    $this->session->unset_userdata('admin_s_email');
    $this->session->unset_userdata('admin_s_designation');
    $this->session->unset_userdata('admin_s_name');

    $this->session->unset_userdata('adminusers_serach_page');
    $this->session->unset_userdata('adminusers_serach_data');
    redirect('admin/adminusers');

  } 
    
    public function index() {
        // Pagination Start
        $w = '';

        if($this->session->adminusers_serach_page != 'adminusers')
        {
            $this->session->unset_userdata('adminusers_serach_data');
            $this->session->unset_userdata('adminusers_serach_page');
        }  

        $paginationdata = $this->data['Settings']->rows_per_page;

        if($_POST) {
            
            if(isset($_POST['admin_s_mobile']) AND $_POST['admin_s_mobile'] != '') {
                $w .= " AND mobile ='".$_POST['admin_s_mobile']."'";
                $this->session->set_userdata('admin_s_mobile',$_POST['admin_s_mobile']);
            }
            if(isset($_POST['admin_s_designation']) AND $_POST['admin_s_designation'] != '') {
                $w .= " AND role ='".$_POST['admin_s_designation']."'";
                $this->session->set_userdata('admin_s_designation',$_POST['admin_s_designation']);
            }
            if(isset($_POST['admin_s_email']) AND $_POST['admin_s_email'] != '') {
                $w .= " AND email_id ='".$_POST['admin_s_email']."'";
                $this->session->set_userdata('admin_s_email',$_POST['admin_s_email']);
            }
            /*if(isset($_POST['s_customer']) AND $_POST['s_customer'] != '') {
                $w .= " AND organization_name like '%".$_POST['s_customer']."%'";
                $this->session->set_userdata('state_s_state',$_POST['state_s_state']);
            }*/
            if(isset($_POST['admin_s_name']) AND $_POST['admin_s_name'] != '') {
                $w .= " AND first_name like '%".$_POST['admin_s_name']."'";
                $this->session->set_userdata('admin_s_name',$_POST['admin_s_name']);
            }
            if(isset($_POST['admin_s_status']) AND $_POST['admin_s_status'] != '') {
                $w .= " AND status ='".$_POST['admin_s_status']."'";
                $this->session->set_userdata('admin_s_status',$_POST['admin_s_status']);
            }

            $_SESSION['adminusers_serach_data'] = $w;
            $this->session->set_userdata('adminusers_serach_data',$w);
            $this->session->set_userdata('adminusers_serach_page','adminusers');

        }

        if(isset($this->session->adminusers_serach_data) AND $this->session->adminusers_serach_data != '')
        {
             $w = $this->session->userdata('adminusers_serach_data');
        }
        if($_POST)
        { 
               $Record = $this->mdl->get_search_count($paginationdata,$w);
        }else{
            if(isset($this->session->adminusers_serach_data) AND $this->session->adminusers_serach_data != '')
            {
                 $w = $this->session->userdata('adminusers_serach_data');
                 $Record = $this->mdl->get_search_count($paginationdata,$w);
            }
            else
            {   
                  $Record =  $this->mdl->get_count();  
            }
        }
        
        $config = $this->sam->pagination_config();
        $config['base_url'] = site_url().'admin/adminusers/index';
        $config['total_rows'] = $Record;
        $config['per_page'] = $paginationdata;
        $this->pagination->initialize($config);
        
        $meta['page_title'] = 'Admin User';
        $this->data['users'] = $this->mdl->get_users($config['per_page'],$this->uri->segment(4),$w);
        $this->data['roles'] = $this->sam->get_roles();
        $this->page_construct('adminusers/view', $meta, $this->data);
    }

    public function edit($id) {
        $meta['page_title'] = 'Admin User';
        if($_POST && $_POST['fname'] != '' && $_POST['sname'] != '') {
            if($this->mdl->update_adminuser($_POST)) {
                $this->session->set_flashdata('success',lang('Admin Update successfully'));
                redirect('admin/adminusers');
            } else {
                $this->session->set_flashdata('error',lang('Admin Update fail'));
                redirect('admin/adminusers');
            }
        } else {
            $this->data['user'] = $this->mdl->get_user($id);
            $this->data['roles'] = $this->sam->get_roles();
            $this->page_construct('adminusers/edit',$meta, $this->data);
        }
    }

}
