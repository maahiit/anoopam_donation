<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends MY_Controller {

	function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            admin_redirect('login');
        }

        $this->load->library('form_validation');
        $this->load->model('Mdl_categories','mdl');
        $this->load->library('pagination');
    }

    public function unset_session_value()
    {
        $this->session->unset_userdata('categories_s_name');
        $this->session->unset_userdata('categories_serach_page');
        $this->session->unset_userdata('categories_serach_data');
        redirect('admin/categories');
    }   

    public function index()
    {
        $w  = '';       
        
       if($this->session->categories_serach_page != 'categories')
        {
            $this->session->unset_userdata('categories_serach_data');
            $this->session->unset_userdata('categories_serach_page');
        }  

        $paginationdata = $this->data['Settings']->rows_per_page;
        if($_POST) 
        {
            if(isset($_POST['categories_s_name']) AND $_POST['categories_s_name'] != '') 
            {
                $w .= " AND name like '%".$_POST['categories_s_name']."%'";
                $this->session->set_userdata('categories_s_name',$_POST['categories_s_name']);
            }
            $_SESSION['categories_serach_data'] = $w;
            $this->session->set_userdata('categories_serach_data',$w);
            $this->session->set_userdata('categories_serach_page','categories');
        }

        if(isset($this->session->categories_serach_data) AND $this->session->categories_serach_data != '')
        {
             $w = $this->session->userdata('categories_serach_data');
        }
        if($_POST)
        { 
               $Record = $this->mdl->get_search_count($paginationdata,$w);
        }else
        {
            if(isset($this->session->categories_serach_data) AND $this->session->categories_serach_data != '')
            {
                 $w = $this->session->userdata('categories_serach_data');
                 $Record = $this->mdl->get_search_count($paginationdata,$w);
            }
            else
            {   
                  $Record =  $this->mdl->get_count();  
            }
        }

        $config = $this->sam->pagination_config();
        $config['base_url'] = site_url().'admin/categories/index';
        $config['total_rows'] = $Record;
        $config['per_page'] = $paginationdata;
        $this->pagination->initialize($config);

        $this->data['rows'] = $this->mdl->get_all_with_pagi('id',$config['per_page'],$this->uri->segment(4),$w);

        $this->data['payment_gateways'] = $this->mdl->get_payment_gateway();
        $this->data['countries'] = $this->mdl->get_country();
        $meta['page_title'] = 'Categories Master';
        $this->page_construct('categories/view', $meta, $this->data);
    }

    public function add_row() 
    {


        $d = ORM::for_table('categories_mst')->create();

        $d->name                     = @$_POST['name'];
        
        $d->trustfund                = isset($_POST['trustfund']) ? @$_POST['trustfund'] : 'no';
        $d->trustfund_minimum_amount           = @$_POST['trustfund_minimum_amount'];

        $d->minimum_amount           = @$_POST['minimum_amount'];
        $d->maximum_amount           = @$_POST['maximum_amount'];
        
        $d->payment_gateway          = implode(',',@$_POST['payment_gateway']);
        $d->country_id               = implode(',',@$_POST['country_id']);
        $d->status                   = isset($_POST['status']) ? $_POST['status'] : 'active';
        $d->is_deleted               = '0';
        $d->inserted_time            = date('Y-m-d H:i:s');
        $d->updated_time             = date('Y-m-d H:i:s');
        $d->created_by_user_id       = $this->session->userdata('loginid');
        $d->updated_by_user_id       = $this->session->userdata('loginid');       

        $d->save();
        echo $d->id;
    }
     

    public function edit($id)
    {

        $meta['page_title'] = 'Edit Categories Master';
        if($_POST && $_POST['id'] != '') {

            if($this->mdl->update_row($_POST)) 
            {
                $this->session->set_flashdata('success','Successfully Update Record ');
                redirect('admin/categories');

            } else {
                $this->session->set_flashdata('error',lang('cupdatef'));
                redirect('admin/categories');
            }
        }else {
            $this->data['row'] = $this->mdl->get($id);
             $this->data['payment_gateways'] = $this->mdl->get_payment_gateway();
             $this->data['countries'] = $this->mdl->get_country();
            $this->page_construct('categories/edit',$meta, $this->data);
        }
    }


    public function delete_row($id)
    {
        $this->sam->_delete_by_id($id,'categories_mst'); 
    }
    
}