<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emaillog extends MY_Controller {

	function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            admin_redirect('login');
        }

        $this->load->library('form_validation');
        $this->load->model('mdl_emaillog','mdl');
        $this->load->library('pagination');
    }
    
	public function index() {

        if($_POST) {
            $this->data['status'] = 'block';
            $this->data['emaillog'] = $this->mdl->get_all_join(NULL,NULL,$_POST);
        } else {
            // Pagination Start
            $config = $this->sam->pagination_config();
            $config['base_url'] = site_url().'emaillog/index';
            $config['total_rows'] = $this->mdl->get_count();
            $config['per_page'] = $this->data['Settings']->rows_per_page;
            $this->pagination->initialize($config);
            // Pagination End
            $this->data['status'] = 'block';
            $this->data['emaillog'] = $this->mdl->get_all_join($config['per_page'],$this->uri->segment(3));
        }
		$meta['page_title'] = lang('emaillog');
		$this->page_construct('emailTemplate/log_view', $meta, $this->data);
	}

}