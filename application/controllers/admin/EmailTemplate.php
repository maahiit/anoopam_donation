<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EmailTemplate extends MY_Controller {

	function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            admin_redirect('login');
        }

        $this->load->library('form_validation');
        $this->load->model('mdl_emailtemplate','mdl');
    }
    
	public function index() {
		$meta['page_title'] = lang('emailtemplates');
        $this->data['email'] = $this->mdl->get_all();
		$this->page_construct('emailTemplate/view', $meta, $this->data);
	}

    public function edit($id) {
        $meta['page_title'] = lang('editemailtemplate');
        if($_POST && $_POST['name'] != '') {
            if($this->mdl->update_email_template($_POST)) {
                $this->session->set_flashdata('success',lang('emailupdate'));
                redirect('emailtemplate');
            } else {
                $this->session->set_flashdata('error',lang('emailupdatef'));
                redirect('emailtemplate');
            }
        } else {
            $this->data['email'] = $this->mdl->get($id);
            $this->page_construct('emailTemplate/edit', $meta, $this->data);
        }
    }

}