<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Smtp_email extends MY_Controller {

	function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            admin_redirect('login');
        }

        $this->load->library('form_validation');
        $this->load->model('Mdl_smtp_email','mdl');
        $this->load->library('pagination');
    }


    public function unset_session_value()
    {
        $this->session->unset_userdata('smtp_email_s_name');
        $this->session->unset_userdata('smtp_email_serach_data');
        redirect('admin/smtp_email');
    }   

    public function index()
    {

        

       

        $w  = '';       
        $paginationdata = $this->data['Settings']->rows_per_page;
        if($_POST) 
        {
            if(isset($_POST['smtp_email_s_name']) AND $_POST['smtp_email_s_name'] != '') 
            {
                $w .= " AND name like '%".$_POST['smtp_email_s_name']."%'";
                $this->session->set_userdata('smtp_email_s_name',$_POST['smtp_email_s_name']);
            }

            $_SESSION['smtp_email_serach_data'] = $w;
            $this->session->set_userdata('smtp_email_serach_data',$w);
            $this->session->set_userdata('smtp_email_serach_page','smtp_email');
        }

        if(isset($this->session->smtp_email_serach_data) AND $this->session->smtp_email_serach_data != '')
        {
             $w = $this->session->userdata('smtp_email_serach_data');
        }
        if($_POST)
        { 
               $Record = $this->mdl->get_search_count($paginationdata,$w);
        }else{
            if(isset($this->session->smtp_email_serach_data) AND $this->session->smtp_email_serach_data != '')
            {
                 $w = $this->session->userdata('smtp_email_serach_data');
                 $Record = $this->mdl->get_search_count($paginationdata,$w);
            }
            else
            {   
                  $Record =  $this->mdl->get_count();  
            }
        }

        $config = $this->sam->pagination_config();
        $config['base_url'] = site_url().'admin/smtp_email/index';
        $config['total_rows'] = $Record;
        $config['per_page'] = $paginationdata;
        $this->pagination->initialize($config);

        $this->data['rows'] = $this->mdl->get_all_with_pagi('id',$config['per_page'],$this->uri->segment(4),$w);
        
        $meta['page_title'] = 'SMTP Email ';
        $this->page_construct('smtp_email/view', $meta, $this->data);
    }

    public function add_row() 
    {
        $d = ORM::for_table('smtp_email')->create();
        $d->name                     = $_POST['name'];
        $d->host                     = $_POST['host'];
        $d->port                     = $_POST['port'];
        $d->username                 = $_POST['username'];
        $d->password                 = $_POST['password'];

        $d->ssl_tls                  = isset($_POST['ssl_tls']) ? $_POST['ssl_tls'] : 'ssl';
        $d->auth                     = isset($_POST['auth']) ? $_POST['auth'] : 'true';
        $d->status                   = isset($_POST['status']) ? $_POST['status'] : 'active';

        $d->is_deleted               = '0';
        $d->inserted_time            = date('Y-m-d H:i:s');
        $d->created_by_user_id       = $this->session->userdata('loginid');
        
        $d->save();
        echo $d->id;
    }     

    public function edit($id)
    {
        
               

        $meta['page_title'] = 'Edit SMTP Email ';
        if($_POST && $_POST['id'] != '') {

            if($this->mdl->update_row($_POST)) 
            {
                $this->session->set_flashdata('success','Successfully Update Record ');
                redirect('admin/smtp_email');

            } else {
                $this->session->set_flashdata('error',lang('cupdatef'));
                redirect('admin/smtp_email');
            }
        } else {
            $this->data['row'] = $this->mdl->get($id);
            $this->page_construct('smtp_email/edit',$meta, $this->data);
        }
    }
    

    public function delete_row($id) {
        $this->sam->_delete_by_id($id,'smtp_email'); 
    }
    

    
}