<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Set_accessrights extends MY_Controller {

	function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            admin_redirect('login');
        }

        $this->load->library('form_validation');
        $this->load->model('mdl_user','mdl');
        // $this->load->model('mdl_accessrights','model');

        $this->load->library('pagination');
    }
    
	public function index() {
        
        $roles = $this->sam->get_roles();
        $modules = $this->sam->get_module();
        
        foreach ($modules as $modulename => $module) 
        {
            $module_title = '';
            foreach ($roles as $role_key => $role) 
            {
                //echo $modulename.'</br>';
                $checkesist =  ORM::for_table('sam_accessrights')->where('role_id',$role_key)->where('page_title',$modulename)->find_one();
                if(empty($checkesist))
                {
                    $d = ORM::for_table('sam_accessrights')->create();
                    $d->role_id                = $role_key;
                    $d->page_title             = $modulename;
                    $d->page_name              = $module;
                    
                    $d->create_access          = 'yes';
                    $d->edit_access            = 'yes';
                    $d->view_access            = 'yes';
                    $d->delete_access          = 'yes';
                    $d->save();    
                }
    
            }
        }
        echo "success";


	}


 

    

    






}
