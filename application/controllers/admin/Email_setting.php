<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email_setting extends MY_Controller {

	function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            admin_redirect('login');
        }

        $this->load->library('form_validation');
        $this->load->model('mdl_settings','mdl');
  
    }


    public function index() {
       
        $meta['page_title'] = 'Email Setting';

        // Form Validation
        $this->form_validation->set_rules('smtp_status', lang('smtp_status'), 'trim|required');
        $this->form_validation->set_rules('smtp_encryption', lang('smtp_encryption'), 'trim|required');
        $this->form_validation->set_rules('smtp_username', lang('smtp_username'), 'trim|required');
        $this->form_validation->set_rules('smtp_password', lang('smtp_password'), 'trim|required');
        $this->form_validation->set_rules('smtp_name', lang('smtp_name'), 'trim|required');
        $this->form_validation->set_rules('smtp_host', lang('smtp_host'), 'trim|required');
        $this->form_validation->set_rules('smtp_port', lang('smtp_port'), 'trim|required');

        if($this->form_validation->run() == true) {
            // update details
            $data = [
                        "smtp_status"       => $this->input->post('smtp_status'),
                        "smtp_encryption"   => $this->input->post('smtp_encryption'),
                        "smtp_username"     => $this->input->post('smtp_username'),
                        "smtp_password"     => $this->input->post('smtp_password'),
                        "smtp_name"         => $this->input->post('smtp_name'),
                        "smtp_host"         => $this->input->post('smtp_host'),
                        "smtp_port"         => $this->input->post('smtp_port'),
                    ];

            if($this->mdl->update_smtp_settings($data)) {
                $this->session->set_flashdata('success','Email Smtp Setting Successfully Update');
                redirect('admin/email_setting');
            } else {
                $this->session->set_flashdata('error',lang('settingupdatef'));
                redirect('admin/email_setting');
            }
        } else {
            $this->data['setting'] = $this->mdl->get(1);
            $this->data['languages'] = $this->sam->get_languages();
            $this->data['timezonelist'] = sam::timezoneList();
            $this->page_construct('email_setting/index', $meta, $this->data);
        }
	}


    
}