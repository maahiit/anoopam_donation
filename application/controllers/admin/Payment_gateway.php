<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_gateway extends MY_Controller {

	function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            admin_redirect('login');
        }

        $this->load->library('form_validation');
        $this->load->model('Mdl_payment_gateway','mdl');
        $this->load->library('pagination');
    }


    public function unset_session_value()
    {
        $this->session->unset_userdata('payment_gateway_s_name');
        $this->session->unset_userdata('payment_gateway_serach_page');
        $this->session->unset_userdata('payment_gateway_serach_data');
        redirect('admin/payment_gateway');
    }   

    public function index()
    {
        $w  = '';       
        
       if($this->session->payment_gateway_serach_page != 'payment_gateway')
        {
            $this->session->unset_userdata('payment_gateway_serach_data');
            $this->session->unset_userdata('payment_gateway_serach_page');
        }  

        $paginationdata = $this->data['Settings']->rows_per_page;
        if($_POST) 
        {
            if(isset($_POST['payment_gateway_s_name']) AND $_POST['payment_gateway_s_name'] != '') 
            {
                $w .= " AND name like '%".$_POST['payment_gateway_s_name']."%'";
                $this->session->set_userdata('payment_gateway_s_name',$_POST['payment_gateway_s_name']);
            }
            $_SESSION['payment_gateway_serach_data'] = $w;
            $this->session->set_userdata('payment_gateway_serach_data',$w);
            $this->session->set_userdata('payment_gateway_serach_page','payment_gateway');
        }

        if(isset($this->session->payment_gateway_serach_data) AND $this->session->payment_gateway_serach_data != '')
        {
             $w = $this->session->userdata('payment_gateway_serach_data');
        }
        if($_POST)
        { 
               $Record = $this->mdl->get_search_count($paginationdata,$w);
        }else{
            if(isset($this->session->payment_gateway_serach_data) AND $this->session->payment_gateway_serach_data != '')
            {
                 $w = $this->session->userdata('payment_gateway_serach_data');
                 $Record = $this->mdl->get_search_count($paginationdata,$w);
            }
            else
            {   
                  $Record =  $this->mdl->get_count();  
            }
        }

        $config = $this->sam->pagination_config();
        $config['base_url'] = site_url().'admin/payment_gateway/index';
        $config['total_rows'] = $Record;
        $config['per_page'] = $paginationdata;
        $this->pagination->initialize($config);

        $this->data['rows'] = $this->mdl->get_all_with_pagi('id',$config['per_page'],$this->uri->segment(4),$w);
        
        $meta['page_title'] = 'Payment Gateway ';
        $this->page_construct('payment_gateway/view', $meta, $this->data);
    }

    public function add_row() 
    {
        $d = ORM::for_table('payment_gateway')->create();
        $d->name                     = $_POST['name'];
        $d->api_url                  = $_POST['api_url'];

        $d->user_name                = $_POST['user_name'];
        $d->password                 = $_POST['password']; 
        $d->product_id               = $_POST['product_id']; 
        $d->client_code              = $_POST['client_code']; 
        $d->account_no               = $_POST['account_no']; 
        $d->req_hash_key             = $_POST['req_hash_key']; 
        
        $d->status                   = isset($_POST['status']) ? $_POST['status'] : 'active';
        $d->is_deleted               = '0';
        $d->inserted_time            = date('Y-m-d H:i:s');
        $d->updated_time             = date('Y-m-d H:i:s');
        $d->created_by_user_id       = $this->session->userdata('loginid');
        $d->updated_by_user_id       = $this->session->userdata('loginid');       

        $d->save();
        echo $d->id;
    }     

    public function edit($id)
    {

        $meta['page_title'] = 'Edit Payment gateway ';
        if($_POST && $_POST['id'] != '') {

            if($this->mdl->update_row($_POST)) 
            {
                $this->session->set_flashdata('success','Successfully Update Record ');
                redirect('admin/payment_gateway');

            } else {
                $this->session->set_flashdata('error',lang('cupdatef'));
                redirect('admin/payment_gateway');
            }
        } else {
            $this->data['row'] = $this->mdl->get($id);
            $this->page_construct('payment_gateway/edit',$meta, $this->data);
        }
    }

    

    public function delete_row($id) {
        $this->sam->_delete_by_id($id,'payment_gateway');  
    }
    

    
}