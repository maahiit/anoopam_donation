<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Subcategories extends MY_Controller {

	function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            admin_redirect('login');
        }

        $this->load->library('form_validation');
        $this->load->model('Mdl_subcategories','mdl');
        $this->load->library('pagination');
    }

    public function unset_session_value()
    {
        $this->session->unset_userdata('subcategories_s_name');
        $this->session->unset_userdata('subcategories_serach_page');
        $this->session->unset_userdata('subcategories_serach_data');
        redirect('admin/subcategories');
    }   

    public function index()
    {
        $w  = '';       
        
       if($this->session->subcategories_serach_page != 'subcategories')
        {
            $this->session->unset_userdata('subcategories_serach_data');
            $this->session->unset_userdata('subcategories_serach_page');
        }  

        $paginationdata = $this->data['Settings']->rows_per_page;
        if($_POST) 
        {
            if(isset($_POST['subcategories_s_name']) AND $_POST['subcategories_s_name'] != '') 
            {
                $w .= " AND name like '%".$_POST['subcategories_s_name']."%'";
                $this->session->set_userdata('subcategories_s_name',$_POST['subcategories_s_name']);
            }
            $_SESSION['subcategories_serach_data'] = $w;
            $this->session->set_userdata('subcategories_serach_data',$w);
            $this->session->set_userdata('subcategories_serach_page','subcategories');
        }

        if(isset($this->session->subcategories_serach_data) AND $this->session->subcategories_serach_data != '')
        {
             $w = $this->session->userdata('subcategories_serach_data');
        }
        if($_POST)
        { 
               $Record = $this->mdl->get_search_count($paginationdata,$w);
        }else
        {
            if(isset($this->session->subcategories_serach_data) AND $this->session->subcategories_serach_data != '')
            {
                 $w = $this->session->userdata('subcategories_serach_data'); 
                 $Record = $this->mdl->get_search_count($paginationdata,$w);
            }
            else
            {   
                  $Record =  $this->mdl->get_count();  
            }
        }

        $config = $this->sam->pagination_config();
        $config['base_url'] = site_url().'admin/subcategories/index';
        $config['total_rows'] = $Record;
        $config['per_page'] = $paginationdata;
        $this->pagination->initialize($config);

        $this->data['rows'] = $this->mdl->get_all_with_pagi('id',$config['per_page'],$this->uri->segment(4),$w);

        $this->data['payment_gateways'] = $this->mdl->get_payment_gateway();
        $this->data['countries'] = $this->mdl->get_country();
        $this->data['categories'] = $this->mdl->get_category();
        $meta['page_title'] = 'Sub Categories';
        $this->page_construct('subcategories/view', $meta, $this->data);
    }

    public function add_row() 
    {


        

        $d = ORM::for_table('subcategories_mst')->create();

        $d->name                     = $_POST['name'];
        $d->category_id              = $_POST['category_id'];

        $d->trustfund                = isset($_POST['trustfund']) ? @$_POST['trustfund'] : 'no';
        $d->trustfund_minimum_amount = @$_POST['trustfund_minimum_amount']; 
        
        $d->minimum_amount           = $_POST['minimum_amount'];
        $d->maximum_amount           = $_POST['maximum_amount'];
        
        $d->payment_gateway          = implode(',',$_POST['payment_gateway']);
        $d->country_id               = implode(',',$_POST['country_id']);
        $d->status                   = isset($_POST['status']) ? $_POST['status'] : 'active';
        $d->is_deleted               = '0';
        $d->inserted_time            = date('Y-m-d H:i:s');
        $d->updated_time             = date('Y-m-d H:i:s');
        $d->created_by_user_id       = $this->session->userdata('loginid');
        $d->updated_by_user_id       = $this->session->userdata('loginid');       

        $d->save();
        echo $d->id;
    }
     

    public function edit($id)
    {

        $meta['page_title'] = 'Edit Sub Categories ';
        if($_POST && $_POST['id'] != '') {

            if($this->mdl->update_row($_POST)) 
            {
                $this->session->set_flashdata('success','Successfully Update Record ');
                redirect('admin/subcategories');

            } else {
                $this->session->set_flashdata('error',lang('cupdatef'));
                redirect('admin/subcategories');
            }
        }else {
            $this->data['row'] = $this->mdl->get($id);
             $this->data['payment_gateways'] = $this->mdl->get_payment_gateway();
             $this->data['countries'] = $this->mdl->get_country();
             $this->data['categories'] = $this->mdl->get_category();
            $this->page_construct('subcategories/edit',$meta, $this->data);
        }
    }


    public function delete_row($id)
    {
        $this->sam->_delete_by_id($id,'subcategories_mst'); 
    }
    
}