<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modal extends MY_Controller {

    function __construct()
    {
        parent::__construct();
    }

   private function _delete_by_id($id,$_table) {
        $d = ORM::for_table($_table)->where('id',$id)->find_one();
        if($d) {
            // change is_deleted to 1
            $d->is_deleted = '1';
            $d->save();
            echo TRUE;
        } else {
            echo FALSE;
        }
    }
/**********************************************************************************
Customer Add Delete Start
***********************************************************************************/
    public function customer_add() {
        $d = ORM::for_table('sam_customers')->create();

        $d->first_name              = isset($_POST['fname']) ? $_POST['fname'] : '';
        $d->last_name               = isset($_POST['lname']) ? $_POST['lname'] : '';
         $d->uname               = isset($_POST['uname']) ? $_POST['uname'] : '';
        $d->organization_name       = $_POST['organizationname'];
        $d->block                = isset($_POST['block']) ? $_POST['block'] : '';
        $d->email_id                = isset($_POST['email']) ? $_POST['email'] : '';
        $d->address                = isset($_POST['address']) ? $_POST['address'] : '';
        $d->phone_no                = $_POST['phone'];
        $d->street                  = isset($_POST['street']) ? $_POST['street'] : '';
        $d->zip_code                = isset($_POST['zipcode']) ? $_POST['zipcode'] : '';
        $d->city                    = isset($_POST['city']) ? $_POST['city'] : '';
        $d->status                  = isset($_POST['status']) ? $_POST['status'] : 'active';
        // $d->created_by              = $this->session->userdata('loginid');
        $d->modified                = date('Y-m-d H:i:s');
        // $d->modified_by             = $this->session->userdata('loginid');
        
        for($i=0;$i<count($_POST['remark'])-1;$i++){  
          
        $remark = $_POST['remark'][$i]; 
        $tmp_time = time(); 
        if(is_uploaded_file($_FILES['documents']['tmp_name'][$i])) {
                    $this->sam->upload_multiple_image('documents','themes/assets/images/attachments/',$i,$tmp_time);
                  $documents_atteched = $tmp_time.$_FILES['documents']['name'][$i];
                  $documents_id = $_POST['documents_id'][$i];
                } else {
                    //$product_images_imp =$product_images;
                }
                $_POST['attechment_detail'][$i]['remark'] = $remark;
                $_POST['attechment_detail'][$i]['doc_attechment'] = $documents_atteched;
                $_POST['attechment_detail'][$i]['documents_id'] = $documents_id; 
        }

       $d->documents = json_encode($_POST['attechment_detail']);    
        $d->save();


         $i=0;               
        foreach ($_POST['cperson'] as $cloop) {                
           

                $g = ORM::for_table('sam_contacts_detail')->create();
                $g->customer_id       = $d->id;
                $g->cperson           = $_POST['cperson'][$i];
                $g->emailperson       = $_POST['emailperson'][$i];
                $g->phoneperson       = $_POST['phoneperson'][$i];
                $g->created          = date('Y-m-d H:i:s');
                $g->save();
                $i++;
            }        
        echo $d->id;
    }

    public function customer_delete($id) {
        $this->_delete_by_id($id,'sam_customers');
    }

/**********************************************************************************
Customer Add Delete End
***********************************************************************************/

/**********************************************************************************
User Add Delete Start
***********************************************************************************/

    public function user_add() {        
      
              
            $d = ORM::for_table('sam_users')->create(); 
            
            $d->first_name              = $_POST['first_name'];
            $d->last_name               = $_POST['last_name'];
            $d->middle_name             = $_POST['middle_name'];
            $d->mobile                  = $_POST['mobile'];
            $d->email_id                = $_POST['email_id'];
            $d->password                = ($_POST['pwd']!= "") ? md5($_POST['pwd']) : $d->password;
            $d->password_txt            = ($_POST['pwd']!= "") ? $_POST['pwd'] : $d->password_txt;
            $d->otp                     = $_POST['otp'];
            $d->google_id               = $_POST['google_id'];
            $d->type                    = $_POST['type'];
            $d->contributor_type        = $_POST['contributor_type'];
            $d->address                 = $_POST['address'];
            $d->city                    = $_POST['city'];
            $d->pan                     = $_POST['pan'];
            $d->status                  = isset($_POST['status']) ? $_POST['status'] : 'active';            
            $d->inserted_time           = date('Y-m-d H:i:s');
            $d->updated_time            = date('Y-m-d H:i:s');
            $d->created_by              = $this->session->userdata('loginid');
            $d->updated_by              = $this->session->userdata('loginid');           
            
            $d->save();
            $this->send_mail();
             $output['status'] = 1;
             $output['msg'] = 'New User Created Successfully';
             echo json_encode($output);        
        
        
    }

    public function user_delete($id) {
        $this->_delete_by_id($id,'sam_users');
    }
/**********************************************************************************
User Add Delete End
***********************************************************************************/

/**********************************************************************************
Send Mail  Start
***********************************************************************************/
    
    public function send_mail() { 
        
        $email = $_POST['email'];
        $Setting = ORM::for_table('sam_settings')->where('id','1')->find_one();
        $RegistrationTemplete = ORM::for_table('zyd_email_template')->where('id','1')->find_one();
        $Massage =  $RegistrationTemplete['email_body'];

        $Massage = str_replace ('[USER_NAME]',$_POST['fname'],$Massage);
        $Massage = str_replace ('[USER_EMAIL]',$_POST['email'],$Massage);
        $Massage = str_replace ('[USER_PASSWORD]',$_POST['pwd'],$Massage);
        $Massage = str_replace ('[SITE_NAME]',$Setting['sitename'],$Massage);
        
        $email_config = ORM::for_table('sam_settings')->where('id',1)->find_one();

        $config = array(
                'protocol'  => $email_config['smtp_name'],
                'smtp_host' => $email_config['smtp_host'],
                'smtp_port' => $email_config['smtp_port'],
                'smtp_user' => $email_config['smtp_username'],
                'smtp_pass' => $email_config['smtp_password'],
                'mailtype'  => 'html', 
                'charset'   => 'iso-8859-1'
            );
    
          $this->load->library('email',$config); 

          $this->email->from('no_reply@maahiit.in', 'Registration Mail');
          $this->email->to($email);
          $this->email->subject($RegistrationTemplete['subject']);
          $this->email->message($Massage);
          
          if($email_config['smtp_status'] == 'on')
          {
            $this->email->send();    
          }
      }

/**********************************************************************************
Send Mail Add Delete End
***********************************************************************************/






/**********************************************************************************
Email Template              Start
***********************************************************************************/
    public function emailtemplate_add() {
        $d = ORM::for_table('zyd_email_template')->create();
        
        $d->template_name        = isset($_POST['name']) ? $_POST['name'] : '';
        $d->subject             = $_POST['subject'];
        $d->email_body          = $_POST['message'];

        $d->status               = isset($_POST['intake_status']) ? $_POST['intake_status'] : 'active';
        $d->inserted_time        = date('Y-m-d H:i:s');
        $d->created_by_user_id   = $this->session->userdata('loginid');

         if(is_uploaded_file($_FILES['attachment']['tmp_name'])) {
                $this->sam->upload_image('attachment','themes/assets/attachment/');
                $d->attachment = $_FILES['attachment']['name'];
            }
        $d->save();
        echo $d->id;
    }

    public function emailtemplate_delete($id) {
        $this->_delete_by_id($id,'uni_email_template');
    }
/**********************************************************************************
Email Template              End
***********************************************************************************/    
/**********************************************************************************
State              Start
***********************************************************************************/
    public function state_add() 
    {
        $d = ORM::for_table('zyd_state')->create();
        $d->state                    = $_POST['state'];
        $d->inserted_time            = date('Y-m-d H:i:s');
        $d->updated_time             = date('Y-m-d H:i:s');
        $d->created_by_user_id       = $this->session->userdata('loginid');
        $d->updated_by_user_id       = $this->session->userdata('loginid');

        $d->save();
        echo $d->id;
    }
   public function State_delete($id) {
        $this->_delete_by_id($id,'zyd_state');
    }
/**********************************************************************************
State              End
***********************************************************************************/


/**********************************************************************************
City              Start
***********************************************************************************/
    public function city_add() 
    {
        $d = ORM::for_table('zyd_city')->create();
        $d->state                     = $_POST['state'];
        $d->city                     = $_POST['city'];
        if(isset( $_FILES['image']['name']) ){
                 
                
                if(is_uploaded_file($_FILES['image']['tmp_name'])) {
                $this->sam->upload_image('image','themes/assets/images/city/');
                $d->image = $_FILES['image']['name'];
                }    
            }
        $d->inserted_time            = date('Y-m-d H:i:s');
        $d->updated_time             = date('Y-m-d H:i:s');
        $d->created_by_user_id       = $this->session->userdata('loginid');
        $d->updated_by_user_id       = $this->session->userdata('loginid');
        $d->save();
    }
    public function city_delete($id) {
        $this->_delete_by_id($id,'zyd_city');
    }
/**********************************************************************************
City              End
***********************************************************************************/


/**********************************************************************************
Admin User              Start
***********************************************************************************/
    public function adminuser_add() {
        $d = ORM::for_table('sam_users')->create(); 

        $d->emp_code                = $_POST['emp_code'];
        $d->first_name              = $_POST['fname'];
        $d->last_name               = $_POST['sname'];
        $d->email_id                = $_POST['email'];
        //$d->phone_no                = $_POST['phone'];
        $d->dob                		= $_POST['dob'];
        $d->mobile                  = $_POST['mobile'];
        $d->login_name              = $_POST['loginname'];
        $d->user_identity           = $_POST['admin_type'];
        
        $d->password_txt            = ($_POST['pwd']!= "") ? $_POST['pwd'] : $d->password_txt;
        $d->password                = ($_POST['pwd']!= "") ? md5($_POST['pwd']) : $d->password;
        
        $d->type                    = 'admin';
        $d->use_dashboard = isset($_POST['udash']) ? $_POST['udash'] : 0;
        $d->status = isset($_POST['status']) ? $_POST['status'] : 'active';
        $d->created_by              = $this->session->userdata('loginid');
        $d->modified                = date('Y-m-d H:i:s');
        $d->modified_by             = $this->session->userdata('loginid');

        if(is_uploaded_file($_FILES['usersign']['tmp_name'])) {
            $this->sam->upload_image('usersign','themes/assets/images/usersign/');
            $d->sign = $_FILES['usersign']['name'];
        }


        $d->save();
        
        echo $d->id;
    }

    public function adminuser_delete($id) {  
        $this->_delete_by_id($id,'sam_users');
    }

/**********************************************************************************
Admin User              End
***********************************************************************************/



/**********************************************************************************
Course Category              Start
***********************************************************************************/
    
/**********************************************************************************
Course Category              End
***********************************************************************************/
    


/**********************************************************************************
Sub Category              Start
***********************************************************************************/    

    public function subcategories_add() 
    {
        $d = ORM::for_table('sub_categories_mst')->create();
        $d->category_id              = $_POST['category_id'];
        $d->name                     = $_POST['name'];
        $d->description              = $_POST['description'];
        $d->status                   = isset($_POST['status']) ? $_POST['status'] : 'active';
        $d->is_deleted               = '0';
        $d->inserted_time            = date('Y-m-d H:i:s');
        $d->updated_time             = date('Y-m-d H:i:s');
        
        $d->created_by_user_id       = $this->session->userdata('loginid');
        $d->updated_by_user_id       = $this->session->userdata('loginid');
        if(is_uploaded_file($_FILES['subcategories_img']['tmp_name'])) {
            $this->sam->upload_image('subcategories_img','themes/assets/images/sub_categories_img/');
            $d->subcategories_img = $_FILES['subcategories_img']['name'];
        }


        $d->save();
        echo $d->id;
    }

      public function subcategories_delete($id) {
        $this->_delete_by_id($id,'sub_categories_mst');
    }
/**********************************************************************************
Sub Category              End
***********************************************************************************/
    


}
/**********************************************************************************
Plan              End
***********************************************************************************/