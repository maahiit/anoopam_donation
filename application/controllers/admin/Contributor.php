<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Contributor extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            admin_redirect('login');
        }

        $this->load->library('form_validation');
        $this->load->model('Mdl_contributor','mdl');
        $this->load->library('pagination');
    }
    
    public function send_mail()
    { 
        
        $config = array(
                'protocol'  => 'smtp',
                'smtp_host' => 'mail.maahiit.in',
                'smtp_port' =>  587,
                'smtp_user' => 'no_reply@maahiit.in',
                'smtp_pass' => 'Maahi@100%',
                'mailtype'  => 'html', 
                'charset'   => 'iso-8859-1'
            );
    
          $this->load->library('email',$config);

          $this->email->from('no_reply@maahiit.in', 'Registration Mail');
          $this->email->to('nirav.2d@gmail.com');
          $this->email->subject('Your client Sridhar, Invites you to join our website');
          $this->email->message('From now on wants to transact business with you.');
          
          $this->email->send();

          echo $this->email->print_debugger();
    }  
    

    public function unset_session_value()
    {
   
      $this->session->unset_userdata('user_s_mobile');
      $this->session->unset_userdata('city_s_search');
      $this->session->unset_userdata('business_s_name');
      $this->session->unset_userdata('business_s_type');
      $this->session->unset_userdata('user_serach_data');
    
      redirect('admin/contributor');

  }  
    
    public function index() {
        
        $w = $l = '';

        if($this->session->user_serach_page !== 'user')
        {
            $this->session->unset_userdata('user_serach_page');
        }
        $paginationdata = $this->data['Settings']->rows_per_page;

        if($_POST) {
            if(isset($_POST['user_s_mobile']) AND $_POST['user_s_mobile'] != '') {
                $w .= " AND mobile ='".$_POST['user_s_mobile']."'";
                $this->session->set_userdata('user_s_mobile',$_POST['user_s_mobile']);
            }
            
            if(isset($_POST['city_s_search']) AND $_POST['city_s_search'] != '') {
                $w .= " AND first.city like '%".$_POST['city_s_search']."'";
                $this->session->set_userdata('city_s_search',$_POST['city_s_search']);
            }
            
            if(isset($_POST['business_s_name']) AND $_POST['business_s_name'] != '') {
                $w .= " AND first.business_name like '%".$_POST['business_s_name']."'";
                $this->session->set_userdata('business_s_name',$_POST['business_s_name']);
            }
            
            
            if(isset($_POST['business_s_type']) AND $_POST['business_s_type'] != '') {
                $w .= " AND first.business_type ='".$_POST['business_s_type']."'";
                $this->session->set_userdata('business_s_type',$_POST['business_s_type']);
            }

            $_SESSION['user_serach_data'] = $w;
            $this->session->set_userdata('user_serach_data',$w);

        }
    
        
        
        if(isset($this->session->user_serach_data) AND $this->session->user_serach_data != '')
        {
           
             $w = $this->session->userdata('user_serach_data');
        }
        if($_POST)
        {   
            
               $Record = $this->mdl->get_search_count($paginationdata,$w);
        }else{
            if(isset($this->session->user_serach_data) AND $this->session->user_serach_data != '')
            {

                 $w = $this->session->userdata('user_serach_data');
                 $Record = $this->mdl->get_search_count($paginationdata,$w);
            }
            else
            {   
                  $Record =  $this->mdl->get_count();  
            }
        }
      
        
        $config = $this->sam->pagination_config();
        $config['base_url'] = site_url().'admin/contributor/index';
        $config['total_rows'] = $Record;
        $config['per_page'] = $paginationdata;
        $this->pagination->initialize($config);

        $meta['page_title'] = 'Payment Gateway master';
        $this->data['users'] = $this->mdl->get_users($config['per_page'],$this->uri->segment(3),$w);
       
        if(isset($_POST['SearchValue']))
        {
          if($_POST['SearchValue'] == 'excel')
          {
            $this->Exportuser($this->data['users']);
          }    
        }

        $this->data['user'] = $this->mdl->all_users();   
        
        $this->page_construct('contributor/view', $meta, $this->data);
        
    }
    
    function Exportuser($d)
    {
       $date =  date('d-m-Y');
       $excelName = 'employee_list_'.$date;
    
      $this->load->library("excel");
      $object = new PHPExcel();
    
      $object->setActiveSheetIndex(0);
    
      $table_columns = array("No","Employee Code","Name", "email", "Mobile", "Designation");
    
      $column = 0;
    
      foreach($table_columns as $field)
      {
       

       $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
       $column++;
      }
    
    
      $excel_row = 2;
    
      foreach($d as $key=>$row)
      {
        
       $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $key+1);
       $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row['emp_code']);
       $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row['first_name'].''.$row['last_name']);
       $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row['email_id']);
       $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row['mobile']);
       $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row['designation_name']);
       $excel_row++;
      }
    
      $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="'.$excelName.'.xls"');
      $object_writer->save('php://output');

    }

    public function edit($id)
    {
        $meta['page_title'] = 'Edit Employee master';
        if($_POST && $_POST['business_name'] != '')
        {

            if($this->mdl->update_user($_POST)) {
                $this->session->set_flashdata('success',lang('Contributor Update successfully'));
                redirect('contributor');
            } else {
                $this->session->set_flashdata('error',lang('Contributor Update fail'));
                redirect('contributor');
            }
        }else
        {
            $this->data['user'] = $this->mdl->get_user($id);
            $this->data['city'] = $this->mdl->getAllCity();
            $this->data['users'] = $this->mdl->getusers();          

            $this->page_construct('contributor/edit',$meta, $this->data);
        }
    }

    public function user_profile_edit($id) 
    {
        $meta['page_title'] = 'Profile';
        if($_POST && $_POST['fname'] != '') {
            if($this->mdl->update_profile_user($_POST)) {
                $this->session->set_flashdata('success',lang('uupdate'));
                //redirect('admin');
                redirect($_SERVER['HTTP_REFERER']);

            } else {
                //$this->session->set_flashdata('success',lang('uupdate'));
                redirect('admin');
                
            }
        } else {
            $this->data['user'] = $this->mdl->get_user($id);
            $this->data['roles'] = $this->sam->get_roles();
            $this->page_construct('contributor/user_profile_update',$meta, $this->data);
        }
    }



}
