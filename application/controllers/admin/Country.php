<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Country extends MY_Controller {

	function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            admin_redirect('login');
        }

        $this->load->library('form_validation');
        $this->load->model('Mdl_country','mdl');
        $this->load->library('pagination');
    }


    public function unset_session_value()
    {
        $this->session->unset_userdata('country_s_name');
        $this->session->unset_userdata('country_serach_page');
        $this->session->unset_userdata('country_serach_data');
        redirect('admin/country');
    }   

    public function index()
    {
        $w  = '';       
        
       if($this->session->country_serach_page != 'country')
        {
            $this->session->unset_userdata('country_serach_data');
            $this->session->unset_userdata('country_serach_page');
        }  

        $paginationdata = $this->data['Settings']->rows_per_page;
        if($_POST) 
        {
            if(isset($_POST['country_s_name']) AND $_POST['country_s_name'] != '') 
            {
                $w .= " AND name like '%".$_POST['country_s_name']."%'";
                $this->session->set_userdata('country_s_name',$_POST['country_s_name']);
            }
            $_SESSION['country_serach_data'] = $w;
            $this->session->set_userdata('country_serach_data',$w);
            $this->session->set_userdata('country_serach_page','country');
        }

        if(isset($this->session->country_serach_data) AND $this->session->country_serach_data != '')
        {
             $w = $this->session->userdata('country_serach_data');
        }
        if($_POST)
        { 
               $Record = $this->mdl->get_search_count($paginationdata,$w);
        }else{
            if(isset($this->session->country_serach_data) AND $this->session->country_serach_data != '')
            {
                 $w = $this->session->userdata('country_serach_data');
                 $Record = $this->mdl->get_search_count($paginationdata,$w);
            }
            else
            {   
                  $Record =  $this->mdl->get_count();  
            }
        }

        $config = $this->sam->pagination_config();
        $config['base_url'] = site_url().'admin/country/index';
        $config['total_rows'] = $Record;
        $config['per_page'] = $paginationdata;
        $this->pagination->initialize($config);

        $this->data['rows'] = $this->mdl->get_all_with_pagi('id',$config['per_page'],$this->uri->segment(4),$w);
        
        $meta['page_title'] = 'country Master';
        $this->page_construct('country/view', $meta, $this->data);
    }

    public function add_row() 
    {
        $d = ORM::for_table('zyd_country')->create();
        $d->name                     = $_POST['name'];
        $d->code                     = $_POST['code'];        
        $d->status                   = isset($_POST['status']) ? $_POST['status'] : 'active';
        $d->is_deleted               = '0';
        $d->inserted_time            = date('Y-m-d H:i:s');
        $d->updated_time             = date('Y-m-d H:i:s');
        $d->created_by_user_id       = $this->session->userdata('loginid');
        $d->updated_by_user_id       = $this->session->userdata('loginid');       

        $d->save();
        echo $d->id;
    }     

    public function edit($id)
    {

        $meta['page_title'] = 'Edit country Master';
        if($_POST && $_POST['id'] != '') {

            if($this->mdl->update_row($_POST)) 
            {
                $this->session->set_flashdata('success','Successfully Update Record ');
                redirect('admin/country');

            } else {
                $this->session->set_flashdata('error',lang('cupdatef'));
                redirect('admin/country');
            }
        } else {
            $this->data['row'] = $this->mdl->get($id);
            $this->page_construct('country/edit',$meta, $this->data);
        }
    }

    

    public function delete_row($id) {
        $this->sam->_delete_by_id($id,'zyd_country');  
    }
    

    
}