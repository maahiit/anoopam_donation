<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class City extends MY_Controller { 

    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            admin_redirect('login');
        }

        $this->load->library('form_validation');
        $this->load->model('Mdl_city','mdl');
        $this->load->library('pagination');
    }

    public function unset_session_value() { 
        
        $this->session->unset_userdata('city_s_district');
        $this->session->unset_userdata('city_s_state');
        $this->session->unset_userdata('city_s_city');
        $this->session->unset_userdata('city_serach_page');
        $this->session->unset_userdata('city_serach_data');
        redirect('admin/city');
    }

    public function index() {

        $w  = '';
        if($this->session->city_serach_page != 'city') 
        {
            //$this->session->unset_userdata('city_serach_data');
            $this->session->unset_userdata('city_serach_page');
        }

        $paginationdata = $this->data['Settings']->rows_per_page;

        if($_POST) {
            if(isset($_POST['city_s_district']) AND $_POST['city_s_district'] != '') {
                $w .= " AND first.district_id ='".$_POST['city_s_district']."'";
                $this->session->set_userdata('city_s_district',$_POST['city_s_district']);
            }
            if(isset($_POST['city_s_state']) AND $_POST['city_s_state'] != '') {
                $w .= " AND first.state_id ='".$_POST['city_s_state']."'";
                $this->session->set_userdata('city_s_state',$_POST['city_s_state']);
            }
            if(isset($_POST['city_s_city']) AND $_POST['city_s_city'] != '') {
                $w .= " AND city like '%".$_POST['city_s_city']."%'";
                $this->session->set_userdata('city_s_city',$_POST['city_s_city']);
            }

            $_SESSION['city_serach_data'] = $w;
            $this->session->set_userdata('city_serach_data',$w);
            $this->session->set_userdata('city_serach_page','city');

        }

        if(isset($this->session->city_serach_data) AND $this->session->city_serach_data != '')
        {
             $w = $this->session->userdata('city_serach_data');
        }

        
        if($_POST)
        { 
               $Record = $this->mdl->get_search_count($paginationdata,$w);
        }else{
            if(isset($this->session->city_serach_data) AND $this->session->city_serach_data != '')
            {

                 $w = $this->session->userdata('city_serach_data');
                 $Record = $this->mdl->get_search_count($paginationdata,$w);
            }
            else
            {   
                  $Record =  $this->mdl->get_count();  
            }
        }

        $config = $this->sam->pagination_config();
        $config['base_url'] = site_url().'admin/city/index'; 
        $config['total_rows'] = $Record;
        $config['per_page'] = $paginationdata;
        $this->pagination->initialize($config);

        $this->data['city'] = $this->mdl->get_all_with_pagi('id',$config['per_page'],$this->uri->segment(4),$w);


        $this->data['state'] = $this->mdl->get_state();
     
        $meta['page_title'] = 'City Master';
        $this->page_construct('city/view', $meta, $this->data);
    }

     public function edit($id) {
        
        $meta['page_title'] = 'City Master'; 
        if($_POST && $_POST['id'] != '') {

            if($this->mdl->update_city($_POST)) 
            {
                $this->session->set_flashdata('success','Successfully Update ');
                redirect('admin/city');

            } else {
                $this->session->set_flashdata('error',lang('cupdatef'));
                redirect('admin/city');
            }
        } else {
             $this->data['state'] = $this->mdl->get_state();
            $this->data['city'] = $this->mdl->get($id);
            $this->page_construct('city/edit',$meta, $this->data);
        }
    }

    function GetAllDistrict()
    {
        $StateList = ORM::for_table('zyd_district')
                ->where('state_id',$_POST['state_id'])
                ->where('is_deleted','0')
                ->find_array();
        
        echo json_encode($StateList);
      }  
    

    
}