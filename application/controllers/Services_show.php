<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

class Services_show extends MY_Front_Controller{   


	function __construct(){
        parent::__construct();
        $this->load->library('form_validation');  
        $this->load->library('Sam','sam'); 
        $this->load->model('Mdl_services_show','mdl'); 
        $this->load->model('Mdl_home_settings','mdl_home');  
  	}
    
    private $_table = 'services';

	function index($id = '')   
	{	
		if(empty($id))
		{
				redirect('home');
		}

		$meta['page_title'] = 'Services - Show';  
		$this->data['service_category'] = $this->mdl->get_service_cat_services_count(); 	 
		if(empty($this->data['row']))
		{
				redirect('home');	
		}
		$this->frontpage_construct('services_show/index', $meta, $this->data); 
	}

} 