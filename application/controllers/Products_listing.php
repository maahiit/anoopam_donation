<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

class Products_listing extends MY_Front_Controller{

	function __construct(){
        parent::__construct();
        $this->load->library('form_validation'); 
        $this->load->library('Sam','sam'); 
        $this->load->model('Mdl_products_listing','mdl');
        $this->load->model('Mdl_home_settings','mdl_home');  
  	}
    
    private $_table = 'products';

	function index()  
	{
		 $meta['page_title'] = 'Products - Listing';
		 $this->data['homes'] = $this->mdl_home->get(1);
         $this->data['products_category'] = $this->mdl->get_products_category_count();   
		 $this->data['products'] = $this->mdl->get_all_products();
		$this->frontpage_construct('products_listing/index', $meta, $this->data);
	}
    
} 