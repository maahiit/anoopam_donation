<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Front_Controller{

	function __construct() {

		parent::__construct(); 
       
        $this->load->model('Mdl_login'); 
	} 

	function index() 
	{
         if ($this->User_loggedIn)  
        {
            $this->session->set_flashdata('error', $this->session->flashdata('error'));
            redirect('mydask');
        }
		$meta['page_title'] = 'Login - Salon';  
		$this->frontpage_construct('login/index', $meta, $this->data); 
	}

    function login_ajex($m = NULL) {

            $d = ORM::for_table('sam_users')
                                ->where('password',md5($this->input->post('donor_password')))
                                ->where('mobile',$this->input->post('username'))
                                ->where('is_deleted','0')
                                ->find_one();
            if($d) 
            {
                if($d->type == 'contributor') {
                    
                    $this->session->set_userdata('business_name',$d->business_name);
                    $this->session->set_userdata('business_type',$d->business_type);
                    $this->session->set_userdata('user_logo',$d->logo);
                    $this->session->set_userdata('user_identity',$d->type);
                    $this->session->set_userdata('user_loginid', $d->id);
                    $this->session->set_userdata('user_loginname', $d->first_name);

                    $output['status'] = 1;
                    $output['msg'] = 'Welcome '. $d->first_name .', Login....!';
                    echo json_encode($output);

                } else {
                    
                    $output['status'] = 0;
                    $output['msg'] = 'Invalid mobile number or password. Please try again.';
                    echo json_encode($output);
                }
            } else {
                $output['status'] = 0;
                $output['msg'] = 'Invalid mobile number or password. Please try again.';
                echo json_encode($output);
            }
    }


      public function logout() {
        $this->session->sess_destroy();
        redirect('/');
    }
	


}