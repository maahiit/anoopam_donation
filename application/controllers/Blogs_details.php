<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

class Blogs_details extends MY_Front_Controller{   


	function __construct(){ 
        parent::__construct();
        $this->load->library('form_validation');   
        $this->load->library('Sam','sam'); 
        $this->load->model('Mdl_blogs_details','mdl'); 
        $this->load->model('Mdl_home_settings','mdl_home'); 
  	}
    
    private $_table = 'blog_mst';

	function index($id = '')   
	{	
		if(empty($id))
		{

				redirect('blogs');  
		}

		$meta['page_title'] = 'Blogs - Details';  
		$this->data['row'] = $this->mdl->get_details($id); 		

		

		if(empty($this->data['row']))
		{
				redirect('blogs');	
		}
		$this->frontpage_construct('blogs_details/index', $meta, $this->data); 
	}

} 