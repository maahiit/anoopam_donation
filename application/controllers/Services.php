<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

class Services extends MY_Front_Controller{ 


	function __construct(){
        parent::__construct();
        if (!$this->User_loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }
        $this->load->library('form_validation'); 
        $this->load->library('Sam','sam'); 
        $this->load->model('Mdl_services','mdl');   
  	}
    
    private $_table = 'services';

	function index()   
	{
        
		 $meta['page_title'] = 'Services - Salon';
         $this->data['services_category'] = $this->mdl->get_services_category();   
		 $this->data['services'] = $this->mdl->get_all_services();
         $this->data['table_ui'] =  $this->load->view('/front/views/services/ajex_table',$this->data,true);
		$this->frontpage_construct('services/index', $meta, $this->data);
	}

	function services_add() 
	{
            $d = ORM::for_table('services')->create();
            $d->user_id                 = $this->session->userdata('user_loginid');
            $d->name            		= $_POST['name'];
            $d->price          			= $_POST['price'];
            $d->category        		= $_POST['category'];
            $d->duration_of_service     = $_POST['duration_of_service'];
            $d->description             = $_POST['description'];
            if (!empty($_FILES['image']['name'])) {
            if(is_uploaded_file($_FILES['image']['tmp_name'])) {
             $this->sam->upload_image('image','themes/assets/images/services/');
              $d->image = $_FILES['image']['name'];
            }
        }

            $d->inserted_time                = date('Y-m-d H:i:s');
            $d->updated_time                = date('Y-m-d H:i:s');            
            $d->created_by_user_id      = $this->session->userdata('user_loginid');  
            $d->updated_by_user_id      = $this->session->userdata('user_loginid');
            
        
            if($d->save())
            {
                 $this->data['services'] = $this->mdl->get_all_services();         
                 $this->data['table_ui'] =  $this->load->view('/front/views/services/ajex_table',$this->data,true);

                $output['status'] = 1;
                $output['msg'] = 'Services Successfully Added';
                $output['table_ui'] = $this->data['table_ui'];
            }
            else
            {
                $output['status'] = 0;
                $output['msg'] = 'Services not added !!'; 
                $output['table_ui'] = '';
            }  
            echo json_encode($output);
    }

    public function get_row() 
    {
        $update_id = $_POST['update_id']; 
        if(!empty($update_id))
        {
            $output['status']    = '1';
            $this->data['row'] = $this->mdl->get($update_id);
            $this->data['services_category'] = $this->mdl->get_services_category(); 
            $output['edit_row'] =  $this->load->view('/front/views/services/ajax_edit_form',$this->data,true);
            echo json_encode($output);       
        } else {
            $output['status']    = '0';
            echo json_encode($output);
        }
    }




    public function update_row() 
    {
        $d = ORM::for_table($this->_table)->where('id',$_POST['update_id'])->find_one();
        if($d){
            
            $d->name                   = $_POST['name'];
            $d->price                   = $_POST['price'];
            $d->category                = $_POST['category'];
            $d->duration_of_service     = $_POST['duration_of_service'];
            $d->description             = $_POST['description'];

            $d->updated_time                = date('Y-m-d H:i:s');            
              
            $d->updated_by_user_id      = $this->session->userdata('user_loginid');
            if($d->save())
            {
                 $this->data['services'] = $this->mdl->get_all_services();
                 $this->data['table_ui'] =  $this->load->view('/front/views/services/ajex_table',$this->data,true);

                $output['status'] = 1;
                $output['msg'] = 'Services Successfully Update';
                $output['table_ui'] = $this->data['table_ui'];
            }
            else
            {
                $output['status'] = 0;
                $output['msg'] = 'Services not added !!'; 
                $output['table_ui'] = '';
            }  
            echo json_encode($output);
        } else  {
            return FALSE;  
        }
    }

     public function delete_row($id) {

        $deletestatus = $this->sam->_delete_by_id($id,$this->_table);
        if($deletestatus)
        {
             $this->data['services'] = $this->mdl->get_all_services();
             $this->data['table_ui'] =  $this->load->view('/front/views/services/ajex_table',$this->data,true);

            $output['status'] = 1;
            $output['msg'] = 'Services Successfully Delete';
            $output['table_ui'] = $this->data['table_ui'];
        }
        else
        {
            $output['status'] = 0;
            $output['msg'] = 'Services not added !!'; 
            $output['table_ui'] = '';
        }  
        echo json_encode($output);
    }




    
} 