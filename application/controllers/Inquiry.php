<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

class Inquiry extends MY_Front_Controller{  


	function __construct(){ 
        parent::__construct();
        $this->load->library('form_validation'); 
        $this->load->library('Sam','sam'); 
        $this->load->model('Mdl_amenities','mdl');     
  	}
    
    private $_table = 'inquiry';

	function index()  
	{
		 $meta['page_title'] = 'Home - Salon';
            
         
		$this->frontpage_construct('index', $meta, $this->data);
	}

	function inquiry_add() 
	{
            $d = ORM::for_table('inquiry')->create();
            $d->user_id                 = $this->session->userdata('loginid');           
            $d->item_id                    = $_POST['item_id'];
            $d->item_status                = $_POST['item_status'];
            $d->name                       = $_POST['name'];           
            $d->mobile                      = $_POST['mobile'];
            $d->inserted_time                = date('Y-m-d H:i:s');
            $d->updated_time                = date('Y-m-d H:i:s');            
            $d->created_by_user_id      = $this->session->userdata('loginid');  
            $d->updated_by_user_id      = $this->session->userdata('loginid');
              
        
            if($d->save())
            {          

                $output['status'] = 1;
                $output['msg'] = 'Inquiry Successfully Added';
                
            }
            else
            {
                $output['status'] = 0;
                $output['msg'] = 'Inquiry not added !!'; 
                
            }  
            echo json_encode($output);
    }

    
}
    