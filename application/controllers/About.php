<?php defined('BASEPATH') OR exit('No direct script access allowed');

class about extends MY_Front_Controller{

	function __construct() {
		parent::__construct();
	}

	function index() 
	{
		$meta['page_title'] = 'About - Salon';
		$this->frontpage_construct('about/index', $meta, $this->data); 
	}
}