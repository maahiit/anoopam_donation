<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

class Home extends MY_Front_Controller{  


  function __construct(){ 
        parent::__construct();
        $this->load->library('form_validation');  
        $this->load->library('Sam','sam'); 
        $this->load->model('Mdl_home_settings','mdl');        
    }
    
    private $_table = 'home';  
 
  function index() 
  {
   $loginid = $this->session->userdata('user_loginid');
   if($loginid)
   {
      $this->data['mydonations'] = ORM::for_table('transition')->where('user_id',$loginid)->find_array();
    }else{
        $this->data['mydonations'] = array();
    }

    $meta['page_title'] = 'Home - Anoopam Mission donation';  
    $this->data['countries'] = $this->mdl->get_country();
    $this->data['categories'] = $this->mdl->get_categories();
    $this->data['table_ui'] =  $this->load->view('index',$this->data,true);
    $this->frontpage_construct('index', $meta, $this->data);
  }


  function thanksyou($transation_id)  
  {
    $this->data['transations'] = ORM::for_table('transition')->where('payment_transition_id',$transation_id)->find_one();
    $meta['page_title'] = 'Thanks You - Anoopam Mission donation';  
    $this->frontpage_construct('thankyou', $meta, $this->data);
  }

  function register_add()
  {
      $checkuser = ORM::for_table('sam_users')->where('mobile',$_POST['donor_mobile'])->find_one(); 
      if($checkuser)
      {
          $output['status'] = 0;
          $output['msg'] = 'Mobile Number Already registered with us.';
      }
      else
      {
      $d = ORM::for_table('sam_users')->create(); 
      $d->last_name               = '';
      $d->middle_name             = '';
      $d->first_name              = $_POST['donor_name'];
      $d->mobile                  = $_POST['donor_email'];
      $d->mobile                  = $_POST['donor_mobile'];
      $d->password                =  md5($_POST['donor_password']);
      $d->password_txt            =  $_POST['donor_password'];
      $d->status                  = isset($_POST['status']) ? $_POST['status'] : 'active';
      $d->type                    = 'contributor';
      $d->inserted_time           = date('Y-m-d H:i:s');
      $d->created_by              = $this->session->userdata('loginid');
      if($d->save())
      {
      
        $transation_update = ORM::for_table('transition')->where('payment_transition_id',$_POST['payment_transition_id'])->find_one();
        $transation_update->user_id = $d->id;
        $transation_update->save();

          $output['status'] = 1;
          $output['msg'] = 'Your Profile created successfully';
      }else{
          $output['status'] = 0;
          $output['msg'] = 'Something wrong from server end !';
      }

      
       } 

       echo json_encode($output);
  }

    

  function add_form()
  {
      date_default_timezone_set('Asia/Calcutta');
      $datenow = date("d/m/Y h:m:s");
      $transactionDate = str_replace(" ", "%20", $datenow);
      $transactionId = $this->generateRandomString();

      $doner_fullname   = $_POST['donor_fname'].' '.@$_POST['donor_mname'].' '.@$_POST['donor_lname'];
      
      $donor_amount     = @$_POST['donation_amount'];
      $donor_address    = $_POST['donor_address'];
      $doner_email      = $_POST['donor_email'];
      $donor_mobile     = $_POST['donor_mobile'];

      $d = ORM::for_table('transition')->create();    
      $d->user_id                   = @$this->session->userdata('user_loginid');
      $d->contributor_type          = @$_POST['donor_type'];
      $d->name                      = @$_POST['donor_fname'].' '.@$_POST['donor_mname'].' '.@$_POST['donor_lname'];
      $d->address                   = @$_POST['donor_address'];
      $d->pan                       = @$_POST['donation_user_pan'];
      $d->city                      = @$_POST['txtCity'];
      $d->country_id                = @$_POST['donation_country_id'];
      
      
      $d->category_id               = @$_POST['donation_category_id'];
      $d->sub_category_id           = @$_POST['donation_sub_category_id'];
      $d->amount                    = @$_POST['donation_amount'];
      $d->address                   = @$_POST['donor_address'];
      $d->mobile                    = @$_POST['donor_mobile'];
      $d->donner_email              = @$_POST['donor_email'];
      $d->donation_trustfund        = @$_POST['donation_trustfund'];
      $d->payment_status            = 'pending';
      $d->payment_transition_id     = @$transactionId;
      $d->payment_request_time      = date('Y-m-d H:i:s');
       $d->inserted_time            = date('Y-m-d H:i:s');
      $d->save();

      $log = ORM::for_table('payment_gateway_log')->create();    
      $log->request                 = json_encode($_POST);
      $log->transition_id           = @$d->id;
      $log->payment_transition_id   = @$transactionId;
      $log->ip                      = @$_SERVER['SERVER_ADDR'];
      $log->user_agent              = @$_SERVER['HTTP_USER_AGENT'];
      $log->request_time            = date('Y-m-d H:i:s');
      $log->save();  
    
    
    
        $get_category = ORM::for_table('categories_mst')->where('id',$_POST['donation_category_id'])->find_one();    
        
        $payment_getway = $get_category->payment_gateway;
        $selected_payment_getway = ORM::for_table('payment_gateway')->where('id',$payment_getway)->find_one();    

       //$transactionRequest->setMode("test");
       //$transactionRequest->setLogin(197);
       //$transactionRequest->setPassword("Test@123");
       //$transactionRequest->setProductId("NSE");

        $user_name    = $selected_payment_getway->user_name;
        $password     = $selected_payment_getway->password;
        $product_id   = $selected_payment_getway->product_id;
        $account_no   = $selected_payment_getway->account_no;
        $req_hash_key = $selected_payment_getway->req_hash_key;
        $client_code = $selected_payment_getway->user_name;
        $getway_url = $selected_payment_getway->api_url;
      
      require_once 'TransactionRequest.php';

      
      $transactionRequest = new TransactionRequest();
      $transactionRequest->setMode("live");
      $transactionRequest->setLogin($user_name);
      $transactionRequest->setPassword($password);
      $transactionRequest->setProductId($product_id);
      
      $transactionRequest->setAmount($donor_amount);
      $transactionRequest->setTransactionCurrency("INR");
      $transactionRequest->setTransactionAmount($donor_amount);
      $transactionRequest->setReturnUrl($getway_url);
      $transactionRequest->setClientCode($client_code);
      $transactionRequest->setTransactionId($transactionId);
      $transactionRequest->setTransactionDate($transactionDate);
      $transactionRequest->setCustomerName($doner_fullname);
      $transactionRequest->setCustomerEmailId($doner_email);
      $transactionRequest->setCustomerMobile($donor_mobile);
      $transactionRequest->setCustomerBillingAddress($donor_address);
      $transactionRequest->setCustomerAccount($account_no);
      $transactionRequest->setReqHashKey($req_hash_key);
      $url = $transactionRequest->getPGUrl();
      $output['status'] = 1;
      $output['url'] = $url;
      echo json_encode($output);
      //header("Location: $url");
      //echo "<script>window.location = '".$url."';</script>";
  }

  function generateRandomString($length = 20) {
        return substr(str_shuffle(str_repeat($x='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }

  function success_payment()
  {


    require_once 'TransactionResponse.php';
    $transactionResponse = new TransactionResponse();
    $transactionResponse->setRespHashKey("KEYRESP123657234");
 
    if($transactionResponse->validateResponse($_POST)){
       
      if($_POST['desc'] == 'Transction Success')
      {
            $d_transation = ORM::for_table('transition')->where('payment_transition_id',$_POST['mer_txn'])->find_one(); 
            $d_transation->payment_status                = 'success';
            $d_transation->payment_response_time         = date('Y-m-d H:i:s');
            $d_transation->payment_receive_amount        = $_POST['amt'];
            $d_transation->save();
    
            $d_log = ORM::for_table('payment_gateway_log')->where('payment_transition_id',$_POST['mer_txn'])->find_one(); 
            $d_log->response                = @json_encode($_POST);
            $d_log->response_time           = date('Y-m-d H:i:s');
            $d_log->save();
        
      } else if($_POST['desc'] == 'Transction Failure') {
         
            $d_transation = ORM::for_table('transition')->where('payment_transition_id',$_POST['mer_txn'])->find_one(); 
            $d_transation->payment_status                = 'fail';
            $d_transation->payment_response_time         = date('Y-m-d H:i:s');
            $d_transation->payment_receive_amount        = $_POST['amt'];
            $d_transation->save();
    
            $d_log = ORM::for_table('payment_gateway_log')->where('payment_transition_id',$_POST['mer_txn'])->find_one(); 
            $d_log->response                = @json_encode($_POST);
            $d_log->response_time           = date('Y-m-d H:i:s');
            $d_log->save();
         

      } else if($_POST['desc'] == 'Cancel by User') {
         
        
        
            $d_transation = ORM::for_table('transition')->where('payment_transition_id',$_POST['mer_txn'])->find_one(); 
            $d_transation->payment_status                = 'cancel';
            $d_transation->payment_response_time         = date('Y-m-d H:i:s');
            $d_transation->payment_receive_amount        = $_POST['amt'];
            $d_transation->save();
    
            $d_log = ORM::for_table('payment_gateway_log')->where('payment_transition_id',$_POST['mer_txn'])->find_one(); 
            $d_log->response                = @json_encode($_POST);
            $d_log->response_time           = date('Y-m-d H:i:s');
            $d_log->save();
         
      } else {
          
            $d_transation = ORM::for_table('transition')->where('payment_transition_id',$_POST['mer_txn'])->find_one(); 
            $d_transation->payment_status                = 'fail';
            $d_transation->payment_response_time         = date('Y-m-d H:i:s');
            $d_transation->payment_receive_amount        = $_POST['amt'];
            $d_transation->save();
    
            $d_log = ORM::for_table('payment_gateway_log')->where('payment_transition_id',$_POST['mer_txn'])->find_one(); 
            $d_log->response                = @json_encode($_POST);
            $d_log->response_time           = date('Y-m-d H:i:s');
            $d_log->save();
      }

        $meta['page_title'] = 'Thanks You - Anoopam Mission donation';  
        redirect('thanksyou/'.$_POST['mer_txn']); 

    } else {

        echo "<pre>";
        echo "========2=======";
        print_r($_POST);
        die(); 

       $meta['page_title'] = 'Thanks You - Anoopam Mission donation';  
       $this->frontpage_construct('thankyou', $meta, $this->data);
    }
  }


  function get_category_by_country()
  {   
     $w = '';
     if($_POST['donation_country_id'] !== '')
        {
            $w .= " AND find_in_set('".$_POST['donation_country_id']."',country_id)";
            $query = "SELECT *  FROM categories_mst WHERE is_deleted = '0' $w ORDER BY name ASC" ;
            
            $d = ORM::for_table('categories_mst')->raw_query($query)->find_array();    
        }else{
            $d = array();
        }
       echo json_encode($d);
  }

  function get_subcategory()
  {   
       $d = ORM::for_table('subcategories_mst')->where('category_id',$_POST['donation_category_id'])->find_array();
       echo json_encode($d);
  }
  function print_receipt($transation_id)
  {   
        $this->load->library('Pdf');
        ob_start();
        
       $this->data['row'] = ORM::for_table('transition')->where('payment_transition_id',$transation_id)->find_one();
       $this->data['settings'] = ORM::for_table('sam_settings')->find_one(1);

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetTitle('Invoice Title');

        $pdf->setPrintHeader(true);
        $pdf->setPrintFooter(true);
        
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetMargins(15, 10, 15,10, true);

        // set Header data
        if(empty($Settings["sitelogo"])){
            $Settings["sitelogo"] = K_PATH_IMAGES.'logo_small.png';
        }
        $pdf->setFooterText('!! with Blessings of Guruvarya Param Pujya Sahebdada !!');

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);   
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
        $pdf->setFontSubsetting(true);
        
        $pdf->AddPage();
            
        // set JPEG quality
        $pdf->setJPEGQuality(100);

        $addres = '';
        $addres .= '<table>';
        $addres .= '<tr>';
        $addres .= '<td align="center" > ANOOPAM MISSION <br>
        Brahamajyoti, Yogij Marg, Mogri, Anand, Gujarat 388345<br>
        Phone : 02692 – 230544; 230483
        </td>';
        $addres .= '</tr>';
        $addres .= '</table>';
        $addres .= '<table>';

        $addres .= '<table>';
        $addres .= '<tr>';
        $addres .= '<td align="center">_________________________________________________________________________________________</td>';
        $addres .= '</tr>';
        $addres .= '</table>';

         $addres .= '<table >';
        $addres .= '<tr>';
        $addres .= '<td align="center" > <p>Online Donation Acknowledgement </p></td>';
        $addres .= '</tr>';
        $addres .= '</table>';
    
        

        //set font
        $pdf->SetFont('helvetica', '', 10);
        $pdf->writeHTML($addres, true, false, false, false, ''); 


        $pdf->Ln(5);
        $pdf->SetFont('helvetica', 'B', 10);
        $pdf->Ln(5);
        $pdf->SetFont('helvetica', 'B', 10);
        
        

        $pdf->writeHTML($tbladdress, true, false, false, false, ''); 
        $addres       = ''; 

         $addres .= '<table cellpadding="5" width="50%">';
         $addres .= '<tr>';
         $addres .= '<th>Transaction ID</th>';
         $addres .= '<th width="60%">'.$this->data['row']->payment_transition_id.'</th>';
         $addres .= '</tr>';
         $addres .= '<tr>';
         $addres .= '<th>Transaction Date</th>';
         $addres .= '<th>'.$this->data['row']->payment_response_time.'</th>';
         $addres .= '</tr>';
         $addres .= '<tr>';
         $addres .= '<th>Name of the Doner</th>';
         $addres .= '<th>'.$this->data['row']->name.'</th>';
         $addres .= '</tr>';
         $addres .= '<tr>';
         $addres .= '<th>Email ID</th>';
         $addres .= '<th>'.$this->data['row']->donner_email.'</th>';
         $addres .= '</tr>';
           
         $addres .= '</table>';

        $addres .= '<table cellpadding="5" width="100%">';
         $addres .= '<tr>';
         $addres .= '<th><br></th>';
         $addres .= '</tr>';
           
         $addres .= '</table>';

        $pdf->setCellHeightRatio(1.3);

         $addres .= '<table cellpadding="5" width="100%">';
         $addres .= '<tr>';
         $addres .= '<th><p>Received with thanks Rs. '.$this->data['row']->amount.' from Shri/Smt. '.$this->data['row']->name.' through online with Transaction ID <b><u>:'.$this->data['row']->payment_transition_id.'</u></b> dated : <b><u>'.$this->data['row']->payment_response_time.'</u></b> towards donation </p></th>';
         $addres .= '</tr>';
           
         $addres .= '</table>';

         $addres .= '<table cellpadding="5" width="100%">';
         $addres .= '<tr>';
         $addres .= '<th>Donation towards: <br></th>';
         $addres .= '</tr>';  
         $addres .= '</table>';

         $addres .= '</table >';
         $addres .= '<table cellpadding="5" border="1" width="100%">';
           $addres .= '<tr>';
           $addres .= '<th>DONATION HEAD</th>';
           $addres .= '<th>DONATION SUB -HEAD</th>';
           $addres .= '<th>AMOUNT</th>';
           $addres .= '</tr>';
           $addres .= '<tr>';
           $addres .= '<th>'.$this->sam->get_real_value('categories_mst','id',$this->data['row']->category_id,'name').'</th>';
           $addres .= '<th>'.$this->sam->get_real_value('subcategories_mst','id',$this->data['row']->sub_category_id,'name').'</th>';
           $addres .= '<th>'.$this->data['row']->amount.'</th>';
           $addres .= '</tr>';
         $addres .= '</table>';

         $addres .= '<table cellpadding="5" width="100%">';
         $addres .= '<tr>';
         $addres .= '<th> <br> <br></th>';
         $addres .= '</tr>';  
         $addres .= '</table>';

         $addres .= '<table cellpadding="5" width="100%">';
         $addres .= '<tr>';
         $addres .= '<th><p>(Qualify for deduction u/s 80G of I.T.Act 1961, vide letter from CIT-II, Baroda, bearing No. BRD/CIT - II/80G/104/20-A/2010-11, Dated 28-10-2010 </p></th>';
         $addres .= '</tr>';  
         $addres .= '</table>';

         $addres .= '<table cellpadding="5" width="100%">';
         $addres .= '<tr>';
         $addres .= '<th> <br></th>';
         $addres .= '</tr>';  
         $addres .= '</table>';

          $addres .= '<table cellpadding="5" width="100%">';
         $addres .= '<tr>';
         $addres .= '<th>To,<br>
                SANJAYKUMAR SHANTIRANJAN DEY<br>
                BRAHMAJYOITI,<br>
                YOGIJI MARG, MOGRI,<br>
                ANAND 388345<br>
                GUJARAT. INDIA<br>
                CELL: 9904400406</th>';
         $addres .= '</tr>';  
         $addres .= '</table>';



         $pdf->SetFont('helvetica', '', 10);
         $pdf->writeHTML($addres, true, false, false, false, ''); 
        

      $filename = time().".pdf";
      ob_end_clean();
      $pdf->Output($filename);

      die();
       
  }

  

    
} 