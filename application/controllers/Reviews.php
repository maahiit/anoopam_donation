    <?php defined('BASEPATH') OR exit('No direct script access allowed'); 

class Reviews extends MY_Front_Controller{ 


	function __construct(){
        parent::__construct();
        if (!$this->User_loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }
        $this->load->library('form_validation'); 
        $this->load->library('Sam','sam'); 
        $this->load->model('Mdl_reviews','mdl');  
  	}
    
    private $_table = 'review';

	function index()  
	{
		 $meta['page_title'] = 'Reviews - Salon';
         //$this->data['services_category'] = $this->mdl->get_services_category();
         //$this->data['product_category'] = $this->mdl->get_products_category();  
         //$this->data['users'] = $this->mdl->get_users();
		 $this->data['reviews'] = $this->mdl->get_all_reviews();
         $this->data['table_ui'] =  $this->load->view('/front/views/reviews/ajex_table',$this->data,true);
		$this->frontpage_construct('reviews/index', $meta, $this->data);
	}

	function reviews_add() 
	{
            $d = ORM::for_table('review')->create();
            $d->user_id                 = $this->session->userdata('loginid');             
            $d->item_id                    = $_POST['item_id'];
            $d->item_status                    = $_POST['item_status'];
            $d->name                    = $_POST['name']; 
           
            $d->email                    = $_POST['email'];
            $d->comment          			= $_POST['comment'];
            $d->inserted_time                = date('Y-m-d H:i:s');
            $d->updated_time                = date('Y-m-d H:i:s');            
            $d->created_by_user_id      = $this->session->userdata('loginid');  
            $d->updated_by_user_id      = $this->session->userdata('loginid');            
        
            if($d->save())
            {
                 $this->data['reviews'] = $this->mdl->get_all_reviews();         
                 $this->data['table_ui'] =  $this->load->view('/front/views/reviews/ajex_table',$this->data,true);

                $output['status'] = 1;
                $output['msg'] = 'Reviews Successfully Added';
                $output['table_ui'] = $this->data['table_ui'];
            }
            else
            {
                $output['status'] = 0;
                $output['msg'] = 'Reviews not added !!'; 
                $output['table_ui'] = '';
            }  
            echo json_encode($output);
    }

    public function get_row() 
    {
        $update_id = $_POST['update_id']; 
        if(!empty($update_id))
        {
            $output['status']    = '1';
            $this->data['row'] = $this->mdl->get($update_id);
            $this->data['services_category'] = $this->mdl->get_services_category();
            $this->data['products_category'] = $this->mdl->get_products_category(); 
            $output['edit_row'] =  $this->load->view('/front/views/reviews/ajax_edit_form',$this->data,true);
            echo json_encode($output);       
        } else {
            $output['status']    = '0';
            echo json_encode($output);
        }
    }




    public function update_row() 
    {
        $d = ORM::for_table($this->_table)->where('id',$_POST['update_id'])->find_one();
        if($d){
            
            $d->name                   = $_POST['name'];
            $d->email                   = $_POST['email'];
            $d->comment                     = $_POST['comment'];
            $d->item_status             = isset($_POST['item_status']) ? $_POST['item_status'] : 'services';

            $d->updated_time                = date('Y-m-d H:i:s');            
              
            $d->updated_by_user_id      = $this->session->userdata('loginid');
            if($d->save())
            {
                 $this->data['reviews'] = $this->mdl->get_all_reviews();
                 $this->data['table_ui'] =  $this->load->view('/front/views/reviews/ajex_table',$this->data,true);

                $output['status'] = 1;
                $output['msg'] = 'Reviews Successfully Update';
                $output['table_ui'] = $this->data['table_ui'];
            }
            else
            {
                $output['status'] = 0;
                $output['msg'] = 'Reviews not added !!'; 
                $output['table_ui'] = '';
            }  
            echo json_encode($output);
        } else  {
            return FALSE;  
        }
    }

     public function delete_row($id) {

        $deletestatus = $this->sam->_delete_by_id($id,$this->_table);
        if($deletestatus)
        {
             $this->data['reviews'] = $this->mdl->get_all_reviews();
             $this->data['table_ui'] =  $this->load->view('/front/views/reviews/ajex_table',$this->data,true);

            $output['status'] = 1;
            $output['msg'] = 'Reviews Successfully Delete';
            $output['table_ui'] = $this->data['table_ui'];
        }
        else
        {
            $output['status'] = 0;
            $output['msg'] = 'Reviews not added !!'; 
            $output['table_ui'] = '';
        }  
        echo json_encode($output);
    }

    
} 