<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends MY_Front_Controller{

	function __construct() {
		parent::__construct();
	}

	function index() 
	{
		$meta['page_title'] = 'contact - Salon';
		$this->frontpage_construct('contact/index', $meta, $this->data); 
	}
}