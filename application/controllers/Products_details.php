<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

class Products_details extends MY_Front_Controller{

	function __construct(){
        parent::__construct();
        $this->load->library('form_validation'); 
        $this->load->library('Sam','sam'); 
        $this->load->model('Mdl_products_details','mdl');
        $this->load->model('Mdl_home_settings','mdl_home');  
  	}
    
    private $_table = 'products';

	function index($id = '')   
	{
		 if(empty($id))
		{
				redirect('products_listing');
		}

		$meta['page_title'] = 'Products - Details';  
		$this->data['row'] = $this->mdl->get($id); 	 
		if(empty($this->data['row']))
		{
				redirect('products_listing');	 
		}
		$this->frontpage_construct('products_details/index', $meta, $this->data);
	}
    
} 