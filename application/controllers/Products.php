<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

class Products extends MY_Front_Controller{


	function __construct(){
        parent::__construct();
        if (!$this->User_loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }
        $this->load->library('form_validation'); 
        $this->load->library('Sam','sam'); 
        $this->load->model('Mdl_products','mdl');  
  	}
    
    private $_table = 'products';

	function index()  
	{
		 $meta['page_title'] = 'Products - Salon';
         $this->data['products_category'] = $this->mdl->get_products_category();   
         $this->data['users'] = $this->mdl->get_users();
		 $this->data['products'] = $this->mdl->get_all_products();
         $this->data['table_ui'] =  $this->load->view('/front/views/products/ajex_table',$this->data,true);
		$this->frontpage_construct('products/index', $meta, $this->data);
	}

	function products_add() 
	{
            $d = ORM::for_table('products')->create();
            $d->user_id                 = $this->session->userdata('user_loginid'); 
            $d->name            		= $_POST['name'];
            $d->price          			= $_POST['price'];
            $d->category        		= $_POST['category'];
            $d->description             = $_POST['description'];
            if (!empty($_FILES['image']['name'])) {
            if(is_uploaded_file($_FILES['image']['tmp_name'])) {
             $this->sam->upload_image('image','themes/assets/images/products/');
              $d->image = $_FILES['image']['name'];
            }
            } 

            $d->inserted_time                = date('Y-m-d H:i:s');
            $d->updated_time                = date('Y-m-d H:i:s');            
            $d->created_by_user_id      = $this->session->userdata('user_loginid');  
            $d->updated_by_user_id      = $this->session->userdata('user_loginid');
        
            if($d->save())
            {
                 $this->data['products'] = $this->mdl->get_all_products();
                 //$this->data['services'] = $this->mdl->get_services();
                 $this->data['table_ui'] =  $this->load->view('/front/views/products/ajex_table',$this->data,true);

                $output['status'] = 1;
                $output['msg'] = 'Products Successfully Added';
                $output['table_ui'] = $this->data['table_ui'];
            }
            else
            {
                $output['status'] = 0;
                $output['msg'] = 'Products not added !!'; 
                $output['table_ui'] = '';
            }  
            echo json_encode($output);
    }

    public function get_row() 
    {
        $update_id = $_POST['update_id'];
        if(!empty($update_id))
        {
            $output['status']    = '1';
            $this->data['row'] = $this->mdl->get($update_id);
            $this->data['products_category'] = $this->mdl->get_products_category(); 
            $output['edit_row'] =  $this->load->view('/front/views/products/ajax_edit_form',$this->data,true);
            echo json_encode($output);       
        } else {
            $output['status']    = '0';
            echo json_encode($output);
        }
    }




    public function update_row() 
    {
        $d = ORM::for_table($this->_table)->where('id',$_POST['update_id'])->find_one();
        if($d){
            
            $d->name                    = $_POST['name'];
            $d->price                   = $_POST['price'];
            $d->category                = $_POST['category'];
            $d->description             = $_POST['description'];

            $d->updated_time                = date('Y-m-d H:i:s');            
              
            $d->updated_by_user_id      = $this->session->userdata('user_loginid');
            if($d->save())
            {
                 $this->data['products'] = $this->mdl->get_all_products();
                 $this->data['table_ui'] =  $this->load->view('/front/views/products/ajex_table',$this->data,true);

                $output['status'] = 1;
                $output['msg'] = 'Products Successfully Update';
                $output['table_ui'] = $this->data['table_ui'];
            }
            else
            {
                $output['status'] = 0;
                $output['msg'] = 'Products not added !!'; 
                $output['table_ui'] = '';
            }  
            echo json_encode($output);
        } else  {
            return FALSE; 
        }
    }

     public function delete_row($id) {

        $deletestatus = $this->sam->_delete_by_id($id,$this->_table);
        if($deletestatus)
        {
             $this->data['products'] = $this->mdl->get_all_products();
             $this->data['table_ui'] =  $this->load->view('/front/views/products/ajex_table',$this->data,true);

            $output['status'] = 1;
            $output['msg'] = 'Products Successfully Delete';
            $output['table_ui'] = $this->data['table_ui'];
        }
        else
        {
            $output['status'] = 0;
            $output['msg'] = 'Products not added !!'; 
            $output['table_ui'] = '';
        }  
        echo json_encode($output);
    }
    
} 