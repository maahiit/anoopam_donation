<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// Reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;

require_once APPPATH.'libraries/REST_Controller.php';

class Api extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mdl_api');
    }

    public function sendloginotp() 
    {

        $CustomerExist = ORM::for_table('sam_customers')->where('username',$_POST['username'])->where('is_deleted','0')
                            ->find_one();
        if($CustomerExist)
        {
            $MobileNo = $CustomerExist->username;
            $OTPCode = $this->GetOTPForRegister($MobileNo); 
            $CustomerExist->login_otp = $OTPCode;
            $CustomerExist->save(); 

            $output['status'] = 1;
            $output['status_msg'] = 'Send OTP In Your Mobile';
            $output['otp'] = $OTPCode;
            echo json_encode($output);
        }
        else
        {
            $MobileNo = $_POST['username'];    
            $OTPCode = $this->GetOTPForRegister($MobileNo); 

            $d = ORM::for_table('sam_customers')->create();    
            $d->login_otp       = $OTPCode;
            $d->username        = $_POST['username'];
            $d->save();
            
            $output['status'] = 1;
            $output['status_msg'] = 'Send OTP In Your Mobile';
            $output['otp'] = $OTPCode;
            
            echo json_encode($output); 

        } 
    }

    public function customerlogin() 
    {

        $CustomerExist = ORM::for_table('sam_customers')->where('username',$_POST['username'])->where('is_deleted','0')
                            ->find_one();
        if($CustomerExist)
        {
            if ($CustomerExist->login_otp == $_POST['login_otp']) {
                $output['status'] = 1;
                $output['status_msg'] = 'success';
                $output['msg'] = 'Login Succesfully...';
                $output['user_id'] = $CustomerExist->id;

            }else{

                $output['status'] = 2;
                $output['status_msg'] = 'Fail';
                $output['msg'] = 'Invalid OTP...';
                $output['user_id'] = '';
            }            
            echo json_encode($output);
        }
        else
        {
            $MobileNo = $_POST['username'];    
            $OTPCode = $this->GetOTPForRegister($MobileNo); 

            $d = ORM::for_table('sam_customers')->create();    
            $d->login_otp       = $OTPCode;
            $d->username          = $_POST['username'];
            $d->save();
            
            $output['status'] = 3;
            $output['msg'] = 'Customer Not Found';
            echo json_encode($output); 

        } 
    }

     private function GetOTPForRegister($MobileNo)
     {

         $fieldcnt    = 6;
         $mobileno = '+974'.$MobileNo;
         //$OTP = mt_rand(100000, 999999);
         $OTP = 123456;
         return $OTP; 
    
         $msg = "IIDDQATAR Team Your Login OTP Code is $OTP";

         $fieldstring = "username=qatardm&password=Genixtech1&api_key=lyy9j2cjjud4dvt&MSG=test&from=QatarDM&to=$mobileno&text=$msg";
         $ch = curl_init();
         curl_setopt($ch, CURLOPT_URL, 'https://www.experttexting.com/ExptRestApi/sms/json/Message/Send');
         curl_setopt($ch, CURLOPT_POST, $fieldcnt);
         curl_setopt($ch, CURLOPT_POSTFIELDS, $fieldstring);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         $res = curl_exec($ch);
         curl_close($ch);
        
        }


    public function get_category() {
        if($data = $this->mdl_api->get_category_list()) {
            $image_path = base_url().'themes/assets/images/categories_img/';


            $this->response($this->json(['status' => true ,'img_path' => $image_path , 'message' => 'Category List', 'data' => $data]),200);
        } else {
            $this->response($this->json(['status' => false , 'message' => 'No Record Found', 'data' => []]),200);
        }
    }
    
    
     public function add_post() {
        
       
       
        if($this->mdl_api->add_post($_POST)) {
            
            $this->response($this->json(['status' => true ,'message' => 'Successfully Added']),200);
        } else {
            $this->response($this->json(['status' => false , 'message' => 'No Record Added']),200);
        }
    }
    
     public function post_list() {
        if($data = $this->mdl_api->post_list()) {
            $this->response($this->json(['status' => true ,'message' => 'Post List', 'data' => $data]),200);
        } else {
            $this->response($this->json(['status' => false , 'message' => 'No Record Found', 'data' => []]),200);
        }
    }
    
    public function add_request_post() {
        if($this->mdl_api->add_request_post()) {
            
            $this->response($this->json(['status' => true ,'message' => 'Successfully Added', 'data' => $data]),200);
        } else {
            $this->response($this->json(['status' => false , 'message' => 'No Record Added', 'data' => []]),200);
        }
    }    
    public function request_post_list() {
        if($data = $this->mdl_api->request_post_list()) {
            
            $this->response($this->json(['status' => true ,'message' => 'Request Post List', 'data' => $data]),200);
        } else {
            $this->response($this->json(['status' => false , 'message' => 'No Record Found', 'data' => []]),200);
        }
    }
    
     public function get_profile() {
         

        if($data = $this->mdl_api->get_profile($_POST))
        {
            
            $this->response($this->json(['status' => true ,'message' => 'Request Post List', 'data' => $data]),200);
        } else {
            $this->response($this->json(['status' => false , 'message' => 'No Record Found', 'data' => []]),200);
        }
    }
    
     public function edit_profile() {
         

        if($data = $this->mdl_api->edit_profile($_POST))
        {
            
            $this->response($this->json(['status' => true ,'message' => 'Successfully update Record', 'data' => $data]),200);
        } else {
            $this->response($this->json(['status' => false , 'message' => 'No Record Found', 'data' => []]),200);
        }
    }
    
    public function mypost() {
       if($data = $this->mdl_api->get_mypost($_POST))
       {
            
            $this->response($this->json(['status' => true ,'message' => 'Request Post List', 'data' => $data]),200);
        } else {
            $this->response($this->json(['status' => false , 'message' => 'No Record Found', 'data' => []]),200);
        }
    }
    
    
     public function single_image_upload() {
         
    if($data = $this->mdl_api->single_image_upload($_POST))
    {
        
        $this->response($this->json(['status' => true ,'message' => 'Successfully Uploaded...', 'id' => $data]),200);
    } else {
        $this->response($this->json(['status' => false , 'message' => 'No Record Found', 'data' => []]),200);
    }
    }

    public function add_post_comment() {
       if($data = $this->mdl_api->add_post_comment($_POST))
       {
            
            $this->response($this->json(['status' => true ,'message' => 'Successfully Send Commnent']),200);
        } else {
            $this->response($this->json(['status' => false , 'message' => 'No Record Found']),200);
        }
    }

    public function get_post_comment() {
       if($data = $this->mdl_api->get_post_comment($_POST))
       {
            
            $this->response($this->json(['status' => true ,'message' => 'Successfully Send Commnent','data' => $data]),200);
        } else {
            $this->response($this->json(['status' => false , 'message' => 'No Record Found','data' => []]),200);
        }
    }
     public function get_post_details() {
       if($data = $this->mdl_api->get_post_details($_POST))
       {
            
            $this->response($this->json(['status' => true ,'message' => 'Record get ','data' => $data]),200);
        } else {
            $this->response($this->json(['status' => false , 'message' => 'No Record Found','data' => []]),200);
        }
    }

     public function update_post_record() {
       if($data = $this->mdl_api->update_post_record($_POST))
       {
            $this->response($this->json(['status' => true ,'message' => 'Successfully Update Record']),200);
        } else {
            $this->response($this->json(['status' => false , 'message' => 'No Record Found','data' => []]),200);
        }
    }

     private function json($data) {
            if (is_array($data)) {
                return json_encode($data);
            }
     }


}