<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_mydask extends CI_Model{ 

	public function __construct(){
		parent::__construct(); 
		$this->load->library('database');  
	}

	private $_table = 'sam_users';  

	function get($id) {
    	$d = ORM::for_table($this->_table)->where('id',$id)->find_one(); 
    	
    	if($d) { return $d; } else { return FALSE; }
    }

    public function get_city() {
		$d = ORM::for_table('zyd_city')
				->where('is_deleted','0')
				->find_many();
		if($d) { return $d; } else { return FALSE; } 
	}

	public function get_plan() {
		$d = ORM::for_table('plan_master')
				->where('is_deleted','0')
				->find_many();
		if($d) { return $d; } else { return FALSE; }
	}
}