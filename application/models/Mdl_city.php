<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once('traits/common_function.php');

class Mdl_city extends CI_Model {

	public function __construct() {
		parent::__construct(); 
		$this->load->library('database'); 
	}

	use common_db_functions;

	private $_table = 'zyd_city';

	public function get_state() {
		$d = ORM::for_table('zyd_state')
				->where('is_deleted','0')
				->find_many();
		if($d) { return $d; } else { return FALSE; }
	}
	
	public function get_count() {
		$d = ORM::for_table($this->_table)
				->count();
		if($d) { return $d; } else { return FALSE; } 
	}


	public function get_all_with_pagi($id,$limit = 10,$start = 0,$w) {
		if($w == ""){
		if($start == '')
		{
			$start = 0;
		}
    	
    	
    	$query = "SELECT first.*,model.state as statename FROM zyd_city as first
                       LEFT OUTER JOIN zyd_state as model ON first.state = model.id
                       WHERE first.is_deleted = '0' ORDER BY id DESC";	
			
		$d = ORM::for_table('zyd_city')->raw_query($query)->find_many();
    	
    	}else{
			if($start == '')
			{
				$start = 0;
			}
			$l = '';					
			$limit = $limit;  
			$offset = $start; 

	        $l = "LIMIT $offset,$limit"; 

	        $query = "SELECT first.*,model.state as statename FROM zyd_city as first
                       LEFT OUTER JOIN zyd_state as model ON first.state = model.id
                       
                       WHERE first.is_deleted = '0' AND  $w ORDER BY id DESC $l";	
			
			
			$d = ORM::for_table('zyd_city')->raw_query($query)->find_many();
		}

    	if($d) { return $d; } else { return FALSE; }
    }

    public function get_search_count($limit = 10,$w) {

			$query = "SELECT FROM zyd_city WHERE is_deleted = '0' $w ORDER BY id DESC";
           	$TotalData = ORM::for_table('zyd_city')->raw_query($query)->find_array();
			$d = count($TotalData);

		if($d) { return $d; } else { return FALSE; } 
	}
	

    public function update_city($post) { 
		$d = ORM::for_table($this->_table)->where('id',$post['id'])->find_one();
		if($d) {
			    $d->state             = $_POST['state'];
			    $d->city             = $_POST['city'];
			    if(is_uploaded_file($_FILES['image']['tmp_name'])) {
			    	$this->sam->upload_image('image','themes/assets/images/city/');
			    	$d->image         =$_FILES['image']['name'];
			    	
			    }
		        $d->updated_time                = date('Y-m-d H:i:s');            
            	$d->updated_by_user_id      = $this->session->userdata('loginid');
		        
	        }

		        if($d->save()) { return $d; } else { return FALSE; }
		    }

	}

