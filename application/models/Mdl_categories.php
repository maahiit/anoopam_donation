<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once('traits/common_function.php');

class Mdl_categories extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('database');
	}

	use common_db_functions;

	private $_table = 'categories_mst';
	
	public function get_count()
	{
		$d = ORM::for_table($this->_table)->where('is_deleted','0')
				->count();
		if($d) { return $d; } else { return FALSE; }
	}

	public function get_all_with_pagi($id,$limit = 10,$start = 0,$w)
	{
		if($w == "")
		{
			if($start == '')
			{
				$start = 0;
			}
	    	$d = ORM::for_table($this->_table)->where('is_deleted','0')
	    						->order_by_desc($id)
	    						->limit($limit)
								->offset($start)
	    						->find_many();
    	}else
    	{    		
			if($start == '')
			{
				$start = 0;
			}
			$l = '';					
			$limit = $limit;
			$offset = $start;

	        $l = "LIMIT $offset,$limit";					

			$query = "SELECT * FROM `$this->_table` WHERE is_deleted = '0' $w ORDER BY id DESC $l";
			
			$d = ORM::for_table('uni_course_categories_mst')->raw_query($query)->find_many();
		}

    	if($d) { return $d; } else { return FALSE; }
    }

    public function get_search_count($limit = 10,$w)
    {

		$query = "SELECT * FROM `$this->_table` WHERE is_deleted = '0' $w ORDER BY id DESC";

		$TotalData = ORM::for_table('uni_course_categories_mst')->raw_query($query)->find_array();
		$d = count($TotalData);

		if($d) { return $d; } else { return FALSE; }
	}

    public function update_row($post)
    {
		$d = ORM::for_table($this->_table)->where('id',$post['id'])->find_one();		

		if($d)
		{
			$d->name                     = $_POST['name'];

			$d->trustfund                = isset($_POST['trustfund']) ? @$_POST['trustfund'] : 'no';
        	$d->trustfund_minimum_amount           = @$_POST['trustfund_minimum_amount'];
	        
	        $d->minimum_amount           = $_POST['minimum_amount'];
	        $d->maximum_amount           = $_POST['maximum_amount'];
	        $d->payment_gateway          = implode(',',$_POST['payment_gateway']);
            $d->country_id               = implode(',',$_POST['country_id']);

			$d->status          		 = isset($_POST['status']) ? $_POST['status'] : 'active';
		    $d->updated_time             = date('Y-m-d H:i:s');            
            $d->updated_by_user_id       = $this->session->userdata('loginid');		        

		    if($d->save()) { return $d; } else { return FALSE; }
		}
	}

	public function get_payment_gateway()
	{
		$query = "SELECT * FROM `payment_gateway` WHERE is_deleted = '0' ";
			
			$d = ORM::for_table('payment_gateway')->raw_query($query)->find_many();
		

    	if($d) { return $d; } else { return FALSE; }
	}

	public function get_country()
	{
		$query = "SELECT * FROM `zyd_country` WHERE is_deleted = '0' ";
			
			$d = ORM::for_table('zyd_country')->raw_query($query)->find_many();
		

    	if($d) { return $d; } else { return FALSE; }
	}

}