<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_user extends CI_Model
{

	public function __construct()
	{ 
		parent::__construct();
		$this->load->library('database');
	}

	private $_table = 'sam_users';

	public function get_users($limit = 10,$start = 0,$w)
	{
		if($w == ""){

		if($start == '')
		{
			$start = 0;
		}
	    $limit = $limit;	
	    if(isset($_POST['SearchValue']))
		{  
			if($_POST['SearchValue'] == 'excel') 
		    {
		    	$d = ORM::for_table('sam_users AS T1')
                    ->raw_query("SELECT first.*,city.city as cityname  FROM sam_users as first
                       LEFT OUTER JOIN zyd_city as city ON first.city = city.id
                       
                       WHERE first.is_deleted = '0' AND first.type = 'user'")
                    ->find_many(); 
		    }else{
		    	$d = ORM::for_table('sam_users AS T1')
                    ->raw_query("SELECT first.*,city.city as cityname FROM sam_users as first
                       LEFT OUTER JOIN zyd_city as city ON first.city = city.id
                       
                       WHERE first.is_deleted = '0' AND first.type = 'user' limit $start,$limit")
                    ->find_many(); 
		    }
		}else{
			$d = ORM::for_table('sam_users AS T1')
                    ->raw_query("SELECT first.*,city.city as cityname FROM sam_users as first
                       LEFT OUTER JOIN zyd_city as city ON first.city = city.id
                       
                       WHERE first.is_deleted = '0' AND first.type = 'user' limit $start,$limit")
                    ->find_many(); 
		}   
	    
		}else
		{
			if($start == '')
			{
				$start = 0;
			}
			
	
			$l = '';					
			$limit = $limit;
			$offset = $start;

	        $l = "LIMIT $offset,$limit";					
	        if(isset($_POST['SearchValue']))
		    {
		      	if($_POST['SearchValue'] == 'excel')
		      	{
		      		$query = "SELECT first.*,city.city as cityname FROM sam_users as first
                       LEFT OUTER JOIN zyd_city as city ON first.city = city.city
                       
                       WHERE first.is_deleted = '0' AND first.type = 'user' $w ORDER BY id DESC";
		      	}else{
		      		$query = "SELECT first.*,city.city as cityname FROM sam_users as first
                       LEFT OUTER JOIN zyd_city as city ON first.city = city.id
                       
                       WHERE first.is_deleted = '0' AND first.type = 'user' $w ORDER BY id DESC $l";
		      	}
		    }else
		    {	
		    	$query = "SELECT first.*,city.city as cityname FROM sam_users as first
                       LEFT OUTER JOIN zyd_city as city ON first.city = city.id
                       
                       WHERE first.is_deleted = '0' AND first.type = 'user' $w ORDER BY id DESC $l";
		    }
			
			
			$d = ORM::for_table('sam_users')->raw_query($query)->find_many();
		}					

		if($d) { return $d; } else { return FALSE; }
	}


	public function get_search_count($limit = 10,$w) {

			$query = "SELECT first.*,model.state as statename FROM sam_users as first
                       LEFT OUTER JOIN zyd_state as model ON first.state_id = model.id
                      
                       WHERE first.is_deleted = '0' AND first.type = 'user' $w ORDER BY id DESC";

			$TotalData = ORM::for_table('sam_users')->raw_query($query)->find_array();
			$d = count($TotalData);

		if($d) { return $d; } else { return FALSE; }
	}

	public function get_user($id) {
		$d = ORM::for_table($this->_table)
							->where('id',$id)
							->find_one();

		if($d) { return $d; } else { return FALSE; }
	}

	
	public function get_companymst() 
	{
		$d = ORM::for_table('zyd_company_mst')
							->where('is_deleted','0')
							->find_many();

		if($d) { return $d; } else { return FALSE; }
	}
	


	public function getusers() {
	    $d = ORM::for_table('sam_users AS T1')
                    ->raw_query("SELECT first.* FROM sam_users as first
                       
                       WHERE first.is_deleted = '0' AND first.type = 'user'")
                    ->find_many();
                    						
		if($d) { return $d; } else { return FALSE; }
	}
	
	public function get_all() {
		$d = ORM::for_table($this->_table)->where('is_deleted','0')
							->order_by_asc('id')
							->find_many();

		if($d) { return $d; } else { return FALSE; }
	}
	
	public function get_user_turbine() {
		$d = ORM::for_table($this->_table)
							->where('status','active')
							->where('is_deleted','0')
							->find_many();

		if($d) { return $d; } else { return FALSE; }
	}
	
	

	public function get_count() {
		$d = ORM::for_table($this->_table)
				->where('status','active')
				->where('is_deleted','0')
				->count();
		if($d) { return $d; } else { return FALSE; }
	}

	public function update_user($post) {
		$d = ORM::for_table($this->_table)->where('id',$post['id'])->find_one();
		if($d)
		{

	        $d->first_name              = $_POST['first_name'];
	        $d->last_name               = $_POST['last_name'];
	        $d->middle_name             = $_POST['middle_name'];
	        $d->mobile                	= $_POST['mobile'];
	        $d->email_id 	            = $_POST['email_id'];
	        $d->password                = ($_POST['pwd']!= "") ? md5($_POST['pwd']) : $d->password;
	        $d->password_txt            = ($_POST['pwd']!= "") ? $_POST['pwd'] : $d->password_txt;
            $d->otp 	            	= $_POST['otp'];
	        $d->google_id           	= $_POST['google_id'];
	        $d->type                    = $_POST['type'];
	        $d->contributor_type        = $_POST['contributor_type'];
	        $d->address              	= $_POST['address'];
	        $d->city                    = $_POST['city'];
	        $d->pan       				= $_POST['pan'];
	        $d->status                  = isset($_POST['status']) ? $_POST['status'] : 'active';
	        $d->updated_time                = date('Y-m-d H:i:s');            
            $d->updated_by_user_id      = $this->session->userdata('loginid');       	
            //$d->user_identity           = 4;            
        	//$d->use_dashboard           = isset($_POST['udash']) ? $_POST['udash'] : 0;  
	        
	    	if($d->save()) 
        	{ 
        		return $d; 
        	} 
        	else 
    		{ 
    			return FALSE; 
    		}
		} else {
			return FALSE;
		}
	}
	
	
	public function update_accessrights($post)
	{

		$d = ORM::for_table('sam_accessrights')->where('id',$post['id'])->find_one();

		if($d)
		{
			// update branch details
			$d->create_access        = isset($post['create_'.$d->page_title]) ? $post['create_'.$d->page_title] : 'no';
			$d->edit_access          = isset($post['edit_'.$d->page_title]) ? $post['edit_'.$d->page_title] : 'no';
			$d->view_access          = isset($post['view_'.$d->page_title]) ? $post['view_'.$d->page_title] : 'no';
			$d->delete_access        = isset($post['delete_'.$d->page_title]) ? $post['delete_'.$d->page_title] : 'no';

			$d->modified             = date('Y-m-d H:i:s');
			$d->save();

	    	return $d->id;
		}else 
		{
			return FALSE;
		}
	}


		
	public function update_profile_user($post) {
		$d = ORM::for_table($this->_table)->where('id',$post['id'])->find_one();
		if($d) {
			// update branch details
			$d->first_name              = $_POST['fname'];
	        $d->last_name               = $_POST['sname'];
	        $d->email_id                = $_POST['email'];
	        if(!empty($_POST['dob'])){ $d->dob                		= $_POST['dob']; }
	        if(!empty($_POST['employee_address'])){ $d->employee_address                		= $_POST['employee_address']; }
	        
	        $d->mobile                  = $_POST['mobile'];
	        
	        $d->password_txt            = ($_POST['pwd']!= "") ? $_POST['pwd'] : $d->password_txt;
            $d->password                = ($_POST['pwd']!= "") ? md5($_POST['pwd']) : $d->password;

	        
	        $d->modified                = date('Y-m-d H:i:s');
	        $d->modified_by             = $this->session->userdata('loginid');

	        $d->save();
	    	return $d->id;
		} else {
			return FALSE;
		}
	}
	
	public function get_city() {

		$d = ORM::for_table('zyd_city')->where('is_deleted','0')->find_array();

		if($d) { return $d; } else { return FALSE; }
	}
	
	public function getAllCity() {
		$d = ORM::for_table('zyd_city')
				->where('is_deleted','0')
				->find_many();
		if($d) { return $d; } else { return FALSE; }
	}


	public function all_users(){
		$query = "SELECT * FROM `sam_users` WHERE `is_deleted` = 0 AND `type`='contributor' ";

		$d = ORM::for_table('sam_users')->raw_query($query)->find_many();

		if($d) { return $d; } else { return FALSE; }
	}

}