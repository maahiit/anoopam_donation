<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once('traits/common_function.php');

class Mdl_payment_gateway extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->library('database');
	}

	use common_db_functions;

	private $_table = 'payment_gateway';
	
	public function get_count() {
		$d = ORM::for_table($this->_table)->where('is_deleted','0')
				->count();
		if($d) { return $d; } else { return FALSE; }
	}

	public function get_all_with_pagi($id,$limit = 10,$start = 0,$w) {
		if($w == ""){
		if($start == '')
		{
			$start = 0;
		}
    	$d = ORM::for_table($this->_table)->where('is_deleted','0')
    						->order_by_desc($id)
    						->limit($limit)
							->offset($start)
    						->find_many();
    	}else{
    		
			if($start == '')
			{
				$start = 0;
			}
			$l = '';					
			$limit = $limit;
			$offset = $start;

	        $l = "LIMIT $offset,$limit";					

			$query = "SELECT * FROM `$this->_table` WHERE is_deleted = '0' $w ORDER BY id DESC $l";
			
			$d = ORM::for_table('payment_gateway')->raw_query($query)->find_many();
		}

    	if($d) { return $d; } else { return FALSE; }
    }

     public function get_search_count($limit = 10,$w) {

			$query = "SELECT * FROM `$this->_table` WHERE is_deleted = '0' $w ORDER BY id DESC";

			$TotalData = ORM::for_table('payment_gateway')->raw_query($query)->find_array();
			$d = count($TotalData);

		if($d) { return $d; } else { return FALSE; }
	}

    public function update_row($post)
    {

		$d = ORM::for_table($this->_table)->where('id',$post['id'])->find_one();
		

		if($d)
		{
	            $d->name                     = $post['name'];
                $d->api_url                  = $post['api_url'];
			    $d->user_name                = $post['user_name'];
                $d->password                 = $post['password']; 
                $d->product_id               = $post['product_id']; 
                $d->client_code              = $post['client_code']; 
                $d->account_no               = $post['account_no']; 
                $d->req_hash_key             = $post['req_hash_key']; 
                $d->status          		 = isset($post['status']) ? $post['status'] : 'active';
		        $d->updated_time             = date('Y-m-d H:i:s');            
            	$d->updated_by_user_id       = $this->session->userdata('loginid');         
        

		        if($d->save()) { return $d; } else { return FALSE; }
		}

	}

	
}