<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once('traits/common_function.php');


class Mdl_home extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->library('database');
	}

	use common_db_functions;

	private $_table = 'sam_settings';

	public function get_customer_count() {
		$d = ORM::for_table('sam_customers')->where('status','active')
							->where('is_deleted','0')->count();
		if($d) {
			return $d;
		} else {
			return '0';
		}
	}
	
	public function get_salon_count() {
		$d = ORM::for_table('sam_users')->where('status','active')->where('type','user')->where('is_deleted','0')->count();
		if($d) { return $d; } else { return '0'; }
	}

	public function get_category() {
		$d = ORM::for_table('categories_mst')->where('status','active')->where('is_deleted','0')->count();
		if($d) { return $d; } else { return '0'; }
	}
	public function sget_ubcategory() {
		$d = ORM::for_table('subcategories_mst')->where('status','active')->where('is_deleted','0')->count();
		if($d) { return $d; } else { return '0'; }
	}
	public function get_payment_getway() {
		$d = ORM::for_table('payment_gateway')->where('status','active')->where('is_deleted','0')->count();
		if($d) { return $d; } else { return '0'; }
	}
	public function get_contributor() {
		$d = ORM::for_table('sam_users')->where('status','active')->where('is_deleted','0')->where('type','contributor')->count();
		if($d) { return $d; } else { return '0'; }
	}
	public function get_total_donation() {
		$d = ORM::for_table('transition')->where('payment_status','success')->sum('amount');
		if($d) { return $d; } else { return '0'; }
	}
	








	

    public function get_city_count() 
    {
		$d = ORM::for_table('zyd_city')->where('is_deleted','no')->count();
		if($d) {
			return $d;
		} else {
			return '0';
		}
	}
	
	

    public function get_products_count() 
    {
		$d = ORM::for_table('products')->where('is_deleted','0')->count();
		if($d) {
			return $d;
		} else {
			return '0';
		}
	}
	
	public function get_services_count() 
    {
		$d = ORM::for_table('services')->where('is_deleted','0')->count();
		if($d) {
			return $d;
		} else {
			return '0';
		}
	}
	
	public function get_amenities_count() 
    {
		$d = ORM::for_table('amenities')->where('is_deleted','0')->count();
		if($d) {
			return $d;
		} else {
			return '0';
		}
	}
	
		public function get_offer_count() 
    {
		$d = ORM::for_table('offer_discount')->where('is_deleted','0')->count();
		if($d) {
			return $d;
		} else {
			return '0';
		}
	}
		public function get_services_category_count() 
    {
		$d = ORM::for_table('service_category_mst')->where('is_deleted','0')->count();
		if($d) {
			return $d;
		} else {
			return '0';
		}
	}
		public function get_products_category_count() 
    {
		$d = ORM::for_table('product_category_mst')->where('is_deleted','0')->count();
		if($d) {
			return $d;
		} else {
			return '0';
		}
	}
		public function get_package_count() 
    {
		$d = ORM::for_table('plan_master')->where('is_deleted','0')->count();
		if($d) {
			return $d;
		} else {
			return '0';
		}
	}
	
	
	
	
	
	public function get_post_count() 
    {
		$d = ORM::for_table('bmo_post')->where('adevertisementType','Post')->where('is_deleted','no')->count();

		if($d) {
			return $d;
		} else {
			return '0';
		}
	}
	public function get_require_post_count() 
    {
		$d = ORM::for_table('bmo_post')->where('adevertisementType','Required')->where('is_deleted','no')->count();
		if($d) {
			return $d;
		} else {
			return '0';
		}
	}
	
	
	
	public function get_users_count() {
		$d = ORM::for_table('sam_users')->where('status','active')
										->where('type','user')
										->where('is_deleted','0')->count();
		if($d) {
			return $d;
		} else {
			return '0';
		}
	}



	
}