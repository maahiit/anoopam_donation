<?php defined('BASEPATH') OR exit('No direct script access allowed');
 
class Mdl_home_settings extends CI_Model{
 
	public function __construct(){
		parent::__construct();
		$this->load->library('database');
	}

	private $_table = 'home';	
	public function update_home($post){
		$d = ORM::for_table($this->_table)->where('id',1)->find_one();
		if($d) {
			$d->title              = $_POST['title'];
			$d->sub_title              = $_POST['sub_title'];
	        $d->updated_time                = date('Y-m-d H:i:s');            
            $d->updated_by_user_id      = $this->session->userdata('loginid');
	        if(is_uploaded_file($_FILES['image']['tmp_name'])) {
	            $this->sam->upload_image('image','themes/assets/images/banner/');
	            $d->image = $_FILES['image']['name'];
	        }

	        
	    	if($d->save()) 
        	{ 
        		return $d; 
        	} 
        	else 
    		{ 
    			return FALSE; 
    		}
		} else {
			return FALSE;
		}
	}
	
	

	
	function get($id) 
  {

    $d = ORM::for_table($this->_table)->where('id',$id)->find_one();  
    if($d) { return $d; } else { return FALSE; }
  }
  function get_country() 
  {
      $d = ORM::for_table('zyd_country')->where('is_deleted',0)->find_many();  
      if($d) { return $d; } else { return FALSE; }
  }


 function get_categories() 
  {
    $d = ORM::for_table('categories_mst')->where('is_deleted','0')->where('status','active')->find_many();
    if($d) { return $d; } else { return FALSE; }
  }

  function get_services()  
  {
    $d = ORM::for_table('services')->where('status','active')->find_many();
    if($d) { return $d; } else { return FALSE; }
  }

  public function get_all_services(){ 

		$query = "SELECT first.*,user.business_type as business_type, city.city as city,user.mobile as mobile,user.business_name as business_name,service_cat.name as servicescatnamee,user.logo as logo FROM services as first
                       LEFT OUTER JOIN sam_users as user ON first.user_id = user.id
                       LEFT OUTER JOIN service_category_mst as service_cat ON first.category = service_cat.id
                       LEFT OUTER JOIN zyd_city as city ON user.city=city.id
                       WHERE first.is_deleted = '0'  ORDER BY id DESC";		    
			
			$d = ORM::for_table('sam_users')->raw_query($query)->find_many();


		if($d) { return $d; } else { return FALSE; }  
	}

  public function get_service_cat_services_count(){ 

    $query = "SELECT first.name as service_name,first.category as categoryid,service_cat.name as services_category,service_cat.category_img as category_img,count(first.category) as total_services FROM services as first
                       
                       LEFT OUTER JOIN service_category_mst as service_cat ON first.category = service_cat.id
                       WHERE first.is_deleted = '0' group by first.category  ORDER BY first.id DESC";       
      
      $d = ORM::for_table('sam_users')->raw_query($query)->find_many();


    if($d) { return $d; } else { return FALSE; } 
  }


  

	public function get_all_products(){

        $query = "SELECT first.*,user.business_type as business_type,user.city as city,city.city as city,user.mobile as mobile,user.business_name as business_name,product_cat.name as category,user.logo as logo FROM products as first
                       LEFT OUTER JOIN sam_users as user ON first.user_id = user.id
                       LEFT OUTER JOIN product_category_mst as product_cat ON first.category = product_cat.id
                       LEFT OUTER JOIN zyd_city as city ON user.city=city.id
                       WHERE first.is_deleted = '0'  ORDER BY id DESC";	    
			
			$d = ORM::for_table('sam_users')->raw_query($query)->find_many();


		if($d) { return $d; } else { return FALSE; }
	}

  function get_users() 
  {
    $d = ORM::for_table('sam_users')->where('type','user')->find_many();
    if($d) { return $d; } else { return FALSE; }
  }

  function get_cities()   
  {
    $d = ORM::for_table('zyd_city')->where('is_deleted','0')->find_many();
    if($d) { return $d; } else { return FALSE; }
  }

  public function get_all_testimonials(){

  $query = "SELECT first.*,user.business_name as business_name FROM testimonials as first
                       LEFT OUTER JOIN sam_users as user ON first.user_id = user.id                       
                       WHERE first.is_deleted = '0' ORDER BY id DESC";

    $d = ORM::for_table('sam_users')->raw_query($query)->find_many();

    if($d) { return $d; } else { return FALSE; }  

    }   

    public function get_all_cities(){ 

  $query = "SELECT first.*,user.business_name as business_name FROM zyd_city as first
                       LEFT OUTER JOIN sam_users as user ON first.user_id = user.id                       
                       WHERE first.is_deleted = '0' ORDER BY id DESC";

    $d = ORM::for_table('sam_users')->raw_query($query)->find_many();

    if($d) { return $d; } else { return FALSE; }  

    }

    public function get_blogs(){
  $query = "SELECT first.*,user.business_name as business_name FROM blog_mst as first
                       LEFT OUTER JOIN sam_users as user ON first.user_id = user.id                       
                       WHERE first.is_deleted = '0'  ORDER BY id DESC"; 
        $d = ORM::for_table('sam_users')->raw_query($query)->find_many();
        if($d) { return $d; } else { return FALSE; } 
}

function get_all_blogs()   
  {
    $d = ORM::for_table('blog_mst')->where('is_deleted','0')->find_many();
    if($d) { return $d; } else { return FALSE; }
  }

}