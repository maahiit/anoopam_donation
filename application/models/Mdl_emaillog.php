<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once('traits/common_function.php');

class Mdl_emaillog extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->library('database');
	}

	use common_db_functions;

	private $_table = 'sam_email_log';

    public function get_count() {
        $d = ORM::for_table($this->_table)->count();
        if($d) { return $d; } else { return FALSE; }
    }

	public function get_all_join($per_page = 5,$start = 0,$post = '') {
		$w = '';
		$l = '';
		if($post != '') {
			// print_r($post);
            // get all data and store it to in session

            // From Date
            if(isset($post['fromdate']) AND $post['fromdate'] != '') {
                //
                $fromdate = date('Y-m-d', strtotime($post['fromdate']));
                $w .= " AND e.date_sent >= '".$fromdate."'";
            }

            // To Date
            if(isset($post['todate']) AND $post['todate'] != '') {
                // 
                $todate = date('Y-m-d', strtotime($post['todate']));
                $w .= " AND e.date_sent <= '".$todate."'";
            }

            // Any Text
            if(isset($post['anytext']) AND $post['anytext'] != '') {
                // 
                $anytext = $post['anytext'];
                $w .= " AND ( e.subject LIKE '%".$anytext."%' OR e.recepient LIKE '%".$anytext."%' ) ";
            }

            // exit;
		} else {
			($start == 0 ? $start = 0 : '');
			$l = "LIMIT $start,$per_page";
		}
		
		// echo $per_page; exit;
		$d = ORM::for_table($this->_table)
			->raw_query("SELECT e.*
						FROM sam_email_log as e
                        WHERE e.is_deleted = '0'
						$w
						ORDER BY e.id DESC
						$l")->find_many();

		if($d) { return $d; } else { return FALSE; }
	}

}