<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once('traits/common_function.php');

class Mdl_state extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->library('database');
	}

	use common_db_functions;

	private $_table = 'zyd_state';
	
	public function get_count() {
		$d = ORM::for_table($this->_table)->where('is_deleted','0')
				->count();
		if($d) { return $d; } else { return FALSE; }
	}

	public function get_all_with_pagi($id,$limit = 10,$start = 0,$w) {
		if($w == ""){
		if($start == '')
		{
			$start = 0;
		}
    	$d = ORM::for_table($this->_table)->where('is_deleted','0')
    						->order_by_asc($id)
    						->limit($limit)
							->offset($start)
    						->find_many();
    	}else{
    		
			if($start == '')
			{
				$start = 0;
			}
			$l = '';					
			$limit = $limit;
			$offset = $start;

	        $l = "LIMIT $offset,$limit";					

			$query = "SELECT * FROM `$this->_table` WHERE is_deleted = '0' $w ORDER BY id DESC $l";
			
			$d = ORM::for_table('zyd_state')->raw_query($query)->find_many();
		}

    	if($d) { return $d; } else { return FALSE; }
    }

     public function get_search_count($limit = 10,$w) {

			$query = "SELECT * FROM `$this->_table` WHERE is_deleted = '0' $w ORDER BY id DESC";

			$TotalData = ORM::for_table('zyd_state')->raw_query($query)->find_array();
			$d = count($TotalData);

		if($d) { return $d; } else { return FALSE; }
	}

    public function update_State($post) {

		$d = ORM::for_table($this->_table)->where('id',$post['id'])->find_one();
		

		if($d) {
			    $d->state            = $_POST['state'];
		        $d->updated_time                = date('Y-m-d H:i:s');            
            	$d->updated_by_user_id      = $this->session->userdata('loginid');

		        if($d->save()) { return $d; } else { return FALSE; }
		    }

	}

   


   


}