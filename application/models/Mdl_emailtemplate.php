<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once('traits/common_function.php');


class Mdl_emailtemplate extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->library('database');
	}

	use common_db_functions;

	private $_table = 'zyd_email_template';

	public function update_email_template($post) {
		$d = $this->get($post['id']);
		if($d) {
			// update bodyimage details
        	$d->template_name           = isset($post['name']) ? $post['name'] : '';
        	$d->subject                 = isset($post['subject']) ? $post['subject'] : '';
        	$d->email_body                 = isset($post['message']) ? $post['message'] : '';
        	$d->status = isset($post['status']) ? $post['status'] : 'active';
			$d->updated_time                = date('Y-m-d H:i:s');
		   

	        $d->save();

		    return $d->id;
		} else {
			return FALSE;
		}
	}
}