<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once('traits/common_function.php');

class Mdl_customer extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->library('database');
	}

	use common_db_functions;

	private $_table = 'sam_customers';
	
	public function get_all() {
    	$d = ORM::for_table($this->_table)->where('is_deleted','0')
    						->order_by_asc('id')
    						->find_many();
    
    	if($d) { return $d; } else { return FALSE; }
    }
    
    
public function get_all_with_pagi($id,$limit = 10,$start = 0,$w) {
		if($w == ""){
    	$d = ORM::for_table($this->_table)->where('is_deleted','0')
    						->order_by_asc($id)
    						->limit($limit)
							->offset($start)
    						->find_many();
    	}else{
			$start = 0;
			$l = '';					
			$limit = $limit;
			$offset = $start;

	        $l = "LIMIT $offset,$limit";					

			$query = "SELECT * FROM `$this->_table` WHERE is_deleted = '0' $w ORDER BY id DESC $l";
			
			$d = ORM::for_table('sam_customers')->raw_query($query)->find_many();
		}

    	if($d) { return $d; } else { return FALSE; }
    }



	public function update_customer($post) {

		$d = ORM::for_table($this->_table)->where('id',$post['id'])->find_one();
		if($d) {
			// update branch details
			
			$d->uname 					= $post['uname'];			
			$d->first_name 				= $post['fname'];
	    	$d->last_name 				= $post['lname'];
	    	$d->organization_name 		= $post['organizationname'];
	    	$d->email_id 				= $post['email'];
	    	$d->phone_no				= $post['phone'];
	  		$d->address					= $post['address'];
	    	$d->block					= $post['block'];	    	
	    	$d->street					= $post['street'];
	    	$d->zip_code 				= $post['zipcode'];
	    	$d->city 					= $post['city'];
	    	$d->modified 				= date('Y-m-d H:i:s');
	    	// $d->modified_by				= $this->session->userdata('loginid');
	    	
	    	
	    	
	                    
      	 //edit docs
	if(isset($_FILES['documents'])){
      	$i=0;
      	foreach (@$_POST['remark'] as $text) {      

	        $documents_id = $_POST['documents_id'][$i];   
	        $doc_attechment = $_POST['doc_attechment'][$i];  
	        $remark = $_POST['remark'][$i];   
			$tmp_time = time();
      
         	if(is_uploaded_file($_FILES['documents']['tmp_name'][$i])) {
            	$this->sam->upload_multiple_image('documents','themes/assets/images/attachments/',$i,$tmp_time);
                $documents_atteched = $tmp_time.$_FILES['documents']['name'][$i];
            } else {
                $documents_atteched = $_POST['doc_attechment'][$i]; 
            }
          
            $_POST['attechment_detail'][$i]['doc_attechment'] = $documents_atteched;
            $_POST['attechment_detail'][$i]['documents_id'] = $documents_id;
            $_POST['attechment_detail'][$i]['remark'] = $remark;
            $i++;
        }

        
       	$db_doc = json_decode($d->documents,true); 
    
	    if($db_doc != null || $db_doc != '' ) {        
	        foreach ($db_doc as $keydv => $dv) {
		       	foreach (@$_POST['attechment_detail'] as $key => $value) {
		   			if($value['documents_id'] == $dv['documents_id']){
	       			  unset($db_doc[$keydv]);
	       			}else{ }
	       		}
	     	}  
	    } else {
	        $db_doc = array();
	    }


     
       $doc_json= @array_merge($db_doc,@$_POST['attechment_detail']);
       $d->documents = json_encode($doc_json);
	 
 }
		//edit docs end
 
            
          	
	         
	         
	        	
	    	
	    	$d->save();

	    	$query =  ORM::for_table('sam_contacts_detail')
                            ->where('customer_id',$d->id)->delete_many();


        $i=0;               
		foreach ($post['cperson'] as $cloop) {		          
			$g = ORM::for_table('sam_contacts_detail')->create();
		        $g->customer_id       = $d->id;
		        $g->cperson           = $post['cperson'][$i];
		        $g->emailperson       = $post['emailperson'][$i];
		        $g->phoneperson       = $post['phoneperson'][$i];
		        $g->modified          = date('Y-m-d H:i:s');
		        $g->save();
				$i++;
            }      
            
      
  

	   	return $d->id;
		} else {
			return FALSE;
		}
	}

}