<?php defined('BASEPATH') OR exit('No direct script access allowed');
    
require_once APPPATH.'third_party/PHPMailer.php';

class Mdl_api extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->library('database');
    }

    private $page_limit = 10;

    public function check_user($email_id) {
        $f = ORM::for_table('sam_users')->where('email_id',$email_id)->find_one();
        if($f) { return TRUE; } else { return FALSE; }
    }

     public function get_category_list() {
        $d = ORM::for_table('categories_mst')->where('status','active')->where('is_deleted','0')->find_array();
        if($d) { return $d; } else { return FALSE; }
    }
    
     



    public function add_post($Post) 
    {
        $bmo_post = ORM::for_table('bmo_post')->create();
        $bmo_post->title_en                 = @$Post['title_en'];
    
        $bmo_post->description_en           = @$Post['description_en'];
        $bmo_post->price                    = @$Post['price'];
        $bmo_post->category                 = @$Post['category'];
        $bmo_post->city                     = @$Post['city'];
        $bmo_post->adevertisementType       = @$Post['adevertisementType'];
        $bmo_post->location                 = @$Post['location'];
        $bmo_post->user_id                  = @$Post['user_id'];
        $bmo_post->inserted_time            = date('Y-m-d H:i:s');
        $bmo_post->post_images              = @$Post['postimages'];
        $bmo_post->save();
        if($bmo_post) { return $bmo_post; } else { return FALSE; }
    }
    
    public function post_list() {
        $d = ORM::for_table('bmo_post')->where('is_deleted','0')->find_array();
        if($d) { return $d; } else { return FALSE; }
    }
    
    public function edit_profile($Post) 
    {
        $record = ORM::for_table('sam_customers')->where('id',$_POST['user_id'])->find_one();
        $record->first_name   = @$Post['first_name'];
        $record->last_name   = @$Post['last_name'];
        $record->username   = @$Post['username'];
        $record->email_id   = @$Post['email_id'];
        $record->city   = @$Post['city'];
        
         if($_FILES['image_logo']['name']!=''){
            if(is_uploaded_file($_FILES['image_logo']['tmp_name'])) {
                $this->sam->upload_image('image_logo','themes/assets/images/usersign/');
                $record->image_logo       = $_FILES['image_logo']['name'];
            }
        }
        
        $record->save();
        
        if($record) { return $record; } else { return FALSE; }
    }
    
  
    
    
      public function get_profile($post) {
         $record = ORM::for_table('sam_customers')->where('id',$_POST['user_id'])->find_one();
            $d['id'] = $record['id'];
            $d['first_name'] = $record['first_name'];
            $d['last_name'] = $record['last_name'];
            $d['username'] = $record['username'];
            $d['email_id'] = $record['email_id'];
            $d['city'] = $record['city'];
            $d['image_logo'] = $record['image_logo'];
            
        if($d) { return $d; } else { return FALSE; }
    }
    
      public function get_mypost($post) {
         $d = ORM::for_table('bmo_post')->where('user_id',$_POST['user_id'])->where('adevertisementType',$_POST['adtype'])->find_array();
            
        if($d) { return $d; } else { return FALSE; }
    }
    
    public function single_image_upload($Post) 
    {
          
        $record =  ORM::for_table('post_images')->create();
       
         if($_FILES['post_images']['name']!='')
         {
            if(is_uploaded_file($_FILES['post_images']['tmp_name'])) 
            {
                $this->sam->upload_image('post_images','themes/assets/images/attachments/');
                $record->post_images       = $_FILES['post_images']['name'];
            }
        }
        
        $record->save();
        
        if($record) { return $record->id; } else { return FALSE; }
    }
    

    public function get_post_comment($Post)
    {
        $query = "SELECT comment.*,customers.first_name as fname,customers.last_name as lname,customers.image_logo as image_logo FROM post_comments as comment
                       LEFT OUTER JOIN sam_customers as customers ON comment.user_id = customers.id
                       WHERE comment.is_deleted = '0' AND comment.post_id = '".$Post['post_id']."' order by id desc";
        
        $d = ORM::for_table('post_comments')->raw_query($query)->find_array();
        if($d) { return $d; } else { return FALSE; }   
    }

    public function get_post_details($Post)
    {
        $query = "SELECT * FROM bmo_post WHERE id = '".$Post['post_id']."'";
        $record = ORM::for_table('bmo_post')->raw_query($query)->find_one();
        $array = implode("','",explode(',', $record->post_images));

        $queryimage = "SELECT * FROM post_images  WHERE id in('".$array."')"; 
        $images_record = ORM::for_table('bmo_post')->raw_query($queryimage)->find_array();

        $d['title_en']              = $record['title_en'];
        $d['description_en']        = $record['description_en'];
        $d['user_id']               = $record['user_id'];
        $d['price']                 = $record['price'];
        $d['category']              = $record['category'];
        $d['adevertisementType']    = $record['adevertisementType'];
        $d['city']                  = $record['city'];
        $d['location']              = $record['location'];
        $d['post_images']           = $images_record;
            //postimages:1,2


        if($d) { return $d; } else { return FALSE; }   
    }

     public function update_post_record($Post)
    {
        $bmo_post = ORM::for_table('bmo_post')->where('id',$Post['post_id'])->find_one();
        $bmo_post->title_en                 = @$Post['title_en'];
        $bmo_post->description_en           = @$Post['description_en'];
        $bmo_post->price                    = @$Post['price'];
        $bmo_post->category                 = @$Post['category'];
        $bmo_post->city                     = @$Post['city'];
        $bmo_post->adevertisementType       = @$Post['adevertisementType'];
        $bmo_post->location                 = @$Post['location'];
        $bmo_post->user_id                  = @$Post['user_id'];
        $bmo_post->inserted_time            = date('Y-m-d H:i:s');
        $bmo_post->post_images              = @$Post['postimages'];
        $bmo_post->save();
        
        if($bmo_post) { return $bmo_post; } else { return FALSE; }   
    }

    


    


}