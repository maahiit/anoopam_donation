<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once('traits/common_function.php');


class Mdl_settings extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->library('database');
	}

	use common_db_functions;

	private $_table = 'sam_settings';

	public function update_settings($post) {
		$d = $this->get(1);
		if($d) {
			$d->sitename 						= $post['sitename'];
			$d->site_title 						= $post['site_title'];
			$d->site_url 						= $post['site_url'];
			$d->copyright 						= $post['copyright'];
			$d->pre_fix_amount 					= $post['pre_fix_amount'];
			$d->pan_amount 						= $post['pan_amount'];
			$d->sms_getway_url 					= $post['sms_getway_url'];
			$d->sms_getway_api_url 				= $post['sms_getway_api_url'];
			$d->sms_getway_api_key 				= $post['sms_getway_api_key'];
			$d->sms_getway_api_secret_key 		= $post['sms_getway_api_secret_key'];
			$d->google_api_app_id 				= $post['google_api_app_id'];
			$d->google_api_key 					= $post['google_api_key'];
			$d->google_api_secret_key 			= $post['google_api_secret_key'];
			$d->language 						= $post['language'];
			$d->rows_per_page					= $post['rows_per_page'];
			$d->timezone 						= $post['timezone'];

			// Site Logo Image
	        if(is_uploaded_file($_FILES['sitelogo']['tmp_name'])) {
	            $this->sam->upload_image('sitelogo','themes/default/admin/assets/upload/logos/');
	            $d->sitelogo = $_FILES['sitelogo']['name'];
	        }
	        
	        if(is_uploaded_file($_FILES['login_bg_image']['tmp_name'])) {
	            $this->sam->upload_image('login_bg_image','themes/default/admin/assets/upload/logos/');
	            $d->login_bg_image = $_FILES['login_bg_image']['name'];
	        }
		    $d->save();
		    return $d->id;
		} else {
			return FALSE;
		}
	}

	public function update_smtp_settings($post) {
		$d = $this->get(1);
		if($d) {
			$d->smtp_status 	= $post['smtp_status'];
			$d->smtp_encryption = $post['smtp_encryption'];
			$d->smtp_username	= $post['smtp_username'];
			$d->smtp_password 	= $post['smtp_password'];
			$d->smtp_name 		= $post['smtp_name'];
			$d->smtp_host 		= $post['smtp_host'];
			$d->smtp_port 		= $post['smtp_port'];
		    $d->save();
		    return $d->id;
		} else {
			return FALSE;
		}
	}
}